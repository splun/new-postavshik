<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use App\Models\Categories;
use App\Models\ChMessage;
use App\Models\Users;
use Auth;
class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            'profile', 'App\Http\View\Composers\ProfileComposer'
        );

        // Using Closure based composers...
        view()->composer('*', function ($view) 
        {
            if(\Session::get('cart') && \Session::get('cart')["variations"]){
                $mini_cart = "active";
            }else{
                $mini_cart = "empty";
            }
            
            $categories = Categories::all();
            $parent_categories = Categories::get_w_parent();

            $user = Auth::user();

            $operator_id = 0;
            $is_admin = FALSE;
            $is_operator = FALSE;
            $count_unread_message = 0;

            if($user){

                if($user->permission == "admin"){
                    $is_admin = TRUE;
                }
                if($user->permission == "operator"){
                    $is_operator = TRUE;
                }

                $active_chat = ChMessage::where('from_id', $user->id)->first();

  
                if($active_chat){
                    
                    $check_current_operator = Users::select("id")->where("id",$active_chat->to_id)->where("permission","operator")->where("is_locked",0)->first();
                    
                    if($check_current_operator){
                        $operator_id = $active_chat->to_id;
                    }else{
                        $operator_id = $this->get_optimal_operator();
                    }
                    
                }else{
                        
                    $operator_id = $this->get_optimal_operator();

                }

                $count_unread_message = ChMessage::where("seen","!=",1)->where('to_id', $user->id)->count();
                
            }

            $favicon = config('settings.favicon_image_url');
            $min_popular_rate = config('settings.popular_rate');
            
            $view->with('mini_cart', $mini_cart ); 
            $view->with('count_unread_message', $count_unread_message );
            $view->with('is_admin', $is_admin ); 
            $view->with('is_operator', $is_operator ); 
            $view->with('categories', $categories );
            $view->with('parent_categories', $parent_categories );
            $view->with('current_user', (Auth::user()) ? $user : false );  
            $view->with('operator', $operator_id );
            $view->with('favicon', $favicon );
            $view->with('min_popular_rate', $min_popular_rate );
        }); 
    }

    /**
     * Get Optimal operator
     *
     */
    public function get_optimal_operator()
    {
        $get_operator = Users::select("id")->where("permission","operator")->where("is_locked",0)->orderBy(Users::raw('RAND()'))->orderBy('id', 'desc')->first();
                    
        if($get_operator){
            return $get_operator->id;
        }else{
            return 0;
        }

    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}