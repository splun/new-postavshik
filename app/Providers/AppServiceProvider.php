<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Pagination\Paginator;
use Illuminate\Http\Request;
use App\Models\Settings;
use Config;
use App\Services\KeyCrm\KeyCrmService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('keycrm', function ($app) {
            return new KeyCrmService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        //dd($request->cookie('utm_source'));
        if (Schema::hasTable('settings')) {
            foreach (Settings::all() as $setting) {
                Config::set('settings.'.$setting->key, $setting->value);
            }
        }

        if($request->cookie('currency')){
            currency()->setUserCurrency($request->cookie('currency'));
        }

        if($request->get("currency") && currency()->getCurrency($request->get("currency"))){

            if($request->cookie('currency')){
                setcookie('currency', null, -1, '/'); 
            }

            setcookie('currency', $request->get("currency"), time() + 3600,'/');

            currency()->setUserCurrency($request->get("currency"));

        }

        if($request->get("utm_source")){
            setcookie('utm_source', $request->get("utm_source"), time() + 7200,'/');
        }

        if($request->get("utm_campaign")){
            setcookie('utm_campaign', $request->get("utm_campaign"), time() + 7200,'/');
        }

    }
}
