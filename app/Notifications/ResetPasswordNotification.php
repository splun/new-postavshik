<?php

namespace App\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends Notification
{
    public $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting('Вітаємо!')
            ->subject('Скидання пароля')
            ->line('Ви отримали цей лист, тому що ми отримали запит на скидання пароля для вашого облікового запису.')
            ->action('Скинути пароль', url(config('app.url') . route('password.reset', $this->token, false)))
            ->line('Це посилання для скидання пароля буде дійсним протягом 60 хвилин.')
            ->line('Якщо ви не запитували скидання пароля, ніяких подальших дій не потрібно.')
            ->salutation('З повагою, команда Postavshik');
    }
}
