<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Categories;
use App\Models\Products;
use App\Models\Product_options;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;

class ProductsController extends Controller
{
    public function category(Request $request, $id)
    {

        $data["category"] = Categories::where("id", $id)->first();


        if (!$data["category"]) return abort(404);

        $productsQuery = Products::query();

        $productsQuery->select('products.*', 'products_images.url', 'products_images.base64');

        $productsQuery->where('category_id', '=', $id);

        $productsQuery->leftJoin('products_images', function ($join) {
            $join->on('products.id', '=', 'products_images.product_id')
                ->where('products_images.preview', '=', 1);
        });

        if (!$request->get("price") && !$request->get("discount") && !$request->get("popular")) {
            $productsQuery->orderBy('created_at', 'DESC');
        }

        if ($request->get("price")) {
            $productsQuery->orderBy('price', $request->get("price"));
        }

        if ($request->get("discount")) {
            $productsQuery->orderBy('discount', "DESC");
        }

        if ($request->get("popular")) {
            $productsQuery->orderBy('is_popular', "DESC");
        }

        $data["products"] = $productsQuery->paginate(18);

        return view('category')->with($data);
    }
    public function search(Request $request)
    {

        $productsQuery = Products::query();

        $productsQuery->select('products.*', 'products_images.url');
        $productsQuery->where('products.category_id', "!=", 999);

        $productsQuery->leftJoin('products_images', function ($join) {
            $join->on('products.id', '=', 'products_images.product_id')
                ->where('products_images.preview', '=', 1);
        });

        if (stripos($request->get("s"), "№") !== false) {

            $provider = str_replace("№", "", $request->get("s"));

            $productsQuery->leftJoin('providers', 'providers.id', '=', 'products.provider_id');

            $productsQuery->where('providers.alias', $provider);
        } else {
            if (isset(explode("-", $request->get("s"))[1])) {
                //$provider = explode("-",$request->get("s"))[0];
                //$productsQuery->orWhere('provider_id', $provider);
                $stock_number = explode("-", $request->get("s"))[1];
                $productsQuery->where('stock_number', $stock_number);
            } else {
                $productsQuery->where('text', 'LIKE', "%" . $request->get("s") . "%")->orWhere('stock_number', $request->get("s"));
                //$productsQuery->orWhere('provider_id', $request->get("s"));
            }
        }


        if (!$request->get("price") && !$request->get("discount") && !$request->get("popular")) {
            $productsQuery->orderBy('created_at', 'DESC');
        }

        if ($request->get("price")) {
            $productsQuery->orderBy('price', $request->get("price"));
        }

        if ($request->get("discount")) {
            $productsQuery->orderBy('discount', "DESC");
        }

        if ($request->get("popular")) {
            $productsQuery->orderBy('is_popular', "DESC");
        }

        $data["products"] = $productsQuery->paginate(18);
        $data["keyword"] = $request->get("s");

        return view('search')->with($data);
    }

    public function product($id)
    {

        $data["product"] = Products::find($id);

        if (!$data["product"]) {
            return abort(404);
        }

        $data["product_images"] = Products::get_images($id);
        $data["product_preview"] = Products::get_preview($id);

        $data["options"] = [];

        if ($data["product"]->variants) {

            foreach ($data["product"]->variants as $variant) {
                $data["options"][$variant->options->option_name][$variant->option_id] = $variant->options->option_value;
            }
        }


        if ($data["product"]) {
            //Update popular count
            //session()->put('product.view', []);

            if (is_array(session("product.view"))) {

                $duplicate_view = 0;
                foreach (session("product.view") as $view) {
                    if ($view === $id) {
                        $duplicate_view = 1;
                    }
                }

                if ($duplicate_view == 0) {
                    session()->push('product.view', $id);
                    Products::where('id', $id)->increment('is_popular', 1);
                }
            } else {
                session()->push('product.view', $id);
                Products::where('id', $id)->increment('is_popular', 1);
            }

            return view('product')->with($data);
        } else {
            return abort(404);
        }
    }

    public function getImageUrl(Request $request)
    {

        $imageurl = $request->url;

        if (strripos($imageurl, "storage")) {
            return false;
        }

        if ($imageurl) {

            $response = Http::withOptions([
                'verify' => false,
            ])
                ->get(url($imageurl));

            return response($response, 200)
                ->header('Content-Type', 'image/jpeg');
        } else {
            return false;
        }
    }

    public function getVideoUrl(Request $request)
    {
        $videourl = $request->url;
        if (strripos($videourl, "storage")) {
            return false;
        }

        if ($videourl) {
            $response = Http::withOptions([
                'verify' => false
            ])->get($videourl);

            $status = $response->status();
            $contentType = $response->header('Content-Type');

            return response()->stream(function () use ($response) {
                echo $response->getBody();
            }, $status, [
                'Content-Type' => $contentType,
            ]);
        } else {
            return false;
        }
    }

    // public function getVideoUrl(Request $request)
    // {
    //     $videourl = $request->url;
    //     if (strripos($videourl, "storage")) {
    //         return false;
    //     }

    //     if ($videourl) {
    //         $headers = [];
    //         if ($request->hasHeader('Range')) {
    //             $range = $request->header('Range');
    //             $headers['Range'] = $range;
    //         }
    //         $response = Http::withOptions([
    //             'verify' => false,
    //             'stream' => true,
    //         ])->get($videourl);

    //         $status = $response->status();
    //         $contentType = $response->header('Content-Type');
    //         $totalSize = (int)$response->header('Content-Length');

    //         //\Log::error($totalSize);//Content-Length

    //         if ($totalSize <= 0) {
    //             return response()->stream(function () use ($response) {
    //                 echo $response->getBody();
    //             }, $status, [
    //                 'Content-Type' => $contentType,
    //                 'Accept-Ranges' => 'bytes',
    //             ]);
    //         }

    //         if (isset($range)) {
    //             if (preg_match('/bytes=(\d+)-(\d*)/', $range, $matches)) {
    //                 $start = (int)$matches[1];
    //                 $end = isset($matches[2]) && $matches[2] !== '' ? (int)$matches[2] : $totalSize - 1;

    //                 \Log::error('Requested Range: start=' . $start . ', end=' . $end . ', totalSize=' . $totalSize);

    //                 if ($start > $end || $end >= $totalSize || $start < 0) {
    //                     return response()->json(['error' => 'Requested range not satisfiable'], 416);
    //                 }

    //                 $length = $end - $start + 1;
    //                 $contentRange = "bytes $start-$end/$totalSize";
    //                 return response()->stream(function () use (
    //                     $response,
    //                     $start,
    //                     $length
    //                 ) {
    //                     $response->getBody()->seek($start);
    //                     echo $response->getBody()->read($length);
    //                 }, 206, [
    //                     'Content-Type' => $contentType,
    //                     'Content-Range' => $contentRange,
    //                     'Content-Length' => $length,
    //                     'Accept-Ranges' => 'bytes',
    //                 ]);
    //             }
    //         }

    //         return response()->stream(
    //             function () use ($response) {
    //                 echo $response->getBody();
    //             },
    //             $status,
    //             [
    //                 'Content-Type' => $contentType,
    //                 'Content-Length' => $totalSize,
    //                 'Accept-Ranges' => 'bytes',
    //             ]
    //         );
    //     } else {
    //         return false;
    //     }
    // }
}
