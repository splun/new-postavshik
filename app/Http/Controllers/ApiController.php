<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

use Illuminate\Http\Request;
use App\Models\Categories;
use App\Models\Currency;
use App\Models\Products;
use App\Models\Product_variants;
use App\Models\Product_options;
use App\Models\Providers;
use App\Models\Tracks;
use App\Models\Settings;
use Str;

use Illuminate\Support\Facades\Storage;

class ApiController extends Controller
{
    //auth_key=9ffd63d5f68b7d103382d9ced0fe54
    public function __construct(Request $request)
    {
        if (!$request->auth_key || $request->auth_key !== "9ffd63d5f68b7d103382d9ced0fe54") {
            return abort(403);
        }
    }
    public function album(Request $request)
    {

        if (!$request->id) {
            $result = Categories::get();
            //$result = ['id' => 0, 'name' => 'Неотсортированные товары',];
        } else {
            $result = Categories::where("id", $request->id)->first();
        }

        return response()->json($result);
    }

    public function albumInfo(Request $request)
    {
        //categories_products_events
        if ($request->id && $request->date) {
            $result = DB::table('categories_products_events')
                ->select('product_id as id', 'event', 'updated_at')
                ->where('category', $request->id)
                ->where('updated_at', '>=', $request->date)
                //  ->where('updated_at', '(
                //      SELECT MAX(e1.updated_at) FROM categories_products_events e1
                //                            GROUP BY e1.product_id
                //                            HAVING e1.product_id = e.product_id
                //      )')
                ->get();

            return response()->json(['result' => $result]);
        } else {
            return response()->json(['result' => 'error', 'message' => 'Missing params: ID or date']);
        }
    }
    public function photo($id, Request $request)
    {
        if ($id) {

            //$query = Products::find((int)$id);
            $query = Products::where(["api_ID" => (int)$id])->first();
            if (!empty($query)) {

                $result[] = array(
                    "id" => $query->id,
                    "api_ID" => $query->api_ID,
                    "stockNumber" => $query->stock_number,
                    "price" => $query->price,
                    "syncyou_category_id" => $query->syncyou_category_id,
                    "text" => $query->text,
                    "name" => $query->name,
                    "vk_source" => $query->vk_source,
                    "album_id" => $query->category_id,
                    "createdAt" => $query->created_at,
                    "uploadedAt" => $query->uploaded_at,
                    "provider" => $query->provider->id,
                    "provider_alias" => $query->provider->alias
                );

                $result = array_map( // modify text to append price in RUB
                    function ($item) {
                        preg_match_all('/(\d+(?:\.\d+)?)\s?$/u', $item['text'], $matches, PREG_SET_ORDER);
                        $text      = &$item['text'];
                        $startFrom = 0;
                        array_map(
                            function ($item) use (&$text, &$startFrom) {
                                $match     = $item[0];
                                $value     = (float)$item[1];
                                $uah       = $value * Currency::getRate('USD');
                                $index     = strpos($text, $match, $startFrom);
                                $text      = substr_replace(
                                    $text,
                                    $match . ' (' . round($uah / Currency::getRate('RUB')) . ' руб)',
                                    $index,
                                    strlen($match)
                                );
                                $startFrom = $index;
                            },
                            $matches
                        );

                        return $item;
                    },
                    $result
                );

                return response()->json([
                    'result' => $result
                ]);
            } else {
                return response()->json(['result' => 'error', 'message' => 'Product not found']);
            }
        } else {
            return response()->json(['result' => 'error', 'message' => 'Missing param: ID']);
        }
    }

    public function photoByCode(Request $request)
    {
        if ($request->code) {

            $query = Products::where("stock_number", $request->code)->first();

            if (!empty($query)) {

                $result[] = array(
                    "id" => $query->id,
                    "stockNumber" => $query->stock_number,
                    "price" => $query->price,
                    "syncyou_category_id" => $query->syncyou_category_id,
                    "text" => $query->text,
                    "name" => $query->name,
                    "vk_source" => $query->vk_source,
                    "album_id" => $query->category_id,
                    "createdAt" => $query->created_at,
                    "uploadedAt" => $query->uploaded_at,
                    "provider" => $query->provider->id,
                    "provider_alias" => $query->provider->alias
                );

                $result = array_map( // modify text to append price in RUB
                    function ($item) {
                        preg_match_all('/(\d+(?:\.\d+)?)\s?$/u', $item['text'], $matches, PREG_SET_ORDER);
                        $text      = &$item['text'];
                        $startFrom = 0;
                        array_map(
                            function ($item) use (&$text, &$startFrom) {
                                $match     = $item[0];
                                $value     = (float)$item[1];
                                $uah       = $value * Currency::getRate('USD');
                                $index     = strpos($text, $match, $startFrom);
                                $text      = substr_replace(
                                    $text,
                                    $match . ' (' . round($uah / Currency::getRate('RUB')) . ' руб)',
                                    $index,
                                    strlen($match)
                                );
                                $startFrom = $index;
                            },
                            $matches
                        );

                        return $item;
                    },
                    $result
                );

                return response()->json([
                    'result' => $result
                ]);
            } else {
                return response()->json(['result' => 'error', 'message' => 'Product not found']);
            }
        } else {
            return response()->json(['result' => 'error', 'message' => 'Missing param: code']);
        }
    }

    public function photoByVkSource(Request $request)
    {
        if ($request->vk_source) {

            $photo = Products::get_image_by_vk_source($request->vk_source);

            if (!$photo) {
                return response()->json(['result' => 'error', 'message' => 'Product not found']);
            }

            $query = Products::where("id", $photo->product_id)->first();
            $options = [];
            if ($query->variants) {
                foreach ($query->variants as $variant) {
                    $options[$variant->options->option_name][$variant->option_id] = $variant->options->option_value;
                }
            }

            if (!empty($query)) {

                $result[] = array(
                    "id" => $query->id,
                    "stockNumber" => $query->stock_number,
                    "not_removed" => $query->not_removed,
                    "price" => $query->price,
                    "syncyou_category_id" => $query->syncyou_category_id,
                    "text" => $query->text,
                    "name" => $query->name,
                    "vk_source" => $query->vk_source,
                    "album_id" => $query->category_id,
                    "createdAt" => $query->created_at,
                    "uploadedAt" => $query->uploaded_at,
                    "provider" => $query->provider->id ?? NULL,
                    "provider_alias" => $query->provider->alias ?? NULL,
                    "options" => $options
                );

                $result = array_map( // modify text to append price in RUB
                    function ($item) {
                        preg_match_all('/(\d+(?:\.\d+)?)\s?$/u', $item['text'], $matches, PREG_SET_ORDER);
                        $text      = &$item['text'];
                        $startFrom = 0;
                        array_map(
                            function ($item) use (&$text, &$startFrom) {
                                $match     = $item[0];
                                $value     = (float)$item[1];
                                $uah       = $value * Currency::getRate('USD');
                                $index     = strpos($text, $match, $startFrom);
                                $text      = substr_replace(
                                    $text,
                                    $match . ' (' . round($uah / Currency::getRate('RUB')) . ' руб)',
                                    $index,
                                    strlen($match)
                                );
                                $startFrom = $index;
                            },
                            $matches
                        );

                        return $item;
                    },
                    $result
                );

                return response()->json([
                    'result' => $result
                ]);
            } else {
                return response()->json(['result' => 'error', 'message' => 'Product not found']);
            }
        } else {
            return response()->json(['result' => 'error', 'message' => 'Missing param:vk_source']);
        }
    }

    public function album_photos($album, Request $request)
    {
        if ($album == 0) {
            //$query = Products::where("category_id","IS NULL");
            return response()->json(['result' => 'error', 'message' => 'Missing param: Album']);
        } else {
            $products = Products::where("category_id", $album)->get();
        }
        if ($products->count() > 0) {

            foreach ($products as $product) {

                $photos = Products::get_images($product->id);

                $options = [];
                if ($product->variants) {
                    foreach ($product->variants as $variant) {
                        $options[$variant->options->option_name][$variant->option_id] = $variant->options->option_value;
                    }
                }

                $result[] = array(
                    "id" => $product->id,
                    "api_ID" => $product->api_ID,
                    "not_removed" => $product->not_removed,
                    "stockNumber" => $product->stock_number,
                    "price" => $product->price,
                    "old_price" => $product->old_price,
                    "syncyou_category_id" => $product->syncyou_category_id,
                    "text" => $product->text . '\n' . $product->description,
                    "name" => $product->name,
                    "photos" => $photos,
                    "album_id" => $product->category_id,
                    "options" => $options,
                    "createdAt" => $product->created_at,
                    "uploadedAt" => $product->updated_at,
                    "provider" => $product->provider->id ?? NULL,
                    "provider_alias" => $product->provider->alias ?? NULL
                );
            }

            $need_rub = (int)$request->need_rub;

            if ($need_rub == 1) {

                return response()->json(
                    [
                        'result' => array_map( // modify text to append price in RUB
                            function ($item) {
                                preg_match_all('/(\d+(?:(?:\.\d+)|(?:,\d+(?:\.\d+)?))?)\s*(грн)/u', $item['text'], $matches, PREG_SET_ORDER);
                                $text      = &$item['text'];
                                $startFrom = 0;
                                array_map(
                                    function ($item) use (&$text, &$startFrom) {
                                        $match     = $item[0];
                                        $value     = (float)$item[1];
                                        //$uah       = $value * Currency::get('usd');
                                        $index     = strpos($text, $match, $startFrom);
                                        $text      = substr_replace(
                                            $text,
                                            $match . ' (' . round($value / Currency::get('rub')) . ' руб)',
                                            $index,
                                            strlen($match)
                                        );
                                        $startFrom = $index;
                                    },
                                    $matches
                                );

                                return $item;
                            },
                            $result
                        ),
                    ]
                );
            } else {

                $result = array_map(
                    function ($item) {
                        //$item["text"] = str_ireplace("цена","Цена:", $item["text"]);
                        $item["text"] = str_replace("Цена", "Цена:", $item["text"]);
                        $item["text"] = str_replace("цена", "Цена:", $item["text"]);
                        $item["text"] = str_replace("Цена::", "Цена:", $item["text"]);
                        return $item;
                    },
                    $result
                );

                return response()->json(['result' => $result, "rub_rate" => Currency::getRate('RUB'), "count" => $products->count()]);
            }
        } else {
            return response()->json(['result' => []]);
        }
    }

    public function save(Request $request)
    {
        $response = [
            'result' =>
            [
                'success' => 0,
                'duplicates' => 0,
                'failed' => 0
            ]
        ];

        if ($request->provider) {
            $provider = Providers::where("alias", $request->provider)->first();
        } else {
            return response()->json(['result' => 'error', 'message' => 'Missing param: Provider']);
        }


        if (!$provider) {
            $provider = Providers::create(["alias" => $request->provider, "extra_uah" => config('settings.providers_default_extra')]);
        }

        $items = $request->items;

        if (!$items) {
            return response()->json(['result' => 'error', 'message' => 'Missing items: Items']);
        }

        $currencyUsd = currency()->getCurrency("USD");
        $currencyUsd = $currencyUsd["exchange_rate"];

        //dd($currencyUsd);
        //$currencyRub = round(currency()->getCurrency("RUB")["exchange_rate"],2);

        //Filter price in text
        $items = array_map(
            function ($i, $k) use (&$items, &$provider, $currencyUsd) {
                preg_match_all(
                    '/(\d+(?:(?:\.\d+)|(?:,\d+(?:\.\d+)?))?)\s*(\$|грн|руб)/u',
                    $i['text'],
                    $out
                );

                if (!empty($out[0])) {
                    $items[$k]['price'] = max(
                        array_map(
                            function ($price, $currency) {
                                $price = (float)str_replace(
                                    ',',
                                    '.',
                                    filter_var(
                                        $price,
                                        FILTER_SANITIZE_NUMBER_FLOAT,
                                        FILTER_FLAG_ALLOW_THOUSAND | FILTER_FLAG_ALLOW_FRACTION
                                    )
                                );
                                if ($currency === '$') {
                                    $price = round($price / currency()->getCurrency("USD")["exchange_rate"]);
                                }
                                /*else {
                                    if ($currency === 'руб') {
                                        $price = round($price / round(currency()->getCurrency("RUB")["exchange_rate"],2));
                                    }
                                }*/

                                return $price;
                            },
                            $out[1],
                            $out[2]
                        )
                    );
                } else {
                    $items[$k]['price'] = 0;
                }

                $items[$k]['hasUsd'] = in_array('$', $out[2], true);

                return [
                    'name' => $items[$k]['name'],
                    'text' => $items[$k]['text'],
                    'syncyou_category_id' => $items[$k]['syncyou_category_id'] ?? NULL,
                    'stock_number' => $items[$k]['stock_number'],
                    'provider_id' => $provider->id,
                    'album'      => $items[$k]['album'] ?? null,
                    'price' => ($items[$k]['price'] + $provider->extra_uah),
                    'images'               => $items[$k]['images'] ?? null,
                    'exchanged_price_by'  => 1 / $currencyUsd,
                    'hasUsd' => in_array('$', $out[2], true),
                    'vk_source'         => $items[$k]['vk_source'] ?? null,
                    'api_ID'         => $items[$k]['api_ID'] ?? null,
                    'uploaded_at'        => $items[$k]['api_ID'] ?? date("Y-m-d h:i:s"),
                    'created_at' => $i['date']
                ];
            },
            $items,
            array_keys($items)
        );

        /* save products*/
        array_walk(
            $items,
            function ($item) use (&$request, &$provider, &$response, $currencyUsd) {

                //try {

                //dd(stripos($item['text'], "распродажа"));
                $dublicated_products = 0;

                if (is_array($item['images'])) {

                    foreach ($item['images'] as $image) {

                        if (isset($image['base64'])) {

                            $url = $image['base64'];

                            $foundDuplicates = Products::get_dublicate_images_base64($url);

                            if ($foundDuplicates) {
                                $dublicated_products = $foundDuplicates->product_id;
                            }
                        }

                        if (isset($image['url'])) {

                            $url = $image['url'];

                            $foundDuplicates = Products::get_dublicate_images($url);

                            if ($foundDuplicates) {
                                $dublicated_products = $foundDuplicates->product_id;
                            }
                        }
                    }
                }

                $sku = Str::upper(Str::random(8));
                $sales_words = [
                    'распродажа',
                    'розпродаж',
                    'новая цена',
                    'нова ціна',
                    'цена снижена',
                    'ціна знижена',
                    'уценка',
                    'скидка',
                    'знижка',
                ];

                $wordsString = implode('|', $sales_words);
                $text_format = Str::lower($item['text']);

                if ($dublicated_products !== 0) {
                    $product = Products::where(["id" => (int)$dublicated_products])->first();

                    if ($product) {
                        $product->text = $item['text'];

                        if (preg_match("/($wordsString)/", $text_format) || $product->price > $item['price']) {
                            if (!$product->old_price) {
                                $product->old_price = $product->price;
                            }
                            Product_variants::where("product_id", $product->id)->delete();
                        }

                        $product->category_id = 999;
                        $product->price = $item['price'];
                        $product->updated_at = date("Y-m-d h:i:s");
                        $product->created_at = date("Y-m-d h:i:s");
                        $product->save();

                        $response['result']['duplicates']++;
                    } else {

                        if (is_array($item['images'])) {
                            foreach ($item['images'] as $image) {
                                Products::unset_image_by_url($image['url']);
                            }
                        }

                        $response['result']['failed']++;

                        return response()->json(['result' => "Error, product not found"]);
                    }
                } else {

                    if (preg_match("/($wordsString)/", $text_format)) {
                        $item['album'] = 999;
                        $oldPrice = $item['price'] + ($item['price'] * 50 / 100);
                    }

                    $photo = Products::create([
                        'api_ID' => $item['api_ID'] ?? null,
                        'name' => $item['name'],
                        'text' => $item['text'],
                        'syncyou_category_id' => $item['syncyou_category_id'] ?? NULL,
                        'stock_number' => $sku,
                        'provider_id'      => $provider->id,
                        'category_id'      => $item['album'] ?? null,
                        'price'             => $item['price'],
                        'old_price'        => $oldPrice ?? NULL,
                        'exchanged_price_by'  => $item['exchanged_price_by'] ?? null,
                        'has_usd_on_uploading' => $item['hasUsd'] ?? 0,
                        'vk_source'         => $item['vk_source'] ?? null,
                        'uploaded_at'        => $item['date'] ?? date("Y-m-d h:i:s"),
                        'updated_at'        => date("Y-m-d h:i:s"),
                        'created_at' => date("Y-m-d h:i:s")
                    ]);

                    //save photos
                    if ($photo && is_array($item['images'])) {

                        $im = 1;
                        $img = array();

                        foreach ($item['images'] as $image) {

                            $preview = 0;

                            if ($im == 1) {
                                $preview = 1;
                            }

                            $img[] = [
                                'product_id' => $photo->id,
                                'url' => (isset($image['url'])) ? $image['url'] : null,
                                'base64' => (isset($image['base64'])) ? $image['base64'] : null,
                                'preview' => $preview,
                                'vk_source' => $image['vk_source'] ?? null,
                                'created_at' => now(),
                                'updated_at' => now()
                            ];
                            $im++;
                        }

                        Products::addImages($img);
                    }
                }

                $response['result']['success']++;

                // } catch (\Exception $e) {
                //     $response['result']['failed']++;
                //     Log::error($e->getMessage());
                // }
            }
        );

        if ($response['result']['success'] > 0) {
            //$this->sendMessageToQueue($response['result']['success']);
        }

        return response()->json($response);
    }

    public function photoEdit($api_ID, Request $request)
    {

        $content = $request->text;
        $album = $request->album;

        //$price = $request->price;
        //$currency = $request->currency;
        //$url = $request->url;

        if (!$content) {
            return response()->json(['result' => 'error', 'message' => 'Missing params: Text text or ID']);
        }

        $photo = Products::where(["api_ID" => (int)$api_ID])->first();


        if ($photo) {
            $extraUah = $photo->provider->extra_uah;
        } else {
            $extraUah = 0;
        }

        $prices = array();
        preg_match_all(
            '/(\d+(?:(?:\.\d+)|(?:,\d+(?:\.\d+)?))?)\s*(\$|грн)/u',
            mb_strtolower($content),
            $matches
        );

        list(, $onlyPrice) = $matches;

        $calc     = function ($price) {
            return round($price * Currency::getRate('USD'));
        };

        $k = 0;
        foreach ($onlyPrice as $price) {

            if ($matches[2] && $matches[2][$k] === "$") {

                $content = str_replace(
                    $matches[0][$k],
                    $calc($price) + $extraUah . " грн",
                    $content
                );


                $prices[] = $calc($price) + $extraUah;
            } else {

                $content = str_ireplace(
                    $matches[0][$k],
                    $price + $extraUah . " грн",
                    $content
                );

                $prices[] = $price + $extraUah;
            }

            $k++;
        }

        if ($prices) {
            $max_price = max($prices);
        } else {
            $max_price = $prices[0];
        }

        //$qb = Products::where(["api_ID"=>(int)$api_ID])->first();

        /*if(isset($album) && !empty($album)){
            $photo->category_id = $album;
        }*/

        $photo->category_id = 999;

        if ($photo->price > $max_price) {
            $photo->old_price = $photo->price;
        } else {
            $sales_words = [
                'распродажа',
                'розпродаж',
                'новая цена',
                'нова ціна',
                'цена снижена',
                'ціна знижена',
                'уценка',
                'скидка',
                'знижка',
            ];

            $wordsString = implode('|', $sales_words);
            $text_format = Str::lower($content);
            if (preg_match("/($wordsString)/", $text_format) && !$photo->old_price) {
                $photo->old_price = $photo->price;
                Product_variants::where("product_id", $photo->id)->delete();
            }
        }

        $photo->price = $max_price;
        $photo->text = $content;
        $photo->exchanged_price_by = Currency::getRate('USD'); //BANK API currency


        $response = $photo->save();

        //Move photo if new album
        /* if($album && $photo && $photo->category_id !== $album){

            $photoName = $id . '.jpg';

            $from = UPLOAD_PATH . ($photo->category_id ? $photo->category_id . '/' : '') . $photoName;

            $to = UPLOAD_PATH . $album;

            # Если файл не существует
            if( !file_exists( $from ) ){
                return response()->json(['result' => "File not found",]);
            }
            
            # Если директории 
            if( !is_dir( $to ) ){
                return response()->json(['result' => "Provider directory not found",]);
            }


            if (!rename($from, $to . '/' . $photoName)) {
                return response()->json(['result' => "Cannot move image into album folder",]);
            }

        } */

        if ($response) {
            return response()->json(['result' => "Success",]);
        } else {
            return response()->json(['result' => "Fail",]);
        }
    }

    public function photoEditbyVkSource($vk_source, Request $request)
    {

        $content = $request->text;
        $album = $request->album;

        //$price = $request->price;
        //$currency = $request->currency;
        //$url = $request->url;

        if (!$content || !$vk_source) {
            return response()->json(['result' => 'error', 'message' => 'Missing params: Text text or vk_source']);
        }

        $get_product_by_photo = Products::get_image_by_vk_source($vk_source);

        if (!$get_product_by_photo) {
            return response()->json(['result' => 'error', 'message' => 'Missing params: Image not found']);
        }

        $product = Products::where(["id" => (int)$get_product_by_photo->product_id])->first();

        if ($product) {
            $extraUah = $product->provider->extra_uah;
        } else {
            $extraUah = 0;
            return response()->json(['result' => 'Success']);
        }

        $prices = array();
        preg_match_all(
            '/(\d+(?:(?:\.\d+)|(?:,\d+(?:\.\d+)?))?)\s*(\$|грн)/u',
            mb_strtolower($content),
            $matches
        );

        list(, $onlyPrice) = $matches;

        $calc     = function ($price) {
            return round($price * Currency::getRate('USD'));
        };

        $k = 0;
        foreach ($onlyPrice as $price) {

            if ($matches[2] && $matches[2][$k] === "$") {

                $content = str_replace(
                    $matches[0][$k],
                    $calc($price) + $extraUah . " грн",
                    $content
                );


                $prices[] = $calc($price) + $extraUah;
            } else {

                $content = str_ireplace(
                    $matches[0][$k],
                    $price + $extraUah . " грн",
                    $content
                );

                $prices[] = $price + $extraUah;
            }

            $k++;
        }

        if ($prices) {
            $max_price = max($prices);
        } else {
            $max_price = $prices[0];
        }

        //$qb = Products::where(["api_ID"=>(int)$api_ID])->first();

        /*if(isset($album) && !empty($album)){
            $product->category_id = $album;
        }*/

        if ($product->price > $max_price && !$product->old_price) {
            $product->old_price = $product->price;
        } else {
            $sales_words = [
                'распродажа',
                'розпродаж',
                'новая цена',
                'нова ціна',
                'цена снижена',
                'ціна знижена',
                'уценка',
                'скидка',
                'знижка',
            ];

            $wordsString = implode('|', $sales_words);
            $text_format = Str::lower($content);

            if (preg_match("/($wordsString)/", $text_format) && !$product->old_price) {
                $product->old_price = $product->price;
                Product_variants::where("product_id", $product->id)->delete();
            }
        }

        $product->category_id = 999;
        $product->price = $max_price;
        $product->text = $content;
        $product->created_at = date("Y-m-d h:i:s");
        $product->exchanged_price_by = Currency::getRate('USD'); //BANK API currency


        $response = $product->save();

        //Move photo if new album
        /* if($album && $product && $product->category_id !== $album){

            $photoName = $id . '.jpg';

            $from = UPLOAD_PATH . ($product->category_id ? $product->category_id . '/' : '') . $photoName;

            $to = UPLOAD_PATH . $album;

            # Если файл не существует
            if( !file_exists( $from ) ){
                return response()->json(['result' => "File not found",]);
            }
            
            # Если директории 
            if( !is_dir( $to ) ){
                return response()->json(['result' => "Provider directory not found",]);
            }


            if (!rename($from, $to . '/' . $photoName)) {
                return response()->json(['result' => "Cannot move image into album folder",]);
            }

        } */

        if ($response) {
            return response()->json(['result' => "Success"]);
        } else {
            return response()->json(['result' => "Fail",]);
        }
    }

    public function photosRemove(Request $request)
    {

        $ids = $request->ids;

        $response = false;

        if (is_array($ids)) {

            foreach ($ids as $id) {

                if (is_numeric($id)) {

                    $res = Products::where(["api_ID" => (int)$id])->first();

                    if ($res && $res->not_removed != 1) {
                        $response = $res->delete();
                    }
                } else {
                    return response()->json(['result' => "Error, incorrect ID- " . $id,]);
                }
            }
        }


        return response()->json(['result' => "Success",]);
    }

    public function photosRemoveByVkSource(Request $request)
    {

        $vk_sources = $request->vk_sources;

        $response = false;

        if (is_array($vk_sources)) {

            foreach ($vk_sources as $vk_source) {


                $photo = Products::get_image_by_vk_source($vk_source);

                if ($photo) {

                    $res = Products::where(["id" => (int)$photo->product_id])->first();

                    if ($res && $res->not_removed != 1) {
                        $unset_image = Products::unset_image($photo->id);

                        $product_images = Products::get_images($photo->product_id);

                        if (count($product_images) < 1) {
                            $response = $res->delete();
                            Product_variants::where("product_id", $photo->product_id)->delete();
                        }
                    }
                } else {
                    //return response()->json(['result' => "Error, incorrect ID- ".$id,]);
                }
            }
        }


        return response()->json(['result' => "Success",]);
    }

    public function addTracks(Request $request)
    {
        if (is_array($request->tracks)) {

            foreach ($request->tracks as $track) {

                if ($track["name"] && $track["number"]) {
                    $query = Tracks::create(["name" => $track["name"], "number" => $track["number"], "created_at" => date("Y-m-d")]);
                }
            }

            return response()->json(['result' => "Success"]);
        } else {
            return response()->json(['erorr' => "Tracks is not array"]);
        }
    }

    public function info(Request $request): Response
    {
        $text = $request->text;
        $type = $request->type;

        if ('track' !== $type) {
            throw new \InvalidArgumentException("{$type} is not supported now");
        }

        $data = $text . '<hr>' . AdminSettings::get($type);
        AdminSettings::set($type, $data);

        $result = AdminSettings::save();

        if (\apcu_exists($type)) {
            \apcu_store($type, $data);
        }

        return response()->json(['result' => $result]);
    }

    public function syncStatus(Request $request)
    {
        $data = $request->post();

        return response()->json(
            Storage::disk('local')->put('config/sync-status.json', json_encode($data, JSON_UNESCAPED_UNICODE)) > 0
        );
    }

    public function syncYouStatus(Request $request)
    {
        $data = $request->post();

        return response()->json(
            Storage::disk('local')->put('config/syncyou-status.json', json_encode($data, JSON_UNESCAPED_UNICODE)) > 0
        );
    }

    public function history(Request $request)
    {
        $eventId = $request->event;

        if (null === $eventId || !in_array(
            (int)$eventId,
            [
                AlbumPhotoEvents::PHOTO_ADDED_EVENT,
                AlbumPhotoEvents::PHOTO_CHANGED_EVENT,
                AlbumPhotoEvents::PHOTO_DELETED_EVENT,
            ],
            true
        )) {
            return response()->json(
                [
                    'error' => 'Event is invalid',
                ]
            );
        }

        $eventId = (int)$eventId;
        $offset  = (int)$request->offset;
        $limit   = (int)$request->limit;

        if ($limit > 50) {
            return response()->json(
                [
                    'error' => 'Limit cannot exceed 50',
                ]
            );
        }

        $where = [];
        if ($fromDatetime = $request->from_datetime) {
            $where['ape.updated_at >= :from_datetime'] = [':from_datetime' => date(DATE_ATOM, $fromDatetime)];
        }
        if ($toDatetime = $request->to_datetime) {
            $where['ape.updated_at <= :to_datetime'] = [':to_datetime' => date(DATE_ATOM, $toDatetime)];
        }

        if ($albumName = $request->album) {
            $where['ape.album = :album'] = [':album' => filter_var($albumName, FILTER_VALIDATE_INT)];
        }

        /** @var AlbumPhotoEventsRepository $ape */
        $ape = $this->em->getRepository(AlbumPhotoEvents::class);

        return response()->json(
            [
                'event'  => $eventId,
                'offset' => $offset,
                'limit'  => $limit,
                'result' => $ape->findAllBy($eventId, $where, $offset, $limit),
            ]
        );
    }

    private function sendMessageToQueue($message)
    {
        return exec(
            'php cli.php enqueue:send ' . QUEUE_IMAGE_UPLOAD_API . " {$message} > /dev/null &"
        );
    }

    public function set_nbu_currency()
    {
        $bank_rate = file_get_contents("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&json");

        $usd_decode = json_decode($bank_rate);

        if ($usd_decode && $usd_decode[0]) {
            $nbu_rate = $usd_decode[0]->rate;
            Settings::where('key', "nbu_rate")->update(['value' => $nbu_rate]);
        }
    }
}
