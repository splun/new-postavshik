<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\Product_variants;
use App\Models\Orders;
use App\Models\Order_items;
use App\Models\ChMessage;
use App\Models\Settings;
use App\Models\Payment;
use Chatify\Facades\ChatifyMessenger as Chatify;
use Dou\NovaPoshtaApi\Requests\Address\CityRequest;
use Dou\NovaPoshtaApi\Requests\Address\WarehousesRequest;
use Dou\NovaPoshtaApi\Service\NovaPoshtaAPI;
use Ukrposhta;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Cart;
use Carbon\Carbon;

class CartController extends Controller
{
    public function index(){

        $cart = session()->has('cart') ? session()->get('cart') : [];

        $data = [];

        if($cart && is_array($cart)){

            $data["total_quantity"] = 0;
            $data["total_price"] = 0;

            if($cart['variations'] && is_array($cart['variations'])){
                foreach($cart['variations'] as $key=>$val){ 

                    if(!$key || $key == "undefined") continue;

                    $product = Products::find($val['product']);
                    $preview = Products::get_preview($val['product']);

                    if(isset($preview)){

                        if($preview->base64){
                            $preview_url = "data:image/jpeg;base64,".$preview->base64;
                        }else{
                            $preview_url = $preview->url;
                        }
                        
                    }else{
                        $preview_url = "";
                    }

                    if(!$product) continue;
                    
                    //Available Options ToDoo Perfect
                    $color = "";
                    $size = "";
                    

                    if(isset($val["color"]) && $val["color"] != 0){
                        $color = Product_variants::where(array("product_id"=>$product->id,"option_id"=>$val["color"]))->first()->options->option_value;
                    }

                    if(isset($val["size"]) && $val["size"] != 0){
                        $size = Product_variants::where(array("product_id"=>$product->id,"option_id"=>$val["size"]))->first()->options->option_value;
                    }

                    $data["items"][] = array(
                            "id"=>$product->id,
                            "key"=>$key,
                            "description"=>$product->text,
                            "price"=>$product->price,
                            "stock_number"=>$product->stock_number,
                            "category"=>$product->category,
                            "provider"=>$product->provider,
                            "total"=>$product->price*$val["quantity"],
                            "quantity"=>$val["quantity"],
                            "preview" => $preview_url,
                            "color"=>($color) ? $color : "",
                            "size"=>($size) ? $size : "",
                    );

                    $data["total_quantity"] += $val["quantity"];
                    $data["total_price"] += $product->price*$val["quantity"];
                }
            }

            
        }

        //dd($this->get_cities('Мико'));

        return view('cart')->with($data);
    }

    public function get_shipping_cities($tag){
        
        if(!$tag){
            return 0;
        }
        $request = new CityRequest();
        $request = $request->setCityName($tag);
        $api = new NovaPoshtaAPI();
        $api->setRequest($request);
        $cities = $api->get();

        $response = array();

        if($api->response){
            
            $items = $api->response->getData()->all();
            foreach($items as $item){
                $response[]['name'] = $item["Description"];
            }

        }
        
        return json_encode($response);
    }
    
    public function get_up_shipping_cities($tag){
        
        if(!$tag){
            return 0;
        }

        $request = Ukrposhta::getCities($tag);

       
        $response = array();

        if($request){

            $items = $request;
            
            foreach($items as $item){
                
                $response[$item->CITY_ID]['name'] = $item->CITY_UA." (".$item->REGION_UA." обл, ".$item->DISTRICT_UA." р-н)";
                $response[$item->CITY_ID]['CITY_ID'] = $item->CITY_ID;
            }

        }
        
        return json_encode($response);
    }

    public function get_np_warehouses($city){

        if(!$city){
            return 0;
        }

        $request = new WarehousesRequest();
        $request = $request->setCityName($city);

        $api = new NovaPoshtaAPI();
        $api->setRequest($request);
        $api->get();

        $response = array();
        
        if($api->response){
            
            $items = $api->response->getData()->all();
            
            foreach($items as $item){
                if($item['CategoryOfWarehouse'] === "Branch"){
                    $response[]['name'] = $item["Description"];
                }  
            }

        }
        
        return json_encode($response);

    }

    public function get_up_warehouses($city_id){

        if(!$city_id){
            return 0;
        }
        
        $request = Ukrposhta::getPostOffices(null,null,(int)$city_id);

        //dump($request);

        $response = array();

        if($request){
            
            $items = $request;

            foreach($items as $item){
                $response[]['name'] = $item->POSTCODE.", ".$item->ADDRESS;
            }

        }
        
        return json_encode($response);

    }

    public function add($id,Request $request){

        $data = [];

        //if(auth()->user()){

        //}else{

            $cart = session()->has('cart') ? session()->get('cart') : [];

            if(empty($cart)){
                $cart["variations"] = array();
            }

            if($request->size || $request->color){

                $quantity = 1;
                $color = 0;
                $size = 0;

                if($request->size){
                    $size = $request->size;
                }

                if($request->color){
                    $color = $request->color;
                }

                if (array_key_exists($id.'-'.$size.'-'.$color, $cart["variations"])) {
                    $cart["variations"][$id.'-'.$size.'-'.$color] = array(
                        'product' => $id,
                        'size'=>$size,
                        'color'=>$color,
                        'quantity' => $cart["variations"][$id.'-'.$size.'-'.$color]['quantity']+1
                    );
                }else{
                    $cart["variations"][$id.'-'.$size.'-'.$color] = array(
                        'product' => $id,
                        'size'=>$size,
                        'color'=>$color,
                        'quantity' => 1
                    );
                }

            }else{

                if (array_key_exists($id, $cart['variations'])) {
                    $cart["variations"][$id] = array(
                        'product' => $id,
                        'quantity'=>$cart["variations"][$id]['quantity']+1
                    );

                }else{
                    $cart["variations"][$id] = array(
                        'product' => $id,
                        'quantity'=>1
                    );
                }

                
            }

            session(['cart' => $cart]);
            session()->flash('message', $id.' added to cart.');

            $data = array(
                'result' =>"success",
                'cart' => session()->has('cart') ? session()->get('cart') : []
            );
            
        //}

        return response()->json($data);
    }

    public function update($id,$count){

            $cart = session()->has('cart') ? session()->get('cart') : [];

            $cart["variations"][$id]['quantity'] = $count;
            
            session(['cart' => $cart]);

            
            $data = array(
                "result" =>"success"
            );
            

        return response()->json($data);
    }

    public function delete($id){


        $cart = session()->has('cart') ? session()->get('cart') : [];


        unset($cart['variations'][$id]);
        
        session(['cart' => $cart]);

        $data = array(
            "result" =>"success",
            "items" => count($cart)
        );
        

        return redirect('/cart')->with('status', 'Корзина обновлена');
    }

    public function order(Request $request){

        $cart = session()->has('cart') ? session()->get('cart') : [];

        if($cart && is_array($cart) && $cart['variations'] && is_array($cart['variations'])){
            
            $total_price = 0;

            if($cart['variations'] && is_array($cart['variations'])){
                foreach($cart['variations'] as $key=>$val){ 
                    $product = Products::find($val['product']);
                    if(!$product) continue;
                    $total_price += $product->price*$val["quantity"];
                }
            }

            /*
            Статусы: 
            Ожидает оплаты
            Оплачен
            Оплачен (Аванс)
            Отправлен/Завершен
            Отменен
            Возврат
            */
            if($request->shipping_method == "Нова Пошта"){
                $city = $request->np_c;
                $post_address = htmlspecialchars($request->np_post_address);
            }else{
                $city = $request->up_c;
                $post_address = htmlspecialchars($request->up_post_address);
            }

            $order_data = array(
                "user_id"=> (int)$request->user,
                "fio"=>$request->firstName." ".$request->lastName." ".$request->fatherName,
                "phone"=>$request->phone,
                "email"=>$request->email,
                "comment"=>($request->comment) ? $request->comment : NULL,
                "status"=>"Ожидает оплаты",
                "payment_method"=>$request->payment_method,
                "shipping_method"=>$request->shipping_method,
                "price_total"=>$total_price,
                "city" => $city,
                "post_address" => $post_address,
                "order_currency"=>$request->currency
            );

            if($request->cookie('utm_source')){
                $order_data["utm_source"] = $request->cookie('utm_source');
                setcookie('utm_source', "0", time() - 7200,'/');
            }

            if($request->cookie('utm_campaign')){
                $order_data["utm_campaign"] = $request->cookie('utm_campaign');
                setcookie('utm_campaign', "0", time() - 7200,'/');
            }

            $order = Orders::create($order_data);
            
            $order_items = "";

            if($order && $order->id){

                foreach($cart['variations'] as $key=>$val){ 

                    $product = Products::find($val['product']);

                    if(!$product) continue;
                    
                    //Available Options ToDoo Perfect
                    $color = "";
                    $size = "";
                    $options = array();

                    if(isset($val["color"]) && $val["color"] != 0){
                        $color = Product_variants::where(array("product_id"=>$product->id,"option_id"=>$val["color"]))->first()->options->option_value;
                        $options["color"] = $color;
                    }
                    
                    if(isset($val["size"]) && $val["size"] != 0){
                        $size = Product_variants::where(array("product_id"=>$product->id,"option_id"=>$val["size"]))->first()->options->option_value;
                        $options["size"] = $size;
                    }
                    $cart_data = array(
                        "order_id"=>$order->id,
                        "product_id"=>$product->id,
                        "product_options"=>serialize($options),
                        "product_qty"=>$val["quantity"],
                        "price"=>$product->price*$val["quantity"],
                        "base_price"=>$product->price
                    );

                    Order_items::create($cart_data);

                }
                
                if($request->payment_method == "Full"){
                    $amount = $total_price*100;
                }else{
                    $amount = config('settings.amount_prepayment')*100;
                }

                $checkoutData = [
                    'order_id' => "INV-".$order->id,
                    'order_desc' => "Замовлення №{$order->id}",
                    'currency' => "UAH",
                    'amount' => $amount
                ];

                try {

                    $invoice = Payment::mono($checkoutData);

                    $order->transaction_id = $invoice['invoiceId'];

                    $order->save();

                    if(!isset($invoice['pageUrl'])){

                        session()->flash('status', $invoice['errCode'].": ".$invoice['errText']);

                        return redirect('/cart?validate="false"');
                    }else{
                        
                        return redirect($invoice['pageUrl']);
                    }

                } catch (Exception $e) {
                    
                    Log::debug("MonoBank Message".$e->getMessage());

                    session()->flash('status', $e->getMessage());

                    return redirect('/cart?validate="false"');
                    
                }

                //return redirect('/checkout/success/'.$order->id); If create order no payment

            }else{
                 return redirect('/cart?validate="false"');
            }

        }else{
            return redirect('/cart?validate="false"');
        }
 
        
    }

    public function checkout_callback($order_id){

        try {

            $decrypted = Crypt::decryptString($order_id);

        } catch (DecryptException $e) {

            Log::debug($e->getMessage());

        }  

        $order_id = str_replace('INV-','',$decrypted);

        $order = Orders::find($order_id);

        if($order){

            $invoiceId = $order->transaction_id;

            try {

                $paymentStatus = Payment::checkMonoInvoiceStatus($invoiceId);

                if($paymentStatus['status'] == "success")
                {

                    $cart = session()->has('cart') ? session()->get('cart') : [];

                    $order->status = "Оплачен";

                    $order->payment_id = $paymentStatus['status'].'-'.$paymentStatus['reference'];

                    $order->save();

                    unset($cart["variations"]);

                    session(['cart' => $cart]);

                    return redirect('/checkout/success/'.$order->id);
                }

                if($paymentStatus['status'] == "processing"){
                    $order->status = "Обработка банком";
                    $order->save();
                }
                
                if($paymentStatus['status'] == "failure"){

                    session()->flash('status', $paymentStatus['errCode'].": ".$paymentStatus['failureReason']);

                    return redirect('/cart?validate="false"');

                }
                

            } catch (Exception $e) {
                
                Log::debug($e->getMessage());
                
            }
            
        }else{
            return redirect('/cart?validate="false"');
        }

    }

    public function checkout_callback_fondy(Request $request){ // Set route in routes file

        $cart = session()->has('cart') ? session()->get('cart') : [];

        $request = $request->all();

        if($request['order_status'] == "approved" && $request['response_status'] == "success"){
            

            /*if($request["additional_info"] && isset(json_decode($request["additional_info"])->transaction_id)){
                $transaction_id = json_decode($request["additional_info"])->transaction_id;
            }*/
            
            $order_id = $request['product_id'];

            $order = Orders::find($order_id);
  
            if($order){

                $order->status = "Оплачен";
                $order->transaction_id = $request['order_id'];
                $order->payment_id = $request['payment_id'];

                $order->save();

                unset($cart["variations"]);

                session(['cart' => $cart]);

                return redirect('/checkout/success/'.$order->id);
            }else{
                return redirect('/cart?validate="false"');
            }

            
        }else{
            return redirect('/cart?validate="false"');
        }


    }

    public function success($order_id){

        if($order_id){

            $order = Orders::find($order_id);
            $fb_purchase_items = array();

            if($order && $order->user_notificated == 0){   
                $order_items = "";
                
                if($order->items){
                    foreach($order->items as $key=>$val){ 

                        $product = Products::find($val['product_id']);

                        if(!$product) continue;
                        
                        //Available Options ToDoo Perfect
                        $color = "";
                        $size = "";
                        $options = array();

                        $product_options = unserialize($val["product_options"]);

                        if(isset($product_options["color"])){
                            $color = "\nКолір: ".$product_options["color"];
                        }

                        if(isset($product_options["size"])){
                            $size = "\nРозмір: ".$product_options["size"];
                        }
                        $price = (int)$product->price;
                        $order_items .= "\n{$product->stock_number} - https://postavshik.net/product/{$product->id}{$color}{$size}\nКількість: {$val['product_qty']}\nВартість: {$price} грн.\n";
                        
                        array_push($fb_purchase_items, array('id'=>(string)$product->id, 'quantity'=>$val['product_qty']));
                    }
                }
                

                //Add order to chat message
                $messageID = mt_rand(9, 999999999) + time();

                if($order->payment_method == "Full"){
                    $text_method = "Повна оплата";
                    $order_message = "<span class='msg-order'>Нове замовлення #{$order->id}\n {$order_items}\nСума: {$order->price_total} грн.\nМетод оплати: {$text_method}\nФІО: {$order->fio}\nТелефон: {$order->phone}\nДоставка: {$order->shipping_method}\n {$order->city}\n {$order->post_address}</span>";
                }else{
                    $text_method = "Часткова оплата";
                    $prepayment_rate = config('settings.amount_prepayment');
                    $payment_total = $order->price_total-$prepayment_rate;
                    $order_message = "<span class='msg-order'>Нове замовлення #{$order->id}\n {$order_items}\nСума: {$order->price_total} грн.\nСплачено: {$prepayment_rate} грн.\nЗалишок при отриманні: {$payment_total} грн.\nМетод оплати: {$text_method}\nФІО: {$order->fio}\nТелефон: {$order->phone}\nДоставка: {$order->shipping_method}\n {$order->city}\n {$order->post_address}</span>";
                }

                
                Chatify::newMessage([
                    'id' => $messageID,
                    'type' => "user",
                    'from_id' => (int)$order->user_id,
                    'to_id' => 19,
                    'body' => $order_message,
                    'attachment' => null
                ]);

                // ChMessage::create([
                //     'id' => $messageID,
                //     'type' => "user",
                //     'from_id' => (int)$order->user_id,
                //     'to_id' => 19,
                //     'is_order' => 1,
                //     'body' => $order_message,
                //     'attachment' => null,
                // ]);

                // // fetch message to send it with the response
                $messageData = Chatify::fetchMessage($messageID);

                // // send to user using pusher
                Chatify::push('private-chatify', 'messaging', [
                    'from_id' => (int)$order->user_id,
                    'to_id' => 19,
                    'message' => Chatify::messageCard($messageData, 'default')
                ]);

                $order->user_notificated = 1;
                $order->save();
            }

            return view("/success")->with(["order_id"=>$order_id, "total"=>$order->price_total, "fb_purchase_items"=>json_encode($fb_purchase_items)]);
        }else{
            return redirect('/');
        }
        

    }

    public function MonoWebhook(){

        try{

            $orders = Orders::whereRaw('`created_at` >= DATE_SUB(NOW() , INTERVAL 360 MINUTE) AND `status` != "Оплачен"')->get();

            if($orders){

                foreach($orders as $order){

                    $invoiceId = $order->transaction_id;

                    $paymentStatus = Payment::checkMonoInvoiceStatus($invoiceId);

                    if($paymentStatus['status'] == "success")
                    {

                        $order->status = "Оплачен";
                        
                        $order->payment_id = $paymentStatus['status'].'-'.$paymentStatus['reference'];

                        $order->save();

                        $fb_purchase_items = array();

                        if($order->user_notificated == 0){   
                            $order_items = "";
                            
                            if($order->items){
                                foreach($order->items as $key=>$val){ 

                                    $product = Products::find($val['product_id']);

                                    if(!$product) continue;
                                    
                                    //Available Options ToDoo Perfect
                                    $color = "";
                                    $size = "";
                                    $options = array();

                                    $product_options = unserialize($val["product_options"]);

                                    if(isset($product_options["color"])){
                                        $color = "\nКолір: ".$product_options["color"];
                                    }

                                    if(isset($product_options["size"])){
                                        $size = "\nРозмір: ".$product_options["size"];
                                    }
                                    $price = (int)$product->price;
                                    $order_items .= "\n{$product->stock_number} - https://postavshik.net/product/{$product->id}{$color}{$size}\nКількість: {$val['product_qty']}\nВартість: {$price} грн.\n";
                                    
                                    array_push($fb_purchase_items, array('id'=>(string)$product->id, 'quantity'=>$val['product_qty']));
                                }
                            }
                            

                            //Add order to chat message
                            $messageID = mt_rand(9, 999999) + time();

                            if($order->payment_method == "Full"){
                                $text_method = "Повна оплата";
                                $order_message = "<span class='msg-order'>Нове замовлення #{$order->id}\n {$order_items}\nСума: {$order->price_total} грн.\nМетод оплати: {$text_method}\nФІО: {$order->fio}\nТелефон: {$order->phone}\nДоставка: {$order->shipping_method}\n {$order->city}\n {$order->post_address}</span>";
                            }else{
                                $text_method = "Часткова оплата";
                                $prepayment_rate = config('settings.amount_prepayment');
                                $payment_total = $order->price_total-$prepayment_rate;
                                $order_message = "<span class='msg-order'>Нове замовлення #{$order->id}\n {$order_items}\nСума: {$order->price_total} грн.\nСплачено: {$prepayment_rate} грн.\nЗалишок при отриманні: {$payment_total} грн.\nМетод оплати: {$text_method}\nФІО: {$order->fio}\nТелефон: {$order->phone}\nДоставка: {$order->shipping_method}\n {$order->city}\n {$order->post_address}</span>";
                            }

                            
                            Chatify::newMessage([
                                'id' => $messageID,
                                'type' => "user",
                                'from_id' => (int)$order->user_id,
                                'to_id' => 19,
                                'body' => $order_message,
                                'attachment' => null
                            ]);

                            // ChMessage::create([
                            //     'id' => $messageID,
                            //     'type' => "user",
                            //     'from_id' => (int)$order->user_id,
                            //     'to_id' => 19,
                            //     'is_order' => 1,
                            //     'body' => $order_message,
                            //     'attachment' => null,
                            // ]);

                            // // fetch message to send it with the response
                            $messageData = Chatify::fetchMessage($messageID);

                            // // send to user using pusher
                            Chatify::push('private-chatify', 'messaging', [
                                'from_id' => (int)$order->user_id,
                                'to_id' => 19,
                                'message' => Chatify::messageCard($messageData, 'default')
                            ]);

                            $order->user_notificated = 1;
                            $order->save();
                        }

                    }

                    if($paymentStatus['status'] == "processing"){
                        $order->status = "Обработка банком";
                        $order->save();

                    }

                    Log::info("Monobank Response - Order:".$order->id." Transaction: ".$order->transaction_id." - ".$order->status);
                    
                    //dump($order->transaction_id." - ".$order->status);
                    
                }

            }

        }catch (Exception $e) {

        Log::error("MonoBank Message ".$e->getMessage());

    }

    }
}
