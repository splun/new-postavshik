<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comments;
use Illuminate\Support\Facades\Storage;
use Validator;
use Auth;
use File;

class CommentsController extends Controller
{
	public function index(){

		if(Auth::user()){
			$data["user"] = Auth::user();
		}else{
			$data["user"] = array();
		}


        $data["comments"] = Comments::whereNull("parent_id")
		->where(array(['status', '=', 1],['text','!=','']))
		->orderBy('created_at','DESC')
		->paginate(25);

		return view("comments.list")->with($data);
	}

	static function child($parent_id){

		$comments = Comments::where(array(['parent_id','=',$parent_id],['status', '=', 1],['text','!=','']))
		->orderBy('created_at','DESC')->get();

		return $comments;

	}

	static function getAttachments($comment_id){

		$attachment = Storage::files('public/comments/'.$comment_id.'/'); 

		//dd($attachment);
		return $attachment;

	}

	public function store(Request $request){

		$data['status'] = config('settings.show_comments_immediately');

		
		$user = Auth::user();

		if($user) {
			$data['user_id'] = $user->id;
		}

		$data['text'] = $request->post("text");

		if($request->post("parent_comment")){
			$data['parent_id'] = $request->post("parent_comment");
		}

		$validator = Validator::make(request()->all(), [
			'text' => 'required',
			'photos.*' => 'file|max:5000|mimes:png,jpg,jpeg'
		 ]);

	

		if ($validator->fails()) {
			return redirect('comments')->with(['error'=>$validator->errors()->all()]);
		}
		
		$comments = Comments::create($data);

		if($request->file("photos")){
			foreach($request->file("photos") as $key => $image){

				$image->store('comments/'.$comments->id.'','public');
			}
		}

		if($data['status'] == 1){
			return redirect('comments')->with(['message'=>'Спасибо! Ваше отзыв успешно опубликован! Ваше мнение очень важно для нас!']);
		}else{
			return redirect('comments')->with(['message'=>'Спасибо! Ваше отзыв появиться после проверки модератором! Ваше мнение очень важно для нас!']);
		}
	}

	public function delete($id){

		if(Auth::user()->permission != "admin"){
            abort(404);
        }

        Comments::where("id",$id)->delete();

		return back();
	}

}