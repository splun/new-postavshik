<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pages;
use App\Models\Tracks as Tracks;
use Mail;
use Carbon\Carbon;
use RayNl\LaravelFacebookCatalog\LaravelFacebookCatalog;
use App\Models\Products;
use App\Models\Product_options;

class PagesController extends Controller
{
    public function index($slug)
    {

        $page = Pages::where('slug', $slug)->firstOrFail();

        if ($page) {
            return view('page', $page);
        } else {
            return abort(404);
        }
    }

    public function contacts()
    {

        $page = Pages::where('slug', "contacts")->firstOrFail();

        return view('contacts', $page);
    }

    public function sendMessage(Request $request)
    {

        $validate = $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'not_robot' => 'required'
        ]);

        if ($validate) {

            $send = Mail::send(
                array('text' => 'emails.contact'),
                array(
                    'name' => $request->post('name'),
                    'email' => $request->post('email'),
                    'user_message' => $request->post('message')
                ),
                function ($message) use ($request) {

                    $message->from($request->post('email'));
                    $message->to(config('settings.admin_email'), $request->post('name'))->subject('Письмо сайта postavshik.net');
                }
            );

            return redirect('contacts')->with(['message' => 'Ваше сообщение успешно отправлено!']);
        }
    }

    public function track()
    {

        //$tracks["np"] = Tracks::where("carrier","NP")->orderBy('created_at')->get();
        //$tracks["up"] = Tracks::where("carrier","UP")->orderBy('created_at')->get();

        $tracks = Tracks::where('created_at', '>', \Carbon\Carbon::now()->addDays(-90))->orderBy('created_at', 'desc')->orderBy('name', 'asc')
            ->paginate(500);

        $tracks->setCollection($tracks->groupBy(function ($lists) {
            return Carbon::parse($lists->created_at)->format('d.m.Y');
        }));
        /*$result = array();
        $previous = null;
        $i = 0;

        foreach ($tracks as $key=>$track) {

            foreach($track as $value){

                $firstLetter = substr($value->name, 0, 1);

                if ($previous !== $firstLetter) {
                    $result[$key][$i]['fl'] = mb_convert_case($firstLetter, MB_CASE_UPPER, "Windows-1251");
                }
                $result[$key][$i]['track'][] = array(
                    "id" => $value->id,
                    "number" => $value->number,
                    "name" => $value->name,
                    "carrier" => $value->carrier
                );

                $previous = $firstLetter;
                $i++;

            }
        }*/

        //dd($result);

        return view('tracks', ['tracks' => $tracks]);
    }

    public function feed()
    {

        LaravelFacebookCatalog::setTitle('Postavshik.net feed');
        LaravelFacebookCatalog::setDescription('Postavshik.net online shop');
        LaravelFacebookCatalog::setLink('https://postavshik.net');

        $productsQuery = Products::query();

        $productsQuery->select('products.*', 'products_images.url');

        $productsQuery->where('not_removed', '=', 1);

        $productsQuery->leftJoin('products_images', function ($join) {
            $join->on('products.id', '=', 'products_images.product_id')
                ->where('products_images.preview', '=', 1);
        });

        $productsQuery->orderBy('created_at', 'DESC');

        $products = $productsQuery->get();

        foreach ($products as $product) {
            $gtin = "";
            $category = "";

            $text_no_price = preg_replace('/Ціна:.*?грн|Ціна :.*?грн|Ціна:.*? грн|Ціна :.*? грн.|Ціна:.*? грн./is', ' ', $product->text);
            $text_no_price = preg_replace('/Цена :.*? грн|Цена:.*?грн|Цена:.*?грн./is', ' ', $text_no_price);

            $lines = preg_split("/[\r\n]+/", $text_no_price);
            $productName = trim($lines[0]);

            $description = $text_no_price . "\n" . $product->description;
            $finalDescription = nl2br($description);

            if ($product->old_price) {
                $price = (float)$product->old_price;
                $sale_price = (float)$product->price;
            } else {
                $price = (float)$product->price;
                $sale_price = null;
            }
            if ($product->provider) {
                $gtin = $product->provider->alias . "-" . $product->stock_number;
            }
            if ($product->category) {
                $category = $product->category->name;
            }
            $fileExtension = pathinfo($product->url, PATHINFO_EXTENSION);

            //if (!in_array($fileExtension, ['jpg', 'jpeg', 'png'])) {// Video }

            if (strripos($product->url, "storage")) {
                $preview_url = "https://postavshik.net" . $product->url;
            } else {
                $preview_url = $product->url;
            }

            LaravelFacebookCatalog::addItem(
                "https://postavshik.net/product/" . $product->id, // URL продукта
                $product->id, // ID
                $productName, // Название продукта (первая строка из $text_no_price)
                $preview_url, // URL изображения-превью
                $finalDescription, // Описание продукта с сохранением переводов строк
                'in stock', // Статус наличия на складе
                $price,      // Цена
                $sale_price, // Цена по акции, если есть
                isset($product->provider->alias) ? $product->provider->alias : "Postavshik", // Бренд / SKU
                $gtin,     // GTIN
                $category, // Категория продукта
                "new"      // Состояние продукта
            );
        }

        return LaravelFacebookCatalog::display();
    }
}
