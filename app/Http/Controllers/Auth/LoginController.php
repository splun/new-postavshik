<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;
use App\Models\User;
use Auth;
use Hash;
use Redirect;
use Socialite;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    protected $loginType;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->loginType = $this->checkLoginInput();
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'login' => 'required|string',
            'password' => 'required|string'
        ]);

        $credentials = [
             $this->loginType => $request->login,
            'password'           => $request->password
        ];


        if (Auth::attempt($credentials)) {
            return redirect()->intended($this->redirectTo);
        }

        return redirect()->back()
            ->withInput()
            ->withErrors([
                'login' => trans("auth.failed")
            ]);
    }

    protected function  checkLoginInput()
    {
        $inputData = request()->get('login');

        return  filter_var($inputData, FILTER_VALIDATE_EMAIL) ? 'email' : 'name';
    }

    public function social(Request $request)
    {
        // Get information about user.
        $data = file_get_contents('http://ulogin.ru/token.php?token=' . $_POST['token'] . '&host=' . $_SERVER['HTTP_HOST']);
        $user = json_decode($data, TRUE);

        $network = $user['network'];

        // Find user in DB.
        $userData = User::where('email', $user['email'])->first();

        // Check exist user.
        if (isset($userData->id)) {

            // Check user status.
            if ($userData->is_locked != 1) {

                // Make login user.
                Auth::loginUsingId($userData->id, TRUE);
            }
            // Wrong status.
            else {
                \Session::flash('error', trans('interface.AccountNotActive'));
            }

            return Redirect::back();
        }
        // Make registration new user.
        else {

            //dd($user);

            if($user["network"] == "vkontakte"){
                $network_id =1;
            }
            if($user["network"] == "odnoklasniki"){
                $network_id =2;
            }
            if($user["network"] == "facebook"){
                $network_id =3;
            }
            if(isset($user["nickname"])){
                $nickname = $user['nickname'];
            }else{
                $nickname = NULL;
            }
            if(isset($user['photo'])){
                $icon = $user['photo'];
            }else{
                $icon = NULL;
            }

            // Create new user in DB.
            $newUser = User::create([
                'name' => $nickname,
                'email' => $user['email'],
                'password' => Hash::make(Str::random(8)),
                'network_id'=>$network_id,
                'identity'=> $user['identity'],
                'user_social_id'=>$user['uid'],
                'firstname' => $user['first_name'],
                'lastname' => $user['last_name'],
                'icon' => $icon,
                'permission' => 'user',
                'is_locked' => 0
            ]);

            // Make login user.
            Auth::loginUsingId($newUser->id, TRUE);

            \Session::flash('flash_message', trans('interface.ActivatedSuccess'));

            return Redirect::back();
        }
    }
    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }
    // public function loginWithFacebook()
    // {
    //     try {
    
    //         $user = Socialite::driver('facebook')->user();
    //         $isUser = User::where('fb_id', $user->id)->first();
     
    //         if($isUser){
    //             Auth::login($isUser);
    //             return redirect('/dashboard');
    //         }else{
    //             $createUser = User::create([
    //                 'name' => $user->name,
    //                 'email' => $user->email,
    //                 'fb_id' => $user->id,
    //                 'password' => encrypt('admin@123')
    //             ]);
    
    //             Auth::login($createUser);
    //             return redirect('/dashboard');
    //         }
    
    //     } catch (Exception $exception) {
    //         dd($exception->getMessage());
    //     }
    // }
    public function loginWithFacebook(Request $request)
    {
 
        try {

            $user = Socialite::driver('facebook')->usingGraphVersion('v19.0')->user();

        
            // Find user in DB.
            $userData = User::where('user_social_id', $user->id)->first();


            // Check exist user.
            if (isset($userData->id)) {

                // Check user status.
                if ($userData->is_locked != 1) {

                    // Make login user.
                    Auth::loginUsingId($userData->id, TRUE);
                }
                // Wrong status.
                else {
                    \Session::flash('error', trans('interface.AccountNotActive'));
                }

                return Redirect::back();
            }
            // Make registration new user.
            else {

                //dd($user);
                $network_id = 3;

                if(isset($user->nickname)){
                    $nickname = $user->nickname;
                }else{
                    $nickname = NULL;
                }

                if(isset($user->avatar)){
                    $avatar = $user->avatar;
                }else{
                    $avatar = NULL;
                }

                $first_name = explode(" ",$user->name)[0];
                $last_name = explode(" ",$user->name)[1];
                // Create new user in DB.
                $newUser = User::create([
                    'name' => ($nickname) ? $nickname : $user->name,
                    'email' => (isset($user->email)) ? $user->email : $user->id,
                    'password' => Hash::make(Str::random(8)),
                    'network_id'=>$network_id,
                    'identity'=> (isset($user->identity)) ? $user->identity : 0,
                    'user_social_id'=>$user->id,
                    'firstname' => ($first_name) ? $first_name : '',
                    'lastname' => ($last_name) ? $last_name : '',
                    'icon' => $avatar,
                    'permission' => 'user',
                    'is_locked' => 0
                ]);

                // Make login user.
                Auth::loginUsingId($newUser->id, TRUE);

                \Session::flash('flash_message', trans('interface.ActivatedSuccess'));

                return Redirect::back();
            }
        } catch (Exception $exception) {
            dd($exception->getMessage());
        }
    }

    protected function authenticated($request,$user){

        
        if($user->permission === 'admin'){
            
            return redirect()->intended('admin'); //redirect to admin panel
        }
    
        return redirect()->intended('/'); //redirect to standard user homepage
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
}
