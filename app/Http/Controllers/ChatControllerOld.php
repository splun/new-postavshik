<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Chat;
use App\Models\User;
use Validator;
use Auth;

class ChatController extends Controller
{
    public function history($start=0){

        $user = Auth::user();

        $html = "";

        $messages = Chat::where('from_user_id', $user->id)
        ->orWhere('to_user_id', $user->id)
		->orderBy('datetime','DESC')
		->skip($start)
		->take(20)->get();

        $messages = $messages->reverse();
        
        foreach($messages as $req){

            if($req->user->permission == "operator"){
                $username = "Оператор";
                $avatar = "";
            }else{
                $username = ($req->user["firstname"] || $req->user["lastname"]) ? $req->user["firstname"]." ".$req->user["lastname"] : $req->user["name"];
                $avatar = $req->user["icon"];
            }
            $data = array(
                "from_user_id"=>$req->from_user_id,
                "avatar"=>$avatar,
                "name"=>$username,
                "text"=>$req->message,
                "time"=>$req->datetime
            );

            $html .= $this->buildMessage($data);

        }
        
        //dd($html);

        return $html;

    }

    public function dropFiles(Request $request){

        $upload_dir = 'uploads/'; //Создадим папку для хранения изображений
        $allowed_ext = array('jpg','jpeg','png','gif'); //форматы для загрузки

        if(array_key_exists('pic',$_FILES) && $_FILES['pic']['error'] == 0 ){
            
            $pic = $_FILES['image'];
            
            if(!in_array($this->get_extension($pic['name']),$allowed_ext)){
                $this->exit_status('Разрешена загрузка следующих форматов: '.implode(',',$allowed_ext));
            }	

           //Загружаем файл на сервер в нашу папку и посылаем команду о том, что все ОК и файл загружен
            if($image->store('chat/'.$pic['name'].'','public')){
                $this->exit_status('Файл Был успешно загружен!');
            }
            
        }

        $this->exit_status('Во время загрузки произошли ошибки');
 
     }
    
    private function exit_status($str){
        echo json_encode(array('status'=>$str));
        exit;
    }

    private function get_extension($file_name){
        $ext = explode('.', $file_name);
        $ext = array_pop($ext);
        return strtolower($ext);
    }


    public function loadhistory(Request $request){

        $start = $request->post("start");
        
        $history = $this->history($start);
     
        return $history;
 
     }
    
    public function newMessageWithCommet(Request $request){

        $user = Auth::user();
        
        $req = $request->all();

        $data = array(
            "from_user_id"=>$req["msg_data"]["from_user_id"],
            "to_user_id"=>$req["msg_data"]["to_user_id"],
            "avatar"=>$req["msg_data"]["avatar"],
            "name"=>$req["msg_data"]["name"],
            "text"=>$req["msg_data"]["text"],
            "time"=>$req["msg_data"]["time"]
        );

        if($data && ($data["to_user_id"] == $user->id || $data["from_user_id"] == $user->id)){

            return $this->buildMessage($data);
            
        }
    }


    function buildMessage($data){

        $user = Auth::user();

        if($data["avatar"]){
            $avatar = $data["avatar"];
        }else{
            $avatar = '/images/review_image.jpg';
        }

        if ($data["from_user_id"] == $user->id){
        
            $html = '<div class="chat_block left">
                        <div class="chat_image">
                            <img src="'.$avatar.'">
                        </div>
                        <div class="chat_content">
                            <p><b>'.$data["name"].'</b></p>
                            <p>'.$data["text"].'</p>
                            <p class="chat_date">'.$data["time"].'</p>
                        </div>
                    </div>';
        }else{

            $html = '<div class="chat_block right">
                    <div class="chat_content">
                        <p><b>'.$data["name"].'</b></p>
                        <p>'.$data["text"].'</p>
                        <p class="chat_date">'.$data["time"].'</p>
                    </div>
                    <div class="chat_image">
                        <img src="'.$avatar.'">
                    </div>
                </div>';
        }

        return $html;
    }

    public function push(Request $request){
        
        $user = Auth::user();
        

        if($request->post("operator")){
            $operator = $request->post("operator");
        }else{
            $operator = User::where("permission","=","operator")->get()->random();
            $operator = $operator->id;
        }
        
        
        if(!$operator) return;

		$validator = Validator::make(request()->all(), [
			'text' => 'required',
            'files.*' => 'file|max:3000|mimes:png,jpg,jpeg'
		 ]);

		if ($validator->fails()) {
            echo "Error:".$validator->errors()->all();
		}
		
        
        $data = array( 
            "from_user_id" => $user->id,
            "to_user_id"=>$operator,
            "room_id"=>0, 
            "message"  => $request->post("text"),
            "datetime"  => date("Y-m-d H:i:s"), 
            "is_read" => 0
        );

        $message = Chat::create($data);

        if($message){

            if($request->file("files")){
                //foreach($request->file("files") as $key => $image){
                    $image->store('chat/files/'.$message->id.'','public');
                //}
            }

            echo "ok";
        }

        //Commet old
        /*if($message){

            $host = "app.comet-server.ru";
            $commentUser = "15";
            $password = "lPXBFPqNg3f661JcegBY0N0dPXqUBdHXqj2cHf04PZgLHxT6z55e20ozojvMRvB8";

            $comet = mysqli_connect($host, $commentUser, $password, "CometQL_v1");

            if(mysqli_errno($comet))
            {
                echo "Error:".mysqli_error($link);
            }

            if($user->permission == "operator"){
                $username = "Оператор";
            }else{
                $username = ($user->firstname || $user->lastname) ? $user->firstname." ".$user->lastname : $user->name;
            }

            $msg = Array(
                "message_id"=>$message->id, 
                "from_user_id" => $user->id,
                "to_user_id"=>$operator,
                "name" => $username, 
                "text"  => $request->post("text"),
                "avatar" => $user->icon,
                "time"  => date("d.m.y H:i"), 
            );

            $msg = json_encode($msg);
            $msg = mysqli_real_escape_string($comet, $msg);

            $query = "INSERT INTO pipes_messages (name, event, message)" .
            "VALUES('simplechat', 'newMessage', '".$msg."')";
            
            mysqli_query($comet, $query); 

            if(mysqli_errno($comet))
            {
                echo "Error:".mysqli_error($comet);
            } 
            else
            {
                echo "ok";
            }
        }*/
        
        else{
            echo "Возникла ошибка при отправке сообщения";
        }
    }
}
