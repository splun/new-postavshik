<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use App\Models\Pages;
use App\Models\Slider;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
        
    }

    /**
     * Show the application homepage.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   

        $slides = Slider::where("public",1)->orderBy(DB::raw('RAND()'))->get();
        
        $data["slider"] = $slides;

        $data["banner"] = config('settings.default_banner_url');
        

        /*******New products*********/
        /*$data["new_products"] = Products::select('products.*', 'products_images.url')
         ->orderByDesc('created_at')
         ->take(config('settings.count_new_products'))
         ->leftJoin('products_images', function ($join) {
                $join->on('products.id', '=', 'products_images.product_id')
                ->where('products_images.preview', '=', 1);
            })
         ->get()
         ->sort();*/

        $data['new_category_products'] = Products::select('products.*', 'products_images.url')
            ->take(config('settings.count_discount_products'))
            ->where(["category_id"=>145])
            ->leftJoin('products_images', function ($join) {
                $join->on('products.id', '=', 'products_images.product_id')
                ->where('products_images.preview', '=', 1);
            })
            ->orderByDesc('created_at')
            ->get()
            ->sort();

        /*******Discount products*********/
        $data["discount_products"] = Products::select('products.*', 'products_images.url')
          ->orderByDesc('created_at')
          ->take(config('settings.count_discount_products'))
          ->where('discount', '=', 1)
          ->leftJoin('products_images', function ($join) {
                $join->on('products.id', '=', 'products_images.product_id')
                ->where('products_images.preview', '=', 1);
            })
          ->get()
          ->sort();
        
        $data['sale_products'] = Products::select('products.*', 'products_images.url')
            ->take(config('settings.count_discount_products'))
            ->where(["category_id"=>8])
            ->leftJoin('products_images', function ($join) {
                $join->on('products.id', '=', 'products_images.product_id')
                ->where('products_images.preview', '=', 1);
            })
            ->orderByDesc('created_at')
            ->get()
            ->sort();

        $data['popular_products'] = Products::select('products.*', 'products_images.url')
            ->take(6)
            ->where('is_popular', '>', 0)
            ->where("category_id","!=",8)
            ->where("category_id","!=",999)
            ->leftJoin('products_images', function ($join) {
                $join->on('products.id', '=', 'products_images.product_id')
                ->where('products_images.preview', '=', 1);
            })
            ->orderByDesc('is_popular')
            ->get()
            ->sort();

        $about_page = Pages::find(5);
        $data["about_text"] = $about_page->content;

        return view('home')->with($data);
    }
}
