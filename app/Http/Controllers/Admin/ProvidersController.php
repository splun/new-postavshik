<?php

namespace App\Http\Controllers\Admin;

use App\Models\Settings;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Providers;
use App\Models\Categories;
use App\Models\Products;
use Auth;
use DB;

class ProvidersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
         if(Auth::user()->permission != "admin"){
            abort(404);
        }
        
        $data["default_extra"] = config('settings.providers_default_extra');
        $data["providers"] = Providers::orderBy("alias")->get();
        $data["categories"] = Categories::all();

        return view('admin/providers')->with($data);
    }

    public function add(Request $request){

        $provider = $request->post("provider_number");

        $check = Providers::where(['alias' => $provider])->first();
        
        if($check){
            return back()->with('error','Такой поставщик уже существует!');
        }else{
            Providers::create(['extra_uah' => config('settings.providers_default_extra'), 'alias' => $provider]);
        }
       

        return back()->with('success','Поставщик успешно создан!');
    }

    public function deleteProvider(Request $request){

        if(!empty($request->post("providers_id"))){
            $providers_id = explode(",",$request->post("providers_id"));

            
            $period = $request->post("datetimes");
            if($period){
                $period = explode(" - ",$period);
                $date_from = $period[0];
                $date_to = $period[1];

                foreach($providers_id as $key => $id) {
                    Products::whereBetween('created_at', [$date_from, $date_to])->where(['provider_id'=> $id])->delete();
                }
            }else{
                foreach($providers_id as $key => $id) {
                    Products::where(['provider_id'=> $id])->delete();
                }
            }

            return back()->with('success','Товар выбранных поставщиков удален!');

        }else{
            return back()->with('error','Выберите хотя бы одного поставщика!');
        }

        
    }

    public function deleteProviderWithProducts(Request $request){

        if(!empty($request->post("providers_id"))){
            $providers_id = explode(",",$request->post("providers_id"));

            
            $period = $request->post("datetimes");

            if($period){
                    $period = explode(" - ",$period);
                    $date_from = $period[0];
                    $date_to = $period[1];
                
                foreach($providers_id as $key => $id) {
                    Products::whereBetween('created_at', [$date_from, $date_to])->where(['provider_id'=> $id])->delete();
                    Providers::where(['id'=> $id])->delete();
                }
            }else{
                foreach($providers_id as $key => $id) {
                    Products::where(['provider_id'=> $id])->delete();
                    Providers::where(['id'=> $id])->delete();
                }
            }

            return back()->with('success','Товар выбранных поставщиков удален!');

        }else{
            return back()->with('error','Выберите хотя бы одного поставщика!');
        }

    }
    
    public function move(Request $request){
        if(empty($request->post("providers_id")))  
            return back()->with('error','Выберите хотя бы одного поставщика!');

        if((int)$request->post("album_to") !== 0){

            $album_from = $request->post("album_from");
            $album_to = $request->post("album_to");
            $providers_id = explode(",",$request->post("providers_id"));
    
            foreach($providers_id as $key => $id) {

                if((int)$album_from !== 0){
                    Products::where(['provider_id'=> $id, 'category_id' => $album_from])->update(['category_id' => $album_to]);
                }else{
                    Products::where('provider_id', $id)->where('category_id','!=', 999)->where('category_id','!=', 8)->update(['category_id' => $album_to]);
                }
                
                //Providers::where('id', $id)->update(["extra_uah"=>$estimate_coast]);
            }

            return back()->with('success','Поставщик перемещен!');
        }else{
            return back()->with('error','Верите альбомы для перемещения!');
        }
        
    }

    public function saveDefaultExtra(Request $request){

        $default_extra = $request->post("default_extra");

        Settings::where('key', "providers_default_extra")->update(['value' => $default_extra]);

        return back()->with('success','Наценка установлена!');
    }

    public function reEstimate(Request $request){

        
        if(!empty($request->post("providers_id"))){
            $providers_id = explode(",",$request->post("providers_id"));

            
            $estimate_cost = $request->post("reestimate_cost");

            foreach($providers_id as $key => $id) {
                //Providers::where('id', $id)->update(["extra_uah"=>$estimate_cost]);
                $products = Products::where('provider_id', $id)->get();
                
                foreach($products as $product){ // Check old_price > 0
                    if($product->old_price > $product->price){
                        $product->price = $product->price+$estimate_cost;
                        $product->old_price = $product->old_price+$estimate_cost;
                    }else{
                        $product->price = $product->price+$estimate_cost;
                    }

                    $product->save();
                    //Products::where('provider_id', $id)->update(['price' => DB::raw('price + ' . $estimate_coast),'old_price' => DB::raw('old_price + ' . $estimate_coast) ]);
                }
                //
            }

            return back()->with('success','Товары выбранных поставщиков переоценены!');

        }else{
            return back()->with('error','Выберите хотя бы одного поставщика!');
        }

        
    }

    public function saveProviderExtra(Request $request){

        $provider_id =$request->post("provider_id");
        $extra_uah = $request->post("extra_uah");
        $provider_alias = $request->post("provider_alias");

        Providers::where('id', $provider_id)->update(["extra_uah"=>$extra_uah]);

        return back()->with('success','Наценка для поставщика '.$provider_alias.' успешно установлена!');
    }

    

}
