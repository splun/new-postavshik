<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Categories;
use Auth;

class CategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(Auth::user()->permission != "admin"){
            abort(404);
        }

        $data = Categories::get_all();
        $parent = Categories::get_parent_all();

        // try {

        //     $syncyouCategories = array_reduce(
        //         $this->getSyncYouCategories(),
        //         static function ($acc, $category) {
        //             $acc[$category->ID] = $category->Name;
        //             return $acc;
        //         },
        //         []
        //     );
        // } catch (\Exception $exception) {
        //     $syncyouCategories = [];
        // }   
        
        $syncyouCategories = [];

        return view('admin/categories/index',['data' => $data, 'parent' => $parent, 'syncyouCategories' =>$syncyouCategories]);
    }

    public function edit($id)
    {
        $data = Categories::get_one($id);
        $parent = Categories::get_parent_all();
        
        // try {

        //     $syncyouCategories = [];

        // } catch (\Exception $exception) {
        //     $syncyouCategories = [];
        // }
        $syncyouCategories = [];

        return view('admin/categories/edit',['data' => $data, 'parent' => $parent, 'syncyouCategories' =>$syncyouCategories]);
    }

    public function moveCategory(){

        $syncyouCategories = [];

        // try {
        //     /** @var ['ID' => 'Name'] */
        //     $syncyouCategories = array_reduce(
        //         $this->getSyncYouCategories(),
        //         static function ($acc, $category) {
        //             $acc[$category->ID] = $category->Name;
        //             return $acc;
        //         },
        //         []
        //     );
        //     $syncyouCategories = [];
        // } catch (\Exception $exception) {
        //     return back()->with('error','Неудалось получить данные от SyncYou.!');
        //     $syncyouCategories = [];
        // }
                
    }

    public function create(Request $request)
    {
        $name = $request->input('name');
        $parent_id = $request->input('parent_id');
        $syncyou_cat = $request->input('syncyou_cat');
        $disposition = $request->input('disposition');
        $data = (object) [
            'name' => $name,
            'parent_id' => $parent_id,
            'disposition' => $disposition,
            'syncyou_cat'=>($syncyou_cat) ? $syncyou_cat : 0
        ];

        $result = Categories::create($data);

        return redirect("admin/categories")->with('success','Категория '.$name.' создана!');
    }

    public function savecat(Request $request) {

        $category_id = $request->input('category_id');
        $name = $request->input('name');
        $parent_id = $request->input('parent_id');
        $syncyou_cat = $request->input('syncyou_cat');
        $disposition = $request->input('disposition');

        $data = (object) [
            'id' => $category_id,
            'name' => $name,
            'disposition' => $disposition,
            'parent_id' => $parent_id,
            'syncyou_cat'=>($syncyou_cat) ? $syncyou_cat : 0
        ];

        $result = Categories::edit($data);
        
        return redirect("admin/categories")->with('success','Категория '.$name.' изменена!');
    }

    public function remove($id)
    {
        $result = Categories::destroy($id);
        return redirect("admin/categories")->with('success','Категория с ID '.$id.' удалена!');
    }

    private function getSyncYouCategories()
    {
        $curl = curl_init();
        curl_setopt_array(
            $curl,
            [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL            => "http://79.132.138.40/get_categories",
                CURLOPT_CONNECTTIMEOUT => 10,
                CURLOPT_TIMEOUT => 10
            ]
        );

        $response = curl_exec($curl);
        curl_close($curl);

        $json = json_decode($response, false);

        return json_decode($json->result, false);
    }
}
