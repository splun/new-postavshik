<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Currency;
use App\Models\Settings;
use App\Models\Orders;
use App\Models\Products;
use App\Models\Payment;

use Validator;
use Storage;
use Auth;
use Hash;
use Carbon\Carbon;

class OrdersController extends Controller
{

        /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        if(Auth::user()->permission != "admin"){
            abort(404);
        }

        $ordersQuery = Orders::query();

        if($request->order_status || $request->order_date_range){

            $data["orders"] = $ordersQuery->where("status", $request->order_status);

            if($request->order_date_range){
                $range = explode(" - ", $request->order_date_range);
                $dateS = new Carbon($range[0]);
                $dateE = new Carbon($range[1]);
                $ordersQuery->whereBetween('created_at', [$dateS->format('Y-m-d')." 00:00:00", $dateE->format('Y-m-d')." 23:59:59"]);
            }
            
        }else{
            $ordersQuery->where("status","Оплачен");
        }

        $data["orders"] = $ordersQuery->orderBy("created_at")->get();

        return view('admin/orders/index')->with($data);
    }

    public function view($id=0)
    {

        if(Auth::user()->permission != "admin"){
            abort(404);
        }

        $data["order"] = Orders::find($id);

        if(isset($_GET['check-mono-status']) && !empty($data["order"]->transaction_id)){

            echo "updated";

            dd($this->check_mono_status($data["order"]));
        }

        return view('admin/orders/view')->with($data);
    }

    public function status_update(Request $request)
    {

        if(Auth::user()->permission != "admin"){
            abort(404);
        }

        $order = Orders::find($request->order);

        $order["status"] = $request->order_status;

        $order->save();

        return redirect()->back();
    }

    private function check_mono_status($order)
    {

        $invoiceId = $order->transaction_id;

        $paymentStatus = Payment::checkMonoInvoiceStatus($invoiceId);

        if($paymentStatus['status'] == "success" || $paymentStatus['status'] == "processing")
        {
            $order->status = "Оплачен";
            
            $order->payment_id = $paymentStatus['status'].'-'.$paymentStatus['reference'];

            $order->save();
        }

        return $paymentStatus;

    }

    

}