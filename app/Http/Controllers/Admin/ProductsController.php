<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Providers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use App\Models\Products;
use App\Models\Options;
use App\Models\Product_variants;
use App\Models\Categories;
use App\Services\KeyCrm\Facades\KeyCrm;

use Validator;
use Storage;
use Auth;
use Hash;


class ProductsController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index(Request $request)
	{
		if (Auth::user()->permission != "admin") {
			abort(404);
		}
		$data['category_id'] = 999;
		$data['provider'] = "";
		$data['selected_all'] = ($request->get("selected_all")) ? 1 : 0;

		if ($request->get("category_id")) {
			$category_id = (int)$request->get("category_id");
			$data['category_id'] = $category_id;
		}
		if ($request->get("provider")) {
			$provider = $request->get("provider");
			$data['provider'] = $provider;
		} else {
			$provider = "";
		}

		$data['categories_w_p'] = Categories::get_w_parent();
		$data['providers'] = Providers::orderByRaw("alias-0")->get();

		$productsQuery = Products::query();
		$productsQuery->select('products.*', 'products_images.url', 'products_images.base64');

		if ($request->get("not_removed")) {
			$productsQuery->where('not_removed', 1);
		} else {
			$productsQuery->where(["category_id" => $data['category_id']]);
		}

		if (!empty($provider)) {
			$productsQuery->leftJoin('providers', 'providers.id', '=', 'products.provider_id')
				->where("providers.alias", $provider);
		}

		$productsQuery->leftJoin('products_images', function ($join) {
			$join->on('products.id', '=', 'products_images.product_id')
				->where('products_images.preview', '=', 1);
		});

		$productsQuery->orderBy("created_at", "desc");
		$data['results'] = $productsQuery->count();
		$data['products'] = $productsQuery->paginate(25);
		$data['page'] = $request->page;
		$data['options'] = Options::all();

		return view('admin/products/index')->with($data);
	}

	public function getProducts(Request $request)
	{

		## Read value
		$draw = $request->get('draw');
		$start = $request->get("start");
		$rowperpage = $request->get("length"); // Rows display per page

		$columnIndex_arr = $request->get('order');
		$columnName_arr = $request->get('columns');
		$order_arr = $request->get('order');
		$search_arr = $request->get('search');

		$columnIndex = $columnIndex_arr[0]['column']; // Column index
		$columnName = $columnName_arr[$columnIndex]['data']; // Column name
		$columnSortOrder = $order_arr[0]['dir']; // asc or desc
		$searchValue = $search_arr['value']; // Search value

		// Total records
		$totalRecords = Products::select('count(*) as allcount')->count();
		$totalRecordswithFilter = Products::select('count(*) as allcount')->where('text', 'like', '%' . $searchValue . '%')->count();

		// Fetch records
		$records = Products::orderBy($columnName, $columnSortOrder)
			->where('products.text', 'like', '%' . $searchValue . '%')
			->where(["category_id" => $data['category_id']])
			->leftJoin('products_images', function ($join) {
				$join->on('products.id', '=', 'products_images.product_id')
					->where('products_images.preview', '=', 1);
			})
			->select('products.*')
			->skip($start)
			->take($rowperpage)
			->get();

		$data_arr = array();

		foreach ($records as $record) {

			$data_arr[] = array(
				'id' => $record->id,
				'preview' => $record->preview,
				'provider' => $record->provider->alias,
				'category' => $record->category->name,
				'stock_number' => $record->stock_number,
				'price' => $record->price,
				'old_price' => $record->old_price,
				'created_at' => $record->created_at,
			);
		}

		$response = array(
			"draw" => intval($draw),
			"iTotalRecords" => $totalRecords,
			"iTotalDisplayRecords" => $totalRecordswithFilter,
			"aaData" => $data_arr
		);

		return response()->json($response);
	}


	public function add(Request $request)
	{

		$provider_id = $request->post('provider_id');
		$category_id = $request->post('category_id');
		$text = $request->post('text');
		$description = $request->post('description');
		$stock_number = $request->post('stock_number');
		$price = $request->post('price');
		$sale = $request->post('old_price');
		$not_removed = ($request->post('not_removed')) ? 1 : 0;

		$product_preview = $request->file("product_preview");
		$product_images = $request->file("product_images");

		$colors = $request->post('option_color');
		$sizes = $request->post('option_size');

		if (!empty($provider_id) && !empty($category_id) && !empty($text) && !empty($stock_number) && !empty($price)) {

			$product_id = Products::create([
				'provider_id' => $provider_id,
				'category_id' => $category_id,
				'text' => $text,
				'description' => $description,
				'stock_number' => $stock_number,
				'price' => $price,
				'old_price' => $sale,
				'not_removed' => $not_removed
			])->id;

			if ($sizes) {
				foreach ($sizes as $size) {
					Product_variants::create([
						'option_id' => $size,
						'product_id' => $product_id,
					]);
				}
			}

			if ($colors) {
				foreach ($colors as $color) {
					Product_variants::create([
						'option_id' => $color,
						'product_id' => $product_id,
					]);
				}
			}

			if ($request->post('new_option_sizes')) {

				$lines = explode("\r\n", $request->post('new_option_sizes'));

				foreach ($lines as $line) {

					if (!$line) {
						return back()->with('error', 'Не удаеться распарсить эту срань! Введи данные корректно');
					}

					$query = array(
						"option_name" => "size",
						"option_value" => $line
					);

					$new_option = Options::create($query);
					Product_variants::create([
						'option_id' => $new_option->id,
						'product_id' => $product_id,
					]);
				}
			}

			if ($request->post('new_option_colors')) {
				$lines = explode("\r\n", $request->post('new_option_colors'));

				foreach ($lines as $line) {

					if (!$line) {
						return back()->with('error', 'Не удаеться распарсить эту срань! Введи данные корректно');
					}

					$query = array(
						"option_name" => "color",
						"option_value" => $line
					);
					$new_option = Options::create($query);
					Product_variants::create([
						'option_id' => $new_option->id,
						'product_id' => $product_id,
					]);
				}
			}

			//$preview_url = Storage::disk('public')->put('product_images', $product_preview);
			// if (!empty($product_preview)) {
			// 	$save_preview_img = Products::upploadImages($product_preview);
			// 	$preview_url = $save_preview_img['data']['url'];
			// }else{
			// 	$preview_url = "";
			// }

			// Products::addImages([
			// 	'product_id' => $product_id,
			// 	'url' => $preview_url,
			// 	'preview' => 1
			// ]);

			if (is_array($product_images)) {
				$i = 1;
				foreach ($product_images as $additional) {
					$additional_img = Storage::disk('public')->put('product_images', $additional);
					//$save_additional_img = Products::upploadImages($additional);
					//$additional_img = $save_additional_img['data']['url'];

					Products::addImages([
						'product_id' => $product_id,
						'url' => "/storage/" . $additional_img,
						'preview' => ($i == 1) ? 1 : 0
					]);

					$i++;
				}
			} else {
				// if (!empty($product_images)) {
				// 	$save_additional_img = Products::upploadImages($product_images);
				// 	$additional_img = $save_preview_img['data']['url'];
				// }else{
				// 	$additional_img = "";
				// }

				$additional_img = Storage::disk('public')->put('product_images', $product_images);
				Products::addImages([
					'product_id' => $product_id,
					'url' => "/storage/" . $additional_img,
					'preview' => 1
				]);
			}
			return back()->with('success', 'Товар успешно создан!');
		} else {
			return back()->with('success', 'Ошибка! Проверте данные товара!');
		}
	}

	public function edit($id, Request $request)
	{

		if (Auth::user()->permission != "admin" && Auth::user()->permission != "operator") {
			abort(404);
		}

		$data['categories_w_p'] = Categories::get_w_parent();
		$data['providers'] = Providers::orderByRaw("alias-0")->get();
		$data['product'] = Products::find($id);
		$data['images'] = Products::get_images($id);
		$data['options'] = Options::all();

		$data['variants'] = array();
		$variants = $data['product']->variants;
		foreach ($variants as $variant) {
			array_push($data['variants'], $variant->option_id);
		}

		if (Auth::user()->permission == "operator" || $request->get("return_url")) {
			$data["return_url"] = $request->get("return_url");
			return view('admin/products/quick-edit')->with($data);
		} else {

			return view('admin/products/edit')->with($data);
		}
	}

	public function remove($id, Request $request)
	{

		$return = Products::delete_product($id);

		if ($request->get("return_url")) {
			return redirect($request->get("return_url"));
		} else {
			return back()->withInput(); //->with('success','Товар успешно удален!');
		}
	}

	public function product_save(Request $request)
	{
		$product_id = $request->post('product_id');
		$image_id = $request->post('remove_image');
		$products = []; // For Key CRM upload
		$currentProduct = Products::find($product_id);

		$request->validate([
			'product_preview' => 'nullable|file|max:20000',
			'product_images.*' => 'nullable|file|max:20000',
		], [
			'product_preview.file' => 'Обложка должен быть файлом.',
			'product_images.*.file' => 'Обложка должны быть файлами.',
		]);

		if ($image_id) {
			Products::unset_image($image_id);
			return array('status' => 'success', 'id' => $image_id);
		}

		if ($request->post('cover_image')) {
			Products::cover_image($request->post('cover_image'), $product_id);
			return array('status' => 'success', 'id' => $request->post('cover_image'));
		} else {
			$not_removed = ($request->post('not_removed')) ? 1 : 0;
			$provider_id = $request->post('provider_id');
			$category_id = $request->post('category_id');
			$text = $request->post('text');
			$description = $request->post('description');
			$stock_number = $request->post('stock_number');
			$price = $request->post('price');
			$sale = $request->post('old_price');
			$product_preview = $request->file("product_preview");
			$product_images = $request->file("product_images");
			$colors = $request->post('option_color');
			$sizes = $request->post('option_size');

			if (!empty($provider_id) && !empty($category_id) && !empty($text) && !empty($stock_number) && !empty($price)) {
				Products::where('id', $product_id)->update([
					'provider_id' => $provider_id,
					'category_id' => $category_id,
					'text' => $text,
					'description' => $description,
					'stock_number' => $stock_number,
					'price' => $price,
					'old_price' => $sale ?? NULL,
					'not_removed' => $not_removed
				]);

				Product_variants::where("product_id", $product_id)->delete();

				if ($sizes) {
					foreach ($sizes as $size) {
						Product_variants::create([
							'option_id' => $size,
							'product_id' => $product_id,
						]);
					}
				}

				if ($colors) {
					foreach ($colors as $color) {
						Product_variants::create([
							'option_id' => $color,
							'product_id' => $product_id,
						]);
					}
				}

				if ($request->post('new_option_sizes')) {
					$lines = explode("\r\n", $request->post('new_option_sizes'));
					foreach ($lines as $line) {
						if (!$line) {
							return back()->with('error', 'Не удается распарсить данные! Введите данные корректно.');
						}
						$query = array(
							"option_name" => "size",
							"option_value" => $line
						);
						$new_option = Options::create($query);
						Product_variants::create([
							'option_id' => $new_option->id,
							'product_id' => $product_id,
						]);
					}
				}

				if ($request->post('new_option_colors')) {
					$lines = explode("\r\n", $request->post('new_option_colors'));
					foreach ($lines as $line) {
						if (!$line) {
							return back()->with('error', 'Не удается распарсить данные! Введите данные корректно.');
						}
						$query = array(
							"option_name" => "color",
							"option_value" => $line
						);
						$new_option = Options::create($query);
						Product_variants::create([
							'option_id' => $new_option->id,
							'product_id' => $product_id,
						]);
					}
				}

				if ($product_images) {
					if (is_array($product_images)) {
						foreach ($product_images as $additional) {
							// Проверяем MIME-тип и сохраняем файл в зависимости от типа
							$mimeType = $additional->getMimeType();
							if (str_starts_with($mimeType, 'image/')) {
								$additional_img = Storage::disk('public')->put('product_images', $additional);
							} elseif ($mimeType === 'video/mp4') {
								$additional_img = Storage::disk('public')->put('product_videos', $additional);
							} else {
								continue; // Пропускаем неподдерживаемые файлы
							}

							if ($additional_img) {
								Products::addImages([
									'product_id' => $product_id,
									'url' => "/storage/" . $additional_img,
									'preview' => 0
								]);
							}
						}
					} else {
						$mimeType = $product_images->getMimeType();
						if (str_starts_with($mimeType, 'image/')) {
							$additional_img = Storage::disk('public')->put('product_images', $product_images);
						} elseif ($mimeType === 'video/mp4') {
							$additional_img = Storage::disk('public')->put('product_videos', $product_images);
						} else {
							$additional_img = null; // Не поддерживаемый тип файла
						}

						if ($additional_img) {
							Products::addImages([
								'product_id' => $product_id,
								'url' => "/storage/" . $additional_img,
								'preview' => 0
							]);
						}
					}
				}

				$updatedProduct = Products::find($product_id);
				if ((int)$currentProduct->category_id == 999 && (int)$category_id != 999 && $updatedProduct->provider->alias == "333") {
					$products[] = $updatedProduct;
					KeyCrm::export($products);
				}
			}
			return redirect($request->back_url);
		}
	}

	public function deleteOneProduct(Request $request)
	{
		if (!empty($request->post("products_id"))) {
			$products_id = explode(",", $request->post("products_id"));
			foreach ($products_id as $key => $id) {
				Products::where(['id' => $id])->delete();
			}
		}

		return back(); //->with('success','Выбранные товары удалены!');
	}

	public function delete(Request $request)
	{
		if (!empty($request->post("products_id"))) {
			$products_id = explode(",", $request->post("products_id"));
			foreach ($products_id as $key => $id) {
				Products::where(['id' => $id])->delete();
			}
		}

		return back(); //->with('success','Выбранные товары удалены!');
	}

	public function moveproduct(Request $request)
	{

		if (!empty($request->post("products_id") && !empty($request->post('category_id')))) {
			$products_id = explode(",", $request->post("products_id"));
			$newCategoryId = $request->post('category_id');
			$products = [];

			foreach ($products_id as $key => $id) {
				$product = Products::find($id);
				$current_category_id = $product->category_id;
				$product->update(['category_id' => $newCategoryId]);

				if ($product->provider->alias == "333" && $current_category_id == 999 && $newCategoryId != "999") {
					$products[] = $product;
				}
			}

			KeyCrm::export($products);
		}

		return back()->with('success', 'Товары перемещены!');
	}


	public function options()
	{

		if (Auth::user()->permission != "admin") {
			abort(404);
		}

		$data = Options::all();

		return view('admin/options/index', ['data' => $data]);
	}

	public function add_option(Request $request)
	{

		if (Auth::user()->permission != "admin") {
			abort(404);
		}

		$query = null;

		if (isset($request->options_list) && isset($request->option_name)) {

			$lines = explode("\r\n", $request->options_list);

			foreach ($lines as $line) {

				//$line = preg_replace('/[^ a-zа-яё\d]/ui', '',$line );

				if (!$line) {
					return back()->with('error', 'Не удаеться распарсить эту срань! Введи данные корректно');
				}

				$query = array(
					"option_name" => $request->option_name,
					"option_value" => $line
				);

				Options::create($query);
			}
		}

		return back(); //->with('success','Добавлены');
	}

	public function edit_option($id)
	{
		if (Auth::user()->permission != "admin") {
			abort(404);
		}

		$data = Options::where("id", $id)->first();

		return view('admin/options/edit', ['data' => $data]);
	}

	public function save_option(Request $request)
	{

		if (Auth::user()->permission != "admin") {
			abort(404);
		}

		$option_id = $request->input('option_id');
		$option_name = $request->input('option_name');
		$option_value = $request->input('option_value');

		if ($option_id && $option_name && $option_value) {

			$option = Options::find($option_id);
			$option->option_name = $option_name;
			$option->option_value = $option_value;

			$option->save();

			return redirect("admin/products_options")->with('success', 'Опция ' . $option_name . ' изменена!');
		} else {
			return redirect("admin/products_options")->with('success', 'Произошла ошибка!');
		}
	}

	public function delete_option($id)
	{

		if (Auth::user()->permission != "admin") {
			abort(404);
		}

		Product_variants::where("option_id", $id)->delete();
		$result = Options::destroy($id);

		return redirect("admin/products_options")->with('success', 'Опция с ID ' . $id . ' удалена!');
	}
}
