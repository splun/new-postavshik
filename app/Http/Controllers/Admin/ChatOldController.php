<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Chat;
use App\Models\User;
use Validator;
use Auth;
use DB;
class ChatController extends Controller
{
    public function __constuct(){

        $user = Auth::user();

        if($user->permission=="user"){
            return abort(404);
        }
    }

    public function index(Request $request){

        $user = Auth::user();

        $chatlist = [];

        $chats = Chat::groupBy('from_user_id')
                ->where("to_user_id",$user->id)
                ->orderBy("datetime","DESC")
                ->paginate(500);

        if(!$chats) return abort(404);

        $data["chats"] = $chats;

        if($request->get("customer")){
            $data['active_chat'] = $request->get("customer");
            $data["history"] = $this->history((int)$request->get("customer"));
        }else{
            $data['active_chat'] = $chats[0]->from_user_id;
            $data["history"] = $this->history($chats[0]->from_user_id);
        }

        return view("admin/chat/main")->with($data);
    }

    public function loadchats(Request $request){

        $user = Auth::user();

        $chats = Chat::groupBy('from_user_id')
                ->where("to_user_id",$user->id)
                ->orderBy("datetime","DESC")
                ->paginate(500);
        
        if(!$chats) return abort(404);

        $data["chats"] = $chats;

        if($request->post("customer")){
            $data['active_chat'] = $request->post("customer");
        }else{
            $data['active_chat'] = $chats[0]->from_user_id;
        }
        return response()->json(view('userlist', compact($data))->render());

    }

    public function loadhistory(Request $request){

       $start = $request->post("start");
       $user_from = $request->post("userFrom");
       
       $history = $this->history($user_from,$start);
    
       return $history;

    }

    public function history($uid,$start=0){

        $user = Auth::user();

        $html = "";

        $messages = Chat::where([
            ['from_user_id', '=', $uid],
            ['to_user_id', '=', $user->id]
        ])
        ->orWhere([
            ['to_user_id', '=', $uid],
            ['from_user_id', '=', $user->id]
        ])
		->orderBy('datetime','DESC')
        ->skip($start)
		->take(20)->get();

        $messages = $messages->reverse();

        foreach($messages as $req){

            if($req->user && $req->user->permission == "operator"){
                $username = "Вы";
                $avatar = "";
            }else{
                $username = ($req->user["firstname"] || $req->user["lastname"]) ? $req->user["firstname"]." ".$req->user["lastname"] : $req->user["name"];
                $avatar = $req->user["icon"];
            }

            $data = array(
                "from_user_id"=>$req->from_user_id,
                "avatar"=>$avatar,
                "name"=>$username,
                "text"=>$req->message,
                "time"=>$req->datetime
            );

            $html .= $this->buildMessage($data);

        }
        
        //dd($html);

        return $html;

    }

    public function newMessage(Request $request){

        $user = Auth::user();
        
        $req = $request->all();

        $data = array(
            "from_user_id"=>$req["msg_data"]["from_user_id"],
            "to_user_id"=>$req["msg_data"]["to_user_id"],
            "avatar"=>$req["msg_data"]["avatar"],
            "name"=>$req["msg_data"]["name"],
            "text"=>$req["msg_data"]["text"],
            "time"=>$req["msg_data"]["time"]
        );

        if($data && 
          ($data["to_user_id"] == $req["active_chat"] && $data["from_user_id"] == $user->id) || 
          ($data["to_user_id"] == $user->id && $data["from_user_id"] ==  $req["active_chat"])){

            return $this->buildMessage($data);
            
        }
    }

    function buildMessage($data){

        $user = Auth::user();

        if($data["avatar"] ){
            $avatar = $data["avatar"];
        }else{
            $avatar = '/images/review_image.jpg';
        }

       
        if ($data["from_user_id"] == $user->id){
        
            $html = '<div class="outgoing_msg">
                <div class="sent_msg">
                <p><b>'.$data["name"].'</b></p>
                <p>'.$data["text"].'</p>
                <p class="chat_date">'.$data["time"].'</p>
                </div>
            </div>';
        }else{

            $html = '<div class="incoming_msg">
            <div class="incoming_msg_img"> <img src="'.$avatar.'"> </div>
                <div class="received_msg">
                    <div class="received_withd_msg">
                        <p><b>'.$data["name"].'</b></p>
                        <p>'.$data["text"].'</p>
                        <p class="chat_date">'.$data["time"].'</p>
                    </div>
                </div>
            </div>';
        }

        return $html;
    }
    
    public function push(Request $request){
        
        $user = Auth::user();
        
        
        if( !$request->post("user") ) return;

		$validator = Validator::make(request()->all(), [
			'text' => 'required',
            'files.*' => 'file|max:3000|mimes:png,jpg,jpeg'
		 ]);

		if ($validator->fails()) {
            echo "Error:".$validator->errors()->all();
		}
		
        
        $data = array( 
            "from_user_id" => $user->id,
            "to_user_id"=>$request->post("user"),
            "room_id"=>0, 
            "message"  => $request->post("text"),
            "datetime"  => date("Y-m-d H:i:s"), 
            "is_read" => 0
        );

        $message = Chat::create($data);

        if($message){

            if($request->file("files")){
                //foreach($request->file("files") as $key => $image){
                    $image->store('chat/files/'.$message->id.'','public');
                //}
            }

            $host = "app.comet-server.ru";
            $commentUser = "15";
            $password = "lPXBFPqNg3f661JcegBY0N0dPXqUBdHXqj2cHf04PZgLHxT6z55e20ozojvMRvB8";

            $comet = mysqli_connect($host, $commentUser, $password, "CometQL_v1");

            if(mysqli_errno($comet))
            {
                echo "Error:".mysqli_error($link);
            }

            
            $msg = Array(
                "message_id"=>$message->id, 
                "from_user_id" => $user->id,
                "to_user_id"=>$request->post("user"),
                "name" => $user->email, 
                "text"  => $request->post("text"),
                "avatar" => $user->icon,
                "time"  => date("d.m.y H:i"), 
            );

            $msg = json_encode($msg);
            $msg = mysqli_real_escape_string($comet, $msg);

            $query = "INSERT INTO pipes_messages (name, event, message)" .
            "VALUES('simplechat', 'newMessage', '".$msg."')";
            
            mysqli_query($comet, $query); 

            if(mysqli_errno($comet))
            {
                echo "Error:".mysqli_error($comet);
            } 
            else
            {
                echo "ok";
            }
        }else{
            echo "Возникла ошибка при отправке сообщения";
        }
    }
}
