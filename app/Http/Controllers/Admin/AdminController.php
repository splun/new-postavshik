<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Currency;
use App\Models\Settings;
use App\Models\User;
use App\Models\Products;

use Validator;
use Storage;
use Auth;
use Hash;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        if(Auth::user()->permission != "admin"){
            abort(404);
        }

        $currency = Currency::all();

        if($currency){
            $data["usd"] = $currency[1]["exchange_rate"]/$currency[0]["exchange_rate"];
            //$data["rub"] = $currency[2]["exchange_rate"];
        }

        $data["off_rate"] = 0;

        $data["nbu_rate"] = config('settings.nbu_rate');
        
        // $bank_rate = file_get_contents("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&json");
        
        // $usd_decode = json_decode($bank_rate);
        // if($usd_decode && $usd_decode[0]){
        //     $data["nbu_rate"] = $usd_decode[0]->rate;
        // }
        
        $data["popular_rate"] = config('settings.popular_rate');
        $data["banner_image_url"] = config('settings.default_banner_url');
        $data["favicon_image_url"] = config('settings.favicon_image_url');
        $data["search_from_photo"] = config('settings.search_from_photo');
        $data["use_nbu_currency"] = config('settings.use_nbu_currency');
        $data["amount_prepayment"] = config('settings.amount_prepayment');

        $data["total_products"] = Products::where("isDeleted","!=", 1)->count();

        return view('admin/dashboard')->with($data);
    }

    public function setExchangeRate(Request $request)
    {
        
        $use_nbu_currency = $request->post("use_nbu_currency");
        $usd_rate = $request->post("usd_rate");
        $rub_rate = $request->post("rub_rate");

        Currency::setRate("USD", 1/$usd_rate);

        Currency::setRate("RUB", $rub_rate);

        $use_nbu_currency = ($use_nbu_currency == "on") ? 1 : 0;

        Settings::where('key', "use_nbu_currency")->update(['value' => $use_nbu_currency]);

        return back()->with('success','Курсы успешно установлены!');
    }
    
    public function setPopularRate(Request $request)
    {
        $popular_rate = $request->post("popular_rate");

        Settings::where('key', "popular_rate")->update(['value' => $popular_rate]);

        return back()->with('success','Максимальный рейтинг популярности установлен!');
    }

    public function setPrepaymentRate(Request $request)
    {
        $prepayment_rate = $request->post("prepayment_rate");

        Settings::where('key', "amount_prepayment")->update(['value' => $prepayment_rate]);

        return back()->with('success','Предоплата для наложки установлена');
    }
    public function setSearchFromPhoto(Request $request)
    {
        $search_from_photo = $request->post("search_from_photo");

        $search_from_photo = ($search_from_photo == "on") ? 1 : 0;

        Settings::where('key', "search_from_photo")->update(['value' => $search_from_photo]);

        return back()->with('success', ($search_from_photo == 1) ? 'Поиск по фото включен!' : 'Поиск по фото отключен!');
    }

    public function addDefaultBanner(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'default_banner' => 'file|max:5000|mimes:png,jpg,jpeg,webp'
        ]);

        if ($validator->fails()) {
			return back()->with('success','Баннер не должен быть не более 5мб и иметь расширение png,jpg,jpeg,webp!');
		}

        $file = $request->file("default_banner");

        //$banner_url = $default_banner->store('banners/'.$default_banner.'','public');

        $banner_url = Storage::disk('public')->put('banners',$file);

        Settings::where('key', "default_banner_url")->update(['value' => "/storage/".$banner_url]);

        return back()->with('success','Баннер успешно установлен!');

        
    }

    public function editFavicon(Request $request)
    {
        $validator = Validator::make(request()->all(), [
            'favicon' => 'file|max:1000|mimes:png,ico'
        ]);

        if ($validator->fails()) {
			return back()->with('success','Favicon не должен быть не более 1мб и иметь расширение png,ico!');
		}

        $file = $request->file("favicon");

        $favicon_url = Storage::disk('public')->put('/',$file);

        Settings::where('key', "favicon_image_url")->update(['value' => "/storage/".$favicon_url]);

        return back()->with('success','Favicon успешно установлен!');
    }

    public function editPswd(Request $request)
    {
        return view('admin/password');
    }

    public function setPswd(Request $request)
    {
        $rules = array(
            'password'         => 'required',
            'password_confirm' => 'required|same:password' 
        );
    
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with('error','Не введен парроль либо пароли не совпадают!');
        }

        $password = $request->post("password");
        $user_id =  Auth::user()->id;
    
        $user = User::find($user_id)->first();
        $user->password = Hash::make($password);
        $user->save();

        return back()->with('success','Пароль успешно установлен!');
    }

    public function settings($key)
    {
  
    }
    
}
