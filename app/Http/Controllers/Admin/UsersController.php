<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Users;
use Auth;

class UsersController extends Controller
{
    public function index()
    {
         if(Auth::user()->permission != "admin"){
            abort(404);
        }
        $users = Users::get_all();
        return view('admin/users/index',['users' => $users]);
    }
    public function managers()
    {
        $users = Users::get_all('operator');
        return view('admin/users/managers',['users' => $users]);
    }
    public function edit($id)
    {
        $user = Users::get_user($id);
        return view('admin/users/edit',['data' => $user]);
    }

    public function saveuser(Request $request)
    {

        $user_id = $request->input('user_id');
        $firstname = $request->input('firstname');
        $lastname = $request->input('lastname');
        $email = $request->input('email');
        $password = $request->input('password');
        $location = $request->input('location');

        $data = (object) [
            "id" => $user_id,
            'firstname' => $firstname,
            'lastname' => $lastname,
            'email' => $email,
            'password' => $password,
            'location' => $location,
        ];

        $result = Users::edit($data);

        return back()->with('success','Пользователь с ID '.$user_id.' успешно изменен!');
    }
    public function opcreate(Request $request)
    {
        $email = $request->input('email');
        $name = $request->input('name');
        $password = $request->input('password');
        $location = $request->input('location');

        $data = (object) [
            'email' => $email,
            'name' => $name,
            'password' => $password,
            'location' => $location,
        ];

        $result = Users::opcreate($data);

        return back()->with('success','Оператор '.$name.' успешно создан!');
    }
    public function lock($id)
    {
        Users::lock($id);
        return back()->with('success','Пользователь с '.$id.' заблокирован!');
    }
    public function unlock($id)
    {
        Users::unlock($id);
        return back()->with('success','Пользователь с '.$id.' разблокирован!');
    }

    public function remove($id)
    {
        Users::remove($id);
        return back()->with('success','Пользователь с '.$id.' удален!');
    }
}
