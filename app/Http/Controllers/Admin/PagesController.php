<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pages as Pages;
use App\Models\Tracks as Tracks;
use Auth;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
         if(Auth::user()->permission != "admin"){
            abort(404);
        }
        $pages = Pages::allpages();
        return view('admin/pages/index',['pages' => $pages]);
    }

    public function edit($id)
    {
        $result = ( new \App\Models\Pages )->getPage($id);

        return view('admin/pages/edit', ['data' => $result]);

    }

    public function create()
    {
        return view('admin/pages/add');
    }

    public function add(Request $request) {

        $title = $request->input('title');
        $slug = $request->input('slug');
        $meta_description = $request->input('meta_description');
        $content = $request->input('content');

        $data = (object) [
            'title' => $title,
            'slug' => $slug,
            'meta_description' => $meta_description,
            'content' => $content
        ];

        $result = ( new \App\Models\Pages )->create($data);

        return redirect()->route('pages')
                         ->with('success','Страница с ID '.$request->id.' успешно создана!');
    }

    public function savepage(Request $request)
    {

        $page_id = $request->input('page_id');
        $title = $request->input('title');
        $slug = $request->input('slug');
        $meta_description = $request->input('meta_description');
        $content = $request->input('content');

        $data = (object) [
            "id" => $page_id,
            'title' => $title,
            'slug' => $slug,
            'meta_description' => $meta_description,
            'content' => $content
        ];

        $result = ( new \App\Models\Pages )->savePage($data);

        return redirect()->route('pages')
                         ->with('success','Страница с ID '.$page_id.' успешно изменена!');
    }

    public function remove($id)
    {
        $result = ( new \App\Models\Pages )->remove($id);
        return redirect()->route('pages')
                        ->with('success','Страница с ID '.$id.' удалена!');
    }

    public function tracks(){
        
        if(Auth::user()->permission != "admin"){
            abort(404);
        }

        $tracks = Tracks::all();

        return view('admin/pages/tracks',['tracks' => $tracks]);

    }

    public function remove_track($id){

        if(Auth::user()->permission != "admin"){
            abort(404);
        }

        $result = Tracks::where('id',$id)->delete();

        return redirect()->route('tracks');

    }

    public function add_track(Request $request){

        if(Auth::user()->permission != "admin"){
            abort(404);
        }   

        $query = null;

        if(isset($request->np_number)){

            $lines = explode("\r\n",$request->np_number);

            foreach($lines as $line){
                $line = preg_replace('/[^ a-zа-яё\d]/ui', '',$line );

                $fields = explode(" ",$line);

                if(count($fields) != 3) {
                    return back()->with('error','Не удаеться распарсить эту срань! Введи данные корректно');
                }

                $query = array(
                    "created_at"=>$request->date,
                    "number"=>$fields[0],
                    "name"=>$fields[1]." ".$fields[2],
                    "carrier"=>"NP"
                );

                Tracks::create($query);
                
            }

        }

        if(isset($request->up_number)){

            $lines = explode("\r\n",$request->up_number);

            foreach($lines as $line){
                $line = preg_replace('/[^ a-zа-яё\d]/ui', '',$line );

                $fields = explode(" ",$line);

                if(count($fields) != 3) {
                    return back()->with('error','Не удаеться распарсить эту срань! Введи данные корректно');
                }

                $query = array(
                    "created_at"=>$request->date,
                    "number"=>$fields[0],
                    "name"=>$fields[1]." ".$fields[2],
                    "carrier"=>"UP"
                );

                Tracks::create($query);
                
            }



        }


        return back(); //->with('success','Добавлен'); 
    }
    
}
