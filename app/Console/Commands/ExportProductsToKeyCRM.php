<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Products;
use App\Services\KeyCrm\KeyCrmService;

class ExportProductsToKeyCRM extends Command
{
    protected $signature = 'sync:products-keycrm';
    protected $description = 'Выгрузка товаров в KeyCRM';

    public function handle(KeyCrmService $keyCrmService)
    {
        $this->info('Начало синхронизации товаров с KeyCRM...');
        
        $products = Products::where('provider_id', 7667)
            ->where('category_id', '!=', 999)
            ->with(['images', 'provider'])
            //->limit(1000)
            ->get();

        $preparedProducts = $keyCrmService->prepareProductsData($products);

        $chunks = array_chunk($preparedProducts, 100);

        if($preparedProducts){
            foreach ($chunks as $index => $chunk) {
                $result = $keyCrmService->createProducts($chunk);
                if ($result) {
                    $this->info('Успешно синхронизирована часть ' . ($index + 1) . ' (' . count($chunk) . ' товаров).');
                } else {
                    $this->error('Ошибка при синхронизации части ' . ($index + 1) . '.');
                }
            }
        }

        $this->info('Синхронизация завершена.');
    }

}