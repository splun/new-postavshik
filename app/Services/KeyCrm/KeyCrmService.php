<?php

namespace App\Services\KeyCrm;

use Illuminate\Support\Facades\Http;
use App\Models\Products;
use Illuminate\Support\Facades\Log;

class KeyCrmService
{
    protected $baseUrl;
    protected $apiKey;

    public function __construct()
    {
        $this->baseUrl = config('keycrm.base_url');
        $this->apiKey = config('keycrm.api_key');
    }

    public function export($data): void
    {
        if (!$data) return;

        $preparedProducts = $this->prepareProductsData($data);

        $chunks = array_chunk($preparedProducts, 100);

        if ($preparedProducts) {
            foreach ($chunks as $index => $chunk) {
                $this->createProducts($chunk);
            }
        }
    }

    public function prepareProductsData($products): array
    {
        $preparedProducts = [];

        foreach ($products as $product) {
            $images = $product->images;
            $provider = $product->provider;
            $title = strtok($product->text, "\r\n");
            $position = 1;
            $productText = mb_strtolower($product->text);

            if (
                preg_match('/мод\.?\s*(\d+)/i', $productText, $matches) ||
                preg_match('/мод\s*(\d+)/i', $productText, $matches) ||
                preg_match('/мод(\d+)/i', $productText, $matches) ||
                preg_match('/модель\s*(\d+)/i', $productText, $matches)
            ) {
                $model = 'Мод. ' . $matches[1];
            } else {
                $model = '';
            }

            foreach ($images as $image) {

                if (!in_array(pathinfo($image->url, PATHINFO_EXTENSION), ['jpg'])) {
                    $image_url = "https://postavshik.net/videourl?url=" . $image->url;
                } else {
                    $image_url = (!empty($image->vk_source)) ? "https://postavshik.net/imgurl?url=" . $image->url : $image->url;
                }

                $preparedProducts[] = [
                    'name' => $title . ' ' . $provider->alias . '-' . $product->stock_number  . '-' . $model . '-' . $position,
                    'description' => $product->text . '\n' . $product->description,
                    'pictures' => [$image_url],
                    'currency_code' => 'UAH',
                    "price" => $product->price - $provider->extra_uah,
                    'sku' => $provider->alias . '-' . $product->stock_number . '-' . $position,
                ];

                $position++;
            }
        }

        return $preparedProducts;
    }

    public function createProducts($preparedProducts): bool
    {
        //dd($preparedProducts);
        $response = Http::withoutVerifying()->withHeaders([
            'Authorization' => 'Bearer ' . $this->apiKey,
            'Content-Type' => 'application/json',
        ])->post("{$this->baseUrl}/products/import", ['products' => $preparedProducts]);

        if ($response->successful()) {
            return true;
        } else {
            Log::error('Ошибка при отправке данных в KeyCRM: ' . $response->body());
            return false;
        }
    }
}
