<?php

namespace App\Services\KeyCrm\Facades;

use Illuminate\Support\Facades\Facade;

class KeyCrm extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'keycrm';
    }
}