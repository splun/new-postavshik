<?php

namespace App\Services\KeyCrm\Providers;

use Illuminate\Support\ServiceProvider;

class KeyCrmServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->publishes([
         __DIR__.'/../config/keycrm.php' => config_path('keycrm.php'),
        ], 'config');
    }

    public function boot()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/keycrm.php', 
            'keycrm'
        );
    }
}