<?php

return [
    'base_url' => env('KEYCRM_BASE_URL', 'https://openapi.keycrm.app/v1'),
    'api_key' => env('KEYCRM_API_KEY'),
];