<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Order_items extends Model
{
    use HasFactory;

    protected $fillable = [
        "order_id",
        "product_id",
        "product_options",
        "product_qty",
        "price",
        "base_price"
    ];

    public $timestamps = false;
    
    public function product()
    {
        return $this->belongsTo(Products::class, 'product_id', 'id');
    }

    

}
