<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChMessage extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'type',
        'from_id',
        'to_id',
        'body',
        'attachment',
        'is_order'
    ];
    public function user()
    {

        return $this->hasOne('App\Models\User', 'id', 'from_user_id');

    }
}
