<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Options extends Model
{
    protected $fillable = ['option_name', 'option_value'];

    public $timestamps = false;
    
}