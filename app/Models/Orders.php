<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Orders extends Model
{
    use HasFactory;

    protected $fillable = [
        "user_id",
        "fio",
        "phone",
        "email",
        "comment",
        "payment_method",
        "shipping_method",
        "city",
        "post_address",
        "price_total",
        "order_currency",
        "status",
        "user_notificated",
        "utm_source",
        "utm_campaign",
        "payment_id",
        "transaction_id"
    ];

    public function items()
    {
        return $this->hasMany(Order_items::class,'order_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(Users::class, 'user_id', 'id');
    }

}
