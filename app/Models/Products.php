<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Product_options;
use App\Models\ProductsImages;
use App\Models\Options;

use Config;

class Products extends Model
{
    use HasFactory;

    protected $fillable = array('name', 'text', 'description', 'syncyou_category_id', 'api_ID', 'stock_number', 'provider_id', 'category_id', 'price', 'old_price', 'url', 'base64', 'exchanged_price_by', 'has_usd_on_uploading', 'vk_source', 'not_removed', 'uploaded_at');

    public function variants()
    {
        return $this->hasMany(Product_variants::class, 'product_id', 'id');
    }

    public function images()
    {
        return $this->hasMany(ProductsImages::class, 'product_id', 'id');
    }

    public static function get_products()
    {
        $result = DB::table('products')->get();

        $products = array();
        $i = 0;
        foreach ($result as $item) {
            $item->images = Products::get_images($item->id);
            $products[$i] = $item;
            if ($i++ == 2) break;
        }
        // dd($products);
        return $products;
    }

    public static function get_product($id)
    {
        $result = DB::table('products')->where('id', $id)->first();

        return $result;
    }
    public static function delete_product($id)
    {
        DB::table('products_images')->where('product_id', $id)->delete();
        DB::table('product_variants')->where('product_id', $id)->delete();
        $result = DB::table('products')->where('id', $id)->delete();
    }

    public static function get_images($id)
    {
        $result = DB::table('products_images')->where('product_id', $id)->get();

        return $result;
    }

    public static function get_image_by_vk_source($vk_source)
    {
        $result = DB::table('products_images')->where('vk_source', $vk_source)->first();
        return $result;
    }

    public static function get_preview($id)
    {
        $result = DB::table('products_images')->where('product_id', '=', $id)->where('preview', '=', 1)->first();

        return $result;
    }

    public static function get_dublicate_images($url)
    {
        $result = DB::table('products_images')
            ->where('url', $url)
            ->first();

        return $result;
    }

    public static function get_dublicate_images_base64($base64)
    {
        $result = DB::table('products_images')
            ->where('base64', $base64)
            ->first();

        return $result;
    }

    public static function unset_image($id)
    {
        $result = DB::table('products_images')->where('id', $id)->delete();

        return $result;
    }

    public static function unset_image_by_url($url)
    {
        $result = DB::table('products_images')->where('url', $url)->delete();

        return $result;
    }

    public static function cover_image($id, $product_id)
    {
        DB::table('products_images')->where('product_id', $product_id)->update(['preview' => 0]);

        $result = DB::table('products_images')->where('id', $id)->update(['preview' => 1]);

        return $result;
    }
    public function category()
    {
        return $this->belongsTo('App\Models\Categories');
    }

    public function provider()
    {
        return $this->belongsTo('App\Models\Providers');
    }

    public static function addImages($fields)
    {
        return DB::table('products_images')->insert($fields);
    }

    public static function upploadImages($image, $name = null)
    {
        $API_KEY = Config::get('app.imgbb_apikey');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.imgbb.com/1/upload?key=' . $API_KEY);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        $extension = $image->extension();
        $file_name = ($name) ? $name . '.' . $extension : $image->getClientOriginalName();
        $data = array('image' => base64_encode(file_get_contents($image->path())), 'name' => $file_name);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            return 'Error:' . curl_error($ch);
        } else {
            return json_decode($result, true);
        }
        curl_close($ch);
    }
}
