<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;

use Cloudipsp\Configuration as FondyConf;
use Cloudipsp\Checkout as FondyCheckout;
use MonoPay\Client as MonoPayConf; 
use MonoPay\Payment as MonoPay; 
use MonoPay\Webhook as MonoWebhook; 
use Config;

class Payment
{
    public function fondy($checkoutData){
        
        //Payment Fondy Payment DEMO MID-1396424; S_KEY-'test'
        
        FondyConf::setMerchantId(config('settings.fondy_merchant_id'));
        FondyConf::setSecretKey(config('settings.fondy_secret'));

        if(empty($checkoutData)){
            return false;
        }

        FondyCheckout::url($checkoutData)->toCheckout();
    }

    public static function mono($checkoutData){

        if(empty($checkoutData)){
            return false;
        }
        
        $monoClient = new MonoPayConf(config('settings.mono_token'));
        $monoPayment = new MonoPay($monoClient);

        $crypt = Crypt::encryptString($checkoutData['order_id']);

        $invoice = $monoPayment->create(
            $checkoutData['amount'],
            [
                //деталі оплати
                'merchantPaymInfo' => [
                    'reference' => $checkoutData['order_id'], //номер чека, замовлення, тощо; визначається мерчантом (вами)
                    'destination' => $checkoutData['order_desc'], //призначення платежу
                    'comment' => 'Оплата товарів',
                    /*'basketOrder' => [ //наповнення замовлення, використовується для відображення кошика замовлення
                        [
                            'name' => 'Товар1', //назва товару
                            'qty' => 2, //кількість
                            'sum' => 500, //сума у мінімальних одиницях валюти за одиницю товару
                            'icon' => 'https://example.com/images/product1.jpg', //посилання на зображення товару
                            'unit' => 'уп.', //назва одиниці вимiру товару
                        ],
                    ],*/
                ],
                'redirectUrl' => config('settings.mono_callback_url').'/'.$crypt, //адреса для повернення (GET) - на цю адресу буде переадресовано користувача після завершення оплати (у разі успіху або помилки)
                //'webHookUrl' => "http://test.postavshik.net/cart/update_order_mono_status/".$crypt, //адреса для CallBack (POST) – на цю адресу буде надіслано дані про стан платежу при кожній зміні статусу. Зміст тіла запиту ідентичний відповіді запиту “перевірки статусу рахунку”
                'validity' => 3600 * 24 * 7, //строк дії в секундах, за замовчуванням рахунок перестає бути дійсним через 24 години
                'paymentType' => 'debit', //debit | hold. Тип операції. Для значення hold термін складає 9 днів. Якщо через 9 днів холд не буде фіналізовано — він скасовується
            ]
        );

        return $invoice;

    }

    public static function checkMonoInvoiceStatus($invoiceId){
        
        $monoClient = new MonoPayConf(config('settings.mono_token'));
        $monoPayment = new MonoPay($monoClient);

        $invoice = $monoPayment->info($invoiceId);

        return $invoice;

    }

    public static function checkMonoInvoiceStatusWebhook(){

        try {

            $monoClient = new MonoPayConf(config('settings.mono_token'));
            $publicKey = $monoClient->getPublicKey();

            $monoWebhook = new MonoWebhook($monoClient, $publicKey, $_SERVER['HTTP_X_SIGN']);

            $body = file_get_contents('php://input');

            if ($monoWebhook->verify($body)) {
                Log::error("MonoBank Response ".$e->getMessage());
            } else {
                Log::error("MonoBank Response ".$e->getMessage());
            }
        }
        catch (Exception $e) {

            Log::debug("MonoBank Response ".$e->getMessage());

        }

    }
}