<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Comments
 */
class Comments extends Model
{
    /**
     * @var array
     */
    protected $guarded = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo("App\Models\User");
    }

}