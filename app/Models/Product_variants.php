<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

use Config;

class Product_variants extends Model
{
    protected $fillable = ['id', 'option_id', 'product_id'];

    public $timestamps = false;

    public function options()
    {
        return $this->belongsTo(Options::class, 'option_id', 'id');
    }

}