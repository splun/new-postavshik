<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    use HasFactory;

    protected $guarded = [];
    public $timestamps = false;
    
    public function user()
    {

        return $this->hasOne('App\Models\User', 'id', 'from_user_id');

    }
}
