<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Tracks extends Model
{
    use HasFactory;

    protected $fillable = [
        "number",
        "name",
        "date",
        "carrier",
        "created_at"
    ];

}
