<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Pages extends Model
{
    use HasFactory;

    protected $fillable = [
        "title",
        "slug",
        "meta_description",
        "content"
    ];

    public static function allpages(){

        $pages = DB::table('pages')->get();

        return $pages;
    }

    public function create($data)
    {
        $result = DB::table('pages')
        ->insert([
            'title' => $data->title,
            'slug' => $data->slug,
            'meta_description' => $data->meta_description,
            'content' => $data->content
        ]);

        return $result;
    }

    public function getPage($id){
        $result = DB::table('pages')->where('id', $id)
                    ->first();

        return $result;
    }

    public function savePage($data) {

        $result = DB::table('pages')
            ->where('id', $data->id)
            ->update([
            'title' => $data->title,
            'slug' => $data->slug,
            'meta_description' => $data->meta_description,
            'content' => $data->content
        ]);

        return $result;
    }

    public function remove($id){

        $result = DB::table('pages')->where('id', '=', $id)->delete();

        return $result;
    }
}
