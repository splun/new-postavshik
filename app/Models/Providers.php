<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Providers extends Model
{
    use HasFactory;

    protected $fillable = array('alias','extra_uah','extra_usd');

//    public static function all()
//    {
//        return 111;
//    }

    public function products()
    {
        //return $this->hasMany('App\Models\Product');
    }
}
