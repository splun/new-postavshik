<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Class Comments
 */
class Slider extends Model
{

    protected $fillable = [
        "title",
        "image_desktop",
        "image_mobile",
        "public"
    ];

    /**
     * @var array
     */
    protected $guarded = [];

}