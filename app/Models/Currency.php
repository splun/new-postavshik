<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Currency extends Model
{
    use HasFactory;

    const FILENAME = 'config/currency.json';

    const PRIVAT_BANK_API_URI = 'https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5';
    const PRIVAT_USD_KEY = 'privatUsd';
    const ENABLED_PRIVAT_USD_KEY = 'enabledPrivatUsd';

    const NEW_API_URL_BANK_GOV = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';

    public static function getRate($code){
        if($code == "USD"){
            return 36.5686;
        }
        if($code == "RUB"){
            return 0.4;
        }
        $content = file_get_contents(self::NEW_API_URL_BANK_GOV.'&valcode='.$code);
        $content = json_decode($content);

        if($content[0] && $content[0]->rate){
            $rate = $content[0]->rate;
        }else{
            if($code == "USD"){
                $rate = 26;
            }

            if($code == "RUB"){
                $rate = 0.4;
            }
        }
        
        return $rate;
    }

    public static function setRate($code,$rate){
        if($rate && $code){
            DB::table('currencies')->where('code', $code)->update(['exchange_rate'=>$rate]);
        }else{
            return false;
        }
    }


    /**
     * @var \stdClass
     */
    private static $currency;

    /**
     * @param $param
     * @param bool $cached
     * @return mixed|\stdClass
     */

    public static function get($param = null, $cached = true)
    {
        if (null === self::$currency || false === $cached) {
            self::$currency = self::init();
        }
        if (\is_string($param)) {
            return isset(self::$currency->$param) ? (float)self::$currency->$param : 1;
        }

        return self::$currency;
    }

    public static function set($currencyCode, $value)
    {
        self::$currency->{$currencyCode} = $value;

        return self::class;
    }

    /**
     * @param array $currency
     * @return self|string
     */
    public static function replace(array $currency)
    {
        self::$currency = (object) $currency;

        return self::class;
    }

    /**
     * @return int
     */
    public static function curSave()
    {
        return Storage::disk('local')->put(self::FILENAME, json_encode(self::$currency));
    }

    /**
     * Currency constructor.
     */
    private static function init()
    {
        if (!file_exists(self::FILENAME)) {
            $std = new \stdClass();
            $std->usd = 26;
            $std->{self::PRIVAT_USD_KEY} = 26;
            $std->rub = 0.4;

            return $std;
        }

        $content = file_get_contents(self::FILENAME);

        return json_decode($content);
    }

    /**
     * @deprecated
     */
    public static function getPrivatBankCurrency($code = 'usd')
    {
        $curl = curl_init();
        curl_setopt_array(
            $curl,
            [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL            => self::PRIVAT_BANK_API_URI,
            ]
        );
        $response = curl_exec($curl);
        curl_close($curl);

        if ($response) {
            $currencyObject = null;
            $response = json_decode($response);
            if (is_array($response)) {
                $currencyObject = array_filter(
                    $response,
                    function ($i) use ($code) {
                        return $i->ccy === strtoupper($code);
                    }
                );
                $currencyObject = array_shift($currencyObject);
            }

            return $currencyObject ? (float)$currencyObject->buy : self::get('usd');
        }

        return self::get('usd');
    }

}
