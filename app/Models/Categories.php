<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Categories extends Model
{
    use HasFactory;

    public function products()
    {
        //return $this->hasMany('App\Models\Product');
    }

    public static function get_all(){

        $result = DB::table('categories')->orderBy("disposition","desc")->get();

        return $result;
    }

    public static function get_w_parent() {
        $result = [];
        foreach (Categories::get_parent_all() as $cat) {
            $result[] = [
                'parent_id'=> $cat->id,
                'parent_name'=> $cat->name,
                'child_cats' => Categories::get_subcat($cat->id),
            ];
        }
        //dd($result);
        return $result;
    }

    public static function get_subcat($parent_id){

        $result = DB::table('categories')->where('parent_id', $parent_id)->orderBy("disposition","asc")->get();

        return $result;
    }

    public static function get_parent_all(){

        $result = DB::table('categories_parent')->orderBy("disposition","asc")->get();

        return $result;
    }

    public static function get_one($id){

        $result = DB::table('categories')->where('id', $id)->first();

        return $result;
    }

    public static function edit($data)
    {
        $result = DB::table('categories')
                    ->where('id', $data->id)
                    ->update([
                        'name' => $data->name,
                        'parent_id' => $data->parent_id,
                        'disposition' => $data->disposition,
                        'syncyou_category'=>$data->syncyou_cat
                    ]);

        return $result;
    }

    public static function create($data)
    {
        $result = DB::table('categories')
                    ->insert([
                        'name' => $data->name,
                        'parent_id' => $data->parent_id,
                        'disposition' => $data->disposition,
                        'syncyou_category'=>$data->syncyou_cat,
                        'created_at' => new \DateTime(),
                    ]);

        return $result;
    }
}
