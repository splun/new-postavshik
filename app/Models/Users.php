<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Hash;

class Users extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'password',
        'phone',
        'network_id',
        'identity',
        'user_social_id',
        'firstname',
        'lastname',
        'icon',
        'is_locked',
        'permission'
    ];

    public static function get_all($permission = 'user')
    {
        $users = DB::table('users')->where('permission', $permission)->get();

        return $users;
    }

    public static function get_user($id) {
        $result = DB::table( 'users' )->where( 'id', $id )
                    ->first();

        return $result;
    }

    public static function create($data)
    {
        $result = DB::table('users')
                    ->insert([
                        'email' => $data->email,
                        'password' => Hash::make($data->password),
                        'firstname' => $data->firstname,
                        'lastname' => $data->lastname,
                        'location' => $data->location,
                        'permission' => 'user'
                    ]);

        return $result;
    }

    public static function opcreate($data)
    {
        $result = DB::table('users')
                    ->insert([
                        'email' => $data->email,
                        'name' => $data->name,
                        'password' => Hash::make($data->password),
                        'location' => $data->location,
                        'permission'=> 'operator'
                    ]);

        return $result;
    }

    public static function edit($data)
    {
         $arr = [
            'firstname' => $data->firstname,
            'lastname' => $data->lastname,
            'email' => $data->email,
            'password' => Hash::make($data->password),
            'location' => $data->location,
        ];

        if(empty($data->password)) {
            unset($arr['password']);
        }

        if(empty($data->location)) {
            unset($arr['location']);
        }

        $result = DB::table('users')
                    ->where('id', $data->id)
                    ->update($arr);

        return $result;
    }

    public static function remove($id)
    {
        $result = DB::table('users')->where('id', '=', $id)->delete();

        return $result;
    }

    public static function lock($id)
    {
        $result = DB::table('users')->where('id', '=', $id)->update(['is_locked' => 1]);

        return $result;
    }

    public static function unlock($id)
    {
        $result = DB::table('users')->where('id', '=', $id)->update(['is_locked' => 0]);

        return $result;
    }
}
