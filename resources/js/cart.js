$(document).ready(function () {
    setTimeout(function () {
        $("#cart .alert").fadeOut(300);
    }, 2000);

});

$("#chat-order").on("click", function () {
    /*var links = "";
    $('.blk_cart table tbody > tr').each(function () {
        var link = $(this).find('a').attr('href');
        links += window.location.origin + "" + link + "\n";
    });
    if (links) {
        $('#chat-message').html(links);
        $('.chat_send').click();
    }
    alert(links);
    $(".chat_close").trigger("click");*/
});

$(".add-to-cart").on("click", function () {

    var product_id = $(this).data("id");
    var size = ($("input[name=product-size]:checked").val() != undefined) ? $("input[name=product-size]:checked").val() : 0;
    var color = ($("#input-option-color").val() != undefined) ? $("#input-option-color").val() : 0;

    if (size || color) {
        var url = "/cart/add/" + product_id + "?size=" + size + "&color=" + color;
    } else {
        var url = "/cart/add/" + product_id;
    }

    var fbname = $(this).data("fbname");
    var fbcategory = $(this).data("fbcategory");
    var fbprice = $(this).data("fbprice");


    $.get(url)
        .done(function (data) {
            if (data.result == "success") {
                flashMessage("Товар успешно добавлен в корзину");

                fbq('track', 'AddToCart', {
                    content_type: 'product',
                    content_ids: [product_id],
                    content_name: fbname,
                    content_category: fbcategory,
                    value: fbprice,
                    currency: 'UAH'
                });
            }
        });

    $(".right_menu a.cart").addClass("active").removeClass("empty");

    return false;
});

/* Quantity buttons */

$('.q_up').click(function () {

    var q_val_up = parseInt($(this).parents(".quantity").find('.input-quantity').val());
    if (isNaN(q_val_up)) {
        q_val_up = 1;
    }
    $(this).parents(".quantity").find('.input-quantity').val(q_val_up + 1).keyup();

    cart_update($(this));

    return false;
});

$('.q_down').click(function () {
    var q_val_up = parseInt($(this).parents(".quantity").find('.input-quantity').val());
    if (isNaN(q_val_up)) {
        q_val_up = 1;
    }

    if (q_val_up > 1) {
        $(this).parents(".quantity").find('.input-quantity').val(q_val_up - 1).keyup();
        cart_update($(this));
    }

    return false;
});

$(".input-quantity").on("change", function () {
    cart_update($(this));
});


/* Delete item */

$(".delete-cart-item a").on("click", function () {
    // var itemRow = $(this).parents(".cart-item-row");
    // var product_id = $(itemRow).data("id");
    // var url = "/cart/delete/"+product_id

    // $.get( url )
    // .done(function( data ) {
    //     if(data.result == "success")
    //     {
    //         var product_price = parseFloat($(itemRow).find(".product-total-price").data("price"));
    //         totals_update();
    //         console.log(data.count);

    //         if(data.count > 0){
    //             $(itemRow).remove();
    //         }else{
    //             location.reload();
    //         }

    //         //alert("Корзина успешно обновлена");
    //     }
    // });

    //return false;
    flashMessage("Товар удален из корзины");
});

/* Update functions */
function cart_update(el) {
    var itemRow = $(el).parents(".cart-item-row");
    var product_id = $(itemRow).data("id");
    var count = $(itemRow).find('.input-quantity').val();

    var url = "/cart/update/" + product_id + "/" + count;

    $.get(url)
        .done(function (data) {
            if (data.result == "success") {
                //var cur_product_total = $(el).parents(".cart-item-row").find(".product-total-price").text();
                var orig_price = $(itemRow).find(".product-total-price").data("price").split(" ");
                var product_price = parseFloat(orig_price[0]); //parseFloat($(itemRow).find(".product-total-price").data("price"));
                var currency = orig_price[1];
                var product_total = product_price * count;
                $(itemRow).find(".product-total-price span").text(product_total + " " + currency);

                totals_update();
                flashMessage("Корзина успешно обновлена");
            }
        });

}

function totals_update() {

    var total = 0;

    if ($(window).width() > 720) {
        var cart_wrapper = $(".blk_cart");
    } else {
        var cart_wrapper = $(".mob_cart ");
    }
    $(cart_wrapper).find(".product-total-price").each(function () {
        total = total + parseInt($(this).find("span").text());
    });

    var currency = ($("#currency").val() !== "") ? $("#currency").val() : "UAH";

    $(".all_price span").text(total.toFixed() + " " + currency);

}

function flashMessage(message, options, settings) {
    return $.notify(
        $.extend({
            message: message
        }, options),
        $.extend({
            element: 'body',
            placement: {
                from: "top",
                align: "center"
            },
            offset: 10,
            delay: 2000,
            z_index: 2000,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            }
        }, settings));
};
