$(window).load(function () {

    $("#chat-message").emojioneArea({
        hideSource: true,
    });

    $("#messages-list .chat_wrap").load("/chat/history", {
        "_token": $('meta[name="csrf-token"]').attr('content'),
    }, function () {
        var block = document.getElementById("messages-list");
        block.scrollTop = block.scrollHeight;
    });

    //setInterval(AjaxLoadNewMessages, 3000);
    function CommetSocketsLoadNewMessages() {
        cometApi.start({ node: "app.comet-server.com", dev_id: 15 })

        cometApi.subscription("simplechat.newMessage", function (event) {

            console.log(event);

            $.post("/chat/message", {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "msg_data": event.data
            }, function (data) {
                $("#messages-list .chat_wrap").append(data);
                var block = document.getElementById("messages-list");
                block.scrollTop = block.scrollHeight;
                if (!$('.chat_fixed').hasClass("opened")) {
                    $(".chat_active_msg").html('<span data-toggle="tooltip" title="" class="badge bg-red" data-original-title="3 New Messages">1</span>');
                }
            });

        })
    }

    PusherLoadNewMessages();

    function PusherLoadNewMessages() {

        Pusher.logToConsole = true;

        var pusher = new Pusher('5bccc5c8b5b22c958aa1', {
            cluster: 'eu'
        });

        var chanel_id = $("#user_id").val() + "-" + $("#operator_id").val();

        var channel = pusher.subscribe('chanel_id');
        channel.bind('my-event', function (data) {
            alert(JSON.stringify(data));
        });

        // $.post("/chat/message", {
        //     "_token": $('meta[name="csrf-token"]').attr('content'),
        //     "msg_data": event.data
        // }, function (data) {
        //     $("#messages-list .chat_wrap").append(data);
        //     var block = document.getElementById("messages-list");
        //     block.scrollTop = block.scrollHeight;
        //     if (!$('.chat_fixed').hasClass("opened")) {
        //         $(".chat_active_msg").html('<span data-toggle="tooltip" title="" class="badge bg-red" data-original-title="3 New Messages">1</span>');
        //     }
        // });


    }

    $('.chat_file').on("click", function () {
        $('.chat_file_input').trigger("click");
    });

    $('.chat_close').click(function () {
        $(this).parents('.chat_fixed').toggleClass('opened')
        $(".chat_active_msg").html("");
    });

    var block = document.getElementById("messages-list");
    block.scrollTop = block.scrollHeight;

    // for html
    $('.op').click(function () {
        $(this).parents('.chat_fixed').hide().removeClass('opened');
        $(this).parents('.chat_fixed').next().show();
    });

    $(".push").on("click", function () {
        send();
    });
})

function HtmlEncode(s) {
    var el = document.createElement("div");
    el.innerText = el.textContent = s;
    s = el.innerHTML;
    return s;
}


function send() {
    var text = $('#chat-message').val();
    var operator = $("#operator_id").val();

    if (text == "") return;
    $.ajax({
        url: "/chat/push",
        type: "POST",
        data: "_token=" + $('meta[name="csrf-token"]').attr('content') + "&operator=" + operator + "&text=" + encodeURIComponent(text)
    });

    $('#chat-message').val("");

}

var startFrom = 20;
var inProgress = false;
var old_height = $("#messages-list").height();
var old_scroll = $("#messages-list").scrollTop();

$("#messages-list").scroll(function () {

    /* Если высота окна + высота прокрутки больше или равны высоте всего документа и ajax-запрос в настоящий момент не выполняется, то запускаем ajax-запрос */
    if ($("#messages-list").scrollTop() == 0 && !inProgress) {
        console.log(startFrom);
        $.ajax({
            url: "/chat/loadhistory",
            method: "POST",
            data: {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "start": startFrom
            },
            beforeSend: function () {
                inProgress = true;
            }
        })
            .done(function (data) {
                if (data) {
                    $("#messages-list").prepend(data);
                    $("#messages-list").scrollTop(old_scroll + $(document).height() - old_height);
                    startFrom += 20;
                    inProgress = false;
                }
            });


    }

});



//Images
$(".chat_file_input").on("change", function () {
    count_files = $(this).get(0).files.length;
    console.log(count_files);
    $(".chat_file").find("svg").remove();
    $(".chat_file").text(count_files);
});
/*$(function () {

    var dropbox = $('#messages-list'),
        message = $('.chat_wrap', dropbox);

    dropbox.filedrop({
        paramname: 'image',

        maxfiles: 5,
        maxfilesize: 2,
        url: '/chat/dropfiles.php',

        uploadFinished: function (i, file, response) {
            $.data(file).addClass('done');
        },

        error: function (err, file) {
            switch (err) {
                case 'BrowserNotSupported':
                    showMessage('Ваш Браузер не поддерживает HTML5!');
                    break;
                case 'TooManyFiles':
                    alert('Разрешено загружать за 1 раз не более 5 изображений');
                    break;
                case 'FileTooLarge':
                    alert(file.name + ' Слишком большой.Разрешена загрузка файлов не более 2мб.');
                    break;
                default:
                    break;
            }
        },

        beforeEach: function (file) {
            if (!file.type.match(/^image\//)) {
                alert('Разрешена загрузка только изображений!!!!');
                return false;
            }
        },

        uploadStarted: function (i, file, len) {
            createImage(file);
        },

        progressUpdated: function (i, file, progress) {
            $.data(file).find('.progress').width(progress);
        }

    });

    var template = '<div class="chat_block left">' +
        '<span class="imageHolder">' +
        '<img />' +
        '<span class="uploaded"></span>' +
        '</span>' +
        '<div class="progressHolder">' +
        '<div class="progress"></div>' +
        '</div>' +
        '</div>';


    function createImage(file) {

        var preview = $(template),
            image = $('img', preview);

        var reader = new FileReader();

        image.width = 100;
        image.height = 100;

        reader.onload = function (e) {

            image.attr('src', e.target.result);
        };

        reader.readAsDataURL(file);

        message.hide();
        preview.appendTo(dropbox);

        $.data(file, preview);
    }

    function showMessage(msg) {
        message.html(msg);
    }

});*/