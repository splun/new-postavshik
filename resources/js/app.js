$ = jQuery;

$(document).keyup(function (e) {
    // if (e.keyCode === 39) {// right
    //     return this.next()
    // }
    // if (e.keyCode === 37) { // left
    //     return this.prev()
    // }
    if (e.key === "Escape") {

        $(".search-box").slideUp();

    }
});

$(document).ready(function () {

    $(window).scroll(function () {
        if ($(this).scrollTop() > 600) {
            $('#back-top').fadeIn();
        } else {
            $('#back-top').fadeOut();
        }
    });
    $("#back-top").click(function (e) {
        e.preventDefault();
        $('body,html').animate({
            scrollTop: 0
        }, 800, 'swing');
    });

    window.onscroll = function () { myFunction() };
    var header = document.getElementById("Header");
    var sticky = header.offsetTop;
    function myFunction() {
        if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
        } else {
            header.classList.remove("sticky");
        }
    }

    $("header .search_mobile button").on('click', function () {
        $(".search-box").slideToggle();
        $('#form-s-mob').focus();
    });

    // Mobile Menu

    (function ($) {
        var $main_nav = $('#main-nav');
        var $toggle = $('.toggle');

        var defaultData = {
            maxWidth: false,
            customToggle: $toggle,
            navTitle: '',
            levelTitles: true,
            levelTitleAsBack: true,
            pushContent: '#container',
            insertClose: 1,
            closeLevels: true
        };

        // call our plugin
        var Nav = $main_nav.hcOffcanvasNav(defaultData);

    })(jQuery);

    // End Mobile Menu

    new WOW().init();

    // Select2

    $('.w-search').select2();
    $('.no-search').select2({
        minimumResultsForSearch: -1
    });

    $('.selectable').styler();

    // Typeahead

    //let states = ['Київ', 'Дніпро', 'Харків', 'Одеса', 'Львів'];

    // $.ajax({
    //     url: 'https://api.nbastatr.com/teams/',
    //     type: 'GET',
    //     dataType: 'json',
    //     success: function (json) {
    //         let arrayLength = json.length;
    //         for (let i = 0; i < arrayLength; i++) {
    //             if (!states.includes(json[i]['name_full'])) {
    //                 states.push(json[i]['name_full']);
    //             }
    //         }
    //         return states;
    //     }
    // });

    // var substringMatcher = function(strs) {
    //     return function findMatches(q, cb) {
    //         var matches, substringRegex;

    //         // an array that will be populated with substring matches
    //         matches = [];

    //         // regex used to determine if a string contains the substring `q`
    //         substrRegex = new RegExp(q, 'i');

    //         // iterate through the pool of strings and for any string that
    //         // contains the substring `q`, add it to the `matches` array
    //         $.each(strs, function(i, str) {
    //             if (substrRegex.test(str)) {
    //             matches.push(str);
    //             }
    //         });

    //         cb(matches);
    //     };
    // };

    // var states = ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'ACalifornia',
    //     'AColorado', 'AConnecticut', 'ADelaware', 'AFlorida', 'AGeorgia', 'AHawaii',
    //     'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana',
    //     'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota',
    //     'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire',
    //     'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
    //     'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island',
    //     'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont',
    //     'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'
    // ];

    // $('.typeahead').typeahead({
    //     hint: true,
    //     highlight: true,
    //     minLength: 0,
    // },
    // {
    //     limit: 'Infinity',
    //     name: 'states',
    //     source: substringMatcher(states)
    // });

    //NovaPochta
    // init Bloodhound
    var np_cities_suggestions = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        /*prefetch: {
            url: 'https://raw.githubusercontent.com/twitter/typeahead.js/gh-pages/data/countries.json',
            transform: function (data) {          // we modify the prefetch response
                var newData = [];                 // here to match the response format 
                data.forEach(function (item) {    // of the remote endpoint
                    newData.push({ 'name': item });
                });
                return newData;
            }
        },*/
        remote: {
            url: '/cart/shipping_cities/%QUERY',
            wildcard: '%QUERY'
        },
        identify: function (response) {
            return response.name;
        }
    });

    // init Typeahead
    $('#np-cities').typeahead({
        hint: true,
        //highlight: true,
        minLength: 0,
    },
        {
            limit: 'Infinity',
            name: 'states',
            source: suggestionsWithDefaults,   // custom function is passed as the source
            display: function (item) { // display: 'name' will also work
                return item.name;
            },
            templates: {
                suggestion: function (item) {
                    return '<div>' + item.name + '</div>';
                }
            }
        }).bind('typeahead:select', function (ev, suggestion) {
            let tag = suggestion.name;
            $("#np-post-number").prop("disabled", true);
            $("#select2-np-post-number-container").text("Почекайте, шукаємо відділення....");
            $.ajax({
                url: "/cart/np_warehouses/" + tag + "",
                dataType: 'json',
                method: 'get',
                data: { tag: tag },
                success: function (data) {
                    let option = " <option value='' selected disabled>Вибрати адресу та відділення:</option>";
                    //console.log(data[0].name);
                    $.each(data, function (key, val) {
                        option += "<option value='" + val.name + "'>" + val.name + "</option>";
                    })

                    $("#np-post-number").html(option).prop("disabled", false);
                }
            });
        });

    function suggestionsWithDefaults(q, sync, async) {
        if (q === '') {
            sync([
                { name: 'Київ' }, { name: 'Дніпро' }, { name: 'Харків' }, { name: 'Одеса' }, { name: 'Львів' }, { name: 'Запоріжжя' }, { name: 'Кривий Ріг' }, { name: 'Миколаїв' }, { name: 'Вінниця' }, { name: 'Полтава' }, { name: 'Чернігів' }, { name: 'Черкаси' }, { name: 'Житомир' }, { name: 'Суми' }, { name: 'Хмельницький' }, { name: 'Чернівці' }, { name: 'Рівне' }, { name: 'Кропивницький' }, { name: 'Івано-Франківськ' }, { name: 'Кременчук' }, { name: 'Тернопіль' }, { name: 'Луцьк' }, { name: 'Біла Церква' }, { name: 'Слов\'янськ' }, { name: 'Ужгород' }, { name: 'Конотоп' }, { name: 'Краматорськ' }
            ]);
        } else {
            // let bloodhound handle the rest
            np_cities_suggestions.search(q, sync, async);

        }
    }
    // END NovaPochta

    // UkrPochta
    var up_cities_suggestions = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: '/cart/up_shipping_cities/%QUERY',
            wildcard: '%QUERY'
        },
        identify: function (response) {
            return response.name;
        }
    });

    $('#up-cities').typeahead({
        hint: true,
        //highlight: true,
        minLength: 0,
    },
        {
            limit: 'Infinity',
            name: 'states',
            source: suggestionsWithDefaultsUP,   // custom function is passed as the source
            display: function (item) { // display: 'name' will also work
                return item.name;
            },
            templates: {
                suggestion: function (item) {
                    return '<div>' + item.name + '</div>';
                }
            }
        }).bind('typeahead:select', function (ev, suggestion) {

            let city_id = suggestion.CITY_ID;
            console.log(suggestion);
            $("#up-post-number").prop("disabled", true);
            $("#select2-up-post-number-container").text("Почекайте, шукаємо відділення....");
            $.ajax({
                url: "/cart/up_warehouses/" + city_id + "",
                dataType: 'json',
                method: 'get',
                data: { city_id: city_id },
                success: function (data) {
                    let option = " <option value='' selected disabled>Вибрати адресу та відділення:</option>";
                    //console.log(data[0].name);
                    $.each(data, function (key, val) {
                        let warhouse = val.name.replace("'", "", val.name);
                        option += "<option value='" + warhouse + "'>" + val.name + "</option>";
                    })

                    $("#up-post-number").html(option).prop("disabled", false);
                }
            });
        });

    function suggestionsWithDefaultsUP(q, sync, async) {
        if (q === '') {
            sync([
                { name: 'Київ', CITY_ID: '29713' }, { name: 'Дніпро', CITY_ID: '3641' }, { name: 'Харків', CITY_ID: '24550' }, { name: 'Одеса', CITY_ID: '17069' }, { name: 'Львів', CITY_ID: '14288' }, { name: 'Запоріжжя', CITY_ID: '8968' }, { name: 'Кривий Ріг', CITY_ID: '4292' }, { name: 'Миколаїв', CITY_ID: '16169' }, { name: 'Вінниця' }, { name: 'Полтава', CITY_ID: '19234' }, { name: 'Чернігів', CITY_ID: '29712' }, { name: 'Черкаси', CITY_ID: '27760' }, { name: 'Житомир', CITY_ID: '6708' }, { name: 'Суми', CITY_ID: '21680' }, { name: 'Хмельницький', CITY_ID: '26481' }, { name: 'Чернівці', CITY_ID: '28188' }, { name: 'Рівне', CITY_ID: '20296' }, { name: 'Кропивницький', CITY_ID: '12069' }, { name: 'Івано-Франківськ', CITY_ID: '9826' }, { name: 'Кременчук', CITY_ID: '17802' }, { name: 'Тернопіль', CITY_ID: '22662' }, { name: 'Луцьк', CITY_ID: '3477' }, { name: 'Біла Церква', CITY_ID: '10472' }, { name: 'Ужгород', CITY_ID: '8553' }, { name: 'Конотоп', CITY_ID: '20583' }
            ]);
        } else {
            // let bloodhound handle the rest
            up_cities_suggestions.search(q, sync, async);

        }
    }

    $("#up-cities").on("change", function () {
        let index = $(this).val();
        $.ajax({
            url: "/cart/up_warehouses/" + index + "",
            dataType: 'json',
            method: 'get',
            data: { tag: index },
            success: function (data) {
                let option = " <option value='' selected disabled>Вибрати адресу та відділення:</option>";
                //console.log(data[0].name);
                $.each(data, function (key, val) {
                    option += "<option value='" + val.name + "'>" + val.name + "</option>";
                })

                $("#post-number").html(option);

            }
        });

    });

    // END UkrPochta

    $(window).on('load', function () {

        // var logo = new TimelineMax({repeat:0, yoyo:false});
        //     logo.from(".navbar", .5, {y:-70, ease: "none"})
        //         .staggerTo($('.navbar-brand path'), .5, {delay:.5, opacity: 1, ease: Back.easeOut.config(3)}, 0.05, "-=.5")
        //         .to(".hamburger", 1, {opacity: 1}, "-=1.5")
        //         .to(".left_icons", 1, {opacity: 1}, "-=1.5")
        //         .to(".right_btn", 1, {opacity: 1},"-=1.5")

    });

    // Fancybox

    $('[data-fancybox]').fancybox({
        touch: false,
        animationDuration: 600,
        animationEffect: 'zoom-in-out'
    });

    // Product more

    $(".product_more").on("click", function () {
        //spoiler
        //$(this).toggleClass('show').parent().find('.product-description').slideToggle();

        //show more
        //$(this).toggleClass('show');

        if ($(this).hasClass("show")) {
            $(this).removeClass('show').parent().find('.product-description').css("max-height", "25px");
        } else {
            $(this).addClass('show').parent().find('.product-description').css("max-height", "300px");
        }

    });

    // Filter TTN

    $('input#filtersearch').bind('keyup change', function () {
        if ($(this).val().trim().length !== 0) {
            $('div.date-filter .alpha-ul, div.date-filter .alpha-ul li')
                .show()
                .hide()
                .each(function () {
                    if (
                        $(this).is(
                            ':icontains(' + $('input#filtersearch').val() + ')'
                        )
                    )
                        $(this).show();
                });
        } else {
            $('div.date-filter .alpha-ul, div.date-filter .alpha-ul li')
                .show()
                .hide()
                .each(function () {
                    $(this).show();
                });
        }
        if ($(this).val().trim().length !== 0) {
            $('div.date-filter')
                .show()
                .hide()
                .each(function () {
                    if (
                        $(this).is(
                            ':icontains(' + $('input#filtersearch').val() + ')'
                        )
                    )
                        $(this).show();
                });
        } else {
            $('div.date-filter')
                .show()
                .hide()
                .each(function () {
                    $(this).show();
                });
        }
    });

    $('input#filtersearch-date').bind('keyup change', function () {
        if ($(this).val().trim().length !== 0) {
            $('div.date-filter')
                .show()
                .hide()
                .each(function () {
                    if (
                        $(this).is(
                            ':icontains(' + $('input#filtersearch-date').val() + ')'
                        )
                    )
                        $(this).show();
                });
        } else {
            $('div.date-filter')
                .show()
                .hide()
                .each(function () {
                    $(this).show();
                });
        }
    });

    $.expr[':'].icontains = function (obj, index, meta, stack) {
        return (
            (obj.textContent || obj.innerText || jQuery(obj).text() || '')
                .toLowerCase()
                .indexOf(meta[3].toLowerCase()) >= 0
        );
    };

    // Copy to clipboard

    spans = document.querySelectorAll(".date-filter ul li");
    for (const span of spans) {
        span.onclick = function () {
            document.execCommand("copy");
        }

        span.addEventListener("copy", function (event) {
            event.preventDefault();
            if (event.clipboardData) {
                event.clipboardData.setData("text/plain", span.textContent.replace(/\D/g, ''));
                alert("Трек номер: " + event.clipboardData.getData("text") + " скопирован")
            }
        });
    }

    // Showmore

    $(document).on('click', '.showmore', function () {
        $(this).toggleClass('open').parent().find('.s_text').toggleClass('show')
        if ($('.showmore').hasClass('open')) {
            $(this).find('span').text('показати меньше');
        } else {
            $(this).find('span').text('показати більше');
        }
    });

    // Change select

    $('.select_block select, .input-option-size').change(function () {

        let pSize = $('input[name="product-size"]:checked').val();
        let pColor = $('select[name="product-color"]').val();

        if ($('select[name="product-color"]').length > 0 && $('input[name="product-size"]').length > 0) {
            if (pSize && pColor) {
                $('.single_info .add-to-cart').removeClass('disabled');
            }
        } else {
            if (pSize || pColor) {
                $('.single_info .add-to-cart').removeClass('disabled');
            }
        }

    });

    // Change input

    $('input[name="shipping_method"]').on('change', function () {
        if ($("#UP").is(':checked')) {
            $('#npCity,#npPost').hide().val("");
            $('#upCity,#upPost').show();
            $('#np-cities,#np-post-number').prop('required', false);
            $('#up-cities,#up-post-number').prop('required', true);
            $("#prepayment-filed").hide().find("#prepayment").prop("checked", false);
            $("#fondy").prop("checked", true);
        }
        else {
            $('#npCity,#npPost').show();
            $('#upCity,#upPost').hide().val("");
            $('#up-cities,#up-post-number').prop('required', false);
            $('#np-cities,#np-post-number').prop('required', true);
            $("#prepayment-filed").show();
        }
    });

    $('input[name="payment_method"]').on('change', function () {
        if ($("#prepayment").is(':checked')) {
            $("#prepayment_total_price").show();
            $("#total_price").hide();
        } else {
            $("#prepayment_total_price").hide();
            $("#total_price").show();
        }
    });
    // Cart spoiler

    $('#cartTitle').on('click', function () {
        $(this).toggleClass('closed').next().slideToggle();
    });
    $('#chkTitle').on('click', function () {
        $(this).toggleClass('closed').next().slideToggle();
    });

    $('#btnSpoiler').on('click', function () {
        $('#cartTitle').click();
        $('#checkRow').slideToggle().css('display', 'flex');
        $(this).hide();
        $('#btnOrder').show();
    });

    // Carousel

    $("#slider").owlCarousel({
        loop: true,
        items: 1,
        margin: 0,
        nav: true,
        autoplay: true,
        autoplayTimeout: 6000,
        autoplayHoverPause: false,
        navText: ["<i class='fa fa-chevron-left'></i>", "<i class='fa fa-chevron-right'></i>"],
        dots: false,
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        responsive: {
            0: {
                items: 1
            }
        }
    });

    $(".new-items, .shares-items").owlCarousel({
        loop: false,
        items: 4,
        margin: 30,
        stagePadding: 40,
        nav: false,
        navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"],
        dots: false,
        autoplay: false,
        autoplayTimeout: 10000,
        autoplayHoverPause: false,
        mouseDrag: true,
        responsive: {
            0: {
                items: 1
            },
            568: {
                items: 2
            },
            768: {
                items: 3
            },
            992: {
                items: 4
            },
            1200: {
                items: 5
            },
            2000: {
                items: 6
            },
            3000: {
                items: 7
            }
        }
    });

    $(".img-carousel").owlCarousel({
        loop: false,
        items: 6,
        margin: 20,
        stagePadding: 20,
        nav: false,
        navText: ["<div class='nav-btn prev-slide'></div>", "<div class='nav-btn next-slide'></div>"],
        dots: false,
        autoplay: false,
        autoplayTimeout: 10000,
        autoplayHoverPause: false,
        mouseDrag: true,
        responsive: {
            0: {
                items: 1,
                stagePadding: 30
            },
            568: {
                items: 2,
                stagePadding: 30
            },
            992: {
                items: 4,
                stagePadding: 30
            },
            1200: {
                items: 5
            },
            1900: {
                items: 6
            },
            2000: {
                items: 8
            },
            3000: {
                items: 10
            }
        }
    });

    $(".menu_categories .menu-parent").on("click", function () {

        if ($(this).parent(".nav-item").hasClass("show")) {
            $(this).parent(".nav-item").removeClass("show");

        } else {
            $(".nav-item").removeClass("show");
            $(this).parent(".nav-item").addClass("show");
        }

        return false;

    });

    var swiper = new Swiper(".mySwiper", {
        spaceBetween: 10,
        slidesPerView: 5,
        freeMode: true,
        watchSlidesProgress: true,
        breakpoints: {
            0: {
                slidesPerView: 3,
                spaceBetween: 10,
            },
            768: {
                slidesPerView: 6,
                spaceBetween: 10,
            },
            992: {
                slidesPerView: 4,
                spaceBetween: 10,
            },
            1200: {
                slidesPerView: 5,
                spaceBetween: 10,
            },
        },
    });

    if ($("html").width() >= 992) {
        var swiper2 = new Swiper(".mySwiper2", {
            allowTouchMove: true,
            effect: 'coverflow',
            spaceBetween: 10,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            thumbs: {
                swiper: swiper,
            },
            on: {
                slideChange: function () {
                    document.querySelectorAll('.swiper-slide video').forEach(function (video) {
                        video.pause();
                    });
                    const currentSlide = this.slides[this.activeIndex];
                    const currentVideo = currentSlide.querySelector('video');
                    const playButton = currentSlide.querySelector('.custom-play-button');

                    if (currentVideo) {
                        playButton.classList.add('hidden');
                        currentVideo.play();
                    }
                },
            },
        });

        // $('.zoomImg').zoom({
        //     on: 'mouseover',
        //     touch: true,
        //     magnify: 1
        // });
    } else if ($("html").width() <= 991) {
        var swiper2 = new Swiper(".mySwiper2", {
            allowTouchMove: true,
            effect: 'coverflow',
            spaceBetween: 10,
            navigation: {
                nextEl: ".swiper-button-next",
                prevEl: ".swiper-button-prev",
            },
            thumbs: {
                swiper: swiper,
            },
            on: {
                slideChange: function () {
                    document.querySelectorAll('.swiper-slide video').forEach(function (video) {
                        video.pause();
                    });
                    const currentSlide = this.slides[this.activeIndex];
                    const currentVideo = currentSlide.querySelector('video');
                    const playButton = currentSlide.querySelector('.custom-play-button');

                    if (currentVideo) {
                        playButton.classList.add('hidden');
                        currentVideo.play();
                    }
                },
            },
        });
    }

});


$("#currency_chooser .currency_menu button").on("click", function () {
    let currency = $(this).attr("name");
    $("input#currency").val(currency);
    $('#set_currency')[0].submit();
});

$(".reply-comment").click(function () { // ID откуда кливаем
    comment_id = $(this).data("parent");
    $(".reply-comment-" + comment_id).show();
    $('html, body').animate({
        scrollTop: $(".reply-comment-" + comment_id).offset().top - 100 // класс объекта к которому приезжаем
    }, 1000); // Скорость прокрутки
});