'use strict';

(function () {

    String.prototype.trimEmojioneContent = function () {
        return this.replace(/^(\s*?\<br\>+\s*?)+|(\s*?\<br\>+\s*?)+$/gm, '').trim()
    }

    // workaround to load emojionearea
    window.emojioneVersion = '3.0'
    $(document.createElement('input')).emojioneArea({
        autocomplete: false
    })

    // event listener on "Заказать" button in the modal
    const opnChatBtn = document.querySelector('.open-chat-btn')
    if (opnChatBtn) {
        opnChatBtn.addEventListener('click', function (e) {
            Chat.toggleWindow(e);
        })
    }

    const TEMPLATE_DELIMETERS = ['${', '}']
    const LOAD_DATA_INTERVAL = 1000 * 5 // 5 sec
    const LOAD_ROOM_ACTIVITY_INTERVAL = 1000 * 30 // 30 sec
    const INCOMING_MESSAGE = 'incoming'
    const OUTGOING_MESSAGE = 'outgoing'

    Vue.options.delimiters = TEMPLATE_DELIMETERS

    const http = Vue.http
    Vue.http.options.root = location.origin + 'admin/chat'

    Vue.prototype.$INCOMING_MESSAGE = INCOMING_MESSAGE
    Vue.prototype.$OUTGOING_MESSAGE = OUTGOING_MESSAGE
    Vue.prototype.$ukrRoomId = 0
    Vue.prototype.$cisAndEuRoomId = 1

    const FileAttacher = {
        template: '<div id="file-attacher">\
            <div class="send__message-form__file-attacher">\
                <label>\
                    <i class="fa fa-camera fa-2x" aria-hidden="true"></i>\
                    <input ref="$uploader" class="fileupload" id="attached[]" type="file" name="attached[]"\
                    multiple accept="image/*" style="display: none;" @change="change">\
                </label>\
            </div>\
            <span>${files.length}</span></div>\
        ',
        props: { 'fromClipboard': Array },
        data: function () {
            return {
                files: [],
                previousRequest: null,
            }
        },
        computed: {
            fileList: function () {
                if (!this.files.length) {
                    return this.fromClipboard
                }
                return this.fromClipboard.concat(...this.files)
            },
        },
        watch: {},
        methods: {
            change: function (e) {
                const files = e.target.files
                for (let i = 0; i < files.length; i++) {
                    if (files.item(i).size > 1048576) {
                        return alert('Максимальный размер загружаемого файла не должен превышать 1Мб: ' + files.item(i).name)
                    }
                }
                this.files = e.target.files
            },
            add: function () {
                const count = this.fileList.length
                if (count < 1 || count > 5 || this.previousRequest) {
                    return new Promise((resolve, reject) => {
                        if (this.previousRequest) {
                            reject(403)
                        }
                        if (count > 5) {
                            alert('Максимальное количество загружаемых файлов за один раз - 5')
                            reject('Max files count has been exceeded')
                        }
                        resolve(null)
                    })
                }

                const formData = new FormData()
                const files = this.fileList
                for (let i = 0; i < count; i++) {
                    formData.append('files[]', files[i])
                }

                const self = this
                return http.post('attach', formData, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    before(request) {
                        self.previousRequest = request
                    }
                }).then(response => {
                    this.previousRequest = null
                    return response.body.data
                })
            },
        }
    }

    const ChatModal = {
        template: '#chat-modal',
        props: {
            'files': Array,
            'index': {
                default: 0,
                type: Number,
            },
        },
        data: function () {
            return {
                currentImgIndex: this.index,
            }
        },
        computed: {
            src: function () {
                return this.files[this.currentIndex]
            },
            currentIndex: {
                get: function () {
                    return this.currentImgIndex
                },
                set: function (value) {
                    if (value < 0 || value >= this.files.length) {
                        return
                    }
                    this.currentImgIndex = value
                }
            },
        },
        watch: {},
        methods: {
            next: function () {
                this.currentIndex++
            },
            prev: function () {
                this.currentIndex--
            },
            onKeyup: function (e) {
                if (e.keyCode === 39) {// right
                    return this.next()
                }
                if (e.keyCode === 37) { // left
                    return this.prev()
                }
                if (e.keyCode === 27) {// esc
                    return this.$emit('close')
                }
            },
            onOverlayClick: function (e) {
                // if (e.target.classList.value === 'modal-wrapper') {
                if (e.target.classList.value.indexOf('modal-wrapper') !== -1) {
                    this.$emit('close')
                }
            },
        },
        mounted: function () {
            window.addEventListener('keyup', this.onKeyup)
            this.$refs.modal.addEventListener('click', this.onOverlayClick)
        },
        beforeDestroy: function () {
            window.removeEventListener('keyup', this.onKeyup)
            this.$refs.modal.removeEventListener('click', this.onOverlayClick)
        }
    }

    const SendMessageForm = {
        template: '#send-message-area',
        components: {
            'file-attacher': FileAttacher,
            'clipboard-modal': ChatModal,
        },
        props: { 'operator': Number, 'room': Number },
        data: function () {
            return {
                message: localStorage.getItem('message') || '',
                processing: false,
                clipboardFiles: [],
                showModal: false,
                base64Files: [],
                focusedImg: null,
                emojiOne: null,
            }
        },
        computed: {
            $emojiOneArea: {
                set(value) {
                    this.emojiOne = value
                },
                get() {
                    return this.emojiOne
                }
            },
            getMessageAsHTML: {
                cache: false,
                get() {
                    return this.$emojiOneArea.editor[0].innerHTML.trimEmojioneContent()
                }
            },
            $emojiOneAreaBtn: function () {
                return $('#chat .emojionearea-button')
            },
            $emojiOneAreaPicker: function () {
                return $('#chat .emojionearea-picker')
            }
        },
        watch: {},
        methods: {
            openModal: function (file) {
                this.$nextTick(() => {
                    this.focusedImg = file
                    this.showModal = true
                })
            },
            closeModal: function () {
                this.$nextTick(() => {
                    this.focusedImg = null
                    this.showModal = false
                })
            },
            removeAttachedFile: function (index) {
                this.clipboardFiles.splice(index, 1)
                this.base64Files.splice(index, 1)
            },
            resetForm: function () {
                this.$nextTick(() => {
                    this.base64Files = []
                    this.clipboardFiles = []
                    this.$refs.$attachedFiles.files = []
                    this.$emojiOneArea.setText('')
                    if (this.$emojiOneAreaBtn) this.$emojiOneAreaBtn.removeClass('active')
                    if (this.$emojiOneAreaPicker) this.$emojiOneAreaPicker.addClass('hidden')
                })
            },
            send: function (e) {
                const countFiles = this.$refs.$attachedFiles.fileList.length > 0
                if ((!this.message.length || this.processing) && !countFiles) {
                    return this.$emojiOneArea.editor[0].focus()
                }

                let requestedBody = {
                    user: this.operator,
                    body: this.getMessageAsHTML,
                    room: this.operator === -1 ? this.room : null
                }

                if (!this.message.length && countFiles) {
                    requestedBody.body = '<span></span>'
                }

                this.processing = true

                const before = () => {
                    this.message = ''
                    this.resetForm();
                }

                return this.$refs.$attachedFiles.add().then(response => {
                    if (response && response['moved'] > 0) {
                        requestedBody = Object.assign(requestedBody, {
                            attachedFilesPath: response['folder']
                        })
                    }

                    return http.post('send', requestedBody, { before }).then(resp => {
                        if (resp.body.error) {
                            this.$emit('error', resp.body.error)
                        } else {
                            this.$emit('submit', resp.body.data)
                        }
                    }, error => {
                        this.$emit('error', 'Не удалось отправить сообщение.')
                        console.error(error)
                    }).then(() => {
                        this.$nextTick(() => {
                            console.log('Processing finished')
                            this.processing = false
                            this.message = ''
                            this.setModel()
                        })
                    })
                }, error => {
                    if (error === 403) {
                        console.log('Request has been started already')
                    } else {
                        this.$emit('error', 'Не удалось отправить сообщение.')
                        console.error(error)
                    }
                })
            },
            setModel: function () {
                if (this.$emojiOneArea) {
                    this.message = this.$emojiOneArea.getText().trimEmojioneContent()
                }
                localStorage.setItem('message', this.message)
            },
            onPasteImage: function () {
                this.$emojiOneArea.editor[0].onpaste = (e) => {
                    const items = (event.clipboardData || event.originalEvent.clipboardData).items
                    // find pasted image among pasted items
                    let blob = null
                    for (let i = 0; i < items.length; i++) {
                        if (items[i].type.indexOf('image') === 0) {
                            blob = items[i].getAsFile()
                        }
                    }
                    // load image if there is a pasted image
                    if (blob !== null) {
                        // blob size accuracy
                        if (blob.size * 0.01 > 1048576) {
                            return alert('Максимальный размер загружаемого файла не должен превышать 1Мб.')
                        }
                        this.clipboardFiles.push(blob)

                        const reader = new FileReader()
                        reader.onload = (e) => {
                            this.base64Files.push(e.target.result)
                        }
                        reader.readAsDataURL(blob)
                        // const reader = new FileReader();
                        // reader.onload = function (event) {
                        //     console.log(blob.name, element);
                        // };
                        // reader.readAsDataURL(blob);
                    }
                }
            },
            initEmojiOneArea: function () {
                try {
                    this.$emojiOneArea = $(this.$refs.$messageInput).emojioneArea({
                        useSprite: false,
                        shortnames: true,
                        placeholder: 'Напишите сообщения...',
                        autocomplete: false,
                        events: {
                            emojibtn_click: this.setModel,
                            keyup: this.setModel,
                            keydown: (editor, e) => {
                                if (e.keyCode === 13 && e.ctrlKey) {
                                    e.preventDefault()
                                    return this.send()
                                }
                            }
                            // paste: this.onPasteImage
                        }
                    })[0].emojioneArea
                    this.onPasteImage()
                    this.$emojiOneArea.setText(this.message)
                } catch (e) {
                    this.$emit('error', 'Произошла ошибка. Попробуйте перезагрузить страницу.')
                    throw new Error('Cannot init emojionearea')
                }
            },
        },
        created: function () {
            // console.log('Form is created');
        },
        mounted: function () {
            this.initEmojiOneArea()
        },
        beforeDestroy: function () {
            // if (this.$emojiOneArea) {
            //     this.$emojiOneArea.remove();
            // }
        }
    }

    const Chat = new Vue({
        el: '#chat',
        components: {
            'send-message-form': SendMessageForm,
            'modal': ChatModal
        },
        data: function () {
            return {
                previousRequest: null,
                roomLastActivityInterval: null,
                loadDataInterval: null,
                selectedRoom: +sessionStorage.getItem('selectedRoom'),
                showWindow: false,
                messages: [[], []],
                notStarted: true,
                loading: true,
                error: null,
                roomLastActivity: [],
                currentOperatorId: null,
                showModal: false,
                focusedImgIndex: 0,
                firstNotReadMessageId: null,
                resized: false,
                onResize: false,
                chatHeight: null,
                chatStartSize: {
                    height: 0,
                    width: 0
                }
            }
        },
        computed: {
            isHistoryEmpty: function () {
                return this.messages.filter(i => {
                    return i.length
                }).length < 1
            },
            lastActivity: function () {
                if (!this.roomLastActivity.length) {
                    return null
                }
                const result = this.roomLastActivity.filter((i) => {
                    return +i.room_id === this.selectedRoom
                })
                return result.length ? result.pop().last_activity : null
            },
            operatorId: {
                set: function (id) {
                    this.currentOperatorId = id
                },
                get: function () {
                    return +this.currentOperatorId
                }
            },
            roomAttachedFileList: function () {
                if (this.messages[this.selectedRoom] && this.messages[this.selectedRoom].length) {
                    return this.messages[this.selectedRoom].reduce((prev, curr) => {
                        return [...prev, ...curr.attached_files]
                    }, []).filter((file) => {
                        return this.isImg(file)
                    })
                }
                return []
            },
            ukrRoomNotRead: function () {
                return this.messages[this.$ukrRoomId].filter(function (i) {
                    return i.message_type === INCOMING_MESSAGE && !+i.is_read
                }).length
            },
            euRoomNotRead: function () {
                return this.messages[this.$cisAndEuRoomId].filter(function (i) {
                    return i.message_type === INCOMING_MESSAGE && !+i.is_read
                }).length
            },
            getChatClientRects() {
                return this.$el.getClientRects().item(0)
            },
            startHeight() {
                return this.$el.offsetHeight
            },
            startWidth() {
                return this.$el.offsetWidth
            }
        },
        watch: {},
        methods: {
            openModal: function (file) {
                this.$nextTick(() => {
                    this.focusedImgIndex = this.roomAttachedFileList.indexOf(file)
                    this.showModal = true
                })
            },
            isImg: function (filename) {
                return (/\.(gif|jpg|jpeg|tiff|png)$/i).test(filename)
            },
            isOnline: function (roomId) {
                return this.roomLastActivity.length && this.roomLastActivity.filter((i) => {
                    return +i.room_id === roomId && i.status === 'online'
                }).length
            },
            isLastActivityShown: function (roomId) {
                return !this.isOnline(roomId) && this.roomLastActivity.length && this.roomLastActivity.filter((i) => {
                    return +i.room_id === roomId && i.last_activity
                }).length
            },
            startChat: function (room) {
                this.notStarted = false
                this.selectedRoom = room
                this.$nextTick(() => { })
            },
            switchRoom: function (room) {
                if (this.selectedRoom === +room) {
                    return
                }
                bootbox.confirm({
                    className: 'confirm-switch-room',
                    title: "Изменить оператора",
                    message: "Вы уверены, что хотите переключить оператора?",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Нет'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Да'
                        }
                    },
                    callback: result => {
                        if (!result) {
                            return
                        }
                        setTimeout(() => {
                            this.selectedRoom = room
                            sessionStorage.setItem('selectedRoom', this.selectedRoom)
                            this.setOperatorId()
                            this.scrollToBottom()
                        }, 500)
                    }
                });
            },
            toggleWindow: function (e) {
                if (e.target.classList.contains('fa-minus')) {
                    return this.restoreSize(e)
                }
                if (this.onResize) {
                    return
                }
                this.showWindow = !this.showWindow

                if (this.resized) {
                    if (!this.showWindow) {
                        this.chatHeight = this.$el.offsetHeight
                        this.$el.style.height = `${this.$el.firstChild.offsetHeight}px`
                    } else {
                        this.$el.style.height = `${this.chatHeight}px`
                    }
                } else if (this.showWindow) {
                    setTimeout(this.initResizer, 100)
                }
            },
            setAsRead: function () {
                if (this.messages[this.selectedRoom].filter((i) => {
                    return i.message_type === INCOMING_MESSAGE && +i.is_read === 0
                }).length < 1) {
                    return
                }
                http.post('read', { room: this.selectedRoom }).then(function (response) {
                    // console.info("Messages is set as read", response);
                }, function (response) {
                    // console.error("Cannot set as read", response);
                }).then(function () {
                    setTimeout(function () {
                        this.messages[this.selectedRoom].map(function (i) {
                            if (i.message_type === INCOMING_MESSAGE) {
                                i.is_read = 1
                            }
                        })
                    }.bind(this), 1000)
                }.bind(this))
            },
            scrollToBottom: function (ms, offset) {
                setTimeout(() => {
                    const widgetWindowHistory = document.querySelector('#chat .widget__window__history__content')
                    if (widgetWindowHistory) {
                        if (offset) {
                            widgetWindowHistory.scrollTop += offset
                        } else {
                            widgetWindowHistory.scrollTop = widgetWindowHistory.scrollHeight
                        }
                    }
                }, ms || 100)
            },
            scrollToNewMessages: function () {
                setTimeout(() => {
                    if (this.showWindow && this.$refs.newMessagesLinear && this.$refs.newMessagesLinear.length) {
                        this.$refs.newMessagesLinear.pop().scrollIntoView({ block: 'start', behavior: 'smooth' })
                    }
                }, 1000)
            },
            isReadMessage: function (room, i) {
                const message = this.messages[room][i]
                return message ? !+message.is_read : false
            },
            thereIsNewMessages: function (room) {
                return this.messages[room].filter(function (i) {
                    return i.message_type === INCOMING_MESSAGE && !+i.is_read
                }).length
            },
            addMessage: function (data) {
                this.operatorId = data.to_user_id
                clearInterval(this.loadDataInterval)
                this.loadData().then(() => {
                    this.scrollToBottom()
                    this.runLoadDataInterval()
                    if (!this.resized) {
                        this.initResizer()
                    }
                })
            },
            setOperatorId: function () {
                const selected = this.roomLastActivity.filter((i) => {
                    return +i.room_id === this.selectedRoom
                })

                this.operatorId = selected.length ? selected.pop().id : -1
            },
            checkIsRoomOnline: function () {
                return http.post('room-last-activity', null).then(response => {
                    this.roomLastActivity = response.body.data
                    this.setOperatorId()
                }, error => {
                    console.error(error)
                })
            },
            loadData: function () {
                const self = this

                return http.post(http.options.root, null, {
                    before(request) {
                        self.error = null
                        // abort previous request, if exists
                        if (self.previousRequest) {
                            self.previousRequest.abort()
                        }
                        self.previousRequest = request
                    }
                }).then(response => {
                    const data = response.body.data

                    if (data.length > 0) {
                        this.notStarted = false
                    }

                    let messages = [[], []]

                    data.map(i => messages[+i.room_id].push(i))

                    if (
                        messages[+this.selectedRoom].length !==
                        this.messages[+this.selectedRoom].length
                    ) {
                        this.firstNotReadMessageId = null
                        // this.scrollToNewMessages()
                    }

                    this.messages = messages
                })
            },
            displayError: function (error) {
                this.error = error || 'Что-то пошло не так... Пожалуйста, попробуйте позже.'
                this.scrollToBottom(0)
            },
            runLoadDataInterval: function () {
                this.loadDataInterval = setInterval(() => {
                    this.loadData()
                }, LOAD_DATA_INTERVAL)
            },
            closeOnEscape: function (e) {
                if (e.keyCode === 27) {
                    // @be-careful
                    if ($('body.modal-open').length) {
                        return $('#imagemodal').modal('hide')
                    }
                    if (!$('.modal-mask, body.modal-open').length && this.showWindow) {
                        this.toggleWindow(e)
                    }
                }
            },
            initResizer() {
                const chat = this.$el
                const startX = this.getChatClientRects.x
                const startY = this.getChatClientRects.y

                this.chatStartSize.height = chat.offsetHeight
                this.chatStartSize.width = chat.offsetWidth

                const startResizing = e => {
                    this.onResize = true
                    e.preventDefault()

                    const dx = startX - e.clientX
                    const dy = startY - e.clientY

                    const dxOffset = dx <= 0 ? 0 : Math.abs(dx)
                    const dyOffset = dy <= 0 ? 0 : Math.abs(dy)

                    chat.style.width = `${e.clientX <= 0 ?
                        window.innerWidth :
                        dxOffset + this.startWidth}px`
                    chat.style.height = `${e.clientY <= 0 ?
                        window.innerHeight :
                        dyOffset + this.startHeight}px`
                }

                const stopResizing = e => {
                    window.removeEventListener('mousemove', startResizing, false)
                    window.removeEventListener('mouseup', stopResizing, false)
                    setTimeout(() => this.onResize = false, 100)
                }

                this.$refs.$resizer.addEventListener('mousedown', e => {
                    window.addEventListener('mousemove', startResizing, false)
                    window.addEventListener('mouseup', stopResizing, false)
                }, false)
            },
            restoreSize(e) {
                this.$el.style = {
                    height: `${this.chatStartSize.height}px`,
                    width: `${this.chatStartSize.width}px`,
                }
                this.resized = false
            }
        },
        filters: {
            filename: function (fullFilePath) {
                return fullFilePath.split('/').pop()
            },
            toLocaleDateString: function (value) {
                return (new Date(value).toLocaleDateString('ru-RU', {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                    hour: 'numeric',
                    minute: 'numeric',
                    second: 'numeric'
                }))
            },
            linkify: function (value) {
                return urlify(value.replace(/&nbsp;/g, ' '))
            },
        },
        mounted: function () {
            window.showPreload = false
            this.checkIsRoomOnline().then(() => {
                this.loadData().then(() => {
                    // if (this.showWindow && this.loading) {
                    this.scrollToBottom(0)
                    // }
                    this.loading = false
                    this.runLoadDataInterval()
                    // console.log('Chat component is mounted.', this);
                }, error => {
                    this.displayError()
                    console.error(error)
                    clearInterval(this.loadDataInterval)
                })
            }).then(() => {
                this.roomLastActivityInterval = setInterval(() => {
                    this.checkIsRoomOnline()
                }, LOAD_ROOM_ACTIVITY_INTERVAL)
            })
            document.addEventListener('keyup', this.closeOnEscape)
        },
        beforeCreate: function () { },
        created: function () { },
        beforeDestroy: function () {
            document.removeEventListener('keyup', this.closeOnEscape)
            clearInterval(this.loadDataInterval)
            clearInterval(this.roomLastActivityInterval)
        }
    })
})()