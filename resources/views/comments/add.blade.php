<form enctype="multipart/form-data" class="review_form" method="POST" action="{{route('comments.add')}}" name="review_form">
	{{ csrf_field()}}
	<div class="user_name blue"><b>{{ ($user->firstname || $user->lastname) ? $user->firstname." ".$user->lastname : $user->name }}</b></div>
	<textarea name="text" class="form-control effect" cols="20" rows="3" placeholder="Ваш отзыв"></textarea>
	@if(Auth::user() && Auth::user()->permission == "admin" && isset($parent_comment))
	<input type="hidden" name="parent_comment" value="{{$parent_comment}}" />
	@endif
	<div class="row">
		<div class="col-lg-8 file align-self-center">
			<input type="file" name="photos[]" multiple accept="image/*,image/jpeg" class="selectable">
		</div>
		<div class="col-lg-4 submit_btn align-self-center">
			<input type="submit" class="submit" value="отправить">
		</div>
	</div>
</form>