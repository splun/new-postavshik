<div class="row" id="comment-{{$comment->id}}">
	<div class="col-12 review_wrapper">
		<div class="col-12 border">
			<div class="cell review_image"><img class="img-fluid" src="{{!empty($comment->user->icon) ? $comment->user->icon : '/images/review_image.jpg' }}" alt="image"></div>
			<div class="cell review_content">
				<div class="user_name blue">
					<b>{{($comment->user->firstname || $comment->user->lastname) ? $comment->user->firstname." ".$comment->user->lastname : $comment->user->name }}</b> 
					@if(Auth::user() && Auth::user()->permission == "admin")
						<span style="float:right"><a style="text-decoration:underline" onclick="return confirm('Точно удалить??')" href="/comments/delete/{{$comment->id}}">Удалить</a></span>
						<span style="float:right"><a data-parent="{{$comment->id}}" class="reply-comment" style="cursor:pointer;text-decoration:underline;padding-right:20px;">Ответить</a></span>
					@endif
				</div>
				<div class="user_text">
					<p>{{$comment->text}}</p>
					
					<?php $attachments = App\Http\Controllers\CommentsController::getAttachments($comment->id); ?>
					
					@if($attachments)
						@foreach($attachments as $attach)
							<a class="attachment" data-fancybox="gallery"  href="{{ asset(str_replace("public/","storage/", $attach))  }}"><img src="{{ asset(str_replace("public/","storage/", $attach))  }}" /></a>
						@endforeach
					@endif
				</div>
				<div class="time_data">
					<p>{{ is_object($comment->created_at) ? $comment->created_at->diffForHumans() : ''}}</p>
				</div>
			</div>
		</div>

		<?php $childrens = App\Http\Controllers\CommentsController::child($comment->id); ?>

		@if($childrens)
			@foreach($childrens as $child)
			<div class="col-11 border answer ml-auto">
				<div class="cell review_image"><img class="img-fluid" src="{{!empty($child->user->icon) ? $child->user->icon : '/images/logo.svg' }}" alt="image"></div>
				<div class="cell review_content">
					<div class="user_name blue">
						<b>Postavshik</b>
					</div>
					<div class="user_text">
						<p>{{$child->text}}</p>
					</div>
					<?php $attachments = App\Http\Controllers\CommentsController::getAttachments($child->id); ?>
					
					@if($attachments)
						@foreach($attachments as $attach)
							<a class="attachment" data-fancybox="gallery"  href="{{ asset(str_replace("public/","storage/", $attach))  }}"><img src="{{ asset(str_replace("public/","storage/", $attach))  }}" /></a>
						@endforeach
					@endif
					<div class="time_data">
						<p>{{ is_object($child->created_at) ? $child->created_at->diffForHumans() : ''}}</p>
					</div>
				</div>
			</div>
			@endforeach
		@endif
	</div>
</div>
