@extends('layouts.app')

@section('content')

<div class="contents">

    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Отзывы</li>
            </ol>
        </nav>

        <section>
            <div class="container review">
                <div class="row">
                    <div class="col-12 text-center page_head">
                        <h3 class="blue">Отзывы</h3>
                    </div>
                </div>
                
                @if (session('message'))
                    <div class="alert alert-success col-12">
                        {{ session('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                @endif
                @if (session('error'))
                    
                    <div class="alert alert-danger col-12">
                        @foreach(session('error') as $error)
                        <span>{{ $error }}</span>
                        @endforeach
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    
                @endif

                <div class="row">
                    <div class="col-12 review_top">
                        <div class="all_reviews">{{ $comments->total() }} Отзывов</div>
						@if(empty($user))
                        	<p><a href="/login">Авторизируйтесь</a> для того чтобы оставить отзыв.</p>
						@endif
                    </div>
                </div>
				@if(!empty($user))
                <div class="row">
                    <div class="col-12 review_form">
                        <div class="col-12 border">
                            <div class="cell review_image"><img class="img-fluid" src="/images/review_image.jpg" alt="image"></div>
                            <div class="cell review_content">
                                @include('comments.add',['user'=> $user])
                            </div>
                        </div>
                    </div>
                </div>
				@endif

				@if($comments)

					@foreach($comments as $comment)

						@include('comments.comment', ['comment' => $comment])

						@if(Auth::user() && Auth::user()->permission == "admin")
                        <div class="row reply-comment-{{$comment->id}}" style="display:none;">
                            <div class="col-12 review_form">
                                <div class="col-12 border">
                                    <div class="cell review_image"><img class="img-fluid" src="/images/review_image.jpg" alt="image"></div>
                                    <div class="cell review_content">
                                        @include('comments.add',['user'=> $user,'parent_comment'=>$comment->id])
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
					@endforeach

					<div class="col-12 justify-content-center pagination">
                        {{ $comments->render("vendor/pagination/bootstrap-4") }}
                    </div>

				@endif

            </div>
        </section>

		@include("sections/instagram")

    </main><!--main-->

</div>


@include("sections/instagram")

</main><!--main-->

</div>

@endsection
