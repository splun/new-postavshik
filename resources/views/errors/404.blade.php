@extends('layouts.app')

@section('content')

<nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
    <ol class="breadcrumb justify-content-center">
        <li class="breadcrumb-item"><a href="#">Главная</a></li>
        <li class="breadcrumb-item active" aria-current="page">404</li>
    </ol>
</nav>
<div class="container">
    <div class="row">
        <div class="col-12 text-center page_head">
            <h3 class="blue text-lg text-gray-500 uppercase tracking-wider">404 Not Found</h3>
        </div>
    </div>
</div>
@endsection