{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Изменить пароль')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')

@if (\Session::has('success'))
    <div class="alert alert-success">
        <div>{!! \Session::get('success') !!}</div>
    </div>
@endif
@if (\Session::has('error'))
    <div class="alert alert-danger">
        <div>{!! \Session::get('error') !!}</div>
    </div>
@endif

<div class="row">
    <div class="col-xs-12 col-md-4 pswd">
        <div class="card card-info ">
            <div class="card-header">
                    <h3 class="card-title">Изменить пароль</h3>
            </div>
            <form class="form-horizontal" method="POST" action="/admin/set-password">
                <div class="card-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="password" name="password" class="form-control" placeholder="Новый пароль">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="password" name="password_confirm" class="form-control" placeholder="Повторите пароль">
                        </div>
                    </div>
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <button type="submit" class="btn btn-info save">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

</script>
@stop
