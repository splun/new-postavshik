{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Dashboard</h1>
@stop

@section('content')

@if (\Session::has('success'))
    <div class="alert alert-success">
        <div>{!! \Session::get('success') !!}</div>
    </div>
@endif
@if (\Session::has('error'))
    <div class="alert alert-danger">
        <div>{!! \Session::get('error') !!}</div>
    </div>
@endif
<div class="row">
    {{--<div class="col-xs-12 col-md-4">

        <div class="card card-info ">
            <div class="card-header">
                    <h3 class="card-title">Синхронизация сайта с ВК группами</h3>
            </div>
            <div class="card-body">
                <p>maraphet_tm - Работает, odessa.boom - Работает</p>
            </div>
        </div>

    </div> --}}

    <div class="col-xs-12 col-md-3">

        <div class="card card-info ">
            <div class="card-header">
                    <h3 class="card-title">Синхронизация с производителями</h3>
            </div>
            <div class="card-body">
                <p>Работает</p>
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-md-3">

        <div class="card card-info ">
            <div class="card-header">
                    <h3 class="card-title">Количество товара на сайте</h3>
            </div>
            <div class="card-body">
                <p>Всего товаров: <strong>{{ $total_products }}</strong></p>
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-md-3">
        <div class="card card-info">
            <div class="card-header">
                    <h3 class="card-title">Рейтинг популярности (Хит)</h3>
            </div>
            <form class="form-horizontal" method="POST" action="/admin/set-popular-rate">
                <div class="card-body">
                    <div class="input-group">
                        <input type="number" step="1" name="popular_rate" class="form-control popularity-rate" placeholder="5 по умолчанию" value="{{ $popular_rate }}">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <span class="input-group-append">
                            <input class="btn btn-default btn-update-popularity-rate" type="submit" value="Сохранить">
                        </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-md-3" style="display:none;">
        <div class="card card-info">
            <div class="card-header">
                    <h3 class="card-title">Поиск по фото</h3>
            </div>
            <form class="form-horizontal" id="sfp" method="POST" action="/admin/search-from-photo">
                <div class="card-body">
                    <div class="form-group">
                        <div class="custom-control custom-switch">
                        @if($search_from_photo == 1)
                            <input onChange="$('#sfp')[0].submit();" type="checkbox" name="search_from_photo" checked="checked" class="custom-control-input" id="customSwitch1">
                        @else
                            <input onChange="$('#sfp')[0].submit();" type="checkbox" name="search_from_photo" class="custom-control-input" id="customSwitch1">
                        @endif

                        <label class="custom-control-label" for="customSwitch1">Выкл/Вкл</label>
                        </div>
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-xs-12 col-md-4 currency">

        <div class="card card-info ">
            <div class="card-header">
                    <h3 class="card-title">Курс</h3>
            </div>
            <form class="form-horizontal" method="POST" action="/admin/set-exchange-rate">
                <div class="card-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="usd_rate" class="form-control usd" placeholder="USD" value="{{ $usd }}" aria-describedby="sizing-addon1">
                            <div class="input-group-append"><span class="input-group-text">$</span></div>
                        </div>
                    </div>
                    <?php /* <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="rub_rate" class="form-control rub" placeholder="RUB" value="{{ $rub }}" aria-describedby="sizing-addon1">
                            <div class="input-group-append"><span class="input-group-text">₽</span></div>
                        </div>
                    </div> */?>
                    <div class="form-check">
                        <span class="input-group-addon">
                        @if($use_nbu_currency == 1 )
                            <input type="checkbox" class="form-check-input" name="use_nbu_currency" checked="checked">
                        @else
                            <input type="checkbox" class="form-check-input" name="use_nbu_currency">
                        @endif
                        </span>
                        <label>Курс НБУ </label>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <input type="number" step="0.10" class="form-control privat-usd" placeholder="Privat USD" value="{{$nbu_rate}}" aria-describedby="sizing-addon1" disabled="">
                            <div class="input-group-append"><span class="input-group-text">$</span></div> 
                        </div>

                        <span id="helpBlock" class="help-block">
                            🕓 {{ date('d/m/Y h:i:s')}} / <a href="https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&amp;json" target="_blank">💱 https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=USD&amp;json</a></span>
                    </div>
                    <div class="form-group">
                        <div class="error text-danger"></div>
                    </div>
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <button type="submit" class="btn btn-info save">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="card card-info ">
            <div class="card-header">
                    <h3 class="card-title">Предоплата для наложки</h3>
            </div>
            <form class="form-horizontal" method="POST" action="/admin/set-prepayment-rate">
                <div class="card-body">
                    <div class="form-group">
                        <div class="input-group">
                            <input type="text" name="prepayment_rate" class="form-control usd" placeholder="Сума аванса" value="{{$amount_prepayment}}" aria-describedby="sizing-addon1">
                            <div class="input-group-append"><span class="input-group-text">UAH</span></div>
                        </div>
                    </div>
                    <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                    <div class="form-group">
                        <button type="submit" class="btn btn-info save">Сохранить</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="card card-info">
            <div class="card-header">
                    <h3 class="card-title">Изменить обложку</h3>
            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/add-default-banner">
                <div class="card-body">
                    <div class="mb-3">
                        <img class="img-fluid" src="{{ $banner_image_url }}">
                    </div>
                    <div class="form-group">
                        <label>Заменить баннер</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input required type="file" name="default_banner" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Выберите файл</label>
                            </div>
                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                            <div class="input-group-append">
                                <input class="btn btn-default btn-update-popularity-rate" type="submit" value="Загрузить">
                            </div>
                        </div>  
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="card card-info">
            <div class="card-header">
                    <h3 class="card-title">Изменить Favicon</h3>
            </div>
            <form class="form-horizontal" method="POST" enctype="multipart/form-data" action="/admin/edit-favicon">
                <div class="card-body">
                <div class="mb-3">
                        <img class="img-fluid" src="{{ $favicon_image_url }}">
                    </div>
                    <div class="form-group">
                        <label>Заменить Favicon</label>
                        <div class="input-group">
                        <div class="custom-file">
                            <input required type="file" name="favicon" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Выберите файл</label>
                        </div>
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        <div class="input-group-append">
                            <input class="btn btn-default btn-update-popularity-rate" type="submit" value="Загрузить">
                        </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script>

</script>
@stop
