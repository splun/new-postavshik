@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Трек номера</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <form action="/admin/tracks/add_track" method="post">
            @csrf
            
            <div class="form-group">
                <label for="date">Дата</label>
                <div class="input-group date" id="trackdate" data-target-input="nearest">
                    <input type="text" name="date" class="form-control datetimepicker-input" data-target="#trackdate">
                    <div class="input-group-append" data-target="#trackdate" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-md-12">
                    <label for="number">Накладные</label>
                    <textarea class="form-control" style="min-height:300px;" name="np_number" value="" id="number" placeholder="номер накладной и ФИ"></textarea>
                </div> 
                <?php /* <div class="form-group col-md-12">
                    <label for="number">Накладные</label>
                    <textarea class="form-control" style="min-height:300px;" name="up_number" value="" id="number" placeholder="номер накладной и ФИ"></textarea>
                </div>*/?>
            </div>
            <button type="submit" class="btn btn-info">Добавить</button>
        </form>

    </div>
    <!-- /.card-header -->
    @php
       //dd($tracks);
    @endphp
    <div class="card-body">
        @if (\Session::has('success'))
            <div class="alert alert-danger">
                <div>{!! \Session::get('success') !!}</div>
            </div>
        @endif
        <table id="all_pages" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Номер накладной</th>
                <th>Получатель</th>
                <th>Дата</th>
                <?php /* <th>Перевозчик</th> */?>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($tracks as $track)
            <tr>
                <td>{{ $track->id }}</td>
                <td>{{ $track->number }}</td>
                <td>{{ $track->name }}</td>
                <td>{{ $track->created_at }}</td>
                <?php /* <td>{{ ($track->carrier == 'NP' ? "Нова Пошта" : "Укр. Пошта") }}</td> */?>

                <td><a onclick="return confirm('Точно удалить?')" class="btn btn-danger" href="/admin/tracks/remove_track/{{ $track->id }}">Удалить</a></td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Номер накладной</th>
                <th>Получатель</th>
                <th>Дата</th>
                <?php /*  <th>Перевозчик</th> */?>
                <th>Действия</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>

@stop
