@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Страницы</h1>
@stop

@section('content')

{{-- CKEditor CDN --}}
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Редактирование страницы: {{ $data->title }}</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            <form action="/admin/pages/savepage" method="post">
                @csrf
                <input type="hidden" name="page_id" value="{{ $data->id }}">
                <div class="form-group">
                    <label for="Title">Заголовок</label>
                    <input required type="text" class="form-control" name="title" value="{{ $data->title }}" id="Title" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="slug">Slug(SEO)</label>
                    <input required type="text" class="form-control" name="slug" value="{{ $data->slug }}" id="slug" placeholder="slug">
                </div>
                <div class="form-group">
                    <label for="meta_description">Meta Description(SEO)</label>
                    <input required type="text" class="form-control" name="meta_description" value="{{ $data->meta_description }}" id="meta_description" placeholder="Meta Description">
                </div>
                <div class="form-group">
                    <label for="content">Контент</label>
                    <textarea required class="form-control" name="content" id="content" placeholder="content">
                        {{ $data->content }}
                    </textarea>
                </div>
                <button type="submit" class="btn btn-info">Сохранить</button>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
<script>
    ClassicEditor
        .create( document.querySelector( '#content' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

@stop
