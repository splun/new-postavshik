@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Страницы</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <a class="btn btn-info" href="/admin/pages/create">Создать страницу</a>
    </div>
    <!-- /.card-header -->
    @php
       // dd($pages);
    @endphp
    <div class="card-body">
        @if (\Session::has('success'))
            <div class="alert alert-danger">
                <div>{!! \Session::get('success') !!}</div>
            </div>
        @endif
        <table id="all_pages" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Slug</th>
                <th>Статус</th>
                <th>Дата создания</th>
                <th>Дата изменения</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($pages as $page)
            <tr>
                <td>{{ $page->id }}</td>
                <td>{{ $page->title }}</td>
                <td>{{ $page->slug }}</td>
                <td>{{ ($page->public == 1 ? "Включена" : "Выключена") }}</td>
                <td>{{ $page->created_at }}</td>
                <td>{{ $page->updated_at }}</td>
                <td><a class="btn btn-info" href="/admin/pages/edit/{{ $page->id }}">Редактировать</a>
                    <a onclick="return confirm('Точно удалить?')" class="btn btn-danger" href="/admin/pages/remove/{{ $page->id }}">Удалить</a>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Slug</th>
                <th>Статус</th>
                <th>Дата создания</th>
                <th>Дата изменения</th>
                <th>Действия</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@stop
