{{-- resources/views/admin/dashboard.blade.php --}}

@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Поставщики и наценка</h1>
@stop

@section('content')

    @if (\Session::has('success'))
        <div class="alert alert-success">
            <div>{!! \Session::get('success') !!}</div>
        </div>
    @endif
    @if (\Session::has('error'))
        <div class="alert alert-danger">
            <div>{!! \Session::get('error') !!}</div>
        </div>
    @endif

    <div class="row">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4>Наценка по умолчанию для новых поставщиков</h4>
                    <form method="POST" action="/admin/providers/save-default-extra">
                        <div class="input-group">
                            <input type="number" class="form-control input-sm default-extra"
                                   value="{{ config('settings.providers_default_extra') }}"
                                   placeholder="грн" name="default_extra" step="1"/>
                            <span class="input-group-append">
                                <input type="submit" class="btn btn-default save-default-extra" value="OK"></input>
                            </span>
                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">
                    <h4>Создать поставщика</h4>
                    <form method="POST" action="/admin/providers/add-provider">
                        <div class="input-group">
                            <input required type="text" name="provider_number" class="form-control input-sm new-provider-id"
                                   placeholder="Номер"/>
                            <span class="input-group-append">
                                <input type="submit" class="btn btn-default create-new-provider" value="Создать"/>
                            </span>
                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                        </div>
                    </form>
                    <span class="error-new-provider-id"></span>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <p>Действия с выбранными:</p>
                <div class="action-buttons mb-3">
                    <button class="btn btn-danger" onclick="$('#collapse2,#collapse3,#collapse4').collapse('hide');" type="button" id="remove_provider"
                            data-toggle="collapse" data-target="#collapse1" aria-expanded="false" aria-controls="collapse1">Удалить товары и поставщика</button>
                    <button class="btn btn-danger" onclick="$('#collapse1,#collapse3,#collapse4').collapse('hide');" type="button" id="remove_provider"
                            data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">Удалить только товары</button>
                    <button class="btn btn-danger" onclick="$('#collapse1,#collapse2,#collapse4').collapse('hide');" type="button" id="move_provider"
                            data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">Переместить поставщика</button>
                    <button class="btn btn-danger" onclick="$('#collapse1,#collapse2,#collapse3').collapse('hide');" type="button" id="reestimate_provider"
                            data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">Переоценить</button>
                    
                    <div class="collapse" id="collapse1">
                        <div class="card card-body mt-2">

                            <p class="help-block">Все товары включая постащика будут удалены.</p>
                            <form class="form remove-providers-form" method="POST" action="/admin/providers/delete-provider-with-products">
                                <div class="form-group" style="display:none;">
                                    <label>Период</label>
                                    <select class="form-control select-period-type" name='datetype' title="Тип">
                                        <option>Не важно</option>
                                        <option value="1">Выбрать диапазон</option>
                                    </select>
                                </div>
                                <input style="display:none;" disabled type="datetimes" class="select-period form-control" data-date-format="mm/dd/yyyy" name="datetimes" class="form-control mb-3" title="Диапазон"/>
                                
                                <input type="hidden" name="providers_id" class="checked_providers" value="" />
                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>       
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-warning remove-providers">Удалить</button>
                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="collapse" id="collapse2">
                        <div class="card card-body mt-2">

                            <p class="help-block">Все товары будут удалены.</p>
                            <form class="form remove-providers-form" method="POST" action="/admin/providers/delete-provider">
                                <div class="form-group">
                                    <label>Период</label>
                                    <select class="form-control select-period-type" name='datetype' title="Тип">
                                        <option>Не важно</option>
                                        <option value="1">Выбрать диапазон</option>
                                    </select>
                                </div>
                                <input style="display:none;" disabled type="datetimes" class="select-period form-control" data-date-format="mm/dd/yyyy" name="datetimes" class="form-control mb-3" title="Диапазон"/>
                                
                                <input type="hidden" name="providers_id" class="checked_providers" value="" />
                                
                                <input name="_token" type="hidden" value="{{ csrf_token() }}"/>       
                                
                                <div class="form-group mt-3">
                                    <button type="submit" class="btn btn-warning remove-providers">Удалить</button>
                                </div>
                            </form>

                        </div>
                    </div>

                    <div class="collapse" id="collapse3">
                        <div class="card mt-2">
                            <div class="card-body">
                                <h3>Переместить поставщика
                                    <small class="providers-selected"></small>
                                </h3>
                                <form method="POST" action="/admin/providers/move">
                                    <div class="row">
                                    
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">⤵️️</span>
                                                <select name="album_from" class="form-control album-select" id="album-select-from">
                                                    <option value="0" data-id="0">Всех альбомов</option>
                                                    @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}" data-id="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">↪️</span>
                                                <select name="album_to" class="form-control album-select" id="album-select-to">
                                                    <option value="0" data-id="0">-- Выберите альбом из списка --</option>
                                                    @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}" data-id="{{ $category->id }}">{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        
                                        <input type="hidden" name="providers_id" class="checked_providers" value="" />
                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>       
                                    
                                        <div class="form-group">
                                            <button type="submit" class="btn btn-warning confirm-move-providers">ОК</button>
                                        </div>
                                    
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="collapse" id="collapse4">
                        <div class="card mt-2">
                            <div class="card-body">
                                <h3>Переоценить
                                    <small class="providers-selected"></small>
                                </h3>
                                <form method="POST" action="/admin/providers/overestimate">
                                    <div class="form-group">
                                        <div class="input-group" style="width:320px">
                                            <input type="number" name="reestimate_cost" class="form-control input-sm revaluation" placeholder="грн" step="1"/>
                                            <input type="hidden" name="providers_id" class="checked_providers" value="" />
                                            <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                            <span class="input-group-append">
                                                <button class="btn btn-default save-revaluation" type="submit">ОК</button>
                                            </span>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <table id="providers" class="mt-3 dataTable table table-hover table-striped providers">
                    <thead>
                    <tr>
                        <th>
                            <label for="select-all-providers" class="checkbox-inline">
                                <input type="checkbox"
                                       id='select-all-providers'
                                       placeholder="Все"
                                       style="margin-right: 10px"><i
                                    class="fa fa-check" aria-hidden="true"></i></label>
                        </th>
                        <th>Наценка (грн)</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($providers as $provider)
                        <tr class="provider-row" data-id="{{ $provider->id }}" data-alias="{{ $provider->alias }}">
                            <form method="POST" action="/admin/providers/save-provider-extra">
                                <td>
                                    <label for="provider" class="checkbox-inline">
                                        <input type="checkbox" name="provider" class="pcheck"> {{ $provider->alias }}
                                    </label>
                                </td>
                                <td>
                                    
                                    <div class="input-group">
                                        <input type="number" name="extra_uah" class="form-control input-sm extra" placeholder="UAH"
                                            value="{{ (int)$provider->extra_uah }}" step="1"/>
                                        <input type="hidden" name="provider_id" value="{{ $provider->id }}" />
                                        <input type="hidden" name="provider_alias" value="{{ $provider->alias }}" />
                                        <input name="_token" type="hidden" value="{{ csrf_token() }}"/>
                                        <span class="input-group-append">
                                            <button type="submit" class="btn btn-default save-extra-price"
                                                data-id="{{ $provider->id }}" type="button">Сохранить</button>
                                        </span>
                                    </div>
                                </td>
                            </form>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@stop
