
<link href="/admin_assets/css/chat.css" rel="stylesheet">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="/vendor/simplebar/simplebar.min.css">
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Чат операторов</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <meta property="og:image" content="/favicon.png">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

</head>
<body>

<div class="messaging">
  <div class="inbox_msg">
	<div class="inbox_people">
	  @include("admin/chat/userlist")
	</div>
	<div class="mesgs">

     @include("admin/chat/history")

	  <div class="type_msg">
		<div class="input_msg_write">
		  <input type="text" name="text" class="write_msg" placeholder="Type a message" />
		  <input type="hidden" name="user" class="chat_user" value="{{$active_chat}}" />
		  <input type="file" class="chat_file_input" style="display: none;">
		  <button class="chat_file">
            <svg xmlns="http://www.w3.org/2000/svg" width="9" height="19" viewBox="0 0 9 19" fill="none" data-inject-url="http://new.postavshik.loc/images/clip.svg">
				<path d="M5.625 19C7.48597 19 9 17.5017 9 15.6602V4.45312C9 1.99767 6.9813 0 4.5 0C2.0187 0 0 1.99767 0 4.45312V15.6602H1.125V4.45312C1.125 2.61154 2.63902 1.11328 4.5 1.11328C6.36097 1.11328 7.875 2.61154 7.875 4.45312V15.6602C7.875 16.8879 6.86565 17.8867 5.625 17.8867C4.38435 17.8867 3.375 16.8879 3.375 15.6602V6.67969C3.375 6.06582 3.87967 5.56641 4.5 5.56641C5.12032 5.56641 5.625 6.06582 5.625 6.67969V15.6602H6.75V6.67969C6.75 5.45196 5.74065 4.45312 4.5 4.45312C3.25935 4.45312 2.25 5.45196 2.25 6.67969V15.6602C2.25 17.5017 3.76402 19 5.625 19Z" fill="#2D8CE3"></path>
			</svg>
         </button>
		  <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
		</div>
	  </div>
	</div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<?/* <script src="https://comet-server.com/CometServerApi.js" type="text/javascript"></script>*/?>
<script src="/admin_assets/js/chat.js" type="text/javascript"></script>
<script src="/vendor/simplebar/simplebar.min.js" type="text/javascript"></script>

</body>
</html>





 