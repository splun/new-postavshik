  <div class="headind_srch">
    <div class="recent_heading">
      <h4>Чаты</h4>
    </div>
    <div class="srch_bar">
      <div class="stylish-input-group">
        <input type="text" class="search-bar"  placeholder="Поиск" >
        <a id="search-chats"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none" data-inject-url="/images/icons/glass.svg">
          <path d="M18.8704 18.2716L13.4553 12.8565C14.6856 11.4705 15.3275 9.65974 15.2449 7.80833C15.1623 5.95692 14.3616 4.21052 13.0128 2.93961C11.664 1.66869 9.87311 0.973243 8.02007 1.00079C6.16702 1.02833 4.3976 1.77671 3.08715 3.08715C1.77671 4.3976 1.02833 6.16702 1.00079 8.02007C0.973243 9.87311 1.66869 11.664 2.93961 13.0128C4.21052 14.3616 5.95692 15.1623 7.80833 15.2449C9.65974 15.3275 11.4705 14.6856 12.8565 13.4553L18.2716 18.8704C18.31 18.9113 18.3563 18.9439 18.4078 18.9662C18.4593 18.9885 18.5149 19 18.571 19C18.6271 19 18.6826 18.9885 18.7341 18.9662C18.7856 18.9439 18.832 18.9113 18.8704 18.8704C18.9113 18.832 18.9439 18.7856 18.9662 18.7341C18.9885 18.6826 19 18.6271 19 18.571C19 18.5149 18.9885 18.4593 18.9662 18.4078C18.9439 18.3563 18.9113 18.31 18.8704 18.2716ZM1.8895 8.14291C1.8895 6.90611 2.25626 5.69707 2.94339 4.6687C3.63052 3.64034 4.60717 2.83882 5.74984 2.36551C6.8925 1.89221 8.14985 1.76837 9.36289 2.00966C10.5759 2.25095 11.6902 2.84653 12.5647 3.72108C13.4393 4.59564 14.0349 5.70989 14.2762 6.92293C14.5175 8.13598 14.3936 9.39333 13.9203 10.536C13.447 11.6787 12.6455 12.6553 11.6171 13.3424C10.5888 14.0296 9.37972 14.3963 8.14291 14.3963C6.4851 14.3941 4.89583 13.7345 3.72358 12.5622C2.55133 11.39 1.89176 9.80073 1.8895 8.14291Z" fill="#9C9898" stroke="#9C9898"></path>
          </svg>
        </a>
        </div>
    </div>
    <div class="col-12 justify-content-center pagination" id="chat-pagination">
      {{ $chats->appends(Request::query())->render("vendor/pagination/bootstrap-4") }}
      </div>
  </div>

  @if($chats)

  <div class="inbox_chat scroll">
    @foreach($chats as $chat)
      @if($chat->user)
      <div id="chat-{{ $chat->from_user_id }}" class="chat_list {{($active_chat == $chat->from_user_id) ? 'active_chat' : ''}}"><!---active_chat-->
        <div class="chat_people">
          <a href="/admin/chat/?customer={{$chat->from_user_id}}#chat-{{$chat->from_user_id}}">
            <div class="chat_img hidden-xs"> @if($chat->user->icon) <img src="{{$chat->user->icon}}" alt="sunil"> @else <img src="/images/review_image.jpg" alt="sunil"> @endif </div>
            <div class="chat_ib">
              <h5>{{($chat->user->firstname || $chat->user->lastname) ? $chat->user->firstname." ".$chat->user->lastname : $chat->user->name}} <span class="chat_date">{{$chat->datetime}}</span></h5>
              <div class="chat-message hidden-xs">{!! $chat->message !!}</div>
            </div>
          </a>
        </div>
      </div>
      @endif

    @endforeach
  </div>
  @endif
