@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Категории</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <a class="btn btn-info mb-2" data-toggle="collapse" href="#collapseOpcreate" role="button" aria-expanded="false" aria-controls="collapseOpcreate">Создать категорию</a>

        <div class="collapse" id="collapseOpcreate">
            <div class="card card-body">
                <form method="post" action="{{ route('category-create') }}" autocomplete="nope">
                    @csrf
                    <div class="form-group">
                        <label for="operator-username">Название</label>
                        <input type="text" class="form-control" name='name' id="name" required
                               autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="parent">Родительская категория</label>
                        <select class="form-control" id="parent" name="parent_id" required>
                            @foreach($parent as $cat)
                            <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="parent">Категория SyncYou</label>
                        <select class="form-control" id="parent_id" name="syncyou_cat">
                            <option value="" disabled>- выбрать -</option>
                            @foreach($syncyouCategories as $catId => $val)
                                <option value="{{ $catId }}">{{ $catId }} - {{ $val }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="disposition">Порядок</label>
                        <input type="number" min="0" class="form-control" name='disposition' id="disposition" value="0" required
                               autocomplete="off">
                    </div>
                    <button type="submit" class="btn btn-primary">Создать</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    @php
       // dd($data);
    @endphp
    <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
        <table id="all_users" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Название</th>
                <th>Родительская категория</th>
                <th>Категория SyncYou</th>
                <th>Распродажа</th>
                <th>Создана</th>
                <th>Порядок</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>@foreach( $parent as $p)
                        @if($p->id == $item->parent_id)
                            {{ $p->name }}
                            @break
                        @endif
                    @endforeach</td>
                <td>@if(isset($syncyouCategories[$item->syncyou_category]))
                    {{ $syncyouCategories[$item->syncyou_category] }}
                    @endif</td>
                <td>{{ $item->sale_type }}</td>
                <td>@php echo date('m/d/Y H:i:s',  $item->created_at); @endphp</td>
                <td>{{ $item->disposition }}</td>
                <td>
{{--                    <a class="btn btn-warning"--}}
{{--                       href="/admin/users/{{ ($item->is_locked == 1 ? "unlock" : "lock") }}/{{ $item->id }}">--}}
{{--                        {{ ($item->is_locked == 1 ? "Разблокировать" : "Заблокировать") }}</a>--}}
                    <a class="btn btn-info" href="/admin/categories/edit/{{ $item->id }}">Редактировать</a>
                    @if($item->id != 8 && $item->id !=145 && $item->id !=999)
                    <a onclick="return confirm('are you sure?')" class="btn btn-danger"
                       href="/admin/categories/remove/{{ $item->id }}">Удалить</a>
                    @endif
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@stop
