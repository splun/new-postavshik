@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Пользователи</h1>
@stop

@section('content')
<div class="card">
{{--    <div class="card-header">--}}
{{--        <a class="btn btn-info" href="/admin/users/create">Создать пользователя</a>--}}
{{--    </div>--}}
    <!-- /.card-header -->
    @php
       // dd($users);
    @endphp
    <div class="card-body">
        @if (\Session::has('success'))
            <div class="alert alert-danger">
                <div>{!! \Session::get('success') !!}</div>
            </div>
        @endif
        <table id="all_users" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
{{--                <th>Фото</th>--}}
                <th>Email</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Блок</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
{{--                <td>{!! html_entity_decode($user->icon != '' ? '<img src="'.$user->icon.'" width="40">' : '') !!}</td>--}}
                <td>{{ $user->email }}</td>
                
                <td>{{ $user->firstname }}</td>
                <td>{{ $user->lastname }}</td>
                <td><input type="checkbox" {{ ($user->is_locked == 1 ? "checked" : "") }} onChange="window.location.href='/admin/users/{{ ($user->is_locked == 1 ? "unlock" : "lock") }}/{{ $user->id }}'"></td>
                <td>
                    
                    {{--<a class="btn btn-warning"
                       href="/admin/users/{{ ($user->is_locked == 1 ? "unlock" : "lock") }}/{{ $user->id }}">
                        {{ ($user->is_locked == 1 ? "Разблокировать" : "Заблокировать") }}</a>--}}

                    <a class="btn btn-info" href="/admin/users/edit/{{ $user->id }}">Редактировать</a>
                    <a onclick="return confirm('are you sure?')" class="btn btn-danger"
                       href="/admin/users/remove/{{ $user->id }}">Удалить</a>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>ID</th>
{{--                <th>Фото</th>--}}
                <th>Email</th>
                <th>Блок</th>
                <th>Имя</th>
                <th>Фамилия</th>
                <th>Действия</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@stop
