@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Редактирование пользователя</h1>
@stop

@section('content')

{{--@php dd($data); @endphp--}}

{{-- CKEditor CDN --}}
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ $data->firstname }} {{ $data->lastname }}</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            <form action="{{ route('users') }}/saveuser" method="post">
                @csrf
                <input type="hidden" name="user_id" value="{{ $data->id }}">
                <div class="form-group">
                    <label for="Title">Имя</label>
                    <input type="text" class="form-control" name="firstname" value="{{ $data->firstname }}" id="firstname" placeholder="Имя">
                </div>
                <div class="form-group">
                    <label for="slug">Фамилия</label>
                    <input type="text" class="form-control" name="lastname" value="{{ $data->lastname }}" id="lastname" placeholder="Фамилия">
                </div>
                <div class="form-group">
                    <label for="meta_description">Email</label>
                    <input required type="text" class="form-control" name="email" value="{{ $data->email }}" id="email" placeholder="email">
                </div>
                <div class="form-group">
                    <label for="password">Пароль</label>
                    <input type="password" class="form-control" name="password" value="" id="password" placeholder="password">
                </div>
                @if($data->permission == 'operator')
                <div class="form-group">
                    <label for="operator-location">Локация</label>
                    <select class="form-control" id="operator-location" name="location" required>
                        <option @if($data->location == '0') selected @endif value="0" > Все направления</option>
                        <?php /*  <option @if($data->location == '0') selected @endif value="0"> по Украине</option>
                        <option @if($data->location == '1') selected @endif value="1"> по России</option> */?>
                    </select>
                </div>
                @endif

                <button type="submit" class="btn btn-primary">Сохранить</button>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
<script>
    ClassicEditor
        .create( document.querySelector( '#content' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

@stop
