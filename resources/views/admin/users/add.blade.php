@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1></h1>
@stop

@section('content')

    {{-- CKEditor CDN --}}
    <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Создание страницы</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            <form action="/admin/pages/add" method="post">
                @csrf
                <div class="form-group">
                    <label for="Title">Title</label>
                    <input required type="text" class="form-control" name="title" value="" id="Title" placeholder="Title">
                </div>
                <div class="form-group">
                    <label for="slug">Slug</label>
                    <input required type="text" class="form-control" name="slug" value="" id="slug" placeholder="slug">
                </div>
                <div class="form-group">
                    <label for="meta_description">Meta Description</label>
                    <input required type="text" class="form-control" name="meta_description" value="" id="meta_description" placeholder="Meta Description">
                </div>
                <div class="form-group">
                    <label for="content">Meta Description</label>
                    <textarea class="form-control" name="content" id="content" placeholder="content"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Create</button>
            </form>
        </div>
        <!-- /.card-body -->
    </div>
    <script>
        ClassicEditor
            .create( document.querySelector( '#content' ) )
            .catch( error => {
                console.error( error );
            } );
    </script>

@stop
