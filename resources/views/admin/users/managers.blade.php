@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Операторы</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <a class="btn btn-info mb-2" data-toggle="collapse" href="#collapseOpcreate" role="button" aria-expanded="false" aria-controls="collapseOpcreate">Создать оператора</a>

        <div class="collapse" id="collapseOpcreate">
            <div class="card card-body">
                <form method="post" action="{{ route('opcreate') }}" autocomplete="nope">
                    @csrf
                    <div class="form-group">
                        <label for="operator-username">Email</label>
                        <input type="email" class="form-control" name='email' id="operator-email" required
                               autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="operator-username">Username</label>
                        <input type="text" class="form-control" name='name' id="operator-username" required
                               pattern="^[a-zA-Z0-9_-]+$" title="Может содержать:
                                                                                                                                              - буквы;
                                                                                                                                              - цифры;
                                                                                                                                              - символы: -, _."
                               autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="operator-pwd">Пароль</label>
                        <input type="password" class="form-control" name='password' id="operator-pwd" minlength="6" required
                               autocomplete="new-password">
                    </div>
                    <div class="form-group">
                        <label for="operator-location">Локация</label>
                        <select class="form-control" id="operator-location" name="location" required>
                            <option value="0"> Все направления</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default">Создать</button>
                </form>
            </div>
        </div>

    </div>
    <!-- /.card-header -->
    @php
       // dd($users);
    @endphp
    <div class="card-body">
        @if (\Session::has('success'))
            <div class="alert alert-danger">
                <div>{!! \Session::get('success') !!}</div>
            </div>
        @endif
        <table id="all_users" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Email</th>
                <th>Name</th>
{{--                <th>Пароль</th>
                <th>Локация</th>--}}
                <th>Отключить</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->name }}</td>
                {{-- <td>{{ $user->password }}</td>
                <td>по {{ ($user->location == 1 ? "России" : "Украине") }}</td>
                --}}
                <td><input type="checkbox" {{ ($user->is_locked == 1 ? "checked" : "") }} onChange="window.location.href='/admin/users/{{ ($user->is_locked == 1 ? "unlock" : "lock") }}/{{ $user->id }}'"></td>
                <?php /* <td><a class="btn btn-warning"
                       href="/admin/users/{{ ($user->is_locked == 1 ? "unlock" : "lock") }}/{{ $user->id }}">
                        {{ ($user->is_locked == 1 ? "Разблокировать" : "Заблокировать") }}</a>
                </td>*/?>
                <td>
                    <a class="btn btn-info" href="/admin/users/edit/{{ $user->id }}">Редактировать</a>
                    <a onclick="return confirm('are you sure?')" class="btn btn-danger"
                       href="/admin/users/remove/{{ $user->id }}">Удалить</a>
                </td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <th>ID</th>
                <th>Email</th>
                <th>Name</th>
{{--                <th>Пароль</th>
                <th>Локация</th>--}}
                <th>Отключить</th>
                <th>Действия</th>
            </tr>
            </tfoot>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@stop
