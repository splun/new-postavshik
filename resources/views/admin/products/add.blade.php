@extends('adminlte::pagecustomer')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Редактирование категории</h1>
@stop

@section('content')

{{--@php dd($data); @endphp--}}

{{-- CKEditor CDN 
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>--}}

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ $data->name }}</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
                <form method="post" id="product-save" action="{{ route('product-add-send') }}" autocomplete="nope">
                    @csrf
                    <input type="hidden" class="form-control" name='category_id' id="category_id" value="{{ $data->id }}" required
                           autocomplete="off">
                    <div class="form-group">
                        <label for="operator-username">Name</label>
                        <input type="text" class="form-control" name='name' id="name" value="{{ $data->name }}" required
                               autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label for="parent">Родительская</label>
                        <select class="form-control" id="parent_id" name="parent_id" required>
                            <option value="" disabled>- выбрать -</option>
                            @foreach($parent as $cat)
                                <option @if ($data->parent_id == $cat->id)
                                    selected
                                    @endif value="{{ $cat->id }}">{{ $cat->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default">Изменить</button>
                </form>
        </div>
        <!-- /.card-body -->
    </div>
<script>
    ClassicEditor
        .create( document.querySelector( '#content' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

@stop
