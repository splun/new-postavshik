@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Товары</h1>
@stop

@section('content')
    {{-- CKEditor CDN --}}
    <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script> 

    <div class="card">
        <div class="card-header">
            <a class="btn btn-info mb-2" data-toggle="collapse" href="#collapseOpcreate" role="button"
               aria-expanded="false" aria-controls="collapseOpcreate">Создать Товар</a>

            <div class="collapse" id="collapseOpcreate">
                <div class="card card-body">
                    <div class="form-group">
                        <form method="POST" id="add_product" action="/admin/products/add" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="provider_id">Выбрать поставщика
                                    <select class="form-control"
                                            id="provider_id" name="provider_id" required>
                                        <option selected value="">- не отсортированное -</option>
                                        @foreach($providers as $prov)
                                            <option value="{{ $prov->id }}">{{ $prov->alias }}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="category_id">Выбрать категорию
                                    <select class="form-control"
                                            id="category_id" name="category_id" required>
                                            <option selected value="999">- не отсортированное -</option>
                                            <option value="8">РОЗПРОДАЖ</option>
                                            <option value="145">Новинки</option>
                                        @foreach($categories_w_p as $parent_cat)
                                            <optgroup label="{{ $parent_cat['parent_name'] }}">
                                                @foreach($parent_cat['child_cats'] as $cat)
                                                    <option value="{{ $cat->id }}">&nbsp;&nbsp;&nbsp;{{ $cat->name }}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="text" style="width:100%">Описание
                                    <textarea class="form-control" id="text" name="text"></textarea>
                                </label>
                            </div>
                            <div class="form-group">
                                <label for="text" style="width:100%">Описание
                                    <textarea class="form-control" id="description" name="description"></textarea>
                                </label>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="stock_number" style="width:100%">Артикул
                                            <input readonly class="form-control" id="stock_number" name="stock_number" value="{{ Str::upper(Str::random(8));}}" type="text">
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="price" style="width:100%">Цена (грн)
                                            <input  min="0" step="0.1" class="form-control" name="price" value="0" type="number">
                                        </label>
                                    </div>
                                 </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="old_price" style="width:100%">Старая цена (грн)
                                            <input min="0" step="0.1" class="form-control" name="old_price" value="0" type="number">
                                        </label>
                                    </div>
                                 </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="option_size" style="width:100%">Выбери Размеры
                                        <select multiple style="width:100%" class="js-select2 form-control"
                                                id="option_size" name="option_size[]">
                                            @foreach($options as $opt)
                                                @if($opt->option_name == 'size')
                                                <option value="{{ $opt->id }}">{{ $opt->option_value }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </label>
                                    <label for="new_option_sizes" style="width:100%">Не хватает? Добавь новые + </label>
                                    <textarea class="form-control" style="width:100%"  name="new_option_sizes"></textarea>
                                </div>
                                <div class="col-md-6">
                                    <label for="option_color" style="width:100%">Выбери Цвета
                                        <select multiple style="width:100%" class="js-select2 form-control"
                                                id="option_color" name="option_color[]">
                                            @foreach($options as $opt)
                                                @if($opt->option_name == 'color')
                                                <option value="{{ $opt->id }}">{{ $opt->option_value }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </label>
                                    <label for="new_option_colors" style="width:100%">Не хватает? Добавь новые + </label>
                                    <textarea class="form-control" style="width:100%" name="new_option_colors"></textarea>
                                </div>
                            </div>
                           {{--  <div class="form-group custom-file mb-4">
                                <input required type="file" name="product_preview" class="custom-file-input" id="exampleInputFile">
                                <label class="custom-file-label" for="exampleInputFile">Выберите фото для обложки</label>
                            </div> --}}
                            <br />
                            <label>Фото</label>
                            <div class="custom-file form-group">
                                <input required type="file" name="product_images[]" multiple class="custom-file-input" id="exampleInputFile2">
                                <label class="custom-file-label" for="exampleInputFile2">Выберите фото (перове фото будет обложкой)</label>
                            </div>
                            <div class="form-group mt-3 mb-5">
                                <input style="width:15px; height:15px;" name="not_removed" type="checkbox" value="1"> <strong>Не удалять товар!</strong>
                            </div>
                            <div>
                               <button type="submit" class="btn btn-primary">Создать</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            <div class="form-group">
                <form method="GET" id="change_category" action="/admin/products">
                    <label for="parent">Фильтр по категории</label>
                    <select onChange="$(this).parents('#change_category')[0].submit();" class="form-control js-select2"
                            id="parent_id" name="category_id" required>
 
                        <option selected value="999">- не отсортированное -</option>
                        <option @if($category_id == 8) {{ "selected" }} @endif value="8">- Распродажа -</option>
                        <option @if($category_id == 145) {{ "selected" }} @endif value="145">- Новинки -</option>

                        @foreach($categories_w_p as $parent_cat)
                            <optgroup label="{{ $parent_cat['parent_name'] }}">
                                @foreach($parent_cat['child_cats'] as $cat)
                                    <option @if($category_id == $cat->id) {{ "selected" }} @endif value="{{ $cat->id }}">&nbsp;&nbsp;&nbsp;{{ $cat->name }}</option>
                                @endforeach
                            </optgroup>
                        @endforeach
                    </select>
                    <label for="parent-2" style="margin-top:15px;">Фильтр по производителю</label>
                    <input class="form-control" onChange="$(this).parents('#change_category')[0].submit();" type="text" name="provider" value="{{ $provider }}">
                </form>
                <div style="margin-top:15px;">
                    <label>
                        <input type="checkbox" id="not_removed_checkbox" @if(request()->get('not_removed')) checked @endif>
                        Показать не удаляемые товары
                    </label>
                </div>
            </div>
            <p>Действия с выбранными:</p>
            <div class="action-buttons mb-3">
                <button class="btn btn-danger collapsed"
                        onclick="$('#collapse2').collapse('hide');" type="button" id="remove_provider"
                        data-toggle="collapse" data-target="#collapse1" aria-expanded="false"
                        aria-controls="collapse1">Переместить выделенные товары</button>
                <button class="btn btn-danger" onclick="$('#collapse1').collapse('hide');" type="button" id="remove_provider"
                        data-toggle="collapse" data-target="#collapse2" aria-expanded="false"
                        aria-controls="collapse2">Удалить выделенные товары</button>

            </div>

            <div class="collapse" id="collapse1">
                <div class="card card-body mt-2">

                    <p class="help-block">Все выделенные товары будут перемещены.</p>

                    <form method="POST" id="change_products_category" action="/admin/products/moveproduct">
                        @csrf
                        <input type="hidden" name="products_id" class="checked_products" value="">
                        <label for="parent">Переместить выбраные товары в
                            <select class="form-control" name="category_id" required>
                                <option selected value="999">- не отсортированное -</option>
                                @foreach($categories as $cat)
                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                @endforeach
                            </select>
                        </label>
                        <div>
                            <button class="btn btn-info" type="submit">Переместить</button>
                        </div>
                    </form>

                </div>
            </div>

            <div class="collapse" id="collapse2">
                <div class="card card-body mt-2">

                    <p class="help-block">Все выделенные товары будут удалены.</p>

                    <form method="POST" id="change_products_category" action="/admin/products/delete">
                        @csrf
                        <input type="hidden" name="products_id" class="checked_products" value="">
                        <div>
                            <button class="btn btn-info" type="submit">Удалить</button>
                        </div>
                    </form>

                </div>
            </div>

            <div style="margin-top:40px;"><strong>Найдено товаров: {{$results}}</strong></div>

            <table id="all_products" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>
                        <label for="select-all-products" class="checkbox-inline">
                            @if($selected_all == 1)
                                <input type="checkbox" checked="checked" id='select-all-products'placeholder="Все">
                            @else
                                <input type="checkbox" id='select-all-products'placeholder="Все">
                            @endif
                        </label>
                    </th>
                    <th aria-sort="descending">ID</th>
                    <th>Обложка</th>
                    <th>Поставщик</th>
                    <th>Категория</th>
                    <th>Модель</th>
                    <th>Stock number</th>
                    <th>Цена (грн)</th>
                    <th>Старая цена</th>
                    <th>Не удалять</th>
                    <th>Добавлено/Обновлено</th>
                    <th>Действия</th>
                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)
                    @php
                        $productText = mb_strtolower($product->text);
                        if (
                            preg_match('/мод\.?\s*(\d+)/i', $productText, $matches) ||
                            preg_match('/мод\s*(\d+)/i', $productText, $matches) ||
                            preg_match('/мод(\d+)/i', $productText, $matches) ||
                            preg_match('/модель\s*(\d+)/i', $productText, $matches)
                        ) {
                            $model = 'Мод. ' . $matches[1];
                        } else {
                            $model = '';
                        }
                    @endphp
                    <tr class="products-row" data-id="{{ $product->id }}">
                        <td>
                            <label>
                                @if($selected_all == 1)
                                    <input type="checkbox" checked="checked" class="prcheck" name="product_id[]" value="{{ $product->id }}">
                                @else
                                    <input type="checkbox" class="prcheck" name="product_id[]" value="{{ $product->id }}">
                                @endif
                            </label>
                        </td>
                        <td>{{ $product->id }}</td>
                        <td style="position:relative">
                            @php
                                $url = $product->base64 ? 'data:image/jpeg;base64,' . $product->base64 : $product->url;
                                $extension = pathinfo($url, PATHINFO_EXTENSION);
                            @endphp
                            
                            @if(in_array($extension, ['mp4','MP4']))
                                <span class="video-icon list-video-icon"><img src="/images/icons/video-icon.png" /></span>
                                <video width="120px" height="120px" class="img-fluid">
                                        <source src="{{ $url }}" type="video/{{ $extension }}">         
                                </video>
                            @else
                                <img width="120px" height="120px" src="{{ $url }}">
                            @endif
                            
                        </td>
                        <td>@if($product->provider)
                                {{$product->provider->alias}}
                            @endif</td>
                        <td>{{ $product->category->name }}</td>
                        <td>{{ $model }}</td>
                        <td>{{ $product->stock_number }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->old_price }}</td>
                        <td>{{ $product->not_removed }}</td>
                        <td>{{ $product->created_at }}</td>
                        <td>
                            <a class="btn btn-info" href="/admin/products/edit/{{ $product->id }}?selected_all={{$selected_all}}&page={{$page}}&provider={{($provider) ?? ''}}">Редактировать</a>
                            <a onclick="return confirm('are you sure?')" class="btn btn-danger" href="/admin/products/remove/{{ $product->id }}">Удалить</a>
                        </td>
                    </tr>
                    {{--                @break--}}
                @endforeach
                </tbody>
            </table>
            {{ $products->appends(Request::query())->render("vendor/pagination/bootstrap-4") }}          

        </div>
        <!-- /.card-body -->
    </div>
    <script>
        // ClassicEditor
        //     .create( document.querySelector( '#text' ) )
        //     .catch( error => {
        //         console.error( error );
        //     } );
        document.getElementById('not_removed_checkbox').addEventListener('change', function() {
            const baseUrl = window.location.origin + window.location.pathname;
            const url = new URL(baseUrl);
            if (this.checked) {
                url.searchParams.set('not_removed', '1');
            }
            window.location.href = url.toString();
        });
            
    </script>
    <style>
        .pagination{float:right;}
    </style>
@stop
