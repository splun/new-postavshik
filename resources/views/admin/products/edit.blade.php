@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Редактирование товара</h1>
@stop

@section('content')

{{--@php dd($data); @endphp--}}

{{-- CKEditor CDN 
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>--}}
<link rel="stylesheet" href="/vendor/fancybox/jquery.fancybox.min.css">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title" style="line-height: 40px;">Редактировать товар {{ $product->stock_number }}</h3>
             {{-- <a style="float: right;" href="#" onclick="javascript:history.back(); return false;" class="btn btn-info">Назад</a>--}}
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            <div class="row">
                <div class="col-md-12">
                    <form method="post" id="add_product" action="{{ route('product_save') }}" autocomplete="nope" enctype="multipart/form-data">
                        @csrf
                        <input name="product_id" type="hidden" value="{{ $product->id }}">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="provider_id" style="width:100%">Выбрать поставщика
                                    <select class="js-select2 form-control"
                                            id="provider_id" name="provider_id" required>
                                        <option value="">- не отсортированное -</option>
                                        @foreach($providers as $prov)
                                            <option @if(isset($product->provider->id) && $product->provider->id == $prov->id) {{ "selected" }} @endif value="{{ $prov->id }}">{{ $prov->alias }}</option>
                                        @endforeach
                                    </select>
                                </label>
                            </div>

                            <div class="col-md-6">
                                <label for="category_id" style="width:100%">Выбрать категорию
                                    <select class="js-select2 form-control"
                                            id="category_id" name="category_id" required>
                                            <option value="999">- не отсортированное -</option>
                                        @foreach($categories as $cat)
                                            @if($product->old_price > 0 && $product->category->id == 999)
                                                <option @if($cat->id == 8) {{ "selected" }} @endif value="{{ $cat->id }}">{{ $cat->name }}</option>
                                            @else
                                                <option @if($product->category->id == $cat->id) {{ "selected" }} @endif value="{{ $cat->id }}">{{ $cat->name }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="text" style="width:100%">Инфо
                                <textarea class="form-control" id="text" name="text">{{ $product->text }}</textarea>
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="text" style="width:100%">Описание
                                <textarea class="form-control" id="description" name="description">{{ $product->description }}</textarea>
                            </label>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label style="width:100%" for="stock_number">Артикул
                                    <input readonly class="disabled form-control" id="stock_number" name="stock_number" type="text" value="{{ $product->stock_number }}">
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label style="width:100%" for="price">Цена (грн)
                                    <input min="0" step="0.1" class="form-control" name="price" value="{{ $product->price }}" type="number">
                                </label>
                            </div>
                            <div class="col-md-4">
                                <label style="width:100%" for="old_price">Старая цена (грн)
                                    <input min="0" step="0.1" class="form-control" name="old_price" value="{{ $product->old_price }}" type="number">
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label for="option_size" style="width:100%">Выбери Размеры
                                    <select multiple class="js-select2 form-control"
                                            id="option_size" name="option_size[]">
                                        @foreach($options as $opt)
                                            @if($opt->option_name == 'size')
                                            <option @if(!empty($variants) && in_array($opt->id,$variants)) {{ "selected" }} @endif value="{{ $opt->id }}">{{ $opt->option_value }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                                <label for="new_option_sizes" style="width:100%">Не хватает? Добавь новые + </label>
                                <textarea class="form-control"  name="new_option_sizes"></textarea>
                            </div>
                            <div class="col-md-6">
                                <label for="option_color" style="width:100%">Выбери Цвета
                                    <select multiple class="js-select2 form-control"
                                            id="option_color" name="option_color[]">
                                        @foreach($options as $opt)
                                            @if($opt->option_name == 'color')
                                            <option @if(!empty($variants) && in_array($opt->id,$variants)) {{ "selected" }} @endif value="{{ $opt->id }}">{{ $opt->option_value }}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </label>
                                <label for="new_option_colors" style="width:100%">Не хватает? Добавь новые + </label>
                                <textarea class="form-control"  name="new_option_colors"></textarea>
                            </div>
                        </div>

                        {{-- <label>Фото для обложки</label>
                        <div class="form-group custom-file mb-4">
                            
                            <input type="file" name="product_preview" class="custom-file-input" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Выберите фото для обложки</label>
                        </div> --}}
                        <label>Добавить фото</label>
                        <div class="custom-file form-group">
                            
                            <input type="file" name="product_images[]" multiple class="custom-file-input" id="exampleInputFile2">
                            <label class="custom-file-label" for="exampleInputFile2"><span>Выберите дополнительные фото</span></label>
                        </div>
                        <div class="form-group mt-3 mb-5">
                            <input style="width:15px; height:15px;" name="not_removed" type="checkbox" value="1" 
                                    @if($product->not_removed == 1) checked @endif> 
                            <strong>Не удалять товар!</strong>
                        </div>
                        <div>
                            <input type="hidden" name="back_url" value="/admin/products/?category_id={{$product->category->id}}&provider={{request()->provider}}&selected_all={{ request()->selected_all }}&page={{request()->page}}">
                            <a href="/admin/products/?category_id={{$product->category->id}}&selected_all={{ request()->selected_all }}&page={{ request()->page}}" class="btn btn-info">Назад</a>
                            <button style="float: right;" type="submit" class="btn btn-info">Сохранить</button>
                            
                        </div>
                        
                    </form>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 mt-3 mb-5">
                    <hr />            
                    <h5>Изображения товара</h5>

                    <form class="mt-2" method="post" id="remove_image" action="{{ route('product_save') }}" autocomplete="nope">
                        @csrf
                        <input name="product_id" type="hidden" value="{{ $product->id }}">
                        <input type="hidden" id="image_to_unset" name="remove_image" value="0">
                        <div class="d-flex flex-row flex-wrap">
                            @foreach ($images as $image)
                                @if($image->preview == 1)
                                    <div id="image-{{ $image->id }}" class="product-images">
                                        <a href="#" onclick="remimage({{ $image->id }})" class="position-relative">
                                            <div class="" aria-hidden="true">Обложка &times;</div>
                                        </a>
                                        
                                        @php
                                            $url = $image->base64 ? 'data:image/jpeg;base64,' . $image->base64 : $image->url;
                                            $extension = pathinfo($url, PATHINFO_EXTENSION);
                                        @endphp
                                        
                                        @if(in_array($extension, ['mp4','MP4']))
                                            <a href="#" data-fancybox="gallery" data-src="{{ $url }}" class="position-relative fancybox">
                                                <span class="video-icon"><img src="/images/icons/video-icon.png" /></span>
                                                <video style="width:150px; height: 150px; object-fit: cover;margin:0 2px;" class="position-relative fancybox">
                                                <source src="{{ $url }}" type="video/{{ $extension }}">
                                            </video>
                                            </a>
                                        @else
                                            <a href="#" data-fancybox="gallery" data-src="{{ $url }} " class="position-relative fancybox">
                                                <img style="width:150px; height: 150px; object-fit: cover;margin:0 2px;" src="{{ $url }}" class="img-thumbnail" width="200px">
                                            </a>
                                        @endif

                                        <br>
                                        <button type="button" style="margin:0 2px;padding: 0.375rem 0.6rem;display:none;" class="btn btn-info mt-2" onclick="cover_image({{ $image->id }})">Сделать обложкой</button>
                                    </div>
                                @else
                                    <div id="image-{{ $image->id }}" class="product-images">
                                        <a href="#" onclick="remimage({{ $image->id }})" class="position-relative">
                                            <div class="" aria-hidden="true">&times;</div>
                                        </a>
                                        @php
                                            $url = $image->base64 ? 'data:image/jpeg;base64,' . $image->base64 : $image->url;
                                            $extension = pathinfo($url, PATHINFO_EXTENSION);
                                        @endphp
                                        @if(in_array($extension, ['mp4','MP4']))
                                            <a href="#" data-fancybox="gallery" data-src="{{ $url }}" class="position-relative fancybox">
                                                <span class="video-icon"><img src="/images/icons/video-icon.png" /></span>
                                                <video style="width:150px; height: 150px; object-fit: cover;margin:0 2px;" class="position-relative fancybox">
                                                <source src="{{ $url }}" type="video/{{ $extension }}">
                                            </video>
                                            </a>
                                        @else
                                            <a href="#" data-fancybox="gallery" data-src="{{ $url }} " class="position-relative fancybox">
                                                <img style="width:150px; height: 150px; object-fit: cover;margin:0 2px;" src="{{ $url }}" class="img-thumbnail" width="200px">
                                            </a>
                                        @endif
                                        <br>
                                    <button type="button" style="margin:0 2px;padding: 0.375rem 0.6rem;" class="btn btn-info mt-2" onclick="cover_image({{ $image->id }})">Сделать обложкой</button>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                    </form>
                    <form method="post" id="cover_image" action="{{ route('product_save') }}" autocomplete="nope">
                        @csrf
                        <input name="product_id" type="hidden" value="{{ $product->id }}">
                        <input type="hidden" id="image_to_cover" name="cover_image" value="0">
                    </form>
                </div>
            </div>

        </div>
        <!-- /.card-body -->
    </div>
    <script>
        function remimage(id) {

            event.preventDefault();

            if(confirm("Вы уверены что хотите удалить фото?")){

                $('#image_to_unset').val( id );
                let url = $('#remove_image').attr("action");
                let data = $('#remove_image').serialize();

                $.ajax({
                    url: url,         
                    method: 'post',
                    dataType: 'json',
                    data: data,
                    success: function(data){

                        if(data.status == "success"){

                            $("#image-"+data.id).remove();
                        }

                    }
                    
                });

                //$('#remove_image').submit();
            }else{
                return false;
            }
            
        }
        
        function cover_image(id) {

            $('#image_to_cover').val( id );

            let url = $('#cover_image').attr("action");
            let data = $('#cover_image').serialize();

            $.ajax({
                url: url,         
                method: 'post',
                dataType: 'json',
                data: data,
                success: function(data){

                    if(data.status == "success"){
                        $(".product-images button").show();
                        $("#image-"+data.id).find("button").hide();
                        $(".product-images .position-relative div").text("×");
                        $("#image-"+data.id+" .position-relative div").text("Обложка ×");
                        //$("#image-"+data.id).remove();
                    }

                }
                
            });
            //$('#cover_image').submit();
        }
        // ClassicEditor
        //     .create( document.querySelector( '#text' ) )
        //     .catch( error => {
        //         console.error( error );
        //     } );
          
    </script>
    <script defer="" src="/vendor/fancybox/jquery.fancybox.min.js"></script>
@stop
