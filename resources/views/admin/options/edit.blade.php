@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Редактирование категории</h1>
@stop

@section('content')

{{--@php dd($data); @endphp--}}

{{-- CKEditor CDN --}}
<script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>

    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ ($data->option_name == 'size') ? 'Размер' : 'Цвет'}} - {{ $data->option_value }}</h3>
            <a style="float: right;"  href="/admin/products_options/" class="btn btn-info">Назад</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            
                <form method="post" action="{{ route('save-option') }}" autocomplete="nope">
                    @csrf
                    
                    <input type="hidden" class="form-control" name='option_id' id="option_id" value="{{ $data->id }}" required
                           autocomplete="off">
                    <div class="form-group">
                        <label for="parent">Название опции*</label>
                        <select class="form-control" id="option_name" name="option_name">
                            <option value="" disabled>- выбрать -</option>
                                <option value="size" {{ ($data->option_name == 'size') ? 'selected' : ''}}>Размер</option>
                                <option value="color" {{ ($data->option_name == 'color') ? 'selected' : ''}}>Цвет</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="option_value">Значение*</label>
                        <input type="text" class="form-control" name="option_value" id="option_value" value="{{ $data->option_value }}">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Изменить</button>
                </form>
        </div>
        <!-- /.card-body -->
    </div>
<script>
    ClassicEditor
        .create( document.querySelector( '#content' ) )
        .catch( error => {
            console.error( error );
        } );
</script>

@stop
