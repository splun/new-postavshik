@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Опции товаров</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <a class="btn btn-info mb-2" data-toggle="collapse" href="#collapseOpcreate" role="button" aria-expanded="false" aria-controls="collapseOpcreate">Добавить опции</a>

        <div class="collapse" id="collapseOpcreate">
            <div class="card card-body">
                <form method="post" action="{{ route('add_option') }}" autocomplete="nope">
                    @csrf
                    <div class="form-group">
                        <label for="parent">Название опции*</label>
                        <select class="form-control" id="option_name" name="option_name">
                            <option value="" disabled>- выбрать -</option>
                                <option value="size">Размер</option>
                                <option value="color">Цвет</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="options_list">Список опций<strong>(каждый запись с новой строки)*</strong></label>
                        <br />
                        <textarea class="form-control" name="options_list" id="options_list" cols="30" rows="10"></textarea>
                    </div>
                    <button type="submit" class="btn btn-primary">Добавить</button>
                </form>
            </div>
        </div>
    </div>
    <!-- /.card-header -->
    @php
       // dd($data);
    @endphp
    <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
        <table id="all_users" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>ID</th>
                <th>Опция</th>
                <th>Значение</th>
                <th>Товаров использует</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $item)
            <tr>
                <td>{{ $item->id }}</td>
                <td>{{ ($item->option_name == 'size') ? 'Размер' : 'Цвет' }}</td>
                <td>{{ $item->option_value }}</td>
                <td><?php echo DB::table('product_variants')->where("option_id",$item->id)->count();?></td>
                <td>
                    <a class="btn btn-info" href="/admin/products_options/edit/{{ $item->id }}">Редактировать</a>
                    <a onclick="return confirm('are you sure?')" class="btn btn-danger"
                       href="/admin/products_options/remove/{{ $item->id }}">Удалить</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@stop
