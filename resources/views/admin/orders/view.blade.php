@extends('adminlte::page')

@section('title', 'Заказы')

@section('content_header')
    <h1>Заказ № {{ $order->id }}</h1>
@stop

@section('content')


    <div class="card">
        <div class="card-header">
            <a style="float: right;"  href="/admin/orders/" class="btn btn-info">Назад</a>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
                      

            <!-- <div>Verstaem Tyt</div> -->
            <div class="box-group" id="accordion">
                <div class="panel">
                    <div id="orderAccount" class="panel-collapse collapse in collapse show" aria-expanded="true">
                        <div class="box-body">
                            <div class="sale">
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Инофрмация о заказе</span>
                                    </div> 
                                    <div class="col-12 section-content">
                                        <div class="section-row">
                                            <span class="title">
                                                Дата заказа:
                                            </span> 
                                            <span class="value">
                                                {{ $order->created_at }}
                                            </span>
                                        </div> 
                                        <div class="section-row">
                                            <span class="title">
                                                Пользователь
                                            </span> 
                                            <span class="value">
                                               {{ $order->user->email }} ({{ $order->user->firstname.' '.$order->user->lastname }})
                                            </span>
                                        </div>
                                        <div class="section-row">
                                            <span class="title">
                                                Метод оплаты
                                            </span> 
                                            <span class="value">
                                               {{ ($order->payment_method == "Full" || $order->payment_method == "Fondy") ? "Повна оплата" : "Часткова оплата" }}
                                            </span>
                                        </div> 
                                        <div class="section-row">
                                            <span class="title">
                                                Валюта в корзине
                                            </span> 
                                            <span class="value">
                                                {{ $order->order_currency }}
                                            </span>
                                        </div>
                                        <div class="section-row">
                                            <span class="title">
                                                Статус
                                            </span> 
                                            <span class="value">
                                                {{ $order->status }}
                                                {{--<form method="POST" action="{{ route('status-update') }}">
                                                    @csrf
                                                    <select class="form-centrol" name="order_status" id="change_status">
                                                        <option {{ ($order->status == 'Ожидает оплаты') ? 'selected' : '' }} value="Ожидает оплаты">Ожидает оплаты</option>
                                                        <option {{ ($order->status == 'Оплачен') ? 'selected' : '' }} value="Оплачен">Оплачен</option>
                                                        <option {{ ($order->status == 'Оплачен (Аванс)') ? 'selected' : '' }} value="Оплачен (Аванс)">Оплачен (Аванс)</option>
                                                        <option {{ ($order->status == 'Отправлен/Завершен') ? 'selected' : '' }} value="Отправлен/Завершен">Отправлен/Завершен</option>
                                                        <option {{ ($order->status == 'Отменен') ? 'selected' : '' }} value="Отменен">Отменен</option>
                                                        <option {{ ($order->status == 'Возврат') ? 'selected' : '' }} value="Возврат">Возврат</option>
                                                    </select>
                                                    <input type="hidden" name="order" value="{{ $order->id }}" />
                                                </form> --}}
                                            </span>
                                        </div>
                                        <div class="section-row">
                                            <span class="title">
                                                Сумма
                                            </span> 
                                            <span class="value">
                                                {{ $order->price_total }} грн
                                            </span>
                                        </div> 
                                        @if($order->transaction_id)
                                        <div class="section-row">
                                            <span class="title">
                                                Номер транзакции
                                            </span> 
                                            <span class="value">
                                                {{ $order->transaction_id }}
                                            </span>
                                        </div>
                                        @endif
                                        @if($order->payment_id)
                                        <div class="section-row">
                                            <span class="title">
                                                Детали оплаты
                                            </span> 
                                            <span class="value">
                                                {{ $order->payment_id }}
                                            </span>
                                        </div>
                                        @endif
                                        <!-- @if($order->payment_id)
                                        <div class="section-row">
                                            <span class="title">
                                                Платежный код
                                            </span> 
                                            <span class="value">
                                                {{ $order->payment_id }}
                                            </span>
                                        </div>
                                        @endif -->
                                        @if($order->utm_source || $order->utm_campaign)
                                        <div class="section-row">
                                            <span class="title">
                                                Метки рекламы
                                            </span> 
                                            <span class="value">
                                                utm_source - {{ $order->utm_source }}<br />
                                                utm_campaign - {{ $order->utm_campaign }}
                                            </span>
                                        </div>
                                        @endif
                                    </div>
                                </div> 
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Информация о доставке</span>
                                    </div> 
                                    <div class="col-12 section-content">
                                        <div class="section-row">

                                            <span class="title">
                                                Метод доставки
                                            </span> 
                                            <span class="value">
                                                {{ $order->shipping_method }}
                                            </span>
                                        </div>
                                        <div class="section-row">
                                            <span class="title">
                                                Населенный пункт
                                            </span> 
                                            <span class="value">
                                                {{ $order->city }}
                                            </span>
                                        </div>

                                        {{--<div class="section-row">
                                            <span class="title">
                                                Индекс
                                            </span> 
                                            <span class="value">
                                                
                                            </span>
                                        </div> 
                                        <div class="section-row">
                                            <span class="title">
                                                Адресс
                                            </span> 
                                            <span class="value">
                                                
                                            </span>
                                        </div> --}}
                                        
                                        <div class="section-row">
                                            <span class="title">
                                                Почтовое отделение
                                            </span> 
                                            <span class="value">
                                                {{ $order->post_address }}
                                            </span>
                                        </div>
                                        <div class="section-row">
                                            <span class="title">
                                                ФИО получателя
                                            </span> 
                                            <span class="value">
                                                {{ $order->fio }}
                                            </span>
                                        </div>
                                        <div class="section-row">
                                            <span class="title">
                                                Контактний телефон
                                            </span> 
                                            <span class="value">
                                                {{ $order->phone }}
                                            </span>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel" style="margin-bottom:30px;">
                    <div id="Comment" class="panel-collapse collapse in collapse show" aria-expanded="true">
                        <div class="box-body">
                            <div class="sale">
                                <div class="sale-section">
                                    <div class="secton-title">
                                        <span>Комментарий к заказу</span>
                                    </div> 
                                    <div class="section-content" style="margin-top:5px;">
                                        <div class="section-row">
                                            <span class="value">
                                                {{ $order->comment }}
                                            </span>
                                        </div> 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div>                            
                @if($order->items)
                <div class="panel">
                    <div id="productsOrdered" class="panel-collapse collapse in collapse show" aria-expanded="true">
                        <div class="box-body">
                            <div class="table">
                                <div class="table-responsive">
                                    <table width="100%">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Артикул</th> 
                                                <th>Размер</th> 
                                                <th>Цвет</th> 
                                                <th>Стоимость</th> 
                                                <th>Количество</th> 
                                                <th>Сумма</th>
                                            </tr>
                                        </thead> 
                                        <tbody>
                                            @foreach($order->items as $item)

                                                @if($item->product)

                                                    <?php 
                                                        $options = unserialize($item->product_options);

                                                        if($options){
                                                            $size = ($options["size"]) ?? '---';
                                                            $color = ($options["color"]) ?? '---';
                                                        }else{
                                                            $size = '---';
                                                            $color = '---';
                                                        }
                                                        
                                                        $preview = App\Models\Products::get_preview($item->product->id);
                                                    ?> 
                                                    <tr>
                                                        
                                                        <td width="6%" class="text-left cart_img">
                                                            @if($preview )
                                                            <a data-src="#product-{{$item->product_id}}" target="_blank" href="/product/{{$item->product_id}}">
                                                                @if(strripos($preview->url, "storage"))
                                                                    <img class="img-fluid" src="{{$preview->url}}">
                                                                @else
                                                                    <img class="img-fluid" src="/imgurl?url={{ $preview->url }}">
                                                                @endif
                                                            </a>
                                                            @endif
                                                        </td>
                                                        <td>{{$item->product->stock_number}}</td> 
                                                        <td>{{$size}}</td> 
                                                        <td>{{$color}}</td> 
                                                        <td>{{$item->base_price}} грн</td> 
                                                        <td>{{$item->product_qty}}</td> 
                                                        <td>{{$item->price}} грн</td>
                                                    </tr> 

                                                @endif

                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <table class="sale-summary">
                                <tbody>
                                    <tr class="bold" style="font-size: 25px;">
                                        <td>Итого: {{ $order->price_total }} грн</td> 
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                @endif
            </div>

        </div>
        <!-- /.card-body -->
    </div>
@stop
