@extends('adminlte::page')

@section('title', 'Заказы')

@section('content_header')
    <h1>Заказы</h1>
@stop

@section('content')
<div class="card">

    <div class="card-body">
            @if (\Session::has('success'))
                <div class="alert alert-success">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif
            @if (\Session::has('error'))
                <div class="alert alert-danger">
                    <div>{!! \Session::get('success') !!}</div>
                </div>
            @endif

        <div class="form-group">
            <form method="GET" id="change_status" action="/admin/orders">
                <div class="row">
                <div class="col-md-4">
                    <label for="order_status">Статус оплаты</label>
                    <select onChange="" class="form-control js-select2"
                            id="order_status" name="order_status" required>
                        <option selected value="Оплачен">Все оплаченные</option>
                        <option {{ request()->order_status == 'Ожидает оплаты' ? 'selected' : ''}} value="Ожидает оплаты">Не оплаченные</option>
                        <option {{ request()->order_status == 'Обработка банком' ? 'selected' : ''}} value="Обработка банком">Обработка банком</option>
                    </select>
                </div>
                <div class="col-md-4">
                    <label for="order-date-range">Фильтр по дате</label>
                    <input class="form-control" id="order-date-range" onChange="" type="text" name="order_date_range" value="{{ request()->order_date_range ?? ''}}">
                </div>
                <div class="col-md-4">
                    <button style="position:absolute;bottom:0;" type="submit" class="btn btn-danger">Искать</button>
                </div>
                </div>
            </form>
            </div>
        <table id="all_users" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>ФИО</th>
                <th>Телефон</th>
                <th>Дата заказа</th>
                <th>Метод доставки</th>
                <th>Метод оплаты</th>
                <th>Сумма</th>
                <th>Статус</th>
                <th>utm_source</th>
                <th>utm_campaign</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            @foreach($orders as $order)
            <tr>
                <td>{{ $order->id }}</td>
                <td>{{ $order->fio }}</td>
                <td>{{ $order->phone }}</td>
                <td>{{ $order->created_at }}</td>
                <td>{{ $order->shipping_method }}</td>
                <td>{{ ($order->payment_method == "Full" || $order->payment_method == "Fondy") ? "Повна оплата" : "Часткова оплата" }}</td>
                <td>{{ $order->price_total }} грн</td>
                <td>{{ $order->status }}</td>
                <td>{{ $order->utm_source }}</td>
                <td>{{ $order->utm_campaign }}</td>
                <td>
                    <a class="btn btn-info" href="/admin/orders/view/{{ $order->id }}">Детали</a>
                    {{--<a onclick="return confirm('are you sure?')" class="btn btn-danger"
                       href="/admin/categories/remove/{{ $item->id }}">Удалить</a> --}}
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
@stop
