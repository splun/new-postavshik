<link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/fancybox/jquery.fancybox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/owl/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/owl/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/formstyler/jquery.formstyler.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/formstyler/jquery.formstyler.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/canvas-nav/hc-offcanvas-nav.css') }}">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <script src="{{ asset('/vendor/js/svg-inject.min.js') }}"></script>

<link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
<div class="container" id="chat">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Chatroom
                    <span class="badge pull-right">@{{ usersInRoom.length }}</span>
                </div>

                <chat-log :messages="messages"></chat-log>
                <chat-composer v-on:messagesent="addMessage"></chat-composer>
            </div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script src="{{ asset('/vendor/js/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendor/js/wow.min.js') }}"></script>
<script src="{{ asset('/vendor/js/TimelineMax.min.js') }}"></script>
<script src="{{ asset('/vendor/js/TweenMax.min.js') }}"></script>
<script defer src="{{ asset('/vendor/fancybox/jquery.fancybox.min.js') }}"></script>
<script defer src="{{ asset('/vendor/owl/owl.carousel.min.js') }}"></script>
<script defer src="{{ asset('/vendor/formstyler/jquery.formstyler.min.js') }}"></script>
<script src="{{ asset('/vendor/canvas-nav/hc-offcanvas-nav.js') }}"></script>

<script src="{{ asset('js/chat.js') }}"></script>