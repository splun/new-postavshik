@extends('layouts.app')

@section('content')
<div class="contents" id="cart">

    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item active" aria-current="page">Замовлення</li>
            </ol>
        </nav> 

        <section class="cart_wrapper">
            <div class="container">
                <div class="row">
                    
                @if(isset($items))
                    <div class="col-12 text-center page_head">
                        <h3 class="blue">Замовлення</h3>
                    </div>
                    <div id="cartTitle" class="col-12 checkout_title">
                        Кошик
                        <i class="fas fa-chevron-up"></i>
                    </div>
                    <div class="col-xl-12 cart_table">
                        <div class="row">
                            
                            @if (session('status'))
                                <div class="alert alert-success col-12">
                                    {{ session('status') }}
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif

                            <div class="col-12 blk_cart table-responsive d-none d-md-block">

                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th colspan="2" class="text-left">товар</th>
                                        <!-- <th width="15%" class="text-left">размер</th> 
                                        <th width="15%" class="text-left"></th>-->
                                        <th class="quant_prod text-left">кількість</th>
                                        <th width="15%" class="text-left">разом</th>
                                        <th width="6%" class="text-center">видалити</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($items as $item)
        
                                        <tr class="cart-item-row" data-id="{{$item['key']}}">
                                            <td width="9%" class="text-left cart_img">
                                                <a data-src="#product-{{$item['id']}}" href="/product/{{$item['id']}}">
                                                    @if(strripos($item['preview'], "storage"))
                                                        <img class="img-fluid" src="{{ $item['preview'] }}" alt="image">
                                                    @else
                                                        @if(!empty($item['preview']))
                                                        <img class="img-fluid" src="/imgurl?url={{ $item['preview'] }}" alt="image">
                                                        @endif
                                                    @endif
                                                </a>
                                            </td>
                                            <td class="text-left cart_prod">
                                                <p class="text_prod">Артикул: {{$item["stock_number"]}} 
                                                    <button class="btn copy-link-to-cart-item" onClick="copy_to_clipboard('https://test-postavshik.com.ua/product/{{$item['id']}}');">
                                                    <i class="far fa-copy"></i> Копіювати
                                                    </button>
                                                </p>
                                                
                                                <!--<a data-fancybox="" data-src="#product-{{$item['id']}}" href="javascript:;">подробнее</a> -->
                                                <!-- <a data-src="#product-{{$item['id']}}" href="/product/{{$item['id']}}">подробнее</a> -->
                                            
                                                <?php 
                                                    //@dump($product->text);
                                                    //$product->text = "Цена:5 $";
                                                    //$text_no_price = preg_replace('/Ціна:\s+.*?грн/is', ' ', $product->text);
                                                    $text_no_price = preg_replace('/Ціна:.*?грн|Ціна :.*?грн|Ціна:.*? грн|Ціна :.*? грн.|Ціна:.*? грн./is', ' ', $item["description"]);
                                                    $text_no_price = preg_replace('/Цена :.*? грн|Цена:.*?грн|Цена:.*?грн./is', ' ', $text_no_price);

                                                    $text_with_price = $text_no_price."\n Ціна: ".currency((float)$item['price'],"UAH",currency()->getUserCurrency());
                                                    
                                                ?>
                                                <div class="product-description">{{$text_with_price}}</div>
                                                <a class="product_more" href="javascript:void(0);">детальніше <i class="fa fa-chevron-down"></i></a>
                                                
                                                <div class="product-size-color">
                                                    @if($item['color'])
                                                        <strong>Колір: {{ $item['color'] }}</strong>
                                                    @endif
                                                    @if($item['size'])
                                                        <strong>Розмір: {{ $item['size'] }}</strong>
                                                    @endif
                                                </div>
                                            </td>
                                            <!-- <td class="text-left">
                                                <div class="size">
                                                    <input type="text" name="size[{{$item['id']}}]" value="" class="form-control">
                                                </div>
                                            </td>
                                            <td class="text-left">
                                                <p class="price"><span>{{ currency((float)$item["price"],"UAH",currency()->getUserCurrency()) }}</span></p>
                                            </td> -->
                                            <td class="text-left quant_prod">
                                                <div class="quantity">
                                                    <div class="chick"><input type="text"  name="quantity[{{$item['id']}}]" value="{{$item['quantity']}}" size="2" class="input-quantity form-control"></div>
                                                    <div class="quant_buttons d-flex">
                                                        <div class="plus"><a href="#" class="q_up"><svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" data-inject-url="http://94.250.252.160/assets/images/icons/quantity_up.svg"><path d="M1 5L5 2L9 5" stroke="#9C9898" stroke-width="2" stroke-linecap="round"></path></svg></a></div>
                                                        <div class="minus"><a href="#" class="q_down"><svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" data-inject-url="http://94.250.252.160/assets/images/icons/quantity_down.svg"><path d="M9 1L5 4L1 1" stroke="#9C9898" stroke-width="2" stroke-linecap="round"></path></svg></a></div>
                                                    </div>
                                                </div>
                                                <span>шт.</span>
                                            </td>
                                            <td class="text-left">
                                                <p class="price product-total-price" data-price="{{ currency((float)$item['price'],'UAH',currency()->getUserCurrency()) }}"><span>{{ currency((float)$item["total"],"UAH",currency()->getUserCurrency()) }}</span></p>
                                            </td>
                                            
                                            <td class="text-center remove delete-cart-item"><a href="/cart/delete/{{$item['key']}}"><img src="/images/icons/trash.svg" alt="trash"></a></td>
                                        </tr>

                                    @endforeach

                                    </tbody>
                                </table>
                                    
                            </div>
                            <!--mobile_cart-->
                            <div class="col-12 mob_cart d-block d-md-none">
                                @foreach($items as $item)

                                <div class="row cart-item-row" data-id="{{$item['key']}}">
                                    <div class="col-12 cart_top">
                                        <div class="row">
                                            <div class="col-3 d-flex pr-0 mob_img">
                                                <div class="cart_img">
                                                    <a data-src="#product-{{$item['id']}}" href="/product/{{$item['id']}}">
                                                        <img class="img-fluid" src="{{ $item['preview'] }}">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="col-5 d-flex">
                                                <div class="cart_prod">
                                                    <p class="text_prod" style="font-size:12px;">Артікул {{$item["stock_number"]}}</p>
                                                    {{--<a data-fancybox="" data-src="#product-{{ $item['stock_number'] }}" href="javascript:;">детальніше</a>--}}
                                                    <!-- <a data-src="#product-{{$item['id']}}" href="/product/{{$item['id']}}">детальніше</a> -->
                                                    <div class="product-size-color">
                                                        @if($item['color'])
                                                            <strong>Колір: {{ $item['color']}}</strong>
                                                        @endif
                                                        @if($item['size'])
                                                            <strong>Розмір: {{ $item['size']}}</strong>
                                                        @endif
                                                    </div>
                                                    <p class="price mb-0 product-total-price mt-2" data-price="{{ currency((float)$item['total'],'UAH',currency()->getUserCurrency()) }}">
                                                        <span>{{ currency((float)$item["total"],"UAH",currency()->getUserCurrency()) }}</span>
                                                    </p>
                                                    {{--<div style="display: none;" id="product-{{ $item['stock_number'] }}" class="mob_prod_info">
                                                        <?php 
                                                            $text_no_price = preg_replace('/Ціна:.*?грн|Ціна :.*?грн|Ціна:.*? грн|Ціна :.*? грн.|Ціна:.*? грн./is', ' ', $item["description"]);
                                                            $text_no_price = preg_replace('/Цена :.*? грн|Цена:.*?грн|Цена:.*?грн./is', ' ', $text_no_price);

                                                            $text_with_price = $text_no_price."\n Ціна: ".currency((float)$item['price'],"UAH",currency()->getUserCurrency());
                                                            
                                                        ?>
                                                        <div class="s_text">
                                                            {{$text_with_price}}
                                                        </div>
                                                    </div>--}}
                                                    
                                                </div>
                                            </div>
                                            <div class="col-2 d-flex pl-0">
                                                <div class="quantity">
                                                    <div class="chick"><input type="text" data-id="{{$item['id']}}" name="quantity[{{$item['id']}}]" value="{{$item['quantity']}}" size="2" class="input-quantity form-control"></div>
                                                    <div class="quant_buttons d-flex">
                                                        <div class="plus"><a href="#" class="q_up"><svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" data-inject-url="http://94.250.252.160/assets/images/icons/quantity_up.svg"><path d="M1 5L5 2L9 5" stroke="#9C9898" stroke-width="2" stroke-linecap="round"></path></svg></a></div>
                                                        <div class="minus"><a href="#" class="q_down"><svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" data-inject-url="http://94.250.252.160/assets/images/icons/quantity_down.svg"><path d="M9 1L5 4L1 1" stroke="#9C9898" stroke-width="2" stroke-linecap="round"></path></svg></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-2 d-flex justify-content-center">
                                                <div class="remove delete-cart-item"><a href="/cart/delete/{{$item['key']}}"><img src="/images/icons/trash.svg" alt="trash"></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 cart_btm">
                                        <div class="row">
                                            <!-- <div class="col-4 pr-0">
                                                <div class="size">
                                                    <input type="text" name="size[{{$item['id']}}]" value="" class="form-control" placeholder="размер">
                                                </div>
                                            </div> -->
                                            <div class="col-6 align-self-center">
                                            
                                            </div>
                                            <div class="col-6 text-right quant_prod pl-0">
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                @endforeach
                            </div>
                            <!--end mobile_cart-->
                        </div>
                    </div>

                    <div class="col-xl-12 cart_checkout">
                        <div class="row">
                            <form class="col-12 checkout_form" method="POST" action="{{ route('save-order') }}">
                                @csrf
                                <div id="checkRow" class="row">
                                    <div id="chkTitle" class="col-12 checkout_title">
                                        Контактні дані
                                        <i class="fas fa-chevron-up"></i>
                                    </div>
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="form-group col-lg-4">
                                                <label for="firstName">Ім'я*</label>
                                                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Ваше ім'я" required value="@if($current_user && $current_user->firstname) {{ $current_user->firstname }} @endif">
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="lastName">Прізвище*</label>
                                                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Ваше прізвище" required value="@if($current_user && $current_user->lastname) {{ $current_user->lastname }} @endif">
                                            </div>
                                            <div class="form-group col-lg-4">
                                                <label for="fatherName">По батькові*</label>
                                                <input type="text" class="form-control" id="fatherName" name="fatherName" placeholder="Як вас по батькові" required value="">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="youPhone">Контактний телефон*</label>
                                                <input type="tel" class="form-control" id="youPhone" name="phone" placeholder="Для зв'язку з вами" required value="@if($current_user && $current_user->phone) {{ $current_user->phone }} @endif">
                                            </div>
                                            <div class="form-group col-lg-6">
                                                <label for="youEmail">Електронна пошта*</label>
                                                <input type="email" class="form-control" id="youEmail" name="email" placeholder="Введіть вашу електронну пошту" required value="@if($current_user && $current_user->email) {{ $current_user->email }} @endif">
                                            </div>

                                            <div id="npCity" class="form-group col-lg-4">
                                                <label for="post">Населений пункт*</label>
                                                <input type="search" class="form-control" id="np-cities" name="np_c" placeholder="Вибрати місто" value="" autocomplete="false" required>
                                            </div>

                                            <div id="npPost" class="form-group col-lg-8">
                                                <label for="newpost">Номер відділення*</label>
                                                <select disabled="disabled" style="width: 100%" class="form-control w-search" name="np_post_address" id="np-post-number" required>
                                                    <option value="" selected disabled>Вибрати адресу та відділення:</option>
                                                </select>
                                            </div>

                                            <div id="upCity" class="form-group col-lg-4" style="display:none;">
                                                <label for="post">Населений пункт*</label>
                                                <input type="text" class="form-control" id="up-cities" name="up_c" placeholder="Вибрати місто" value="" autocomplete="adadf">
                                            </div>

                                            <div id="upPost" class="form-group col-lg-8" style="display:none;">
                                                <label for="newpost">Номер відділення*</label>
                                                <select style="width: 100%" class="form-control w-search" name="up_post_address" id="up-post-number">
                                                    <option value="" selected disabled>Вибрати адресу та відділення:</option>
                                                </select>
                                            </div>

                                            <?php /*<div id="ukrCode" class="form-group col-lg-4">
                                                <label for="postcode">Поштовий Індекс*</label>
                                                <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Введіть поштовий індекс" value="">
                                            </div>
                                            <div id="ukrAddr" class="form-group col-lg-4">
                                                <label for="address">Адреса*</label>
                                                <input type="text" class="form-control" id="address" name="address" placeholder="Введіть адресу" value="">
                                            </div> */?>
                                            
                                            <div class="col-md-6 pay_select">
                                                <label>Вибір оплати</label>
                                                <div class="form-radio" id="fondy-filed">
                                                    <input class="form-check-input radio" type="radio" name="payment_method" id="fondy" value="Full" checked>
                                                    <label class="form-check-label" for="fondy">
                                                        Онлайн-оплата карткою (Apple Pay, Google Pay)
                                                    </label>
                                                </div>
                                                <div class="form-radio" id="prepayment-filed">
                                                    <input class="form-check-input radio" type="radio" name="payment_method" id="prepayment" value="Prepayment">
                                                    <label class="form-check-label" for="prepayment">
                                                        Оплата при отриманні (обов'язковий аванс)
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 pay_select">
                                                <label>Вибір доставки</label>
                                                <div class="form-radio">
                                                    <input class="form-check-input radio" type="radio" name="shipping_method" id="NP" value="Нова Пошта" checked>
                                                    <label class="form-check-label" for="NP">
                                                        Нова Пошта
                                                    </label>
                                                </div>
                                                <div class="form-radio">
                                                    <input class="form-check-input radio" type="radio" name="shipping_method" id="UP" value="Укр Пошта">
                                                    <label class="form-check-label" for="UP">
                                                        Укр. Пошта
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="form-group col-12 mt-3 d-none">
                                                <label for="youArea">Коментар</label>
                                                <textarea type="email" class="form-control" id="youArea" name="comment" placeholder="Залиште свій коментар"></textarea>
                                            </div>

                                            <input type="hidden" name="user" value="@if($current_user) {{ $current_user->id }} @endif">
                                            <input type="hidden"  name="price_total" value="{{ (float)$total_price }}">
                                            <input type="hidden"  name="currency" value="{{ currency()->getUserCurrency() }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 blk_btn_cart">
                                        <div class="row">
                                            <div class="col-md-5" style="display:none;" id="prepayment_total_price">
                                                <p class="all_price mb-0 align-self-left">Предоплата: <span>{{ currency((float)config('settings.amount_prepayment'),'UAH',currency()->getUserCurrency()) }}</span></p>
                                                <p class="all_price mb-0 align-self-left" style="font-size:16px;">Залишок до сплати на відділенні : <span>{{ currency((float)$total_price-config('settings.amount_prepayment'),'UAH',currency()->getUserCurrency()) }}</span></p>
                                            </div>
                                            <div class="col-md-5 chat_btn" id="total_price">
                                                <p class="all_price mb-0 align-self-left">Всього до сплати: <span>{{ currency((float)$total_price,'UAH',currency()->getUserCurrency()) }}</span></p>
                                                <?php /* @if($current_user)
                                                    <a class="btn_all btn_blue hvr-sweep-to-top"  href="#" id="chat-order">заказать через чат</a>
                                                @else
                                                    <a class="btn_all btn_blue hvr-sweep-to-top" href="/login">заказать через чат</a>
                                                @endif */?>
                                            </div>
                                            <div class="col-md-7 d-flex justify-content-end cart_buttons">
                                                <a class="btn_cart btn_add hvr-sweep-to-top btn_hvr"  href="/category/8">У каталог</a>
                                                @if($current_user)
                                                    <button type="button" class="btn_cart btn_blue btn_black" id="btnSpoiler">замовити</button>
                                                @else
                                                    <a class="btn_cart btn_black" href="/login">авторізуватись</a>
                                                @endif
                                                

                                                <div id="btnOrder">
                                                    @if($current_user)
                                                        <button type="submit" class="btn_cart btn_black" value="замовити" id="chat-order">оформити</button>
                                                    @else
                                                        <a class="btn_cart btn_black" href="/login">авторізуватись</a>
                                                    @endif
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                        
                @else
                    <div class="col-12 text-center page_head">
                        <h3 class="blue">Кошик</h3>
                    </div>
                    <div class="col-12 text-center">
                        <p class="text-center">На жаль Ваш кошик порожній...</p>
                    </div>

                @endif

                </div>
            </div>
        </section>

        {{-- <div style="display: none;" id="more_content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 p-0 single_image">
                        <img class="img-fluid" src="assets/images/single_img.jpg" alt="image">
                    </div>
                    <div class="col-lg-6 single_info single_content align-self-center">
                        <ul class="list-unstyled">
                            <li class="s_num">0452</li>
                            <li class="s_article"><b>Артикул:</b> <span>D7ECDC0C</span></li>
                            <li class="s_category"><b>Категория:</b> <span>Платья</span></li>
                            <li class="s_price"><b>Цена:</b> <span>260 грн</span></li>
                            <li class="s_art_num"><b>Арт:</b> <span>009</span></li>
                            <li class="s_cloth"><b>Ткань:</b> <span>Мягенький трикотаж ангора</span></li>
                            <li class="s_dimensions"><b>Размеры:</b> <span>sm, lxl</span></li>
                            <li class="s_growth"><b>Рост на фото <span>170 см</span></b></li>
                        </ul>
                        <div class="s_text">
                            <p>Базовое чёрное платье с горлышком.
                                Идеально садится по фигуре 👌</p>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}

        @include("sections/instagram")

    </main><!--main-->

</div>
<script>
function copy_to_clipboard(copyLink) {
    var copytext = document.createElement('input');
    copytext.value = copyLink;
    document.body.appendChild(copytext);
    copytext.select();
    document.execCommand("copy");
    //alert("Ссылка на товар скопирвоана");
    document.body.removeChild(copytext);
}

</script>
@endsection