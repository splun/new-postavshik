@extends('layouts.app')

@section('content')

<div class="contents single-page">
    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
            </ol>
        </nav>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center page_head">
                        <h3 class="blue">{{$title}}</h3>
                    </div>
                    @if (session('message'))
                        <div class="alert alert-success col-12">
                            {{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                    <div class="col-12 contact_wrapper">
                        <div class="row row_wrapper">
                            <div class="col-12 contact_left">
                                {!! $content !!}  
                            </div>
                            {{-- <div class="col-md-6 contact_right">
                                <form action="/sendmail" name="contacts_form" method="POST">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input name="name" type="text" class="form-control effect" placeholder="Имя" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input name="email" type="email" class="form-control effect" placeholder="Email" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <textarea name="message" class="form-control effect" cols="20" rows="4" placeholder="Сообщение"></textarea>
                                        </div>
                                        <div class="col-12 block_btn">
                                            <div class="row">
                                                <div class="col-sm-6 check_box align-self-center">
                                                    <label class="checkbox">
                                                        <input name="not_robot" type="checkbox" required>
                                                        <div class="checkbox_text">I’m not a robot</div>
                                                    </label>
                                                </div>
                                                <div class="col-sm-6 submit_btn align-self-center">
                                                    <input type="submit" class="submit" value="отправить">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @include("sections/instagram")

    </main><!--main-->

</div>
@endsection