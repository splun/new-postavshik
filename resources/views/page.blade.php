@extends('layouts.app')

@section('content')

<div class="contents single-page">
    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$title}}</li>
            </ol>
        </nav>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center page_head">
                        <h1 class="blue">{{$title}}</h1>
                    </div>
                    <div class="col-12 order_text_wrapper page_text_wrapper">
                        {!! $content !!}  
                    </div>
                </div>
            </div>
        </section>

        @include("sections/instagram")

    </main><!--main-->

</div>
@endsection