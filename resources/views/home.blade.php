@extends('layouts.app')

@section('content')

{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> --}}
<div class="contents">

    <main>
        @if($slider)
        <div id="slider" class="owl-carousel owl-theme">
            @foreach($slider as $slide)
            <div class="owl-slide">
                <picture>
                    <source srcset="{{$slide->image_desktop}}" media="(min-width:768px)">
                    <source srcset="{{$slide->image_mobile}}" media="(max-width:767px)">
                    <img src="{{$slide->image_desktop}}">
                </picture>
            </div>
            @endforeach
        </div>
        @else
            <div class="owl-slide d-flex align-items-center cover" style="background-image: url({{ $banner }});">
                <div class="container">
                    <div class="row">
                        <div class="col-12 caption">
                            <h1>Новинки!</h1>
                            <p class="mid">Распродажи!</p>
                            <p class="bott mb-0">Успей сейчас!</p>
                            <a class="more slide_btn hvr-sweep-to-top" href="/category/145">подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        @endif

    {{--<section class="gray_bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 dropshiping_wrapper">
                        <div class="row">
                            <div class="col-md-4 dropshiping_images d-none d-md-block">
                                <img class="img-fluid" src="/images/cart_image.jpg" alt="cart">
                            </div>
                            <div class="col-md-8 dropshiping_text align-self-center">
                                {!! $about_text !!}  
                                <a class="read_more" href="/about">читать далее...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>--}}

        <section class="banners">
            <div class="container">
                <div class="row">
                    <div class="col-12 banner_text">
                        <h3>Про нас</h3>
                        {{--{!! $about_text !!}--}}
                        <p>
                            <span class="exerpt">Ласкаво просимо до нашого магазину жіночого одягу, де ми пропонуємо великий вибір товарів, щоденні новинки, розпродажі та дуже низькі ціни. Ми дбаємо про наших клієнтів з 2015 року та пишаємось безліччю позитивних відгуків. Наша основна команда співробітників не змінювалася протягом усього часу, що підтверджує наше чудове обслуговування та професіоналізм.</span>
                            <a class="read_more ml-2" href="/about">читати далі...</a>  
                        </p>
                    </div>
                    <div class="col-lg-6 col-12">
                        <div class="banner-left">
                            <div class="banner-figcaption">
                                <div class="banner-body">
                                    <div class="banner-title">Розпродаж</div>
                                    <div class="banner-text">Трендові та модні моделі за найвигіднішими цінами</div>
                                    <a class="btn_all btn_blue hvr-sweep-to-top" href="/category/145">Детальніше</a>
                                </div>
                            </div>
                            <div class="banner-figure">
                                <a href=""><img class="img-fluid" src="/images/block1.webp" alt="cart"></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 trand_left">
                        <div class="woman_tranding_title">Встигни зараз</div>
                        <div class="tranding_box row">
                            <div class="container-fluid">
                                <div class="row">

                                    @if($sale_products)

                                        @include("sections/product_wrapper",['products' => $sale_products,'type'=>'module']) 

                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="banners gray_bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 banner_text">
                        <h3>Співпраця</h3>
                        <div class="exerpt">
                            <p>Ми пропонуємо готову бізнес-модель, співпрацю за системою Дропшипінг.</p>
                            <p>Дропшипінг (пряме постачання) – це метод ведення бізнесу, при якому виробник товару довіряє цінності свого товару посереднику – дропшипперу. Посередник знаходить покупців товарів та послуг у них.</p>
                        </div>
                        <a class="read_more" href="/cooperation">читати далі...</a>
                    </div>
                    <div class="col-lg-6 col-12 trand_right order-lg-1 order-2">
                        <div class="kids_tranding_title">Тренди</div>
                        <div class="tranding_box row">
                            <div class="container-fluid">
                                <div class="row">
                                    @if($new_category_products)

                                        @include("sections/product_wrapper",['products' => $new_category_products,'type'=>'module']) 

                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-12 order-lg-2 order-1">
                        <div class="banner-right">
                            <div class="banner-figcaption">
                                <div class="banner-body">
                                    <div class="banner-title">Новинки</div>
                                    <div class="banner-text">Останні новинки цього тижня</div>
                                    <a class="btn_all btn_blue hvr-sweep-to-top" href="/category/145">Детальніше</a>
                                </div>
                            </div>
                            <div class="banner-figure">
                                <a href=""><img class="img-fluid" src="/images/block2.webp" alt="cart"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="featured_blk">
            <div class="container feature-product-home">
                <div class="row">
                    <div class="col-12 justify-content-center box-title">
                        Популярні продукти
                    </div>
                    @if($popular_products)

                        @include("sections/product_wrapper",['products' => $popular_products,'type'=>'popular']) 

                    @endif

                </div>
            </div>
        </section>


        {{--Discount Products Section--}}
        @if($discount_products)
        {{-- <section id="discount_products" class="home_products_list">
            <div class="container-fluid">
                <div class="row row_wrapper">
                    <div class="col-12 text-center head_block">
                        <h2 class="red">Акции</h2>
                    </div>
                    <div class="shares-items owl-carousel owl-theme">
                        @include("sections/product_wrapper",['products' => $discount_products,'type'=>'module'])
                    </div>
                    <div class="col-12 text-center btn_block">
                        <a class="btn_all btn_red hvr-sweep-to-top" href="/category/8">ВСЕ АКЦИИ</a>
                    </div>
                </div>
            </div>
        </section> --}}
        @endif

        @include("sections/instagram")

    </main><!--main-->

</div>
<div id="chat"></div>


<script>
    function aboutCollapse(el){
        if(!jQuery('.about-content').hasClass("collapsed")){
            jQuery('.about-content').fadeIn().addClass('collapsed');  
        }else{
            jQuery('.about-content').fadeOut().removeClass('collapsed');
        }
    }
</script>
@endsection
