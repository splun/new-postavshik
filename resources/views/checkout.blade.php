@extends('layouts.app')

@section('content')
<div class="contents" id="cart">

    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item active" aria-current="page">Замовлення</li>
            </ol>
        </nav> 

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center page_head">
                        <h3 class="blue">Замовлення</h3>
                    </div>

                    @if (session('status'))
                        <div class="alert alert-success col-12">
                            {{ session('status') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    @if(isset($items))

                    <?php /* Тут корзина если что, можно сбоку от формы поставить как на Анастезии только без выборов уже всяких
                    <div class="col-12 blk_cart table-responsive d-none d-md-block">

                        <table class="table table-bordered">
                            <thead>
                            <tr>
                                <th width="9%" class="text-left cart_img">малюнок</th>
                                <th class="cart_prod text-left">товар</th>
                                <!-- <th width="15%" class="text-left">размер</th> 
                                <th width="15%" class="text-left"></th>-->
                                <th width="15%" class="text-left">разом</th>
                                <th width="20%" class="quant_prod text-left">кількість</th>
                                <th width="6%" class="text-center">видалити</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($items as $item)
 
                                <tr class="cart-item-row" data-id="{{$item['id']}}">
                                    <td class="text-left cart_img">
                                        <a data-src="#product-{{$item['id']}}" href="/product/{{$item['id']}}">
                                            <img class="img-fluid" src="{{ $item['preview'] }}">
                                        </a>
                                    </td>
                                    <td class="text-left cart_prod">
                                        <p class="text_prod">Артикул: {{$item["stock_number"]}} 
                                            <button class="btn copy-link-to-cart-item" onClick="copy_to_clipboard('https://test-postavshik.com.ua/product/{{$item['id']}}');">
                                            <i class="far fa-copy"></i> Копіювати
                                            </button>
                                        </p>
                                        
                                        <!--<a data-fancybox="" data-src="#product-{{$item['id']}}" href="javascript:;">подробнее</a> -->
                                        <!-- <a data-src="#product-{{$item['id']}}" href="/product/{{$item['id']}}">подробнее</a> -->
                                       
                                        <?php 
                                            //@dump($product->text);
                                            //$product->text = "Цена:5 $";
                                            //$text_no_price = preg_replace('/Ціна:\s+.*?грн/is', ' ', $product->text);
                                            $text_no_price = preg_replace('/Ціна:.*?грн|Ціна :.*?грн|Ціна:.*? грн|Ціна :.*? грн.|Ціна:.*? грн./is', ' ', $item["description"]);
                                            $text_no_price = preg_replace('/Цена :.*? грн|Цена:.*?грн|Цена:.*?грн./is', ' ', $text_no_price);

                                            $text_with_price = $text_no_price."\n Ціна: ".currency((float)$item['price'],"UAH",currency()->getUserCurrency());
                                            
                                        ?>
                                        <div class="product-description">{{$text_with_price}}</div>
                                        <a class="product_more" href="javascript:void(0);">детальніше <i class="fa fa-chevron-down"></i></a>
 
                                    </td>
                                    <!-- <td class="text-left">
                                        <div class="size">
                                            <input type="text" name="size[{{$item['id']}}]" value="" class="form-control">
                                        </div>
                                    </td>
                                    <td class="text-left">
                                        <p class="price"><span>{{ currency((float)$item["price"],"UAH",currency()->getUserCurrency()) }}</span></p>
                                    </td> -->
                                    <td class="text-left">
                                        <p class="price product-total-price" data-price="{{ currency((float)$item['price'],'UAH',currency()->getUserCurrency()) }}"><span>{{ currency((float)$item["total"],"UAH",currency()->getUserCurrency()) }}</span></p>
                                    </td>
                                    <td class="text-left quant_prod">
                                        <div class="quantity">
                                            <div class="chick"><input type="text"  name="quantity[{{$item['id']}}]" value="{{$item['quantity']}}" size="2" class="input-quantity form-control"></div>
                                            <div class="quant_buttons d-flex">
                                                <div class="plus"><a href="#" class="q_up"><svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" data-inject-url="http://94.250.252.160/assets/images/icons/quantity_up.svg"><path d="M1 5L5 2L9 5" stroke="#9C9898" stroke-width="2" stroke-linecap="round"></path></svg></a></div>
                                                <div class="minus"><a href="#" class="q_down"><svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" data-inject-url="http://94.250.252.160/assets/images/icons/quantity_down.svg"><path d="M9 1L5 4L1 1" stroke="#9C9898" stroke-width="2" stroke-linecap="round"></path></svg></a></div>
                                            </div>
                                        </div>
                                        <span>шт.</span>
                                    </td>
                                    <td class="text-center remove delete-cart-item"><a href="/cart/delete/{{$item['id']}}"><img src="/images/icons/trash.svg" alt="trash"></a></td>
                                </tr>

                            @endforeach

                            </tbody>
                        </table>
                            
                    </div>
                    
                    <!--mobile_cart-->
                    <div class="col-12 mob_cart d-block d-md-none">
                        @foreach($items as $item)

                        <div class="row cart-item-row">
                            <div class="col-12 cart_top">
                                <div class="row">
                                    <div class="col-3 d-flex pr-0 mob_img">
                                        <div class="cart_img">
                                            <a data-src="#product-{{$item['id']}}" href="/product/{{$item['id']}}">
                                                <img class="img-fluid" src="{{ $item['preview'] }}">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-5 d-flex">
                                        <div class="cart_prod">
                                            <p class="text_prod" style="font-size:12px;">Артікул {{$item["stock_number"]}}</p>
                                            {{--<a data-fancybox="" data-src="#product-{{ $item['stock_number'] }}" href="javascript:;">детальніше</a>--}}
                                            <!-- <a data-src="#product-{{$item['id']}}" href="/product/{{$item['id']}}">детальніше</a> -->
                                            <p class="price mb-0 product-total-price mt-2" data-price="{{ currency((float)$item['total'],'UAH',currency()->getUserCurrency()) }}">
                                                <span>{{ currency((float)$item["total"],"UAH",currency()->getUserCurrency()) }}</span>
                                            </p>
                                            {{--<div style="display: none;" id="product-{{ $item['stock_number'] }}" class="mob_prod_info">
                                                <?php 
                                                    $text_no_price = preg_replace('/Ціна:.*?грн|Ціна :.*?грн|Ціна:.*? грн|Ціна :.*? грн.|Ціна:.*? грн./is', ' ', $item["description"]);
                                                    $text_no_price = preg_replace('/Цена :.*? грн|Цена:.*?грн|Цена:.*?грн./is', ' ', $text_no_price);

                                                    $text_with_price = $text_no_price."\n Ціна: ".currency((float)$item['price'],"UAH",currency()->getUserCurrency());
                                                    
                                                ?>
                                                <div class="s_text">
                                                    {{$text_with_price}}
                                                </div>
                                            </div>--}}
                                        </div>
                                    </div>
                                    <div class="col-2 d-flex pl-0">
                                        <div class="quantity">
                                            <div class="chick"><input type="text" data-id="{{$item['id']}}" name="quantity[{{$item['id']}}]" value="{{$item['quantity']}}" size="2" class="input-quantity form-control"></div>
                                            <div class="quant_buttons d-flex">
                                                <div class="plus"><a href="#" class="q_up"><svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" data-inject-url="http://94.250.252.160/assets/images/icons/quantity_up.svg"><path d="M1 5L5 2L9 5" stroke="#9C9898" stroke-width="2" stroke-linecap="round"></path></svg></a></div>
                                                <div class="minus"><a href="#" class="q_down"><svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" data-inject-url="http://94.250.252.160/assets/images/icons/quantity_down.svg"><path d="M9 1L5 4L1 1" stroke="#9C9898" stroke-width="2" stroke-linecap="round"></path></svg></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-2 d-flex justify-content-center">
                                        <div class="remove delete-cart-item"><a href="/cart/delete/{{$item['id']}}"><img src="/images/icons/trash.svg" alt="trash"></a></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 cart_btm">
                                <div class="row">
                                    <!-- <div class="col-4 pr-0">
                                        <div class="size">
                                            <input type="text" name="size[{{$item['id']}}]" value="" class="form-control" placeholder="размер">
                                        </div>
                                    </div> -->
                                    <div class="col-6 align-self-center">
                                       
                                    </div>
                                    <div class="col-6 text-right quant_prod pl-0">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        @endforeach
                    </div> <!--end mobile_cart-->
                    
                    */?>
                    
                    <form class="col-12 checkout_form">
                        <div class="checkout_title">Контактні дані</div>
                        <div class="row">
                            <div class="form-group col-lg-4">
                                <label for="firstName">Ім'я*</label>
                                <input type="text" class="form-control" id="firstName" placeholder="Ваше ім'я" required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="lastName">Прізвище*</label>
                                <input type="text" class="form-control" id="lastName" placeholder="Ваше прізвище" required>
                            </div>
                            <div class="form-group col-lg-4">
                                <label for="fatherName">По батькові*</label>
                                <input type="text" class="form-control" id="fatherName" placeholder="Як вас по батькові" required>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="youPhone">Контактний телефон*</label>
                                <input type="tel" class="form-control" id="youPhone" placeholder="Для зв'язку з вами" required>
                            </div>
                            <div class="form-group col-lg-6">
                                <label for="youEmail">Електронна пошта*</label>
                                <input type="email" class="form-control" id="youEmail" placeholder="Введіть вашу електронну пошту" required>
                            </div>
                        </div>
                    </form>

                    <div class="col-12 blk_btn_cart">
                        <div class="row">
                            <div class="col-md-6 chat_btn">
                            <p class="all_price mb-0 align-self-center">Всього до сплати: <span>{{ currency((float)$total_price,'UAH',currency()->getUserCurrency()) }}</span></p>
                                <?php /* @if($current_user)
                                    <a class="btn_all btn_blue hvr-sweep-to-top"  href="#" id="chat-order">заказать через чат</a>
                                @else
                                    <a class="btn_all btn_blue hvr-sweep-to-top" href="/login">заказать через чат</a>
                                @endif */?>
                            </div>
                            <div class="col-md-6 d-flex justify-content-end cart_buttons">
                                @if($current_user)
                                    <a class="btn_cart btn_blue hvr-sweep-to-top"  href="#" id="chat-order">замовити</a>
                                @else
                                    <a class="btn_cart btn_blue hvr-sweep-to-top" href="/login">замовити</a>
                                @endif
                                {{--<a class="btn_cart align-self-center" href="#">оформить заказ</a>--}}
                            </div>
                        </div>
                    </div>

                    @else

                    <div class="col-12 text-center">
                        <p class="text-center">На жаль Ваш кошик порожній...</p>
                    </div>

                    @endif
                </div>
            </div>
        </section>


        @include("sections/instagram")

    </main><!--main-->

</div>

@endsection