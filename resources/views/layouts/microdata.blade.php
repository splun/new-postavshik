@if(isset($product))
<?php 
if(isset($product_preview) && !empty($product_preview)){
    if(strripos($product_preview->url, "storage")){
        $preview_url = "https://postavshik.net".$product_preview->url;
    }else{
        $preview_url = "https://postavshik.net/imgurl?url=".$product_preview->url;
    }
}else{
    $preview_url = "https://postavshik.net";
}

$text_no_price = preg_replace('/Ціна:.*?грн|Ціна :.*?грн|Ціна:.*? грн|Ціна :.*? грн.|Ціна:.*? грн./is', ' ', $product->text);
$text_no_price = preg_replace('/Цена :.*? грн|Цена:.*?грн|Цена:.*?грн./is', ' ', $text_no_price);
?>

<meta property="og:title" content="{{ $product->stock_number }}">
<meta property="og:description" content="{{ $text_no_price }}">
<meta property="og:url" content="{{ \Request()->url() }}">
<meta property="og:image" content="{{ $preview_url }}">
<meta property="product:category" content="{{$product->category->name}}">
<meta property="product:brand" content="<?php echo (isset($product->provider->alias)) ? $product->provider->alias : "Postavshik";?>">
<meta property="product:availability" content="in stock">
<meta property="product:condition" content="new">
<meta property="product:price:amount" content="{{$product->price}}">
<meta property="product:price:currency" content="UAH">
<meta property="product:retailer_item_id" content="{{$product->id}}">
@else
<meta property="og:title" content="Postavshik.net - Магазин жіночого та чоловічого одягу. Постачальник одягу в Україні">
<meta property="og:description" content="Ласкаво просимо до нашого магазину жіночого одягу, де ми пропонуємо великий вибір товарів, щоденні новинки, розпродажі та дуже низькі ціни">
<meta property="og:url" content="{{ \Request()->url() }}">
<meta property="og:image" content="{{ $favicon }}">
@endif

