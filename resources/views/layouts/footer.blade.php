<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 block_wrapper">
                <div class="row">
                    <div class="col-sm-4 col-6">
                        <div class="footer_block">
                            <h6>Обслуговування клієнтів</h6>
                            <ul class="list-unstyled">
                                <li><a class="up" href="/order-info">Як замовити</a></li>
                                <li><a class="up" href="/track">Трек номера</a></li>
                                <li><a class="up" href="/cooperation">Співпраця</a></li>
                                <li><a class="up" href="/product_export">Вивантаження товару</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4 col-6">
                        <div class="footer_block second">
                            <h6>Информація</h6>
                            <ul class="list-unstyled">
                                <li><a class="up" href="/about">Про нас</a></li>
                                <li><a class="up" href="/comments">Відгуки</a></li>
                                <li><a class="up" href="/contacts">Контакти</a></li>
                                <li><a class="up" href="/privacy">Політика</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-4 col-12">
                        <div class="footer_block soc_wrapper">
                            <h6>Ми у соцмережах</h6>
                            <ul class="list-unstyled text-center">
                                <li>
                                    <ul class="list-unstyled d-flex">
                                    {{-- <li><a target="_blank" href="https://vk.com/maraphet_tm"><i class="fab fa-vk"></i></a></li>--}}
                                        <li><a target="_blank" href="https://www.instagram.com/postavshik_net/"><i class="fab fa-instagram"></i></a></li>
                                        <li><a target="_blank" href="https://t.me/postavshik_net"><i class="fab fa-telegram-plane"></i></a></li>
                                        <li><a target="_blank" href="https://www.tiktok.com/@postavshik.net?_t=ZN-8uOh4Li5UTW&_r=1"><i class="fab fa-tiktok"></i></a></li>
                                        <li><img class="mono-mobile" width="30px" height="30px" src="/images/monobang_acquiring.webp" /></li>
                                    
                                    </ul>
                                </li>
                                
                                {{-- <li>
                                    <ul class="socials list-unstyled d-flex">
                                        <li><a href="#"><img src="/images/socials/odnoklassniki.svg" alt="odnoklassniki"></a></li> 
                                        <li><a target="_blank" href="#"><img src="/images/socials/facebook.svg" alt="facebook"></a></li>
                                        <li><a target="_blank" href="#"><img src="/images/socials/youtube.svg" alt="youtube"></a></li>
                                        <li><a target="_blank" href="https://vk.com/maraphet_tm"><img src="/images/socials/instagram.svg" alt="instagram"></a></li>
                                    </ul>
                                </li> --}}
                            </ul>
                            <img class="mono" width="50px" height="50px" src="/images/monobang_acquiring.webp" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 text-center">
                <p class="copyright mb-0">2021 © Postavshik. Всі права захищені</p>
            </div>
        </div>
    </div>
</footer>

<div id="back-top" class="back-top" title="Back To Top" style="display: none;">
    <span>
        <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 512 512"><path d="M233.4 105.4c12.5-12.5 32.8-12.5 45.3 0l192 192c12.5 12.5 12.5 32.8 0 45.3s-32.8 12.5-45.3 0L256 173.3 86.6 342.6c-12.5 12.5-32.8 12.5-45.3 0s-12.5-32.8 0-45.3l192-192z"/></svg>
    </span>
</div>