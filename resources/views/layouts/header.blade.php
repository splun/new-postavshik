<div id="top" class="d-none d-lg-flex">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="d-flex align-items-center">
                    <div class="d-flex align-items-center">
                        <form id="set_currency" name="set_currency" method="GET" action="">
                            {{--<select class="d-none" name="currency" id="currency" onChange="$('#set_currency')[0].submit();">
                                @if (currency()->getCurrencies())
                                    @foreach(currency()->getCurrencies() as $cur) 
                                        <option date-rate="{{ $cur['exchange_rate'] }}" value="{{$cur['code']}}" {{ (currency()->getUserCurrency() == $cur['code']) ? "selected" : "" }}>{{$cur['code']}}</option>
                                    @endforeach
                                @endif
                            </select>--}}
                            <div class="currency_blk" id="currency_chooser">
                                <div class="active_currency">
                                    <span>{{ currency()->getUserCurrency() }}</span> <img src="/images/icons/arrow_down.svg" alt="icon" onload="SVGInject(this)">
                                </div>
                                <ul class="currency_menu">
                                    @if (currency()->getCurrencies())
                                        @foreach(currency()->getCurrencies() as $cur) 
                                            <li><button class="btn" type="button" date-rate="{{ $cur['exchange_rate'] }}" name="{{$cur['code']}}">{{$cur['code']}}</button></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <input type="hidden" name="currency" id="currency" value="{{ currency()->getUserCurrency() }}">
                        </form>
                        <div class="account">
                            <div>
                                @if($current_user)
                                    <a href="/logout"><i style="color:#ffffff" class="fas fa-sign-out-alt"></i>Вихід</a></div>
                                @else
                                    <a href="/login">
                                        <svg width="16" height="20" viewBox="0 0 16 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M1.3856 16.6504L1.38544 16.6502C1.13469 16.444 1 16.151 1 15.8558C1 13.7396 2.78499 11.9785 5.08479 11.9785H10.9232C13.2215 11.9785 15 13.7375 15 15.8558C15 16.1525 14.866 16.4437 14.6158 16.6492L14.6148 16.65C12.8375 18.1131 10.5283 19 8 19C5.47244 19 3.16378 18.1132 1.3856 16.6504ZM8.00333 1C10.2578 1 12.0215 2.75265 12.0215 4.8374C12.0215 6.9227 10.2579 8.67479 8.00333 8.67479C5.75032 8.67479 3.98651 6.92287 3.98651 4.8374C3.98651 2.75248 5.75041 1 8.00333 1Z" stroke="#ffffff" stroke-width="2"/>
                                        </svg> Мій аккаунт</a></div>
                                @endif
                        </div>
                    </div>
                    <div class="ml-auto">
                        <div class="free-shipping hidden-xs">
                            <div class="items d-flex align-items-center">
                                <div class="item item_text">Одяг від виробників</div>
                                <div class="item item_text">Останні модні тенденції</div>
                                <div class="item">
                                    <ul class="list-unstyled d-flex mb-0">
                                        <li><a target="_blank" href="https://www.instagram.com/postavshik_net/"><i class="fab fa-instagram"></i></a></li>
                                        <li><a target="_blank" href="https://t.me/postavshik_net"><i class="fab fa-telegram-plane"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<header id="Header">

    <div class="container">
        <div class="row">
            <div class="col-12">
                <nav class="navbar navbar-expand-lg navbar-white bg-white">
        
                    <div class="left_menu logo">
                        <a class="navbar-brand" href="/">Postavshik</a>
                        <!-- <a class="navbar-brand d-flex d-lg-none" href="/"><img class="img-fluid" src="/images/logo_mobile.svg" alt="logo"></a> -->
                    </div>
                    <div class="collapse navbar-collapse justify-content-md-center d-none d-lg-flex" id="navbar_menu">
                        <ul class="navbar-nav">
                            <li class="nav-item"><a href="/category/145" class="nav-link">Новинки</a></li>
                            <li class="nav-item shares active"><a class="nav-link" href="/category/8/">Розпродаж</a></li>

                            @foreach($parent_categories as $parent)
                        
                            
                            <li class="nav-item">
                                <a class="nav-link menu-parent" href="#">{{ $parent['parent_name'] }}<img src="/images/icons/arrow_down.svg" alt="icon" onload="SVGInject(this)"></a>
                                @if($parent['child_cats'])
                            
                                <ul class="submenu list-unstyled">
                                    
                                    @foreach($parent['child_cats'] as $cat) 

                                            <li><a href="/category/{{ $cat->id }}">{{ $cat->name }}</a></li>
                                    @endforeach
                                </ul>
                                
                                @endif

                            </li>
                            
                            @endforeach

                            <?php /* <li class="nav-item">
                                <a class="nav-link" href="/information">Инфо+</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/contacts">Контакты</a>
                            </li> */?>
                        </ul>
                    </div>
                    <div class="right_menu d-flex">

                        <form id="set_currency" name="set_currency" method="GET" action="" class="d-lg-none">
                            {{-- <select class="selectable" name="currency" id="currency" onChange="$('#set_currency')[0].submit();">
                                @if (currency()->getCurrencies())
                                    @foreach(currency()->getCurrencies() as $cur) 
                                        <option date-rate="{{ $cur['exchange_rate'] }}" value="{{$cur['code']}}" {{ (currency()->getUserCurrency() == $cur['code']) ? "selected" : "" }}>{{$cur['code']}}</option>
                                    @endforeach
                                @endif
                            </select>
                            <div class="currency_blk">
                                <div class="active_currency">
                                    <span>UAH</span> <img src="/images/icons/arrow_down.svg" alt="icon" onload="SVGInject(this)">
                                </div>
                                <ul class="currency_menu">
                                    <li><button class="btn" type="button" name="usd">USD</button></li>
                                    <li><button class="btn" type="button" name="uah">UAH</button></li>
                                </ul>
                            </div> --}}
                            <div class="currency_blk" id="currency_chooser">
                                <div class="active_currency">
                                    <span>{{ currency()->getUserCurrency() }}</span> <img src="/images/icons/arrow_down.svg" alt="icon" onload="SVGInject(this)">
                                </div>
                                <ul class="currency_menu">
                                    @if (currency()->getCurrencies())
                                        @foreach(currency()->getCurrencies() as $cur) 
                                            <li><button class="btn" type="button" date-rate="{{ $cur['exchange_rate'] }}" name="{{$cur['code']}}">{{$cur['code']}}</button></li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                            <input type="hidden" name="currency" id="currency" value="{{ currency()->getUserCurrency() }}">
                        </form>
                        
                        <ul class="list_menu list-unstyled d-flex mb-0 ml-auto">
                            <li>

                            <!-- mobile search -->
                            <div class="search_mobile d-flex align-items-center">
                                <button type="button"><img src="/images/icons/glass.svg" alt="search" onload="SVGInject(this)" alt="search"><span class="d-none d-lg-block">Пошук</span></button>
                                <div class="search-box">
                                    <div class="searchform">
                                        <form role="search" method="get" id="searchform_mobile" action="{{route('search')}}">
                                            <div class="search-navigation">
                                                <input type="search" value="" name="s" id="form-s-mob" class="form-s" placeholder="Пошук..." data-min-chars="3" autocomplete="off">
                                                <button type="submit" id="searchsubmit-mobile"><img src="/images/icons/glass.svg" alt="search" onload="SVGInject(this)" alt="search"></button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- end mobile search -->
                            
                            </li>
                        
                            @if($is_admin)
                            <li>
                                <a target="_blank" href="/admin"><i style="color:#9C9898" class="fas fa-code"></i></a></li>
                            </li>
                            @endif

                            @if($is_operator)
                            <li>
                                @if($count_unread_message > 0)
                                    <a target="_blank" href="/operator/chat">
                                        <span class="newmsg"> {{ $count_unread_message }} </span>
                                        <i style="display:inline;" style="color:#2d8ce3" class="fas fa-envelope"></i>
                                    </a>
                                @else
                                    <a target="_blank" href="/operator/chat"><i style="color:#9C9898" class="fas fa-envelope"></i></a>
                                @endif    
                            </li>
                            @endif
                            
                            <li class="d-lg-none">
                                @if($current_user)
                                    <a href="/logout"><i style="color:#878787" class="fas fa-sign-out-alt"></i></a></li>
                                @else
                                    <a href="/login"><img src="/images/icons/man.svg" alt="user"></a></li>
                                @endif
                            
                            <li>
                                <a class="cart {{ $mini_cart }}" href="/cart/"><img src="/images/icons/cart.svg" alt="cart"><span class="d-none d-lg-block">Кошик</span></a>
                            </li>
                        </ul>
                    </div>
                    <div class="wrapper cf d-block d-lg-none">

                        <nav id="main-nav">

                            <ul class="first-nav">

                                <li class="nav-item"><a href="/category/145" class="nav-link">Новинки</a></li>
                                <li class="nav-item shares active"><a class="nav-link" href="/category/8/">Розпродаж</a></li>
                                
                                    @foreach($parent_categories as $parent) 
                                
                                        <li class="nav-item">
                                            <a href="#">{{ $parent['parent_name'] }}</a>
                                            @if($parent['child_cats'])
                                        
                                            <ul>
                                                
                                                @foreach($parent['child_cats'] as $cat) 

                                                        <li><a href="/category/{{ $cat->id }}">{{ $cat->name }}</a></li>

                                                @endforeach
                                            </ul>
                                            
                                            @endif

                                        </li>
                                    
                                    @endforeach
                                </ul>
                                <?php /*<li><a href="/information">Инфо+</a></li> */?>
                                <li><a href="/cooperation">Сотрудничество</a></li>
                                <li><a href="/order-info">Как заказать</a></li>
                                <li><a href="/about">О нас</a></li>
                                <li><a href="/track">Трек номера</a></li>
                                <li><a href="/review">Отзывы</a></li>
                                <?php /* <li><a href="/contacts">Контакты</a></li>  */?>
                            </ul> 

                        </nav>

                        <a class="toggle">
                            <span></span>
                        </a>

                    </div>
                </nav>
            </div>
        </div>
    </div>
    
</header><!--header-->
