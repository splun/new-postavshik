<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">


    @include('layouts.microdata')

    <title>{{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="Вітаємо! Ми — інтернет-магазин жіночого одягу Postavshik, який з 2015 року пропонує широкий асортимент стильного та якісного одягу за доступними цінами." />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Comforter+Brush&family=Lato:wght@100;300;400;700;900&family=Poppins:wght@100;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@900&family=Yeseva+One&display=swap" rel="stylesheet">
    <link rel="shortcut icon" href="{{ $favicon }}" type="image/x-icon">
    <!-- Bootstrap -->

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/fancybox/jquery.fancybox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/owl/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/owl/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/formstyler/jquery.formstyler.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/formstyler/jquery.formstyler.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/vendor/canvas-nav/hc-offcanvas-nav.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.2/emojionearea.min.css">
    <link href="{{ asset('css/chatify/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/chatify/light.mode.css') }}" rel="stylesheet" />
    <link rel='stylesheet' href='https://unpkg.com/nprogress@0.2.0/nprogress.css'/>

    <?php /* <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.2/emojionearea.min.css"> */?>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.css" />

    <link href="{{ asset('css/app.css') }}?ver='<?php echo filemtime('css/app.css')?>" rel="stylesheet">

    <script src="{{ asset('/vendor/js/svg-inject.min.js') }}"></script>
    


    @if(config('app.env') != "local")
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-Q0G1C0HF63"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'G-Q0G1C0HF63');
    </script>
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','G-Q0G1C0HF63');</script>
    <!-- End Google Tag Manager -->


    <!-- Meta Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '633795445386386');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=633795445386386&ev=PageView&noscript=1"
      /></noscript>
      <!-- End Meta Pixel Code -->

    @endif
</head>
<body>

  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MFPDZRJ7"
  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <!-- End Google Tag Manager (noscript) -->

    @include('layouts.header')

    @yield("content")

    @include('layouts.footer')

</body>

<!-- Scripts -->
<script src="{{ asset('/vendor/js/jquery.min.js') }}"></script>
<script src="{{ asset('/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/vendor/js/wow.min.js') }}"></script>
<script src="{{ asset('/vendor/js/TimelineMax.min.js') }}"></script>
<script src="{{ asset('/vendor/js/TweenMax.min.js') }}"></script>
<script src="{{ asset('/vendor/js/jquery.zoom.min.js') }}"></script>
<script defer src="{{ asset('/vendor/fancybox/jquery.fancybox.min.js') }}"></script>
<script defer src="{{ asset('/vendor/owl/owl.carousel.min.js') }}"></script>
<script defer src="{{ asset('/vendor/formstyler/jquery.formstyler.min.js') }}"></script>
<script src="{{ asset('/vendor/select2/select2.min.js') }}"></script>
<script src="{{ asset('/vendor/canvas-nav/hc-offcanvas-nav.js') }}"></script>
<script src="{{ asset('/vendor/js/bootstrap-notify.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/corejs-typeahead/1.2.1/bloodhound.min.js"></script>
<script src="{{ asset('/vendor/js/typeahead.js') }}"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script>

<?php /* <script src="{{ asset('/vendor/js/jquery.filedrop.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/emojionearea/3.4.2/emojionearea.min.js"></script> */?>
<script src="{{ asset('js/app.js') }}?ver='<?php echo filemtime('js/app.js')?>" defer></script>

@include("sections/widget-chat")

</html>
