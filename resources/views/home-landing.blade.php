@extends('layouts.app')

@section('content')

{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div> --}}
<div class="contents">

    <main>
        <div id="slider" class="owl-carousel owl-theme">
            <div class="owl-slide d-flex align-items-center cover" style="background-image: url(/images/slide_image.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-12 caption">
                            <h1>Новинки!</h1>
                            <p class="mid">Распродажи!</p>
                            <p class="bott mb-0">Успей сейчас!</p>
                            <a class="more slide_btn hvr-sweep-to-top" href="/category/145">подробнее</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="gray_bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 dropshiping_wrapper">
                        <div class="row">
                            <div class="col-md-4 dropshiping_images d-none d-md-block">
                                <img class="img-fluid" src="/images/cart_image.jpg" alt="cart">
                            </div>
                            <div class="col-md-8 dropshiping_text align-self-center">
                                <p>Мы предлагаем готовую бизнес модель, сотрудничество по системе Дропшиппинга. <b>Дропшиппинг</b> (прямая поставка) – это метод ведения бизнеса, при котором производитель товара доверяет реализацию своего товара посреднику – дропшипперу. Посредник находит покупателей товара и принимает от них оплату. Создав интернет магазин в соц сети или же на другой платформе, выгружаете наш товар со своей наценкой (в ВК, ОК и FB это можно сделать автоматически нашим сервисом). Принимаете оплату от вашего клиента, после того заказываете у нас нужную вещь и мы отправляем ее на ваш адрес.</p>
                                <p><b>С нами вы сможете работать напрямую без посредников с производителями одежды Турции, Китая, Украины.</b></p>
                                <a class="read_more" href="/about">читать далее...</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{--New Products Section--}}
        @if($new_products)
            <section id="new_products" class="home_products_list">
                <div class="container-fluid">
                    <div class="row row_wrapper">
                        <div class="col-12 text-center head_block">
                            <h2 class="blue">Новинки</h2>
                        </div>
                        <div class="new-items owl-carousel owl-theme">
                            @include("sections/product_wrapper",['products' => $new_products,'type'=>'module'])               
                        </div>
                        <div class="col-12 text-center btn_block">
                            <a class="btn_all btn_blue hvr-sweep-to-top" href="/category/145">ВСЕ НОВИНКИ</a>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        <section>
            <div class="container-fluid">
                <div class="row row_wrapper">
                    <div class="col-lg-6 banner_left">
                        <div class="row">
                            <div class="col-12 banner_block">
                                <img class="img-fluid d-none d-sm-block" src="/images/banner1.jpg" alt="banner">
                                <img class="img-fluid d-block d-sm-none" src="/images/banner1_mob.jpg" alt="banner">
                                <div class="banner_info banner1">
                                    <p data-text="ЧАСЫ">ЧАСЫ</p>
                                    <a class="btn_all btn_lyellow hvr-sweep-to-top" href="/category/125">подробнее</a>
                                </div>
                            </div>
                            <div class="col-12 banner_block">
                                <img class="img-fluid d-none d-sm-block" src="/images/banner2.jpg" alt="banner">
                                <img class="img-fluid d-block d-sm-none" src="/images/banner2_mob.jpg" alt="banner">
                                <div class="banner_info banner2">
                                    <p data-text="TV shop">TV shop</p>
                                    <a class="btn_all btn_lred hvr-sweep-to-top" href="/category/146">подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 banner_right">
                        <div class="row">
                            <div class="col-12 banner_block">
                                <img class="img-fluid" src="/images/banner3.jpg" alt="banner">
                                <div class="banner_info banner3">
                                    <p class="dt1" data-text="размеры">размеры</p>
                                    <p class="dt2" data-text="48+">48+</p>
                                    <a class="btn_all btn_lblue hvr-sweep-to-top" href="/category/15">подробнее</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{--Discount Products Section--}}
        @if($discount_products)
            <section id="discount_products" class="home_products_list">
                <div class="container-fluid">
                    <div class="row row_wrapper">
                        <div class="col-12 text-center head_block">
                            <h2 class="red">Акции</h2>
                        </div>
                        <div class="shares-items owl-carousel owl-theme">
                            @include("sections/product_wrapper",['products' => $discount_products,'type'=>'module'])
                        </div>
                        <div class="col-12 text-center btn_block">
                            <a class="btn_all btn_red hvr-sweep-to-top" href="/category/8">ВСЕ АКЦИИ</a>
                        </div>
                    </div>
                </div>
            </section>
        @endif

        @include("sections/instagram")

    </main><!--main-->

</div>
<div id="chat"></div>

@endsection
