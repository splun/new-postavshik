@extends('layouts.app')

@section('content')

<script>
fbq('track', 'Purchase', { 
   value: {{$total}},
   currency: 'UAH',
   contents:{!! $fb_purchase_items !!},
   content_type: 'product'
});
</script>
<div class="contents" id="cart">

    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item active" aria-current="page">Замовлення</li>
            </ol>
        </nav> 

        <section class="cart_wrapper">
            <div class="container">
                <div class="row">

                    <div class="col-12 text-center page_head">
                        <h3 class="blue">Дякуємо за замовлення! #{{$order_id}}</h3>
                    </div>
                    <div class="col-12 text-center">
                        <p class="text-center">Найближчим часом замовлення буде оброблено автоматично та відправлено на вказане відділення. </p>
                        <p class="text-center">Після відправлення замовлення, ваш номер ТТН буде розміщений у розділі - <a href="/track">Трек номери</a></p>
                        <p style="font-size: 12px;" class="text-center">Якщо на складі виникнуть труднощі з вашим товаром, ми обов'язково зв'яжемося з вами у нашому чаті</p>
                    </div>

                </div>
            </div>
        </section>

        @include("sections/instagram")

    </main><!--main-->

</div>
@endsection