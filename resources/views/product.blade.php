@extends('layouts.app')
@section('content')
<script>
fbq('track', 'ViewContent', { 
   content_type: "product",
   content_ids: [{{$product->id}}],
   content_name: "<?php echo ($product->provider) ? $product->provider->alias. '-' .$product->stock_number :  $product->stock_number; ?>",
   content_category: "<?php echo isset($product->category) ? $product->category->name : ''; ?>",
   value: {{$product->price}},
   currency: 'UAH'
});
</script>
<link rel="stylesheet" href="https://cdn.plyr.io/3.7.8/plyr.css" />
<script src="https://cdn.plyr.io/3.7.8/plyr.polyfilled.js"></script>
<div class="contents">
    <main>
        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item"><a href="#">Жінки</a></li>
                @if($product->category)
                <li class="breadcrumb-item " aria-current="page"><a href="/category/{{$product->category->id}}">{{$product->category->name}}</a></li>
                @endif
                <li class="breadcrumb-item active" aria-current="page">{{$product->stock_number}}</li>
            </ol>
        </nav>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12 single_wrapper single-product">
                        <div class="row">
                            @php
                                $productImagesArray = $product_images->toArray();

                                usort($productImagesArray, function($a, $b) {

                                    $aExtension = pathinfo($a->url, PATHINFO_EXTENSION);
                                    $bExtension = pathinfo($b->url, PATHINFO_EXTENSION);

                                    $aIsVideo = in_array($aExtension, ['mp4','MP4']);
                                    $bIsVideo = in_array($bExtension, ['mp4','MP4']);

                                    return $bIsVideo <=> $aIsVideo;
                                });
                            @endphp

                            <div class="col-lg-6">
                                @if($current_user && ($current_user->permission == "admin" || $current_user->permission == "operator"))
                                    <div class="product_delete">
                                        <a title="Delete" onclick="return confirm('Точно удалить??')" href="/admin/products/remove/{{ $product->id }}?return_url=/category/{{$product->category->id}}"><i class="fas fa-times"></i></a>
                                    </div>
                                @endif
                                @if(!empty($productImagesArray))
                                    <div class="swiper mySwiper2">
                                        <div class="swiper-wrapper">
                                            @foreach($productImagesArray as $image)
                                            <div class="swiper-slide">
                                                @if($product->old_price)
                                                    <div class="sales">
                                                        -{{ round((($product->old_price - $product->price) * 100) / $product->old_price)  }}%
                                                    </div>
                                                @elseif($product->is_popular >= $min_popular_rate && $product->category->id !== 8)
                                                    <div class="hits">хіт</div>
                                                @endif
                                                <div><a class="zoomImg" style="cursor:default" href="javascript:void(0);">
                                                    @php
                                                        $mediaUrl = $image->base64 ? 'data:image/jpeg;base64,' . $image->base64 : $image->url;
                                                        $extension = pathinfo($mediaUrl, PATHINFO_EXTENSION);
                                                        $isVideo = in_array($extension, ['mp4','MP4']);
                                                    @endphp
                                                    @if($isVideo)
                                                    <div class="video-container">
                                                        <video class="plyr__video-embed" id="player" preload="metadata" playsinline controls style="width:100% height:600px;">
                                                            <source src="{{ $mediaUrl }}" type="video/{{ $extension }}">
                                                            Ваш браузер не поддерживает видео.
                                                        </video>
                                                        <svg class="custom-play-button" width="47" height="53" viewBox="0 0 47 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M45 23.0357C47.6667 24.5753 47.6667 28.4243 45 29.9639L6.75 52.0475C4.08333 53.5871 0.75 51.6626 0.75 48.5834L0.750002 4.41612C0.750002 1.33692 4.08334 -0.587579 6.75 0.952022L45 23.0357Z" fill="white" fill-opacity="0.5"></path>
                                                        </svg>
                                                    </div>
                                                    @else
                                                        <img class="img-fluid" src="{{ $mediaUrl }}" alt="image">
                                                    @endif
                                                </a></div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="swiper-button-next"></div>
                                        <div class="swiper-button-prev"></div>
                                    </div>
                                    <div class="swiper mySwiper">
                                        <div class="swiper-wrapper">
                                            @foreach($productImagesArray as $image)
                                            <div class="swiper-slide">
                                                @php
                                                    $mediaUrl = $image->base64 ? 'data:image/jpeg;base64,' . $image->base64 : $image->url;
                                                    $extension = pathinfo($mediaUrl, PATHINFO_EXTENSION);
                                                    $isVideo = in_array($extension, ['mp4','MP4']);
                                                @endphp
                                                <div class="video-thumb">
                                                    @if($isVideo)
                                                       <video id="videothumb-{{ $loop->index }}" preload="metadata" crossorigin="anonymous">
                                                            <source src="{{ $image->url }}" type="video/mp4">
                                                            Ваш браузер не поддерживает видео.
                                                        </video> 
                                                        <canvas id="canvas-{{ $loop->index }}" style="display: none;"></canvas>
                                                        <img class="img-fluid" id="thumbnail-{{ $loop->index }}" style="display: none;" alt="image">                                        
                                                    @else
                                                        <img class="img-fluid" src="{{ $mediaUrl }}" alt="image">
                                                    @endif
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        <div class="swiper-button-next"></div>
                                        <div class="swiper-button-prev"></div>
                                    </div>
                                    <script>
                                        document.addEventListener('DOMContentLoaded', function() {
                                            if (/Mobi|Android/i.test(navigator.userAgent)) {
                                                const swiperVideos = document.querySelectorAll('.mySwiper video');
                                                swiperVideos.forEach((video, index) => {
                                                    video.addEventListener('loadedmetadata', function() {
                                                        video.currentTime = 0; // Используем 0, чтобы получить миниатюру с начала
                                                    });

                                                    video.addEventListener('seeked', function() {
                                                        const canvas = document.getElementById('canvas-' + index);
                                                        const ctx = canvas.getContext('2d');
                                                        canvas.width = video.videoWidth;
                                                        canvas.height = video.videoHeight;
                                                        ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

                                                        const thumbnail = document.getElementById('thumbnail-' + index);
                                                        thumbnail.src = canvas.toDataURL('image/jpeg');

                                                        video.style.display = 'none';
                                                        canvas.style.display = 'none';
                                                    });

                                                    video.addEventListener('canplaythrough', function() {
                                                        if (video.readyState >= 2) {
                                                            video.currentTime = 0;
                                                        }
                                                    });
                                                });
                                            }
                                        });
                                    </script>
                                @else
                                    <a class="zoomImg" style="cursor:default" href="javascript:void(0);">
                                        <img class="img-fluid" src="{{ Config::get('app.products_image_path').'/'.$product->category_id.'/'.$product->id }}.jpg" alt="image">
                                    </a>
                                @endif

                            </div>
                            
                            <div class="col-lg-6 single_info">
                                @if($current_user && ($current_user->permission == "admin" || $current_user->permission == "operator"))
                                    <div class="text-right" >
                                        <a href="/admin/products/edit/{{$product->id}}/?return_url=/product/{{$product->id}}">Редагувати</a>
                                    </div>
                                @endif
                                <ul class="list-unstyled">
                                    <li>
                                        <a class="copy_product_link first_product_link" style="line-height: 20px;padding-top:10px;padding-bottom:10px;cursor: pointer;display: inline-block;font-size: 13px;" data-toggle="tooltip" data-placement="top" title="Copy product Link" onclick="copy_to_clipboard();"><i class="far fa-copy"></i>&nbsp;Скопіювати посилання на товар </a>
                                    </li>
                                    @if($current_user)
                                    <li>
                                        <a class="copy_product_link" style="line-height: 20px;padding-top:10px;cursor: pointer;display: inline-block;font-size: 13px;" onclick="downloadMedia();"><i class="far fa-copy"></i>&nbsp;Вигрузити фото та відео</a>
                                    </li>
                                    <li>
                                        <a class="copy_product_link" style="line-height: 20px;padding-bottom:10px;cursor: pointer;display: inline-block;font-size: 13px;" onclick="copyDescription();"><i class="far fa-copy"></i>&nbsp;Скопіювати опис товару</a>
                                    </li>
                                    @endif
                                    <li><b>Добавлено:</b> <span>{{$product->created_at}}</span></li>
                                    <li class="s_article"><b>Артикул:</b> <span> 
                                        @if($product->provider)
                                            {{$product->provider->alias}}-{{$product->stock_number}}                
                                        @else
                                            {{$product->stock_number}}
                                        @endif
                                    </span></li>
                                    <li class="s_category"><b>Категорія:</b> <span>{{$product->category->name}}</span></li>
                                </ul>
                                <div class="main_info">
                                    <?php 
                                    $text_no_price = preg_replace('/Ціна:.*?грн|Ціна :.*?грн|Ціна:.*? грн|Ціна :.*? грн.|Ціна:.*? грн./is', ' ', $product->text);
                                    $text_no_price = preg_replace('/Цена :.*? грн|Цена:.*?грн|Цена:.*?грн./is', ' ', $text_no_price);
                                    ?>
                                    {!! str_replace("\r\n","<br />", str_replace("\r\n\r\n","<p></p>", $text_no_price)) !!}
                                </div>
                                <div class="s_text">
                                    {!! str_replace("\r\n","<br />", str_replace("\r\n\r\n","<p></p>", $product->description)) !!}
                                </div>
                                <button type="button" class="btn btn-link showmore"><span>показати більше</span> <i class="fas fa-chevron-down"></i></button>
                                <div class="prod_price">
                                    <strong>Ціна: {{ currency((float)$product->price,"UAH",currency()->getUserCurrency()) }}</strong>
                                    @if($product->old_price)
                                        <span class="old_price">{{ currency((float)$product->old_price,"UAH",currency()->getUserCurrency()) }}</span></p>
                                    @endif
                                </div>
                                @php
                                    $sizes = [];
                                    $colors = [];
                                @endphp
                                @if(!empty($options))
                                <div class="select_block pt-3">
                                    @foreach($options as $key => $option)
                                        <div class="form-group">
                                        @if($key === "size")
                                            <label class="control-label" for="input-option-{{ $key }}"><span>*</span>Розмір</label>
                                            <br />
                                            @foreach($option as $opt_id => $val)
                                                <label class="radios">
                                                    <input type="radio" name="product-{{ $key }}" class="input-option-{{ $key }}" value="{{ $opt_id }}">
                                                    <div class="radio__text">{{ $val }}</div>
                                                </label>
                                                @php
                                                    $sizes[] = $val;
                                                @endphp
                                            @endforeach
                                        @endif
                                        @if($key === "color")
                                            <label class="control-label" for="input-option-{{ $key }}"><span>*</span>Колір</label>
                                            <select name="product-{{ $key }}" id="input-option-{{ $key }}" class="form-control" required>
                                                <option value="" selected disabled> --- Вибрати  Колір--- </option>
                                                @foreach($option as $opt_id => $val)
                                                    <option value="{{ $opt_id }}">{{ $val }}</option>
                                                    @php
                                                        $colors[] = $val;
                                                    @endphp
                                                @endforeach
                                            </select>
                                        @endif
                                        </div>
                                    @endforeach
                                </div>
                                @endif
                                <div class="button_single_wrap row">
                                    <div class="col-6 mb-3">
                                        <a href="#" 
                                        data-id="{{$product->id}}" 
                                        data-fbname="<?php echo ($product->provider) ? $product->provider->alias. '-' .$product->stock_number :  $product->stock_number; ?>" 
                                        data-fbcategory="{{$product->category->name}}" 
                                        data-fbprice="{{$product->price}}" 
                                        class="add-to-cart btn_add {{ (!empty($options)) ? 'disabled' : '' }}">
                                            <img src="/images/icons/cart.svg" alt="cart" onload="SVGInject(this)" />
                                            &nbsp;&nbsp;У кошик
                                        </a>
                                    </div>
                                    @if(!$current_user)
                                    <div class="col-6 mb-3">
                                        <a href="/login" class="btn_all btn_blue hvr-sweep-to-top">авторизуватися</a>
                                    </div>
                                    @endif
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        @include("sections/instagram")
    </main><!--main-->
</div>
@php
    $sizesString = implode(', ', $sizes);
    $colorsString = implode(', ', $colors);
@endphp
@endsection
<script>
function copy_to_clipboard() {
    var copytext = document.createElement('input');
    copytext.value = window.location.href ;
    document.body.appendChild(copytext);
    copytext.select();
    document.execCommand("copy");
    alert("Посилання на товар скопійовано");
    document.body.removeChild(copytext);
}

async function downloadMedia() {
    var index = 1;
    for (let image of @json($product_images)) {
        let url;
        let isStorage = false;

        if (image.base64) {
            url = 'data:image/jpeg;base64,' + image.base64;
        } else {
            url = image.url;
            if (url.includes('storage')) {
                isStorage = true;
            }
        }

        let extension = url.split('.').pop();
        try {
            if (extension === 'mp4' || extension === 'MP4') {
                await downloadFile(url, 'video_' + index + '.' + extension);
            } else {
                await downloadFile(url, 'image_' + index + '.' + extension);
            }
        } catch (error) {
            console.error('Error downloading file:', error);
        }
        index++;
        await new Promise(resolve => setTimeout(resolve, 1000)); 
    }
}

function downloadFile(url, filename) {
    fetch(url)
        .then(response => {
            if (response.ok) {
                return response.blob();
            }
            throw new Error('Network response was not ok.');
        })
        .then(blob => {
            const blobUrl = URL.createObjectURL(blob);
            const a = document.createElement('a');
            a.href = blobUrl;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            URL.revokeObjectURL(blobUrl);
        })
        .catch(error => console.error('Error downloading file:', error));
}

function copyDescription() {

    var props = `{!! $text_no_price !!}`;
    var additionalInfo = `Колір: {{ $colorsString }}
Розмір: {{ $sizesString }}
Ціна: {{ (int)$product->price }} грн`;
    var description = `{!! $product->description !!}`;

    if (!description.trim()) {
        if (!/(\r\n|\n|\r){2}/.test(props)) {
            props += '\n\n';
        }
        updatedTextNoPrice = props.replace(/(\r\n|\n|\r){2}/, `\n${additionalInfo}\n\n`);
        var text = `Артикул: {{ $product->provider ? $product->provider->alias . '-' . $product->stock_number : $product->stock_number }} 
${updatedTextNoPrice}`;
    } else {
        var text = `Артикул: {{ $product->provider ? $product->provider->alias . '-' . $product->stock_number : $product->stock_number }} 
${props}\n${additionalInfo}\n\n${description}`;
    }


    var textarea = document.createElement('textarea');
    textarea.value = text;
    document.body.appendChild(textarea);
    textarea.select();
    document.execCommand("copy");
    document.body.removeChild(textarea);
    alert('Опис товара скопійовано');
    //alert(text);
}


//Video
document.addEventListener('DOMContentLoaded', () => {
    @foreach($productImagesArray as $image)
        @if(in_array(pathinfo($image->url, PATHINFO_EXTENSION), ['mp4', 'MP4']))
            const video{{ $loop->index }} = document.getElementById('videothumb-{{ $loop->index }}');
                if (video{{ $loop->index }}.paused) {
                     if (window.innerWidth < 768) {
                        video{{ $loop->index }}.setAttribute('autoplay', '');
                        setTimeout(() => {
                            video{{ $loop->index }}.removeAttribute('autoplay');
                        }, 000);
                    }
                }
        @endif
    @endforeach
});

document.addEventListener('DOMContentLoaded', () => {
    const videoContainers = document.querySelectorAll('.video-container');

    videoContainers.forEach((container, index) => {
        const video = container.querySelector('.plyr__video-embed');
        const playButton = container.querySelector('.custom-play-button');

        if (video) {
            const player = new Plyr(video);
            if (index === 0) {
                player.play();
            }

            playButton.addEventListener('click', () => {
                playButton.classList.add('hidden');
                player.play();
            });

            player.on('play', () => {
                playButton.classList.add('hidden');
            });
            player.on('pause', () => {
                playButton.classList.remove('hidden');
            });
            player.on('ended', () => {
                playButton.classList.remove('hidden');
            });
        }
    });
});

// document.addEventListener('DOMContentLoaded', () => {
    //     if (window.innerWidth < 768) {
    //         console.log('Mobile device detected');
    //         @foreach($productImagesArray as $image)
    //             @if(in_array(pathinfo($image->url, PATHINFO_EXTENSION), ['mp4','MP4']))
    //                 const video{{ $loop->index }} = document.getElementById('video-{{ $loop->index }}');
    //                 const canvas{{ $loop->index }} = document.getElementById('canvas-{{ $loop->index }}');
    //                 const poster{{ $loop->index }} = document.getElementById('poster-{{ $loop->index }}');
                    
    //                 video{{ $loop->index }}.addEventListener('loadeddata', () => {
    //                     console.log(`Video data loaded for video {{ $loop->index }}`);
    //                     if (video{{ $loop->index }}.videoWidth > 0) {
    //                         canvas{{ $loop->index }}.width = video{{ $loop->index }}.videoWidth;
    //                         canvas{{ $loop->index }}.height = video{{ $loop->index }}.videoHeight;
    //                         const context = canvas{{ $loop->index }}.getContext('2d');
    //                         context.drawImage(video{{ $loop->index }}, 0, 0, canvas{{ $loop->index }}.width, canvas{{ $loop->index }}.height);
    //                         poster{{ $loop->index }}.src = canvas{{ $loop->index }}.toDataURL('image/jpeg');
    //                         poster{{ $loop->index }}.style.display = 'block';
    //                     } else {
    //                         console.error(`Video width is 0 for video {{ $loop->index }}`);
    //                     }
    //                 });

    //                 video{{ $loop->index }}.addEventListener('error', (e) => {
    //                     console.error(`Error loading video {{ $loop->index }}:`, e);
    //                 });

    //                 video{{ $loop->index }}.load(); // Загрузить видео
    //             @endif
    //         @endforeach
    //     }
    // });
</script>