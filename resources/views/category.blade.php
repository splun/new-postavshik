@extends('layouts.app')

@section('content')

<div class="contents">

    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <?php /* <li class="breadcrumb-item">Женщинам</li> */?>
                <li class="breadcrumb-item active" aria-current="page">{{ $category->name }}</li>
            </ol>
        </nav>
    
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center page_head">
                        <h3 class="blue">{{ $category->name }}</h3>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">

					<!--desktop buttons-->

                    <div class="col-12 control_wrapper d-none d-md-block">
                        <div class="row">
                            <div class="col-lg-9 ml-auto d-flex">
                                <div class="leftblock_control d-flex">
                                    <p class="mb-0">Усього товару <span>{{ $products->total() }}</span></p>
                                </div>
                                <div class="rightblock_control ml-auto d-flex align-items-center">
                                    <button type="button" onclick="window.location.href='?new=1'" class="btn_control btn_control_big blue {{ (!request()->price && !request()->discount && !request()->popular) ? 'active' : '' }}">Нове</button>
                                    <?php /* <button type="button" onclick="window.location.href='?discount=1'" class="btn_control btn_control_big red {{ (request()->has('discount')) ? 'active' : '' }}">Скидка</button> */?>
                                    <button type="button" onclick="window.location.href='?popular=1'" class="btn_control btn_control_big blue {{ (request()->has('popular')) ? 'active' : '' }}"><img class="star" src="/images/icons/star.svg" onload="SVGInject(this)">Популярне</button>
                                    <p class="mb-0 rightblock_price">Ціна</p>
                                    <button type="button" onclick="window.location.href='?price=desc'"  class="btn_control btn_control_small {{ (request()->price == 'desc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_up.svg" onload="SVGInject(this)"></button>
                                    <button type="button" onclick="window.location.href='?price=asc'"  class="btn_control btn_control_small {{ (request()->price == 'asc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_down.svg" onload="SVGInject(this)"></button>
                                </div>
                            </div>
                        </div>
                        
                    </div>

					<!--end desktop buttons-->

					<!--mobile buttons-->

                    <div class="col-12 control_wrapper d-flex d-md-none">
                        <div class="d-flex mob_block_control">
                            <div class="leftblock_control align-self-center">
                                <p class="mb-0">Усього товару <span>{{ $products->total() }}</span></p>
                            </div>
                            <div class="rightblock_control ml-auto d-flex align-self-center">
                                <p class="mb-0">Ціна</p>
                                <button type="button" onclick="window.location.href='?price=desc'"  class="btn_control btn_control_small {{ (request()->price == 'desc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_up.svg" onload="SVGInject(this)"></button>
                                <button type="button" onclick="window.location.href='?price=asc'"  class="btn_control btn_control_small {{ (request()->price == 'asc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_down.svg" onload="SVGInject(this)"></button>
                            </div>
                        </div>
                        <div class="d-flex">
                            <button type="button" onclick="window.location.href='?discount=1'" class="ml-0 btn_control btn_control_big red {{ (request()->has('discount') == '1') ? 'active' : '' }}">Розпродаж</button>
                            <button type="button" onclick="window.location.href='?popular=1'" class="ml-auto btn_control btn_control_big blue {{ (request()->has('popular') == '1') ? 'active' : '' }}"><img class="star" src="/images/icons/star.svg" onload="SVGInject(this)">Популярне</button>
                        </div>
                    </div>

					<!--end mobile buttons-->

                    @include("sections/sidebar")

                    <div class="col-lg-9 catalogue_wrapper">
                        <div class="row">

                            @include("sections/product_wrapper",['products' => $products,'type'=>'catalog']) 

                        </div>

                    </div>
                </div>
            </div>

            <!-- <div class="container mt-5">
                <div class="row">
                    <div class="col-12 text-center page_head">
                        <br /><br /><br />
                        <p>{{ $category->description }} {{--Летние платья и сарафаны - это лучший вариант одежды в жаркую погоду, когда хочется чувствовать себя легко, уютно, но в тоже время женственно. В отличие от популярных интернет-магазинов, наш каталог предлагает великолепный выбор продукции по минимальным ценам. В жаркие летние дни все девушки переодеваются в модные платья и сарафаны, купить которые можно в самых разнообразных вариантах исполнения. --}}</p>
                    </div>
                </div>
            </div> -->
            
            <div class="container">
                <div class="row">        
                    <div class="col-12 justify-content-center pagination">
                        {{ $products->appends(Request::query())->render("vendor/pagination/bootstrap-4") }}
                    </div>
                </div>
            </div>
        </section>

        @include("sections/instagram")

    </main><!--main-->

</div>

@endsection