@extends('layouts.app')

@section('content')

<div class="contents single-page">
    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item active" aria-current="page">Трек номери</li>
            </ol>
        </nav>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center page_head">
                        <h3 class="blue">Трек номери</h3>
                    </div>
                    <div class="col-12 track_search_wrapper">
                        <div class="row">
                            <div class="col-12 track_search form-group">
                                <input class="form-control effect" type="text" id="filtersearch" value="" placeholder="Введіть прізвище одержувача або номер накладної"  />
                            </div>
                            <!-- <div class="col-md-6 track_search form-group">
                                <input class="form-control effect" type="text" id="filtersearch-date" value="" placeholder="Введите дату отправки (напр.09.11.2020)"  />
                            </div>
                            <div class="col-md-6 content_ukrposhta mt-3 mb-4">
                                <button class="btn_all btn_blue hvr-sweep-to-top" onclick="window.open('https://tracking.novaposhta.ua/#/uk', '_blank');" id="pills-newmail">Нова пошта</button>
                            </div>  
                            <div class="col-md-6 content_newmail mt-3 mb-4">
                                <button class="btn_all btn_blue hvr-sweep-to-top" onclick="window.open('https://track.ukrposhta.ua/tracking_UA.html', '_blank');">УкрПошта</button>
                            </div>    -->   
                        </div>
                    </div>
                    
                    @if($tracks)
                        <div class="col-12 search_content">
                            
                            @foreach($tracks as $key=>$val)

                            <!-- <div class="block_date ttn text-center mb-3">{{ $key }}</div> -->

                            <div class="row">
                                
                                <div class="col-md-12">
                                    <div class="date-filter">
                                        <div class="block_date ttn text-center mb-3">{{ $key }}</div>
                                        <div class="t_cell row">
                                            
                                            <?php $previous = null; ?>

                                            @foreach($val as $track)

                                            <?php  $firstLetter = mb_substr($track->name, 0, 1); ?>

                                            @if ($previous !== $firstLetter)
                                            </ul>
                                            <ul class="list-unstyled col-md-6 alpha-ul" style="-webkit-columns:1">
                                                <div class="t_letter">{{ mb_convert_case($firstLetter, MB_CASE_UPPER, "UTF-8") }}</div>
                                            @endif
                                            
                                                <li>{{ $track->number." ".$track->name }}</li>

                                            <?php $previous = $firstLetter; ?>

                                            @if ($previous !== $firstLetter)
                                            </ul>
                                            @endif

                                            @endforeach

                                        </div>
                                    </div>
                                </div>

                                {{--<div class="col-md-6 content_newmail">
                                    <div class="date-filter">
                                        <ul class="list-unstyled">
                                            @foreach($val as $track)
                                                @if($track->carrier === "UP")
                                                <li>{{ $track->number." ".$track->name }}</li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </div>
                                    
                                </div>--}}
                                
                            </div>

                            @endforeach
                        </div>
       
                        <div class="col-12 justify-content-center pagination">
                            {{ $tracks->appends(Request::query())->render("vendor/pagination/bootstrap-4") }}
                        </div>
    
                    @endif

                    
                    <!-- <div class="col-12 pills_wrapper">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item" role="presentation">
                                <button class="nav-link active mr-3" id="pills-ukrposhta-tab" data-toggle="pill" data-target="#pills-ukrposhta" type="button" role="tab" aria-controls="pills-ukrposhta" aria-selected="true">Укрпочта</button>
                            </li>
                            <li class="nav-item" role="presentation">
                                <button class="nav-link" id="pills-newmail-tab" data-toggle="pill" data-target="#pills-newmail" type="button" role="tab" aria-controls="pills-newmail" aria-selected="false">Новая почта</button>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabTTN">
                            <div class="tab-pane fade show active" onclick="window.open('https://track.ukrposhta.ua/tracking_UA.html', '_blank');" id="pills-ukrposhta" role="tabpanel" aria-labelledby="pills-ukrposhta-tab">
                                <div class="date-filter">
                                    <span class="ttn text-center">09.11.2020:</span>
                                    <ul class="list-unstyled">
                                        <li>0500335493527 Альберт О</li>
                                        <li>0500335469952 Андрусенко Н</li>
                                        <li>0500335884488 Бабак Т</li>
                                        <li>0500335487195 Билкей О</li>
                                        <li>0500335477530 Бойко М</li>
                                        <li>0500335480514 Бородина З</li>
                                        <li>0500335470250 Букоємська А</li>
                                        <li>0500335888823 Бурак М</li>
                                        <li>0500335834456 Бутковская Д</li>
                                        <li>0500335901595 Верстка Т</li>
                                        <li>0500335469707 Винограденко И</li>
                                        <li>0500335474514 Каракулина Н</li>
                                        <li>0500335469464 Кернична Т</li>
                                        <li>0500335866773 Кибиш Ю</li>
                                        <li>0500335484200 Кислюк В</li>
                                        <li>0500335468697 Клименко Ю</li>
                                        <li>0500335462532 Коваль С</li>
                                        <li>0500335880458 Кондрат Р</li>
                                        <li>0500335480271 Корнега О</li>
                                        <li>0500335844753 Котик А</li>
                                        <li>0500335835312 Кошеленко А</li>
                                        <li>0500335885883 Кошмак А</li>
                                        <li>0500335468093 Кравченко Т</li>
                                    </ul>
                                </div>
                                <div class="date-filter">
                                    <span class="ttn text-center">ТТН за 10.11.2020:</span>
                                    <ul class="list-unstyled">
                                        <li>0500335490250 Магаль С</li>
                                        <li>0500335468425 Михайленко І</li>
                                        <li>0500335907372 Михальская О</li>
                                        <li>0500335830345 Мусієнко Н</li>
                                        <li>0500335473925 Негина А</li>
                                        <li>0500335888602 Неделько М</li>
                                        <li>0500335469863 Онищенко Н</li>
                                        <li>0500335858525 Онуфриенко С</li>
                                        <li>0500335832020 Осійчук Я</li>
                                        <li>0500335468930 Павленко А</li>
                                        <li>0500335485508 Пасенко О</li>
                                        <li>0500335473216 Писаренко К</li>
                                        <li>0500335469227 Подлесная Т</li>
                                        <li>0500335470390 Пупена О</li>
                                        <li>0500335481154 Салия С</li>
                                        <li>0500335857979 Семкив Н</li>
                                        <li>0500335886588 Сорока Х</li>
                                        <li>0500335915723 Субботник Т</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab-pane fade" onclick="window.open('https://tracking.novaposhta.ua/#/uk', '_blank');" id="pills-newmail" role="tabpanel" aria-labelledby="pills-newmail-tab">
                                <div class="date-filter">
                                        <span class="ttn">ТТН за 09.11.2020:</span>
                                        <ul class="list-unstyled">
                                            <li>0500335493527 Альберт О</li>
                                            <li>0500335469952 Андрусенко Н</li>
                                            <li>0500335884488 Бабак Т</li>
                                            <li>0500335487195 Билкей О</li>
                                            <li>0500335477530 Бойко М</li>
                                            <li>0500335480514 Бородина З</li>
                                            <li>0500335470250 Букоємська А</li>
                                            <li>0500335888823 Бурак М</li>
                                            <li>0500335834456 Бутковская Д</li>
                                            <li>0500335901595 Верстка Т</li>
                                            <li>0500335469707 Винограденко И</li>
                                            <li>0500335474514 Каракулина Н</li>
                                            <li>0500335469464 Кернична Т</li>
                                            <li>0500335866773 Кибиш Ю</li>
                                            <li>0500335484200 Кислюк В</li>
                                            <li>0500335468697 Клименко Ю</li>
                                            <li>0500335462532 Коваль С</li>
                                            <li>0500335880458 Кондрат Р</li>
                                            <li>0500335480271 Корнега О</li>
                                            <li>0500335844753 Котик А</li>
                                            <li>0500335835312 Кошеленко А</li>
                                            <li>0500335885883 Кошмак А</li>
                                            <li>0500335468093 Кравченко Т</li>
                                        </ul>
                                </div>
                                <div class="date-filter">
                                    <span class="ttn">ТТН за 10.11.2020:</span>
                                    <ul class="list-unstyled">
                                        <li>0500335490250 Магаль С</li>
                                        <li>0500335468425 Михайленко І</li>
                                        <li>0500335907372 Михальская О</li>
                                        <li>0500335830345 Мусієнко Н</li>
                                        <li>0500335473925 Негина А</li>
                                        <li>0500335888602 Неделько М</li>
                                        <li>0500335469863 Онищенко Н</li>
                                        <li>0500335858525 Онуфриенко С</li>
                                        <li>0500335832020 Осійчук Я</li>
                                        <li>0500335468930 Павленко А</li>
                                        <li>0500335485508 Пасенко О</li>
                                        <li>0500335473216 Писаренко К</li>
                                        <li>0500335469227 Подлесная Т</li>
                                        <li>0500335470390 Пупена О</li>
                                        <li>0500335481154 Салия С</li>
                                        <li>0500335857979 Семкив Н</li>
                                        <li>0500335886588 Сорока Х</li>
                                        <li>0500335915723 Субботник Т</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div> -->

                    <!-- <div class="col-12 track_wrapper">
                        <div class="ttn">ТТН Укрпочта:</div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">А</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335493527 Альберт О</p>
                                            <p>0500335469952 Андрусенко Н</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">Б</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335884488 Бабак Т</p>
                                            <p>0500335487195 Билкей О</p>
                                            <p>0500335477530 Бойко М</p>
                                            <p>0500335480514 Бородина З</p>
                                            <p>0500335470250 Букоємська А</p>
                                            <p>0500335888823 Бурак М</p>
                                            <p>0500335834456 Бутковская Д</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">В</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335901595 Верстка Т</p>
                                            <p>0500335469707 Винограденко И</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">К</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335474514 Каракулина Н</p>
                                            <p>0500335469464 Кернична Т</p>
                                            <p>0500335866773 Кибиш Ю</p>
                                            <p>0500335484200 Кислюк В</p>
                                            <p>0500335468697 Клименко Ю</p>
                                            <p>0500335462532 Коваль С</p>
                                            <p>0500335880458 Кондрат Р</p>
                                            <p>0500335480271 Корнега О</p>
                                            <p>0500335844753 Котик А</p>
                                            <p>0500335835312 Кошеленко А</p>
                                            <p>0500335885883 Кошмак А</p>
                                            <p>0500335468093 Кравченко Т</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">М</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335490250 Магаль С</p>
                                            <p>0500335468425 Михайленко І</p>
                                            <p>0500335907372 Михальская О</p>
                                            <p>0500335830345 Мусієнко Н</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">Н</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335473925 Негина А</p>
                                            <p>0500335888602 Неделько М</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">О</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335469863 Онищенко Н</p>
                                            <p>0500335858525 Онуфриенко С</p>
                                            <p>0500335832020 Осійчук Я</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">П</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335468930 Павленко А</p>
                                            <p>0500335485508 Пасенко О</p>
                                            <p>0500335473216 Писаренко К</p>
                                            <p>0500335469227 Подлесная Т</p>
                                            <p>0500335470390 Пупена О</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">С</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335481154 Салия С</p>
                                            <p>0500335857979 Семкив Н</p>
                                            <p>0500335886588 Сорока Х</p>
                                            <p>0500335915723 Субботник Т</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 track_wrapper">
                        <div class="ttn">ТТН за 09.11.2020:</div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">А</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335493527 Альберт О</p>
                                            <p>0500335469952 Андрусенко Н</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">Б</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335884488 Бабак Т</p>
                                            <p>0500335487195 Билкей О</p>
                                            <p>0500335477530 Бойко М</p>
                                            <p>0500335480514 Бородина З</p>
                                            <p>0500335470250 Букоємська А</p>
                                            <p>0500335888823 Бурак М</p>
                                            <p>0500335834456 Бутковская Д</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">В</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335901595 Верстка Т</p>
                                            <p>0500335469707 Винограденко И</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">К</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335474514 Каракулина Н</p>
                                            <p>0500335469464 Кернична Т</p>
                                            <p>0500335866773 Кибиш Ю</p>
                                            <p>0500335484200 Кислюк В</p>
                                            <p>0500335468697 Клименко Ю</p>
                                            <p>0500335462532 Коваль С</p>
                                            <p>0500335880458 Кондрат Р</p>
                                            <p>0500335480271 Корнега О</p>
                                            <p>0500335844753 Котик А</p>
                                            <p>0500335835312 Кошеленко А</p>
                                            <p>0500335885883 Кошмак А</p>
                                            <p>0500335468093 Кравченко Т</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">М</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335490250 Магаль С</p>
                                            <p>0500335468425 Михайленко І</p>
                                            <p>0500335907372 Михальская О</p>
                                            <p>0500335830345 Мусієнко Н</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">Н</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335473925 Негина А</p>
                                            <p>0500335888602 Неделько М</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">О</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335469863 Онищенко Н</p>
                                            <p>0500335858525 Онуфриенко С</p>
                                            <p>0500335832020 Осійчук Я</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">П</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335468930 Павленко А</p>
                                            <p>0500335485508 Пасенко О</p>
                                            <p>0500335473216 Писаренко К</p>
                                            <p>0500335469227 Подлесная Т</p>
                                            <p>0500335470390 Пупена О</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="t_table">
                                    <div class="t_cell">
                                        <div class="t_letter">С</div>
                                    </div>
                                    <div class="t_cell">
                                        <div class="t_number">
                                            <p>0500335481154 Салия С</p>
                                            <p>0500335857979 Семкив Н</p>
                                            <p>0500335886588 Сорока Х</p>
                                            <p>0500335915723 Субботник Т</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>

        @include("sections/instagram")

    </main><!--main-->

</div>

@endsection