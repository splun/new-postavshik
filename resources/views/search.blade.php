@extends('layouts.app')

@section('content')

<div class="contents">

    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item active" aria-current="page">Пошук</li>
            </ol>
        </nav>

        <section id="searchSection">
            <div class="container">
                <div class="row">
                    <div class="col-12 text-center page_head">
                        <h3 class="blue">Пошук : {{$keyword}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
            @if($keyword)
            <div class="container">
                <div class="row">

					<!--desktop buttons-->

                    <div class="col-12 control_wrapper d-none d-md-flex">
                        <div class="leftblock_control d-flex">
                            <p class="mb-0">Усього товару <span>{{ $products->total() }}</span></p>
                        </div>
                        <div class="rightblock_control ml-auto d-flex">
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&discount=1'" class="btn_control btn_control_big red {{ (request()->has('discount')) ? 'active' : '' }}">Новинка</button>
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&popular=1'" class="btn_control btn_control_big blue {{ (request()->has('popular')) ? 'active' : '' }}"><img class="star" src="/images/icons/star.svg" onload="SVGInject(this)">Популярне</button>
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&new=1'" class="btn_control btn_control_big blue {{ (!request()->price && !request()->discount && !request()->popular) ? 'active' : '' }}">Нове</button>
                            <p class="mb-0 rightblock_price">Ціна</p>
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&price=desc'"  class="btn_control btn_control_small {{ (request()->price == 'desc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_up.svg" onload="SVGInject(this)"></button>
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&price=asc'"  class="btn_control btn_control_small {{ (request()->price == 'asc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_down.svg" onload="SVGInject(this)"></button>
                        </div>
                    </div>

					<!--end desktop buttons-->

					<!--mobile buttons-->

                    <div class="col-12 control_wrapper d-flex d-md-none">
                        <div class="d-flex mob_block_control">
                            <div class="leftblock_control align-self-center">
                                <p class="mb-0">Усього товару <span>{{ $products->total() }}</span></p>
                            </div>
                            <div class="rightblock_control ml-auto d-flex align-self-center">
                                <p class="mb-0">Ціна</p>
                                <button type="button" onclick="window.location.href='?price=desc'"  class="btn_control btn_control_small {{ (request()->price == 'desc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_up.svg" onload="SVGInject(this)"></button>
                                <button type="button" onclick="window.location.href='?price=asc'"  class="btn_control btn_control_small {{ (request()->price == 'asc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_down.svg" onload="SVGInject(this)"></button>
                            </div>
                        </div>
                        <div class="d-flex">
                            <button type="button" onclick="window.location.href='?discount=1'" class="ml-0 btn_control btn_control_big red {{ (request()->has('discount') == '1') ? 'active' : '' }}">Розпродаж</button>
                            <button type="button" onclick="window.location.href='?popular=1'" class="ml-auto btn_control btn_control_big blue {{ (request()->has('popular') == '1') ? 'active' : '' }}"><img class="star" src="/images/icons/star.svg" onload="SVGInject(this)">Популярне</button>
                        </div>
                    </div>

					<!--end mobile buttons-->

                    <div class="col-12 catalogue_wrapper">
                        <div class="row">
                                @include("sections/product_wrapper",['products' => $products,'type'=>'catalog']) 

                        </div>
                    </div>
                    <div class="col-12 justify-content-center pagination">
                        {{ $products->appends(Request::query())->render("vendor/pagination/bootstrap-4") }}
                    </div>
                </div>
            </div>
            @else
            <div class="container">
                <div class="row">

					<!--desktop buttons-->

                    <div class="col-12 control_wrapper d-none d-md-flex">
                        <div class="leftblock_control d-flex">
                            <p class="mb-0">Усього товару <span>0</span></p>
                        </div>
                        <div class="rightblock_control ml-auto d-flex">
                            <p class="mb-0">Ціна</p>
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&price=desc'"  class="btn_control btn_control_small {{ (request()->price == 'desc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_down.svg" onload="SVGInject(this)"></button>
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&price=asc'"  class="btn_control btn_control_small {{ (request()->price == 'asc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_up.svg" onload="SVGInject(this)"></button>
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&discount=1'" class="btn_control btn_control_big red {{ (request()->has('discount')) ? 'active' : '' }}">Новинка</button>
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&popular=1'" class="btn_control btn_control_big blue {{ (request()->has('popular')) ? 'active' : '' }}"><img class="star" src="/images/icons/star.svg" onload="SVGInject(this)">Популярне</button>
                            <button type="button" onclick="window.location.href='?s={{$keyword}}&new=1'" class="btn_control btn_control_big blue {{ (!request()->price && !request()->discount && !request()->popular) ? 'active' : '' }}">Нове</button>
                        </div>
                    </div>

					<!--end desktop buttons-->

					<!--mobile buttons-->

                    <div class="col-12 control_wrapper d-flex d-md-none">
                        <div class="d-flex mob_block_control">
                            <div class="leftblock_control align-self-center">
                                <p class="mb-0">Усього товару <span>0</span></p>
                            </div>
                            <div class="rightblock_control ml-auto d-flex align-self-center">
                                <p class="mb-0">Ціна</p>
                                <button type="button" onclick="window.location.href='?price=desc'"  class="btn_control btn_control_small {{ (request()->price == 'desc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_down.svg" onload="SVGInject(this)"></button>
                                <button type="button" onclick="window.location.href='?price=asc'"  class="btn_control btn_control_small {{ (request()->price == 'asc') ? 'active' : '' }}"><img src="/images/icons/btn_arrow_up.svg" onload="SVGInject(this)"></button>
                            </div>
                        </div>
                        <div class="d-flex">
                            <button type="button" onclick="window.location.href='?discount=1'" class="ml-0 btn_control btn_control_big red {{ (request()->has('discount') == '1') ? 'active' : '' }}">Розпродаж</button>
                            <button type="button" onclick="window.location.href='?popular=1'" class="ml-auto btn_control btn_control_big blue {{ (request()->has('popular') == '1') ? 'active' : '' }}"><img class="star" src="/images/icons/star.svg" onload="SVGInject(this)">Популярне</button>
                        </div>
                    </div>

					<!--end mobile buttons-->

                    <div class="col-12 catalogue_wrapper">
                        <div class="row">
                                
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </section>

        @include("sections/instagram")

    </main><!--main-->

</div>

@endsection