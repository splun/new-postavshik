<div class="messenger-sendCard">
    <form id="message-form" method="POST" action="{{ route('send.message') }}" enctype="multipart/form-data">
        @csrf
        <label><span class="fas fa-paperclip"></span><input disabled='disabled' multiple="multiple" type="file" class="upload-attachment" name="file[]" accept=".{{implode(', .',config('chatify.attachments.allowed_images'))}}, .{{implode(', .',config('chatify.attachments.allowed_files'))}}" /></label>
        <textarea style="display:none;" class="otherFiles" name="other_files"></textarea>
        <textarea readonly='readonly' id="chat-message" name="message" class="m-send app-scroll" placeholder="Введіть повідомлення.."></textarea>
        <button class="submit-chat-message" disabled='disabled'><span class="fas fa-paper-plane"></span></button>
    </form>
</div>
