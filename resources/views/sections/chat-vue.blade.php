<div id="chat" :class="{'chat--opened': showWindow}" v-cloak>
    <header v-click.once="scrollToBottom"
            v-click="toggleWindow"
            class="widget__header"
            :class="{'widget__header--progress': loading}"
            v-cloak>

        <i class="fa fa-rss chat__resizer" aria-hidden="true" ref="$resizer" v-show="showWindow" v-mousedown.capture="resized = true" title="Зажмите, чтобы растянуть окно."></i>

        <h4 v-if="ukrRoomNotRead || euRoomNotRead"
            class="widget__header__new-message">Вам пришло новое сообщение (1)</h4>
        <h4 v-else class="widget__header__title">Чат</h4>

        <i class="fa fa-minus" aria-hidden="true" v-show="showWindow && resized && !onResize"></i>

        <transition name="fade">
            <i v-if="showWindow" class="fa fa-chevron-down" aria-hidden="true"></i>
            <i v-else class="fa fa-chevron-up" aria-hidden="true"></i>
        </transition>

    </header>

    <transition name="fade">

        <div v-show="showWindow" class="widget__window">


                <template v-if="loading">
                    <h3>
                        Загрузка...
                    </h3>
                </template>

                <template v-else-if="isHistoryEmpty && notStarted">
                    <div class="widget__window__history--empty">
                        <h3>Для заказа скиньте ссылку на фото нужной модели, укажите цвет, размер и количество. Оператор
                            ответит на ваше сообщение в течении некоторого времени.</h3>
                        <button v-click="startChat($ukrRoomId)">Оператор (Украина)</button>
                        <button v-click="startChat($cisAndEuRoomId)">Оператор (Международные отправки)</button>
                    </div>
                </template>

                <template v-else>

                    <div class="widget__window__room__switcher">
                        <div class="widget__window__room"
                             v-click="switchRoom($cisAndEuRoomId)"
                             :class="{
                                'widget__window__room--selected': selectedRoom === $cisAndEuRoomId,
                                'widget__window__room--online': isOnline($cisAndEuRoomId)
                             }">
                            <img src="{{ constant('entity\\Chat::DEFAULT_ROOM_ICON') }}"
                                 width="40"
                                 height="48"
                                 title="operator-icon"/>
                            <h5>    4r </h5>

                            <template v-if="euRoomNotRead > 0">
                                <i class="fa fa-envelope-o" aria-hidden="true"> ${euRoomNotRead} </i>
                            </template>
                        </div>
                        <div class="widget__window__room"
                             v-click="switchRoom($ukrRoomId)"
                             :class="{
                                'widget__window__room--selected': selectedRoom === $ukrRoomId,
                                'widget__window__room--online': isOnline($ukrRoomId)
                             }">
                            <img src=""
                                 width="40"
                                 height="48"
                                 title="opeartor-icon"/>
                            <h5>{{ constant('entity\\Chat::ROOM_NAMES')[constant('entity\\Chat::UKR_ROOM_ID')] }}</h5>

                            <template v-if="ukrRoomNotRead > 0">
                                <i class="fa fa-envelope-o" aria-hidden="true"> ${ukrRoomNotRead} </i>
                            </template>
                        </div>
                    </div>

                    <div class="widget__window__history">
                        <div class="widget__window__history__content" v-mouseenter="setAsRead">

                            <template v-for="(message, index) in messages[selectedRoom]">

                                <div ref="newMessagesLinear"
                                     v-if="
                                 (message.message_type === $INCOMING_MESSAGE
                                     && !+message.is_read
                                     && firstNotReadMessageId === null
                                     && (firstNotReadMessageId = +message.id)
                                 ) || (firstNotReadMessageId == +message.id) ? true : false"
                                     class="clearfix room__chat__area__message--not-read-linear">
                                    <span><b>Новые сообщения</b></span>
                                </div>

                                <div class="widget__window__message"
                                     :class="{
                        'widget__window__message--not-read': isReadMessage(selectedRoom, index)
                      }">
                                    <img :src="message.icon" alt="" width="46" height="46"
                                         class="widget__window__message__avatar">
                                    <div class="widget__window__message__content">
                                        <h5>&nbsp;<small class="chat-time pull-right">
                                            </small>
                                        </h5>
                                        <p :inner-html.prop="message.body | linkify"></p>
                                        <div class="room__chat__area__message__attaches" v-if="message.attached_files.length">
                                            <template v-for="file in message.attached_files">
                                                <template v-if="isImg(file)">
                                                    <img class="attached-file img-thumbnail"
                                                         :src="file" :alt="file | filename" v-click="openModal(file)" width="140"
                                                         height="140"
                                                         title="Открыть изображение">
                                                </template>
                                                <template v-else>
                                            <span class="attached-file" title="Загрузить файл">
                                                <a :href="file" download>${file | filename}</a>
                                            </span>
                                                </template>
                                            </template>
                                        </div>
                                    </div> <!-- end widget__window__message__content -->
                                </div> <!-- end widget__window__message -->

                            </template>

                            <hr>
                            <div class="room__error" v-if="error">
                                <h5>fr</h5>
                            </div>
                        </div> <!-- end widget__window__history -->
                    </div>

                    <div v-show="selectedRoom > -1 && isLastActivityShown(selectedRoom)"
                         class="widget__window__last-activity">
                        Оператор был(а) в сети 
                    </div>

                    <send-message-form
                            v-if="showWindow"
                            :operator="operatorId"
                            :room="selectedRoom"
                            submit="addMessage"
                            v-error="displayError($event)"
                            v-cloak>
                    </send-message-form>

                </template>



        </div> <!-- end widget -->

    </transition>

    <modal v-if="showModal" v-close="showModal = false" :files="roomAttachedFileList" :index="focusedImgIndex"></modal>

</div>

<script type="text/x-template" id="send-message-area">
    <div>
        <form class="form send__message-form">
              <textarea
                      :disabled="processing"
                      ref="$messageInput"
                      rows="1"
                      name="message"
                      autofocus
                      style="width: 290px; visibility: hidden"></textarea>
            <i v-if="processing" class="fa fa-spinner fa-pulse fa-2x" style=""></i>
            <a v-else :disabled="processing" v-click="send">
                <i class="fa fa-share fa-2x"></i>
            </a>
            <file-attacher ref="$attachedFiles" :fromClipboard="clipboardFiles"></file-attacher>
        </form>

        <div v-if="base64Files.length" class="room__chat__area__message__attaches">
            <div class="thumbnail preview"  v-for="(file, index) in base64Files">
                <img v-click="openModal(file)" :src="file" alt="attach"
                     width="64" height="64"
                     class="attached-file">
                <span v-click="removeAttachedFile(index)" class="caption">Удалить</span>
            </div>
        </div>

        <clipboard-modal v-if="showModal" v-close="closeModal" :files="[focusedImg]"></clipboard-modal>
    </div>
</script>
