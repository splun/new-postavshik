@if($current_user && $current_user->permission == "user")

<div class="chat_fixed">
  <div class="chat_wrapper">
        <div class="chat_header">
            <i class="fa fa-rss chat__resizer" aria-hidden="true" ref="$resizer" v-show="showWindow" @mousedown.capture="resized = true" title="Зажмите, чтобы растянуть окно."></i>
            <div class="chat_title">
                <p>Чат с менеджером</p>
            </div>
            <div class="chat_block_buttons d-flex align-self-center">
                <button class="chat_active_msg">

                </button>
                <?php /* <button class="chat_lang">
                    <img src="/images/flag.jpg">
                </button> */?>
                <button class="chat_close">
                    <img src="/images/icons/arrow_down1.svg" onload="SVGInject(this)">
                </button>
            </div>
        </div>

        <div id="messages-list" class="chat_body">
            <div class="chat_wrap">

            </div>
        </div>
        <div class="chat_footer">
            <div class="form d-flex w-100">
                <div class="block_wrap w-100">
                    <textarea id="chat-message" class="form-control effect" placeholder="Введите сообщение" required resize></textarea>

                    <?php /* <button class="chat_icons">
                        <img src="/images/smile.svg" onload="SVGInject(this)">
                    </button> */?>

                </div>
                <button class="chat_file">
                    <img src="/images/clip.svg" onload="SVGInject(this)">
                </button>
                <input type="hidden" id="user_id" name="user_id" value="{{ $current_user->id }}">
                <input type="hidden" id="operator_id" name="operator" value="{{ $operator }}">
                <input type="file" multiple="multiple" class="chat_file_input" style="display: none;">
                <button type="submit" class="push chat_send">ок</button>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.21/vue.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue-resource@1.3.4"></script>
<script src="{{ asset('js/chat.js') }}" defer></script>
<script src="https://js.pusher.com/7.2/pusher.min.js"></script>
<?php /* <script src="https://comet-server.com/CometServerApi.js" type="text/javascript"></script> */?>

@endif
