
<?php /* @include('Chatify::layouts.headLinks',[
    'id' => $operator,
    'type' => 'user',
    'messengerColor'=>'#2180f3',
    'dark_mode'=>'light'
]) */?>

@if($current_user && $current_user->permission == "user")

{{-- Meta tags --}}

    <meta name="id" content="{{ $operator }}">
    <meta name="type" content="user">
    <meta name="messenger-color" content="#2180f3">
    <meta name="url" content="{{ url('').'/'.config('chatify.routes.prefix') }}" data-user="{{ Auth::user()->id }}">
{{-- scripts --}}


{{-- Messenger Color Style--}}
@include('Chatify::layouts.messengerColor',[
    'id' => $operator,
    'type' => 'user',
    'messengerColor'=>'#2180f3',
    'dark_mode'=>'light'
])

<button class="chat_button">
    <!-- <span class="newmsg">{{ $count_unread_message }}</span> -->
    <span class="newmsg" data-count="{{ ($count_unread_message) ?? 0 }}">{{ ($count_unread_message > 0) ? $count_unread_message : "..."}}</span>
    <img src="/images/chat2.png">
</button>

<div class="chat_fixed" id="chat-widget">

    <div class="chat_wrapper" id="chat-resize">
        <div id="res-area">
            <div class="chat_header">
                <i class="fa fa-rss chat__resizer" id="block_resize" aria-hidden="true" title="Натисніть, щоб розтягнути вікно."></i>
                <div class="chat_title">
                    <p>{{ config('chatify.name') }}</p>
                </div>
                <div class="chat_block_buttons d-flex align-self-center" >
                    <button class="chat_active_msg">

                    </button>
                    <button class="chat_close">
                        <img src="/images/icons/arrow_down1.svg" onload="SVGInject(this)">
                    </button>
                </div>
            </div>

            <div id="messages-list" class="chat_body">
                <div class="chat_wrap">
                    <div class="messenger">
                        {{-- ----------------------Users/Groups lists side---------------------- --}}
                        <div class="messenger-listView" id="user-area-list-operator">
                            {{-- Header and search bar --}}
                            {{-- tabs and lists --}}
                            <div class="m-body contacts-container">
                            {{-- Lists [Users/Group] --}}
                            {{-- ---------------- [ User Tab ] ---------------- --}}
                            <div style="display:none;" data-view="users">

                                {{-- Favorites --}}
                                <div class="favorites-section" style="display:none;">
                                    <p class="messenger-title">Favorites</p>
                                    <div class="messenger-favorites app-scroll-thin"></div>
                                </div>

                                {{-- Saved Messages --}}
                                <?php /*{!! view('Chatify::layouts.listItem', ['get' => 'saved']) !!}*/?>

                                {{-- Contact --}}
                               
                            </div>

                            {{-- ---------------- [ Group Tab ] ---------------- --}}
                            <?php /* <div class="@if($type == 'group') show @endif messenger-tab groups-tab app-scroll" data-view="groups">
                                    {{-- items --}}
                                    <p style="text-align: center;color:grey;margin-top:30px">
                                        <a target="_blank" style="color:{{$messengerColor}};" href="https://chatify.munafio.com/notes#groups-feature">Click here</a> for more info!
                                    </p>
                                </div> */?>


                            </div>
                        </div>

                        {{-- ----------------------Messaging side---------------------- --}}
                        <div class="messenger-messagingView">
                            {{-- Internet connection --}}
                            <div class="internet-connection">
                                <span class="ic-connected">Підключено</span>
                                <span class="ic-connecting">Підключення...</span>
                                <span class="ic-noInternet">Немає підключення</span>
                            </div>
                            {{-- Messaging area --}}
                            <div class="m-body messages-container app-scroll">
                                <div class="messages">
                                    
                                </div>
                                {{-- Typing indicator --}}
                                <div class="typing-indicator">
                                    <div class="message-card typing">
                                        <p>
                                            <span class="typing-dots">
                                                <span class="dot dot-1"></span>
                                                <span class="dot dot-2"></span>
                                                <span class="dot dot-3"></span>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                                {{-- Send Message Form --}}
                                @include('Chatify::layouts.sendForm')
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
{{-- ---------------------- Info side ---------------------- --}}

<script src="{{ asset('js/chatify/resize.js') }}"></script>
<script src="{{ asset('js/chatify/font.awesome.min.js') }}"></script>
<script src="{{ asset('js/chatify/autosize.js') }}"></script>
<script src='https://unpkg.com/nprogress@0.2.0/nprogress.js'></script>

@include('Chatify::layouts.modals',[
    'id' => $operator,
    'type' => 'user',
    'messengerColor'=>'#2180f3',
    'dark_mode'=>'light'
])

@include('Chatify::layouts.footerLinks')

@else

<div id="contact_telegram">
    <a target="_blank" href="https://t.me/Operator_postavshik"><img src="/images/telegram.svg" alt="ico"></a>
</div>

@endif


