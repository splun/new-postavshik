<div class="col-lg-3 menu_categories">
    <h4>Категорії</h4>
    <ul class="navbar-nav">
        <li class="nav-item <?php echo ($category->id == 145) ? 'active' : '';?>"><a href="/category/145" class="nav-link">Новинки</a></li>
        <li class="nav-item shares <?php echo ($category->id == 8) ? 'active' : '';?>"><a class="nav-link" href="/category/8/">Розпродаж</a></li>

        @foreach($parent_categories as $parent)

        <li class="nav-item <?php echo ($category->parent_id == $parent['parent_id']) ? 'show' : '';?>">

            <a class="nav-link menu-parent" href="#"><i class="fa fa-plus d-inline-flex"></i><i class="fa fa-minus d-none"></i>{{ $parent['parent_name'] }}</a>
            @if($parent['child_cats'])
        
            <ul class="submenu list-unstyled">
                
                @foreach($parent['child_cats'] as $cat) 
                        
                        <li class="<?php echo ($category->id == $cat->id) ? 'active' : '';?>">
                            <a href="/category/{{ $cat->id }}"><i class="fa fa-minus"></i>{{ $cat->name }}</a>
                        </li>
                @endforeach
            </ul>
            
            @endif

        </li>
        
        @endforeach

        <?php /* <li class="nav-item">
            <a class="nav-link" href="/information">Инфо+</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/contacts">Контакты</a>
        </li> */?>
    </ul>
</div>