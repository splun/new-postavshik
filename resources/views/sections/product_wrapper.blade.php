@foreach ($products as $product)

    @if($type == 'popular')
        <div class="col-sm-4 col-6">
            <div class="box-content">
                <div class="row">
                    <div class="product-layout col-12">
                        <div class="row">
                            <div class="cat_product_wrapper col-sm-6 col-12">
                                <div class="product_image">
                                    <a href="/product/{{$product->id}}">
                                        @php
                                            $url = $product->base64 ? 'data:image/jpeg;base64,' . $product->base64 : $product->url;
                                            $extension = pathinfo($url, PATHINFO_EXTENSION);
                                        @endphp

                                        @if(in_array($extension, ['mp4','MP4']))
                                        <div class="video-container">
                                            <div class="play-button">
                                                <svg width="47" height="53" viewBox="0 0 47 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M45 23.0357C47.6667 24.5753 47.6667 28.4243 45 29.9639L6.75 52.0475C4.08333 53.5871 0.75 51.6626 0.75 48.5834L0.750002 4.41612C0.750002 1.33692 4.08334 -0.587579 6.75 0.952022L45 23.0357Z" fill="white" fill-opacity="0.5"/>
                                                </svg>
                                            </div>
                                            <video class="video-element" autoplay muted loop playsinline preload="metadata" webkit-playinginline>
                                                <source src="{{ $url }}" type="video/{{ $extension }}">
                                                Ваш браузер не поддерживает видео.
                                            </video>
                                        </div>
                                        @else
                                            <img {{ ($type == "catalog") ? 'class="img-fluid"' : ''}} class="img-fluid" src="{{ $url }}" alt="image">
                                        @endif
                                        
                                    </a>
                                    <?php /*@if($product->discount > 0)
                                        <div class="sales">
                                            {{-- ucfirst($product->category->name) --}} 
                                            {{$product->discount}}%
                                            <!-- <a href="/category/{{$product->category->id}}">{{ ucfirst($product->category->name) }}</a> -->
                                        </div>
                                    @endif */?>
                                    
                                    @if($product->is_popular >= $min_popular_rate && isset($product->category) && $product->category->id !== 8)
                                        <div class="hits">хіт</div>
                                    @endif
                                    
                                    {{--<div class="button-group">
                                        <a href="#" data-id="{{$product->id}}" class="add-to-cart btn_add">
                                            <img src="/images/icons/cart.svg" alt="cart" onload="SVGInject(this)">
                                        </a>
                                        <a href="/product/{{$product->id}}" data-id="{{$product->id}}" class="btn_add">
                                            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 576 512"><path d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0 144 144 0 1 1 -288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z"/></svg>
                                        </a>
                                    </div>--}}

                                </div>
                            </div>
                            <div class="product_info col-sm-6 col-12">
                                <div class="s_article mb-2">
                                    @if($product->provider)
                                        {{$product->provider->alias}}-{{$product->stock_number}}                
                                    @else
                                        {{$product->stock_number}}
                                    @endif
                                </div>
                                <div class="prod_left align-self-center">
                                <p class="price {{ ($type == 'catalog') ? 'mb-0' : '' }}">{{ currency((float)$product->price,"UAH",currency()->getUserCurrency()) }} 
                                @if($product->old_price)
                                    <span class="old_price">{{ currency((float)$product->old_price,"UAH",currency()->getUserCurrency()) }}</span></p>
                                @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
 
        <div class="{{ ($type == 'module') ? 'product-layout product_wrapper col-sm-6' : 'product col-xl-4 col-lg-4 col-sm-4 col-6' }}">
            <div class="cat_product_wrapper">
                {{-- <div class="badges">
                            <div class="discount-badge"><img src=/images/percent.svg" alt="percent"></div>
                            <div class="popular-badge"></div>
                </div>   --}} 
                <div class="product_image">
                    <a href="/product/{{$product->id}}">
                        @php
                            $url = $product->base64 ? 'data:image/jpeg;base64,' . $product->base64 : $product->url;
                            $extension = pathinfo($url, PATHINFO_EXTENSION);
                        @endphp
                        @if(strripos($url, "storage"))
                            @if(in_array($extension, ['mp4','MP4']))
                            <div class="video-container">
                                <div class="play-button">
                                    <svg width="47" height="53" viewBox="0 0 47 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M45 23.0357C47.6667 24.5753 47.6667 28.4243 45 29.9639L6.75 52.0475C4.08333 53.5871 0.75 51.6626 0.75 48.5834L0.750002 4.41612C0.750002 1.33692 4.08334 -0.587579 6.75 0.952022L45 23.0357Z" fill="white" fill-opacity="0.5"/>
                                    </svg>
                                </div>
                                <video class="video-element" autoplay muted loop playsinline preload="metadata" webkit-playinginline>
                                        <source src="{{ $url }}" type="video/{{ $extension }}">
                                    Ваш браузер не поддерживает видео.
                                </video>
                            </div>
                            @else
                                <img {{ ($type == "catalog") ? 'class="img-fluid"' : ''}} class="img-fluid" src="{{ $url }}" alt="image">
                            @endif
                        @else
                            @if(in_array($extension, ['mp4','MP4']))
                            <div class="video-container">
                                <div class="play-button">
                                    <svg width="47" height="53" viewBox="0 0 47 53" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M45 23.0357C47.6667 24.5753 47.6667 28.4243 45 29.9639L6.75 52.0475C4.08333 53.5871 0.75 51.6626 0.75 48.5834L0.750002 4.41612C0.750002 1.33692 4.08334 -0.587579 6.75 0.952022L45 23.0357Z" fill="white" fill-opacity="0.5"/>
                                    </svg>
                                </div>
                                <video class="video-element" autoplay muted loop playsinline preload="metadata" webkit-playinginline>
                                        <source src="{{ $url }}" type="video/{{ $extension }}">
                                    Ваш браузер не поддерживает видео.
                                </video>
                            </div>
                            @else
                                <img {{ ($type == "catalog") ? 'class="img-fluid"' : ''}} class="img-fluid" src="{{ $url }}" alt="image">
                            @endif
                        @endif
                    </a>

                    <?php /* @if($product->discount > 0)
                        <div class="sales">
                            {{-- ucfirst($product->category->name) --}} 
                            {{$product->discount}}%
                            <!-- <a href="/category/{{$product->category->id}}">{{ ucfirst($product->category->name) }}</a> -->
                        </div>
                    @endif */?>

                    @if($product->old_price)
                        <div class="sales">
                            -{{ round((($product->old_price - $product->price) * 100) / $product->old_price)  }}%
                            <!-- <a href="/category/{{$product->category->id}}">{{ ucfirst($product->category->name) }}</a> -->
                        </div>
                    @elseif($product->is_popular >= $min_popular_rate && $product->category->id !== 8)
                        <div class="hits">хіт</div>
                    @endif

                    {{--<div class="button-group">
                        <a href="#" data-id="{{$product->id}}" class="add-to-cart btn_add">
                            <img src="/images/icons/cart.svg" alt="cart" onload="SVGInject(this)">
                        </a>
                        <a href="/product/{{$product->id}}" data-id="{{$product->id}}" class="btn_add">
                            <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 576 512"><path d="M288 32c-80.8 0-145.5 36.8-192.6 80.6C48.6 156 17.3 208 2.5 243.7c-3.3 7.9-3.3 16.7 0 24.6C17.3 304 48.6 356 95.4 399.4C142.5 443.2 207.2 480 288 480s145.5-36.8 192.6-80.6c46.8-43.5 78.1-95.4 93-131.1c3.3-7.9 3.3-16.7 0-24.6c-14.9-35.7-46.2-87.7-93-131.1C433.5 68.8 368.8 32 288 32zM144 256a144 144 0 1 1 288 0 144 144 0 1 1 -288 0zm144-64c0 35.3-28.7 64-64 64c-7.1 0-13.9-1.2-20.3-3.3c-5.5-1.8-11.9 1.6-11.7 7.4c.3 6.9 1.3 13.8 3.2 20.7c13.7 51.2 66.4 81.6 117.6 67.9s81.6-66.4 67.9-117.6c-11.1-41.5-47.8-69.4-88.6-71.1c-5.8-.2-9.2 6.1-7.4 11.7c2.1 6.4 3.3 13.2 3.3 20.3z"/></svg>
                        </a>
                    </div>--}}
                    
                </div>
                <div class="product_info">
                    <div class="s_article mb-2">
                        @if($product->provider)
                            {{$product->provider->alias}}-{{$product->stock_number}}                
                        @else
                            {{$product->stock_number}}
                        @endif
                    </div>
                    <div class="prod_left align-self-center">
                        {{--<?php $text_no_price = preg_replace('/Ціна:.*?грн|Ціна :.*?грн|Ціна:.*? грн|Ціна :.*? грн.|Ціна:.*? грн./is', ' ', $product->text);
                        $text_no_price = preg_replace('/Цена :.*? грн|Цена:.*?грн|Цена:.*?грн./is', ' ', $text_no_price);
                        $text_no_price = explode(" ",$text_no_price);
                        ?>--}}

                        {{-- $text_no_price[0] --}}
                        <p class="price {{ ($type == 'catalog') ? 'mb-0' : '' }}">{{ currency((float)$product->price,"UAH",currency()->getUserCurrency()) }} 
                            @if($product->old_price)
                                <span class="old_price">{{ currency((float)$product->old_price,"UAH",currency()->getUserCurrency()) }}</span></p>
                            @endif
                        </p>
                    </div>
                    <!-- <div class="prod_right ml-auto">
                        <a href="#" data-id="{{$product->id}}" class="add-to-cart btn_add">
                            <img src="/images/icons/cart.svg" alt="cart" onload="SVGInject(this)">
                        </a>
                    </div> -->
                </div>
            </div>
        </div>

    @endif

@endforeach

<script>

document.addEventListener('DOMContentLoaded', function() {

    var videoContainers = document.querySelectorAll('.video-container');
    videoContainers.forEach(function(container) {
        var video = container.querySelector('.video-element');

        if (window.innerWidth < 768) {
            if (video) {
                video.setAttribute('autoplay', '');
                video.setAttribute('playsinline', '');
            }
        } else {
                video.removeAttribute('autoplay');
                container.addEventListener('mouseenter', function() {
                    playVideo(container);
                });
                container.addEventListener('mouseleave', function() {
                    pauseVideo(container);
                });
        }
    });
});

function playVideo(container) {
    var video = container.querySelector('.video-element');
    if (video) {
        video.muted = true;
        video.play();
    }
}

function pauseVideo(container) {
    var video = container.querySelector('.video-element');
    if (video) {
        video.pause();
    }
}

</script>