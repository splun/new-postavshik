@extends('layouts.app')

@section('content')

<div class="contents">

    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item active" aria-current="page">Реєстрація</li>
            </ol>
        </nav>
        <section>
            <div class="container">    
                <div class="row justify-content-center single-page">
                    <div class="col-12 text-center page_head">
                        <h1 class="blue">Реєстрація</h1>
                        <p>Якщо у вас уже є обліковий запис у нас, увійдіть на сторінці входу.</p>
                    </div>
                    <div class="col-12 mb-5">

                        <div class="card card_wrap">

                            <div class="card-body">
                                <form method="POST" action="{{ route('register') }}">
                                    @csrf

                                    <div class="login_text">Ваші персональні дані</div>

                                    <div class="form-group row">
                                        <label for="name" class="col-md-3 col-form-label text-md-right"><span>*</span>Логін</label>

                                        <div class="col-md-9 inpt_blk">
                                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                            @error('name')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ trans("validation.unique_login") }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="email" class="col-md-3 col-form-label text-md-right"><span>*</span>Почта</label>

                                        <div class="col-md-9 inpt_blk">
                                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                            @error('email')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="phone" class="col-md-3 col-form-label text-md-right"><span>*</span>Телефон</label>

                                        <div class="col-md-9 inpt_blk">
                                            <input id="phone" type="tel" class="form-control @error('phone') is-invalid @enderror" name="phone" value="{{ old('phone') }}" required autocomplete="phone">

                                            @error('phone')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="login_text">Ваш пароль</div>

                                    <div class="form-group row">
                                        <label for="password" class="col-md-3 col-form-label text-md-right"><span>*</span>Пароль</label>

                                        <div class="col-md-9 inpt_blk">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password-confirm" class="col-md-3 col-form-label text-md-right"><span>*</span>Підтвердіть пароль</label>

                                        <div class="col-md-9 inpt_blk">
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-12 text-center">
                                            <button type="submit" class="btn btn-primary register">
                                                Реєстрація
                                            </button>
                                        </div>
                                    </div>
                                </form>
                                @include("sections/social-widget")
                           </div>
                        </div>

                        
                    </div>
                </div>
            </div>
        </section>

        @include("sections/instagram")

    </main>

</div>
@endsection

<?php /* <script src="//ulogin.ru/js/ulogin.js"></script> */?>
