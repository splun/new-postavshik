@extends('layouts.app')

@section('content')

<div class="contents">

    <main>

        <nav class="breadcrumb_wrapper text-center" aria-label="breadcrumb">
            <ol class="breadcrumb justify-content-center">
                <li class="breadcrumb-item"><a href="/">Головна</a></li>
                <li class="breadcrumb-item active" aria-current="page">Авторизація</li>
            </ol>
        </nav>
        <section>
            <div class="container">    
                <div class="row justify-content-center single-page">
                    <div class="col-12 text-center page_head">
                        <h1 class="blue">Авторизація</h1>
                    </div>
                    <div class="col-12 mb-5">
                        @if (session('error'))
                        <div class="alert alert-danger col-12">
                            {{ session('error') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @endif

                        <p class="text-center margin-bottom-20 margin-top-20">Немає облікового запису? <a class="btn-link" href="/register">Зареєструватись</a></p>
                           
                        <div class="card card_wrap">

                            <div class="card-body">
          
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div class="form-group row">
                                        <label for="login" class="col-md-3 col-form-label text-md-right"><span>*</span>Логін/Почта</label>

                                        <div class="col-md-9 inpt_blk">
                                            <input id="login" type="text" class="form-control @error('login') is-invalid @enderror" name="login" value="{{ old('login') }}" required autocomplete="login" autofocus>
                                            @error('login')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="password" class="col-md-3 col-form-label text-md-right"><span>*</span>Пароль{{-- __('Password') --}}</label>

                                        <div class="col-md-9 inpt_blk">
                                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                            @error('password')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-6 offset-md-4">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                <label class="form-check-label" for="remember">
                                                    Запам'ятати мене
                                                </label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                Увійти
                                            </button>
                                            @if (Route::has('password.request'))
                                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                                    Забули пароль?
                                                </a>
                                            @endif
                                        </div>
                                    </div>
                                </form>

                                @include("sections/social-widget")
                                
                         
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        @include("sections/instagram")

    </main>

</div>
@endsection

<?php /* <script src="//ulogin.ru/js/ulogin.js"></script> */?>

