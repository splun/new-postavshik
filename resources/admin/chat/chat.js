$(document).ready(function () {
    var block = document.getElementById("messages-list");
    block.scrollTop = block.scrollHeight;

    var active_chat_id = $("#messages-list").data("chat");

    $("#messages-list .chat_wrap").load("/chat/history", {
        "_token": $('meta[name="csrf-token"]').attr('content'),
    });

    function CommetSocketsLoadNewMessages() {

        cometApi.start({ node: "app.comet-server.ru", dev_id: 15 })

        cometApi.subscription("simplechat.newMessage", function (event) {


            $.post("/admin/chat/message", {
                "_token": $('meta[name="csrf-token"]').attr('content'),
                "msg_data": event.data,
                "active_chat": active_chat_id,
            }, function (data) {
                if (data) {
                    $("#messages-list").append(data);
                    var block = document.getElementById("messages-list");
                    block.scrollTop = block.scrollHeight;
                    //loadchats(event.data.from_user_id);
                }
            }
            );

            $(".inbox_chat div#chat-" + event.data.from_user_id).addClass("active_chat");
            $(".inbox_chat div#chat-" + event.data.from_user_id + " div.chat-message").html(event.data.text);

        })
    }

    $('.chat_file').on("click", function () {
        $('.chat_file_input').trigger("click");
    });

    $(".msg_send_btn").on("click", function () {
        send();
    });


    var startFrom = 20;
    var inProgress = false;
    var old_height = $("#messages-list").height();
    var old_scroll = $("#messages-list").scrollTop();

    $("#messages-list").scroll(function () {

        /* Если высота окна + высота прокрутки больше или равны высоте всего документа и ajax-запрос в настоящий момент не выполняется, то запускаем ajax-запрос */
        if ($("#messages-list").scrollTop() == 0 && !inProgress) {
            console.log(startFrom);
            $.ajax({
                url: "/admin/chat/loadhistory",
                method: "POST",
                data: {
                    "_token": $('meta[name="csrf-token"]').attr('content'),
                    "userFrom": $("#messages-list").data("chat"),
                    "start": startFrom
                },
                beforeSend: function () {
                    inProgress = true;
                }
            })
                .done(function (data) {
                    console.log(data);
                    if (data) {
                        $("#messages-list").prepend(data);
                        $("#messages-list").scrollTop(old_scroll + $(document).height() - old_height);
                        startFrom += 20;
                        inProgress = false;
                    }
                });


        }

    });

    function send() {
        var text = $('.write_msg').val();
        var user = $(".chat_user").val();

        if (text == "") return;
        $.ajax({
            url: "/admin/chat/push",
            type: "POST",
            data: "_token=" + $('meta[name="csrf-token"]').attr('content') + "&user=" + user + "&text=" + encodeURIComponent(text)
        });

        $('.write_msg').val("");

    }
});
