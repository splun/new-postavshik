<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ваш пароль был сброшен!',
    'sent' => 'Ми відправили вам посилання для скидання пароля!',
    'throttled' => 'Будь ласка, зачекайте перед повторною спробою.',
    'token' => 'Цей токен для скидання пароля недійсний.',
    'user' => "Користувача з такою електронною адресою не знайдено.",

];
