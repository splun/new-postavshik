<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Route::post('/auth/social', '\App\Http\Controllers\Auth\LoginController@social');
Route::get('/auth/facebook/', '\App\Http\Controllers\Auth\LoginController@facebookRedirect');
Route::get('/auth/facebook/callback', '\App\Http\Controllers\Auth\LoginController@loginWithFacebook');

//Homepage
Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index']);

//Catalog
Route::get('/category/{id}', [App\Http\Controllers\ProductsController::class, 'category'])->name('category')->whereNumber('id');
Route::get('/search', [App\Http\Controllers\ProductsController::class, 'search'])->name('search');
Route::get('/product/{id}', [App\Http\Controllers\ProductsController::class, 'product'])->name('category')->whereNumber('id');
Route::get('/imgurl', [App\Http\Controllers\ProductsController::class, 'getImageUrl'])->name('getImageUrl');
Route::get('/videourl', [App\Http\Controllers\ProductsController::class, 'getVideoUrl'])->name('getVideoUrl');

//Cart
Route::prefix('cart')->group(function () {
	Route::get('/', [App\Http\Controllers\CartController::class, 'index'])->name('cart');
	Route::get('/add/{id}', [App\Http\Controllers\CartController::class, 'add'])->name('cart.add');
	Route::get('/update/{id}/{count}', [App\Http\Controllers\CartController::class, 'update'])->name('cart.update');
	Route::get('/delete/{id}', [App\Http\Controllers\CartController::class, 'delete'])->name('cart.delete');
	Route::post('/order', [App\Http\Controllers\CartController::class, 'order'])->name('save-order');
	Route::post('/success', [App\Http\Controllers\CartController::class, 'success'])->name('success');
	Route::get('/shipping_cities/{tag}', [App\Http\Controllers\CartController::class, 'get_shipping_cities'])->name('get_shipping_cities');
	Route::get('/np_warehouses/{city}', [App\Http\Controllers\CartController::class, 'get_np_warehouses'])->name('get_np_warehouses');
	Route::get('/up_shipping_cities/{tag}', [App\Http\Controllers\CartController::class, 'get_up_shipping_cities'])->name('get_up_shipping_cities');
	Route::get('/up_warehouses/{city}', [App\Http\Controllers\CartController::class, 'get_up_warehouses'])->name('get_up_warehouses');
	Route::post('/checkout_callback_fondy', [App\Http\Controllers\CartController::class, 'checkout_callback_fondy'])->name('checkout_callback_fondy-callback');
	Route::get('/checkout_callback/{order_id}', [App\Http\Controllers\CartController::class, 'checkout_callback'])->name('checkout-callback');
	Route::get('/update_order_mono_status', [App\Http\Controllers\CartController::class, 'MonoWebhook'])->name('order-webhook');
});

//Static Pages
Route::prefix('comments')->group(function () {
	Route::get('/', [App\Http\Controllers\CommentsController::class, 'index']);
	Route::post('/add', [App\Http\Controllers\CommentsController::class, 'store'])->name('comments.add');
	Route::get('/delete/{id}', [App\Http\Controllers\CommentsController::class, 'delete'])->name('comments-delete');
});

Route::get('/contacts', [App\Http\Controllers\PagesController::class, 'contacts']);
Route::get('/track', [App\Http\Controllers\PagesController::class, 'track']);
Route::get('/logging/{id}', [App\Http\Controllers\PagesController::class, 'track']);

//Contact FORM
Route::post('/sendmail', [App\Http\Controllers\PagesController::class, 'sendMessage']);


//User Chat

Route::get('/user/chat/{id}', [App\Http\Controllers\Chat\MessagesController::class, 'usersChat'])->name('usersChat')->middleware('auth');

// Route::post('/chat/history', [App\Http\Controllers\ChatController::class, 'history'])->name('history')->middleware('auth');
// Route::post('/chat/message', [App\Http\Controllers\ChatController::class, 'newMessage'])->name('newMessage')->middleware('auth');
// Route::post('/chat/push', [App\Http\Controllers\ChatController::class, 'push'])->name('push')->middleware('auth');
// Route::post('/chat/loadhistory', [App\Http\Controllers\ChatController::class, 'loadhistory'])->name('loadhistory')->middleware('auth');
// Route::post('/chat/dropfiles', [App\Http\Controllers\ChatController::class, 'dropFiles'])->name('dropFiles')->middleware('auth');

// Route::get('/chat', function () {
//     return view('chat');
// })->middleware('auth');
//Route::get('/messages', [App\Http\Controllers\ChatController::class, 'index'])->name('chat')->middleware('auth');

Route::post('/addchat', function () {
	// Store the new message
	$user = Auth::user();

	$message = $user->chat()->create([
		'message' => request()->get('message')
	]);


	return ['status' => 'OK'];
})->middleware('auth');

Route::get('/feed', [App\Http\Controllers\PagesController::class, 'feed'])->name('feed');

//Admin
Route::prefix('admin')->group(function () {

	Route::get('/', [App\Http\Controllers\Admin\AdminController::class, 'index'])->name('admin');

	Route::post('/set-exchange-rate', [App\Http\Controllers\Admin\AdminController::class, 'setExchangeRate'])->name('setExchangeRate');
	Route::post('/set-popular-rate', [App\Http\Controllers\Admin\AdminController::class, 'setPopularRate'])->name('setPopularRate');
	Route::post('/set-prepayment-rate', [App\Http\Controllers\Admin\AdminController::class, 'setPrepaymentRate'])->name('setPrepaymentRate');
	Route::post('/search-from-photo', [App\Http\Controllers\Admin\AdminController::class, 'setSearchFromPhoto'])->name('setSearchFromPhoto');
	Route::post('/add-default-banner', [App\Http\Controllers\Admin\AdminController::class, 'addDefaultBanner'])->name('addDefaultBanner');
	Route::post('/edit-favicon', [App\Http\Controllers\Admin\AdminController::class, 'editFavicon'])->name('editFavicon');

	Route::prefix('providers')->group(function () {
		Route::get('/', [App\Http\Controllers\Admin\ProvidersController::class, 'index'])->name('providers');
		Route::post('/save-default-extra', [App\Http\Controllers\Admin\ProvidersController::class, 'saveDefaultExtra'])->name('setExtras');
		Route::post('/add-provider', [App\Http\Controllers\Admin\ProvidersController::class, 'add'])->name('addProvider');
		Route::post('/delete-provider', [App\Http\Controllers\Admin\ProvidersController::class, 'deleteProvider'])->name('deleteProvider');
		Route::post('/delete-provider-with-products', [App\Http\Controllers\Admin\ProvidersController::class, 'deleteProviderWithProducts'])->name('deleteProviderWithProducts');
		Route::post('/move', [App\Http\Controllers\Admin\ProvidersController::class, 'move'])->name('moveProvider');
		Route::post('/save-provider-extra', [App\Http\Controllers\Admin\ProvidersController::class, 'saveProviderExtra'])->name('saveProviderExtra');
		Route::post('/overestimate', [App\Http\Controllers\Admin\ProvidersController::class, 'reEstimate'])->name('reEstimate');
	});

	Route::prefix('categories')->group(function () {
		Route::get('/', [App\Http\Controllers\Admin\CategoriesController::class, 'index'])->name('categories');
		Route::post('/move', [App\Http\Controllers\Admin\CategoriesController::class, 'moveCategory'])->name('moveCategory');
		Route::post('/create', [App\Http\Controllers\Admin\CategoriesController::class, 'create'])->name('category-create');
		Route::post('/savecat', [App\Http\Controllers\Admin\CategoriesController::class, 'savecat'])->name('category-edit-send');
		Route::get('/edit/{id}', [App\Http\Controllers\Admin\CategoriesController::class, 'edit'])->name('category-edit');
		Route::get('/remove/{id}', [App\Http\Controllers\Admin\CategoriesController::class, 'remove'])->name('removeCategory');
	});

	Route::prefix('products')->group(function () {
		Route::get('/', [App\Http\Controllers\Admin\ProductsController::class, 'index'])->name('products');
		Route::get('/getProducts', [App\Http\Controllers\Admin\ProductsController::class, 'getProducts'])->name('getProducts');
		Route::get('/edit/{api_ID}', [App\Http\Controllers\Admin\ProductsController::class, 'edit'])->name('edit_products');
		Route::post('/add', [App\Http\Controllers\Admin\ProductsController::class, 'add'])->name('add_products');
		Route::post('/moveproduct', [App\Http\Controllers\Admin\ProductsController::class, 'moveproduct'])->name('move_Product');
		Route::post('/delete', [App\Http\Controllers\Admin\ProductsController::class, 'delete'])->name('delete_Product');
		Route::post('/product_save', [App\Http\Controllers\Admin\ProductsController::class, 'product_save'])->name('product_save');
		Route::get('/remove/{id}', [App\Http\Controllers\Admin\ProductsController::class, 'remove'])->name('remove_product');
	});

	Route::prefix('products_options')->group(function () {
		Route::get('/', [App\Http\Controllers\Admin\ProductsController::class, 'options'])->name('products_options');
		Route::get('/edit/{option_id}', [App\Http\Controllers\Admin\ProductsController::class, 'edit_option'])->name('edit_option');
		Route::post('/save', [App\Http\Controllers\Admin\ProductsController::class, 'save_option'])->name('save-option');
		Route::post('/add', [App\Http\Controllers\Admin\ProductsController::class, 'add_option'])->name('add_option');
		Route::get('/remove/{id}', [App\Http\Controllers\Admin\ProductsController::class, 'delete_option'])->name('delete_option');
	});



	Route::get('/managers', [App\Http\Controllers\Admin\UsersController::class, 'managers'])->name('managers');

	Route::prefix('users')->group(function () {
		Route::get('/', [App\Http\Controllers\Admin\UsersController::class, 'index'])->name('users')->middleware('auth');
		Route::get('/create', [App\Http\Controllers\Admin\UsersController::class, 'create'])->name('user_create')->middleware('auth');
		Route::get('/edit/{id}', [App\Http\Controllers\Admin\UsersController::class, 'edit'])->name('user_edit')->middleware('auth');
		Route::get('/remove/{id}', [App\Http\Controllers\Admin\UsersController::class, 'remove'])->name('user_remove')->middleware('auth');
		Route::post('/saveuser', [App\Http\Controllers\Admin\UsersController::class, 'saveuser'])->name('saveuser')->middleware('auth');
		Route::post('/add', [App\Http\Controllers\Admin\UsersController::class, 'add'])->name('add')->middleware('auth');
		Route::get('/lock/{id}', [App\Http\Controllers\Admin\UsersController::class, 'lock'])->name('lock')->middleware('auth');
		Route::get('/unlock/{id}', [App\Http\Controllers\Admin\UsersController::class, 'unlock'])->name('unlock')->middleware('auth');
		Route::get('/remove/{id}', [App\Http\Controllers\Admin\UsersController::class, 'remove'])->name('remove')->middleware('auth');
		Route::post('/opcreate', [App\Http\Controllers\Admin\UsersController::class, 'opcreate'])->name('opcreate')->middleware('auth');
	});

	Route::prefix('pages')->group(function () {
		Route::get('/', [App\Http\Controllers\Admin\PagesController::class, 'index'])->name('pages');
		Route::get('/remove/{id}', [App\Http\Controllers\Admin\PagesController::class, 'remove'])->name('remove');
		Route::get('/edit/{id}', [App\Http\Controllers\Admin\PagesController::class, 'edit'])->name('edit');
		Route::get('/create', [App\Http\Controllers\Admin\PagesController::class, 'create'])->name('create');
		Route::post('/savepage', [App\Http\Controllers\Admin\PagesController::class, 'savepage'])->name('savepage');
		Route::post('/add', [App\Http\Controllers\Admin\PagesController::class, 'add'])->name('add');
	});

	Route::prefix('tracks')->group(function () {
		Route::get('/', [App\Http\Controllers\Admin\PagesController::class, 'tracks'])->name('tracks');
		Route::get('/remove_track/{id}', [App\Http\Controllers\Admin\PagesController::class, 'remove_track'])->name('remove_track');
		Route::post('/add_track', [App\Http\Controllers\Admin\PagesController::class, 'add_track'])->name('add_track');
	});

	// Route::prefix('chat')->group(function () {
	//     Route::get('/', [App\Http\Controllers\Admin\ChatController::class, 'index'])->name('op_chat')->middleware('auth');
	//     Route::post('/loadhistory', [App\Http\Controllers\Admin\ChatController::class, 'loadhistory'])->name('op_loadhistory')->middleware('auth');
	//     Route::post('/push', [App\Http\Controllers\Admin\ChatController::class, 'push'])->name('op_push')->middleware('auth');
	//     Route::post('/message', [App\Http\Controllers\Admin\ChatController::class, 'newMessage'])->name('op_newMessage')->middleware('auth');
	//     Route::post('/loadchats', [App\Http\Controllers\Admin\ChatController::class, 'loadchats'])->name('op_loadchats')->middleware('auth');
	// });

	Route::prefix('orders')->group(function () {
		Route::get('/', [App\Http\Controllers\Admin\OrdersController::class, 'index'])->name('order-list');
		Route::get('/view/{id}', [App\Http\Controllers\Admin\OrdersController::class, 'view'])->name('order-view');
		Route::post('/status', [App\Http\Controllers\Admin\OrdersController::class, 'status_update'])->name('status-update');
	});

	Route::get('/password', [App\Http\Controllers\Admin\AdminController::class, 'editPswd'])->name('editPswd');
	Route::post('/set-password', [App\Http\Controllers\Admin\AdminController::class, 'setPswd'])->name('setPswd');
});

//Dynamic Pages
Route::get('/{slug?}', [App\Http\Controllers\PagesController::class, 'index']);

//Checkout Success
Route::get('/checkout/success/{order_id}', [App\Http\Controllers\CartController::class, 'success']);
