<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/album', [App\Http\Controllers\ApiController::class, 'album'])->name('album')->whereNumber('id');
Route::get('/album/info', [App\Http\Controllers\ApiController::class, 'albumInfo'])->name('albumInfo')->whereNumber('id');
Route::get('/photo/{id}', [App\Http\Controllers\ApiController::class, 'photo'])->name('photo')->whereNumber('id');
Route::get('/album_photos/{album}', [App\Http\Controllers\ApiController::class, 'album_photos'])->name('album_photos')->whereNumber('album');
Route::get('/photoByCode', [App\Http\Controllers\ApiController::class, 'photoByCode'])->name('photoByCode');
Route::get('/photoByVkSource', [App\Http\Controllers\ApiController::class, 'photoByVkSource'])->name('photoByVkSource');
Route::get('/history', [App\Http\Controllers\ApiController::class, 'history'])->name('history');

Route::post('/save', [App\Http\Controllers\ApiController::class, 'save'])->name('save');
Route::post('/photoEdit/{id}', [App\Http\Controllers\ApiController::class, 'photoEdit'])->name('photoEdit')->whereNumber('id');
Route::post('/photoEditbyVkSource/{vk_source}', [App\Http\Controllers\ApiController::class, 'photoEditbyVkSource'])->name('photoEditbyVkSource')->whereNumber('id');
Route::post('/photosRemove', [App\Http\Controllers\ApiController::class, 'photosRemove'])->name('photosRemove');
Route::post('/photosRemoveByVkSource', [App\Http\Controllers\ApiController::class, 'photosRemoveByVkSource'])->name('photosRemoveByVkSource');
Route::post('/addTracks', [App\Http\Controllers\ApiController::class, 'addTracks'])->name('addTracks');
Route::post('/info', [App\Http\Controllers\ApiController::class, 'info'])->name('info');
Route::post('/syncStatus', [App\Http\Controllers\ApiController::class, 'syncStatus'])->name('syncStatus');
Route::post('/syncYouStatus', [App\Http\Controllers\ApiController::class, 'syncYouStatus'])->name('syncYouStatus');

Route::get('/set_nbu_currency', [App\Http\Controllers\ApiController::class, 'set_nbu_currency'])->name('set_nbu_currency');