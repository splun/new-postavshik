const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js(['resources/js/app.js', 'resources/js/cart.js'], 'public/js/app.js');
//mix.js('resources/js/chat-old.js', 'public/js/chat.js');
//mix.js('resources/js/chat-vue.js', 'public/js/chat.js');


mix.styles([
    'resources/css/animate.css',
    'resources/css/style.css',
    'resources/css/media.css',
    'resources/css/chat.css',
], 'public/css/app.css');


//Admin
/*mix.js('resources/admin/chat/chat.js', 'public/admin_assets/js/chat.js');*/

// mix.styles([
//     'resources/admin/chat/chat.css',
// ], 'public/admin_assets/css/chat.css');