#!/usr/bin/env php
<?php
require 'vendor/autoload.php';

$config = require 'config/console.php';

$app = new Slim\App(['settings' => $config]);

/* @var $container \Slim\Container */
$container = $app->getContainer();
require 'console/dependencies.php';

$dispatcher = new \Symfony\Component\EventDispatcher\EventDispatcher();
$dispatcher->addListener(
    \Symfony\Component\Console\ConsoleEvents::ERROR,
    function (\Symfony\Component\Console\Event\ConsoleErrorEvent $event) use ($container) {
        $container->get('logger.error')->error($event->getError()->getMessage());
    }
);

$application = new \Symfony\Component\Console\Application();
$application->setDispatcher($dispatcher);

$monolog         = $container->get('logger');
$loggerError     = $container->get('logger.error');
$loggerSavePhoto = $container->get('logger.save.photo');
$loggerRmPhoto   = $container->get('logger_rm_photos');
$loggerMovePhoto = $container->get('logger_move_photos');
$enqueueContext  = $container->get('enqueue.context');
$em              = $container->get('em');
$settings        = $container->get('settings');

//$application->add(new \console\command\TestSMTPCommand());
//$application->add(new \console\command\SendSMTPMailCommand($settings['mailer']));
//$application->add(new \console\command\ReduceImageSizeCommand());
//$application->add(new \console\command\UpdateTextPriceCommand($em));
//$application->add(new \console\command\RemoveAlbumCommand($loggerError));

$application->addCommands(
    [
        new \console\command\CreateEnqueueTableCommand($monolog, $enqueueContext),
        new \console\command\SendMessageToQueueCommand($loggerSavePhoto, $enqueueContext),
        new \console\command\ConsumeMessageFromQueueCommand($loggerSavePhoto, $loggerError, $enqueueContext, $em),
        new \console\command\CleanupChatAttachesCommand($loggerError, $em),
        new \console\command\RemovePhotoCommand($loggerRmPhoto, $em),
        new \console\command\OnUpdateCurrencyCommand($em),
        new \console\command\UpdatePhotoWithUsdPriceCommand($em),
        new \console\command\MovePhotoCommand($em, $loggerError, $loggerMovePhoto),
        new \console\command\DeleteFromAlbumPhotoEventsCommand($em),
        new \console\command\ProcessPopularCommand($em),
        new \console\command\ClenupStockNumberCommand($em),
        //new \console\command\AttachWatermarkCommand($em),
        new \console\command\CategoryMovingCommand($em, $loggerError, $loggerMovePhoto),
        new \console\command\SetSyncYouCategoryIdCommand($em),
        new \console\command\CleanupTrackNumberHistoryCommand(),
        new \console\command\SaveNbuCurrencyCommand(),
    ]
);

$application->run();