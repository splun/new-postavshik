$(function () {
    $('#all_pages,#all_users').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "order": [[0, "desc"]],
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });

    $('body').on('click', '#select-all-providers', function () {
        if ($(this).prop("checked")) {
            $('#providers tbody input[type="checkbox"]').prop('checked', 'checked');
        } else {
            $('#providers tbody input[type="checkbox"]').prop('checked', false);
        }

        reEstimateProviders();
    });

    $('body').on('click', '.pcheck', function () {
        reEstimateProviders();
    });
    $('body').on('click', '.prcheck', function () {
        reEstimateProducts();
    });
    $('.js-select2').select2();
    $('input[name="datetimes"]').daterangepicker({
        timePicker: true,
        startDate: moment().startOf('hour'),
        endDate: moment().startOf('hour').add(32, 'hour'),
        locale: {
            format: 'Y-M-D H:M:SS'
        }
    });
    $('#order-date-range').daterangepicker(
        {
            locale: {
                format: 'DD-MM-YYYY'
            },
            startDate: moment().startOf('month'),
            endDate: moment().endOf('month'),
            ranges: {
                'Сегодня': [moment(), moment()],
                'Вчера': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'За месяц': [moment().startOf('month'), moment().endOf('month')],
                'Последние 7д': [moment().subtract(6, 'days'), moment()],
                'Последние 30 д': [moment().subtract(29, 'days'), moment()],
                'Прошлый месяц': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
        }
    );
    let example = $('#providers').DataTable({
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        order: [
            [0, 'asc']
        ],
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ]
    });
    example.on("click", "th.select-checkbox", function () {
        if ($("th.select-checkbox").hasClass("selected")) {
            example.rows().deselect();
            $("th.select-checkbox").removeClass("selected");
        } else {
            example.rows().select();
            $("th.select-checkbox").addClass("selected");
        }
    }).on("select deselect", function () {
        ("Some selection or deselection going on")
        if (example.rows({
            selected: true
        }).count() !== example.rows().count()) {
            $("th.select-checkbox").removeClass("selected");
        } else {
            $("th.select-checkbox").addClass("selected");
        }
    });


    let products = $('#all_products').DataTable({
        "paging": false,
        "bPaginate": false,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
        aLengthMenu: [
            [25, 50, 100, 200, -1],
            [25, 50, 100, 200, "All"]
        ],
        columnDefs: [{
            orderable: false,
            className: 'select-checkbox',
            targets: 0
        }],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        /*processing: true,
        serverSide: true,
        ajax: "/admin/products/getProducts",
        columns: [
            { data: 'id' },
            { data: 'preview' }
        ]*/
    });


    // Add the following code if you want the name of the file appear on select
    $(".custom-file-input").on("change", function () {
        //var fileName = $(this).val().split("\\").pop();
        //$(this).siblings(".custom-file-label").addClass("selected").html(fileName);
        $('.custom-file .custom-file-label').text("Выбрано " + $('#exampleInputFile2')[0].files.length + " фото");
    });

    $(".select-period-type").change(function () {
        if ($(this).val() == 1) {
            $(this).parents("form").find("input.select-period").show().removeAttr("disabled");
        } else {
            $(this).parents("form").find("input.select-period").hide().addAttr("disabled");
        }
    });
});

function reEstimateProviders() {

    var ids = "";
    var aliases = "";


    $(".pcheck").each(function () {

        if ($(this).prop("checked")) {
            ids += $(this).parents(".provider-row").data("id") + ",";
            aliases += $(this).parents(".provider-row").data("alias") + ",";
        }

    });

    $(".checked_providers").val(ids.slice(0, -1));
    $(".providers-selected").text(aliases.slice(0, -1));
}

function reEstimateProducts() {

    var ids = "";
    var aliases = "";


    $(".prcheck").each(function () {

        if ($(this).prop("checked")) {
            ids += $(this).parents(".products-row").data("id") + ",";
        }

    });

    $(".checked_products").val(ids.slice(0, -1));
}

$("#select-all-products").on("change", function () {

    if ($(this).is(":checked")) {
        $(".prcheck").prop("checked", true);
        history.pushState(null, null, window.location + '&selected_all=1');
    } else {
        $(".prcheck").prop("checked", false);

        var url = new URL(window.location);
        url.searchParams.delete('selected_all');
        history.pushState(null, null, url);
    }

    reEstimateProducts();
});

$("#change_status").on("change", function () {
    $(this).parent("form").trigger("submit");
});




//$('#add_product #text').summernote();
